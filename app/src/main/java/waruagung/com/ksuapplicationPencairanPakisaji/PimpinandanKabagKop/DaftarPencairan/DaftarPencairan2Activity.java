package waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.DaftarPencairan;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.PopUpPencairanTidakdisetujuiFragment;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.PopUppencairanFragment;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class DaftarPencairan2Activity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nama_staf)
    TextView namaStaf;
    @BindView(R.id.nomor_pinjaman)
    TextView nomorPinjaman;
    @BindView(R.id.jumlah_pinjaman)
    TextView jumlahPinjaman;
    @BindView(R.id.jenis_pinjaman)
    TextView jenisPinjaman;
    @BindView(R.id.jangka_waktu)
    TextView jangkaWaktu;
    @BindView(R.id.agunan)
    TextView agunan;
    @BindView(R.id.Status_Pencairan)
    TextView StatusPencairan;
    @BindView(R.id.nama_penyetuju)
    TextView namaPenyetuju;
    @BindView(R.id.admin_kredit)
    TextView adminKredit;
    @BindView(R.id.tolak)
    Button tolak;
    @BindView(R.id.setuju)
    Button setuju;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pencairan2);
        ButterKnife.bind(this);
        restoreActionBar();
        bnMain.getMenu().clear();

        bnMain.inflateMenu(R.menu.menu_page_pencairan4);
//        bnMain.inflateMenu(R.menu.menu_page_pencairan4);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;

            }
        });


        setuju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                // Melihat Status Kondisi Pada Pilihan Menu Chekbox
//                if(setifikat.isChecked()){
//                }else if(!setifikat.isChecked()){
//                }
//                if(bpkbmotor.isChecked()){
//                }else if(!bpkbmotor.isChecked()){
//                }
//                if(bpkbmobil.isChecked()){
//                }else if(!bpkbmobil.isChecked()){
//                }


                PopUppencairanFragment checkFragment = new PopUppencairanFragment();
                FragmentManager mFragmentManager = getSupportFragmentManager();
                checkFragment.setCancelable(false);
                checkFragment.show(mFragmentManager, PopUppencairanFragment.class.getSimpleName());

            }
        });
        tolak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                // Melihat Status Kondisi Pada Pilihan Menu Chekbox
//                if(setifikat.isChecked()){
//                }else if(!setifikat.isChecked()){
//                }
//                if(bpkbmotor.isChecked()){
//                }else if(!bpkbmotor.isChecked()){
//                }
//                if(bpkbmobil.isChecked()){
//                }else if(!bpkbmobil.isChecked()){
//                }


                PopUpPencairanTidakdisetujuiFragment checkFragment = new PopUpPencairanTidakdisetujuiFragment();
                FragmentManager mFragmentManager = getSupportFragmentManager();
                checkFragment.setCancelable(false);
                checkFragment.show(mFragmentManager, PopUpPencairanTidakdisetujuiFragment.class.getSimpleName());

            }
        });
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
