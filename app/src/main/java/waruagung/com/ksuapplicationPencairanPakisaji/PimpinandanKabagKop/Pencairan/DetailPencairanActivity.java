package waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.ModelDaftarPencairanListResponse2;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.VolleyErrorHelper;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.PopUppencairanFragment;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class DetailPencairanActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 321;
    static final int REQUEST_FILE_MANAGER = 223;
    private static final String TAG = "";
    //storage permission code
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public String base64_image_1 = "", base64_image_2 = "", base64_image_3 = "", type_upload = "";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.Nama_Nasabah)
    TextView NamaNasabah;
    @BindView(R.id.RLTanggalPencairan)
    RelativeLayout RLTanggalPencairan;
    @BindView(R.id.tvpencairan)
    TextView tvpencairan;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.alasan)
    TextView alasan;
    @BindView(R.id.jumlahpencairan)
    TextView jumlahpencairan;
    @BindView(R.id.jumlahDiterima)
    TextView jumlahDiterima;
    @BindView(R.id.uploadfoto1)
    TextView uploadfoto1;
    @BindView(R.id.lv_listViewItem1)
    RecyclerView lvListViewItem1;
    @BindView(R.id.uploadfotoimage1)
    ImageView uploadfotoimage1;
    @BindView(R.id.lv_listViewItem2)
    RecyclerView lvListViewItem2;
    @BindView(R.id.uploadfotoimage2)
    ImageView uploadfotoimage2;
    @BindView(R.id.clear_button)
    TextView clearButton;
    @BindView(R.id.signature_pad_description)
    TextView signaturePadDescription;
    @BindView(R.id.signature_pad)
    SignaturePad signaturePad;
    @BindView(R.id.signature_pad_container)
    RelativeLayout signaturePadContainer;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.uploadfoto2)
    TextView uploadfoto2;
    @BindView(R.id.save_button)
    Button saveButton;
    @BindView(R.id.btn_simpan)
    LinearLayout btnSimpan;
    //foto
    TextView lingkaran1;
    ImageView uploadfoto;
    //ttd
    private byte[] byteArray;
    private String base64_signature = "";
    private SignaturePad mSignaturePad;
    private TextView mClearButton;
    private Button mSaveButton;
    private SessionManager sessionManager;
    private int PICK_IMAGE_REQUEST = 124;
    private Uri filePath;
    private Bitmap bitmap;
    private Boolean APP_PENCAIRAN_LAPANGAN = false;
    private Boolean FOTOBUKTIPENCAIRAN = false, FOTOBERKASPENCAIRAN = false, TTDNASABAH = false;
    private ModelDaftarPencairanListResponse2 modelDaftarPencairanListResponse2;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pencairan);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        restoreActionBar();
        if (!getIntent().hasExtra("DATA")) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        modelDaftarPencairanListResponse2 = getIntent().getParcelableExtra("DATA");
        NamaNasabah.setText(modelDaftarPencairanListResponse2.getMembername());
        jumlahpencairan.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format(modelDaftarPencairanListResponse2.getTrnloanamt())));
        jumlahDiterima.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format(modelDaftarPencairanListResponse2.getTrnloanamtrev())));
        tvpencairan.setText("");
        Calendar calendar = Calendar.getInstance();
        String currentDate = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()).format(calendar.getTime());
        tvpencairan.setText(currentDate);
        RLTanggalPencairan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog picker = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, monthOfYear, dayOfMonth);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
                                String formatted = dateFormat.format(newDate.getTime());
                                tvpencairan.setText(formatted);
                            }
                        }, year, month, day);
                picker.show();
            }
        });
        //navigationonclik
        try {
            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    APP_PENCAIRAN_LAPANGAN = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        bnMain.getMenu().clear();
        if (APP_PENCAIRAN_LAPANGAN) {
            bnMain.inflateMenu(R.menu.menu_page_pencairan4);
        } else {
            bnMain.inflateMenu(R.menu.menu_page_pencairan);
        }
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (base64_image_1.equals("")) {
                    Toast.makeText(DetailPencairanActivity.this, "Foto Bukti Pencairan Belum Di pilih", Toast.LENGTH_SHORT).show();
                } else if (base64_image_2.equals("")) {
                    Toast.makeText(DetailPencairanActivity.this, "Foto Berkas Pencairan Belum Di pilih", Toast.LENGTH_SHORT).show();
                } else if (!TTDNASABAH) {
                    Toast.makeText(DetailPencairanActivity.this, "Belum ada TTD Nasabah", Toast.LENGTH_SHORT).show();
                } else {
                    sendPencairan("");
                }
            }
        });
        //foto
        uploadfoto1 = findViewById(R.id.uploadfoto1);
        uploadfoto = findViewById(R.id.uploadfotoimage1);
        onclickBtnUpload(uploadfoto1, "image_1");
        onclickBtnUpload(uploadfoto2, "image_2");

        //tanda tangan 1
        mSignaturePad = findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
//                Toast.makeText(Detail_pinjamanActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
                TTDNASABAH = true;
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
                TTDNASABAH = false;
            }
        });

        mClearButton = findViewById(R.id.clear_button);
        mSaveButton = findViewById(R.id.save_button);

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bitmap bitmap = mSignaturePad.getSignatureBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
////                keterangandetailpembayaran.setText(encoded);
                Log.e("emcode", "onClick: " + encoded);
                try {
                    base64_signature = encoded;
                    Log.e("Base64 Encode ", "onClick: " + encoded);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }


    //foto
    private void onclickBtnUpload(TextView uploadfoto1, String image_1) {
        uploadfoto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type_upload = image_1;
                @SuppressLint("ResourceType")
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DetailPencairanActivity.this);
                LayoutInflater inflater = DetailPencairanActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alert_uploadfoto, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(true);
                dialogBuilder.setTitle("Upload File");
                CardView takePhoto = dialogView.findViewById(R.id.cv_takePhoto);
                CardView selectPhoto = dialogView.findViewById(R.id.cv_selectPhoto);
//                CardView selectFile = dialogView.findViewById(R.id.cv_File);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                alertDialog.setCancelable(true);
                alertDialog.setCanceledOnTouchOutside(true);
                takePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dispatchTakePictureIntent();
                        alertDialog.dismiss();
                    }
                });
                selectPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showFileChooser();
                        alertDialog.dismiss();
                    }
                });


            }

        });

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {


            if (requestCode == PICK_IMAGE_REQUEST) {
                filePath = data.getData();
                Bitmap bm = null;
                if (data != null) {
                    try {
                        bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Bitmap selectedImage = getResizedBitmap(bm, 300);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                assert bm != null;
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                if (type_upload.equals("image_1")) {
                    try {
                        base64_image_1 = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    selectImageViewREQUEST_Pick_CAPTURE(base64_image_1, filePath, byteArrayOutputStream, uploadfotoimage1);
                } else if (type_upload.equals("image_2")) {
                    try {
                        base64_image_2 = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    selectImageViewREQUEST_Pick_CAPTURE(base64_image_2, filePath, byteArrayOutputStream, uploadfotoimage2);
                }

            }

            if (requestCode == REQUEST_FILE_MANAGER) {
                try {
                    Uri uri = data.getData();
                    Log.d(TAG, "File Uri: " + uri.toString());
                    String path = FileUtils.getPath(this, uri);
                    String filename = path.substring(path.lastIndexOf("/") + 1);
                    Log.e(TAG, "onActivityResult: " + FileUtils.getExtension(path) + "  " + filename);

                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
                }
            }
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                Bitmap selectedImage = getResizedBitmap(thumbnail, 300);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                assert thumbnail != null;
                Log.e(TAG, "onActivityResult REQUEST_IMAGE_CAPTURE: " + type_upload);
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                if (type_upload.equals("image_1")) {
                    try {
                        base64_image_1 = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    selectImageViewREQUEST_IMAGE_CAPTURE(base64_image_1, uploadfotoimage1, data, byteArrayOutputStream);
                } else if (type_upload.equals("image_2")) {
                    try {
                        base64_image_2 = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    selectImageViewREQUEST_IMAGE_CAPTURE(base64_image_2, uploadfotoimage2, data, byteArrayOutputStream);
                }
            }

        }
    }

    private void selectImageViewREQUEST_Pick_CAPTURE(String base64ssd, Uri filePath, ByteArrayOutputStream byteArrayOutputStream, ImageView viewImage) {

        try {
            base64ssd = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            Log.e("base64_image_1", base64ssd);
            Log.e("base64_image_1_length", String.valueOf(base64ssd.length()));
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
            viewImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void selectImageViewREQUEST_IMAGE_CAPTURE(String base64sd, ImageView xuploadfotoimage, Intent data, ByteArrayOutputStream bytes) {
        try {
            base64sd = Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
            Log.e("base64_image_1", base64sd);
            Log.e("base64_image_1_length", String.valueOf(base64sd.length()));
        } catch (Exception e) {
            Log.e(TAG, "selectImageViewREQUEST_IMAGE_CAPTURE: " + e.getMessage());
        }
        Log.e("base64_image_1", base64sd);
        Bundle extras = data.getExtras();
        Log.e("base64_image_1_length", String.valueOf(base64sd.length()));
        Bitmap imageBitmap = (Bitmap) extras.get("data");
        xuploadfotoimage.setImageBitmap(imageBitmap);

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);

    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void sendPencairan(String appactionss) {
        String url = Http.getUrl() + "InsertPencairan";
        Log.e(TAG, "sendCheckIn: " + url);
        final ProgressDialog dialog1 = new ProgressDialog(DetailPencairanActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Sedang Menyimpan...");
        dialog1.show();
        RequestQueue mQueue = Volley.newRequestQueue(DetailPencairanActivity.this);
        //        Log.e(TAG, "accessWebService: Katalog Start with Token " + sessionManager.getKeyToken());
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog1.dismiss();

                        try {

                            Log.d(TAG, "onResponse() called with: response = " + response);
                            if (response != null) {
                                try {
                                    String[] respondde = response.split(";");
                                    if (respondde[0].equals("OK")) {
                                        PopUppencairanFragment checkFragment = new PopUppencairanFragment();
                                        FragmentManager mFragmentManager = getSupportFragmentManager();
                                        checkFragment.setCancelable(false);
                                        checkFragment.show(mFragmentManager, PopUppencairanFragment.class.getSimpleName());
                                    } else {
                                        Toast.makeText(DetailPencairanActivity.this, respondde[1], Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(DetailPencairanActivity.this, "Gagal Menyimpan.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }

                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse() called with: response = [" + e.getMessage() + "]");
                            Toast.makeText(DetailPencairanActivity.this, "Gagal Menyimpan.", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                try {
                    String bodyError = VolleyErrorHelper.getMessage(error, DetailPencairanActivity.this);
                    Toast.makeText(DetailPencairanActivity.this, bodyError, Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    Log.e(TAG, "onErrorResponse: " + e.getMessage());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                //                params.put("Authorization", "Bearer " + sessionManager.getKeyToken());
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sCmp", Http.getsCmp());
                params.put("apploancashoid", String.valueOf(modelDaftarPencairanListResponse2.getApploancashoid()));
                params.put("loancashimg", base64_image_1);
                params.put("loancashdoc", base64_image_2);
                params.put("loancashttd", base64_signature);
                params.put("createuser", sessionManager.getPID());
                Log.e(TAG, "getParams: " + params.toString());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjectRequest);
    }
}
