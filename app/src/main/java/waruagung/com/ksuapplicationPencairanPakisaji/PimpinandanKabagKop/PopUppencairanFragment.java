package waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;


public class PopUppencairanFragment extends DialogFragment implements View.OnClickListener {
    private View view;
    private Intent intent;
    public PopUppencairanFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_pop_uppencairan, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        view.findViewById(R.id.btnclose).setOnClickListener(this);
        view.findViewById(R.id.Submit_tutup).setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnclose:
                Intent intent1 = new Intent(view.getContext(), MainActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
//                getDialog().dismiss();
                break;
            case R.id.Submit_tutup:
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
                break;

        }
    }

}
