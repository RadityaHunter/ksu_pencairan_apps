package waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.ModelDaftarPencairanListResponse2;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.VolleyErrorHelper;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class PertanyaanPencairanActivity extends AppCompatActivity {

    public static final String TAG = PertanyaanPencairanActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.label_check)
    TextView labelCheck;
    @BindView(R.id.label_check1)
    TextView labelCheck1;
    @BindView(R.id.name_mahasiswa_checkin)
    TextView nameMahasiswaCheckin;
    @BindView(R.id.tvLatitude)
    TextView tvLatitude;
    @BindView(R.id.tvLongitude)
    TextView tvLongitude;
    @BindView(R.id.distance_between)
    TextView distanceBetween;
    @BindView(R.id.btn_tidak)
    Button btnTidak;
    @BindView(R.id.btn_ya)
    Button btnYa;
    private SessionManager sessionManager;
    private ModelDaftarPencairanListResponse2 modelDaftarPencairanListResponse2;
    private String geoLat, geoLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertanyaan_pencairan);
        ButterKnife.bind(this);
        restoreActionBar();
        sessionManager = new SessionManager(this);
        if (!getIntent().hasExtra("DATA") & !getIntent().hasExtra("MapsKoordinat")) {
            Toast.makeText(this, "Data Tidak Valid", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            modelDaftarPencairanListResponse2 = getIntent().getParcelableExtra("DATA");
            String[] geoLN = getIntent().getStringExtra("MapsKoordinat").split(",");
            geoLat = geoLN[0];
            geoLng = geoLN[1];
        }
        btnYa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (modelDaftarPencairanListResponse2.getLoancashstatus().equals("Dalam Proses")) {
//                    Intent intent = new Intent(getApplicationContext(), DetailPencairanActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent.putExtra("DATA", modelDaftarPencairanListResponse2);
//                    startActivity(intent);
//                }else{
                sendApproved("ya");
//                }
            }
        });
        btnTidak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendApproved("tidak");
            }
        });
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void sendApproved(String appactionss) {
        String url = Http.getUrl() + "CheckInPencairan";
        Log.e(TAG, "sendCheckIn: " + url);
        final ProgressDialog dialog1 = new ProgressDialog(PertanyaanPencairanActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Sedang Menyimpan...");
        dialog1.show();
        RequestQueue mQueue = Volley.newRequestQueue(PertanyaanPencairanActivity.this);
        //        Log.e(TAG, "accessWebService: Katalog Start with Token " + sessionManager.getKeyToken());
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog1.dismiss();

                        try {

                            Log.d(TAG, "onResponse() called with: response = " + response);
                            if (response != null) {
                                try {
                                    String[] respondde = response.split(";");
                                    if (respondde[0].equals("OK")) {
                                        if (appactionss.equals("tidak")) {
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(getApplicationContext(), DetailPencairanActivity.class);
                                            intent.putExtra("DATA", (Parcelable) modelDaftarPencairanListResponse2);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }

                                    } else {
                                        Toast.makeText(PertanyaanPencairanActivity.this, respondde[1], Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(PertanyaanPencairanActivity.this, "Gagal Menyimpan.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }

                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse() called with: response = [" + e.getMessage() + "]");
                            Toast.makeText(PertanyaanPencairanActivity.this, "Gagal Menyimpan.", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                try {
                    String bodyError = VolleyErrorHelper.getMessage(error, PertanyaanPencairanActivity.this);
                    Toast.makeText(PertanyaanPencairanActivity.this, bodyError, Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    Log.e(TAG, "onErrorResponse: " + e.getMessage());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                //                params.put("Authorization", "Bearer " + sessionManager.getKeyToken());
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sCmp", Http.getsCmp());
                params.put("apploancashoid", String.valueOf(modelDaftarPencairanListResponse2.getApploancashoid()));
                params.put("nasabahdilokasi", appactionss);
                params.put("geolat", geoLat);
                params.put("geolng", geoLng);
                params.put("createuser", sessionManager.getPID());
                Log.e(TAG, "getParams: " + params.toString());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjectRequest);
    }
}
