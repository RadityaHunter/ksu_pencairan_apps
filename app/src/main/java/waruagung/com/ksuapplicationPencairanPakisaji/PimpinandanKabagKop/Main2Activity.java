package waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Login_Activity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.DaftarPencairan.DaftarPencairan4Activity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.NotifikasiPencairanActivity;

public class Main2Activity extends AppCompatActivity {

    @BindView(R.id.notif)
    ImageView notif;
    @BindView(R.id.keluar)
    ImageView keluar;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvId)
    TextView tvId;
    @BindView(R.id.drawer_layout)
    CoordinatorLayout drawerLayout;
    @BindView(R.id.pencairan)
    LinearLayout pencairan;
    @BindView(R.id.daftarpencairan)
    LinearLayout daftarpencairan;
    @BindView(R.id.LaporanPencairan)
    LinearLayout LaporanPencairan;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(Main2Activity.this)
                        .setMessage("Apakah Anda yakin untuk keluar ?")
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sessionManager.destroySession();
                                Intent intent = new Intent(Main2Activity.this, Login_Activity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
        pencairan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PencairanActivity.class);
                startActivity(intent);
            }
        });
        daftarpencairan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DaftarPencairan4Activity.class);
                startActivity(intent);
            }
        });


        LaporanPencairan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CariLaporanActivity.class);
                startActivity(intent);
            }
        });


        notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), NotifikasiPencairanActivity.class);
                startActivity(intent);
            }
        });
    }

}
