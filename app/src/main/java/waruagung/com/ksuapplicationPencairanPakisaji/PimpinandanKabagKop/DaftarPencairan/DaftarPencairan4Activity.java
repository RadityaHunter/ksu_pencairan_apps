package waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.DaftarPencairan;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.Model.ModelDaftarPencairan;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.DaftarPencairan.Adapter.AdapterDaftarPencairan4;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class DaftarPencairan4Activity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cari)
    EditText cari;
    @BindView(R.id.rvdaftar_pencairan)
    RecyclerView rvdaftarPencairan;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pencairan4);
        ButterKnife.bind(this);
        restoreActionBar();
        DaftarPencairan();
    }
    private void DaftarPencairan() {
        rvdaftarPencairan.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false));
        rvdaftarPencairan.setHasFixedSize(true);
        rvdaftarPencairan.setNestedScrollingEnabled(false);
        ArrayList<ModelDaftarPencairan> listCategory = new ArrayList<>();
        listCategory.add(new ModelDaftarPencairan("APRILLIA", "PB139204", "Rp 15.000.000", "Belum Disetujui", 1));
        listCategory.add(new ModelDaftarPencairan("LUSI APRILLIA", "PB231248", "Rp 40.000.000", "Disetujui Admin Kredit", 2));
        listCategory.add(new ModelDaftarPencairan("OCTA", "PB286823", "Rp 90.000.000", "Disetujui Kepala Bagian", 3));
        listCategory.add(new ModelDaftarPencairan("OLIS", "PB094842", "Rp 60.000.000", "Belum Disetujui", 4));
        listCategory.add(new ModelDaftarPencairan("ULIL LATIFAH", "PB2435261", "Rp 45.000.000", "Belum Disetujui", 5));
        AdapterDaftarPencairan4 adapterDaftarPencairan4 = new AdapterDaftarPencairan4(this, listCategory);
        rvdaftarPencairan.setAdapter(adapterDaftarPencairan4);
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //navigationonclik
        bnMain.getMenu().clear();

        bnMain.inflateMenu(R.menu.menu_page_pencairan4);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
    }
}
