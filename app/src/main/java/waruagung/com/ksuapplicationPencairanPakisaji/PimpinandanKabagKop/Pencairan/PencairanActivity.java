package waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.LaporanPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.Adapter.AdapterDaftarPencairanList;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanTabelActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.ModelDaftarPencairanListResponse2;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.VolleyErrorHelper;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MapsTagActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class PencairanActivity extends AppCompatActivity implements AdapterDaftarPencairanList.AdapterDaftarPencairanListCallback {

    public static final String TAG = PencairanActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cari)
    EditText cari;
    @BindView(R.id.rvcari_laporan)
    RecyclerView rvcariLaporan;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.swap)
    SwipeRefreshLayout swap;
    private Button mButtonOne, mButtonTwo;
    private int mMenuSet = 0;
    private SessionManager sessionManager;
    private Boolean APP_PENCAIRAN_LAPANGAN = false;
    private List<ModelDaftarPencairanListResponse2> modelDaftarPencairanListResponses = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pencairan);
        ButterKnife.bind(this);
        restoreActionBar();
        sessionManager = new SessionManager(this);
        try {
            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    APP_PENCAIRAN_LAPANGAN = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        bnMain.getMenu().clear();
        if (APP_PENCAIRAN_LAPANGAN) {
            bnMain.inflateMenu(R.menu.menu_page_pencairan4);
        } else {
            bnMain.inflateMenu(R.menu.menu_page_pencairan);
        }
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
//                    case R.id.DaftarPencairan:
//                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
//                        startActivity(intent2);
//                        break;

                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanTabelActivity.class);
                        startActivity(intent2);
                        break;


                    case R.id.LaporanPencairan:
//                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
//                        startActivity(intent3);
//                        break;
                        Intent intent3 = new Intent(getApplicationContext(), LaporanPencairanActivity.class);
                        startActivity(intent3);
                        break;


                }
                return false;

            }
        });
        DaftarPencairan("");
        swap.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                DaftarPencairan("");
                swap.setRefreshing(false);

            }
        });
        cari.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    DaftarPencairan(v.getText().toString().trim());
                    //keyboard otomatis hilang jika sudah cari nama/item selesai
                    InputMethodManager inputMethodManager1 = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager1.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    return true;
                }
                return false;
            }
        });

    }

    private void DaftarPencairan(String s) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PencairanActivity.this);
        rvcariLaporan.setLayoutManager(linearLayoutManager);
        rvcariLaporan.setHasFixedSize(true);
        rvcariLaporan.setNestedScrollingEnabled(false);
        rvcariLaporan.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                swap.setEnabled(linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0);
            }
        });
        rvcariLaporan.setAdapter(null);
//        String url = Http.getUrl() + "Pencairan_List?sCmp=" + Http.getsCmp() + "&membername=" + s;
        String url = Http.getUrl() + "GetListApprovalPencairan?sCmp=" + Http.getsCmp() + "&useroid=" + sessionManager.getPID() + "&membername=" + s;
        Log.e(TAG, "rvlaporan: " + url);
        final ProgressDialog dialog1 = new ProgressDialog(PencairanActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Harap Menunggu...");
        dialog1.show();
        RequestQueue mQueue = Volley.newRequestQueue(PencairanActivity.this);
        //        Log.e(TAG, "accessWebService: Katalog Start with Token " + sessionManager.getKeyToken());
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onResponse(String response) {
                        dialog1.dismiss();
                        try {

                            Log.d(TAG, "onResponse() called with: response = " + response);
                            if (response != null && !response.equals("[]")) {
//                                versi1(response);
                                versi2(response);
                            } else {
                                Toast.makeText(PencairanActivity.this, "Data Kosong.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse() called with: response = [" + e.getMessage() + "]");
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                String responseBodyError = VolleyErrorHelper.getMessage(error, PencairanActivity.this);
                Toast.makeText(PencairanActivity.this, responseBodyError, Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/json");
                //                params.put("Authorization", "Bearer " + sessionManager.getKeyToken());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjectRequest);
    }

    private void versi2(String response) {
        Log.e(TAG, "versi2: start");
        List<ModelDaftarPencairanListResponse2> modelDaftarPencairanListResponses22 = new Gson().fromJson(response, new TypeToken<List<ModelDaftarPencairanListResponse2>>() {
        }.getType());
        for (ModelDaftarPencairanListResponse2 ms : modelDaftarPencairanListResponses22) {
            if (!ms.getCheckinstatus().isEmpty()) {
                if (ms.getCheckinstatus().equals("Dalam Proses")) {
                    modelDaftarPencairanListResponses.add(ms);
                }
            }
        }
        Log.e(TAG, "versi2: " + modelDaftarPencairanListResponses.size());
        if (modelDaftarPencairanListResponses.isEmpty()) {
            Toast.makeText(PencairanActivity.this, "Belum Ada Pencairan Ke Lapangan!", Toast.LENGTH_SHORT).show();
        } else {
            AdapterDaftarPencairanList adapterListLaporan = new AdapterDaftarPencairanList(PencairanActivity.this, -1, modelDaftarPencairanListResponses, PencairanActivity.this);
            rvcariLaporan.setAdapter(adapterListLaporan);
        }
    }


    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onRowAdapterDaftarPencairanListClicked(int position) {
        ModelDaftarPencairanListResponse2 m = modelDaftarPencairanListResponses.get(position);
        if (m.getAPPPENCAIRANPENGAWAS().equals("Approved") &&
                m.getAPPPENCAIRANKABAG().equals("Approved") &&
                m.getAPPPENCAIRANCASHIER().equals("Approved") &&
                m.getAPPPENCAIRANLAPANGAN().equals("Approved") &&
                m.getCheckinstatus().equals("Dalam Proses") &&
                !m.getApploancashstatus().equals("Closed") &&
                APP_PENCAIRAN_LAPANGAN) {
            Intent intent = new Intent(PencairanActivity.this, MapsTagActivity.class);
            intent.putExtra("DATA", (Parcelable) m);
            intent.putExtra("TAG", "Pencairan");
            startActivity(intent);
        } else {
            Toast.makeText(this, m.getApploancashnote(), Toast.LENGTH_SHORT).show();
        }
    }
}
