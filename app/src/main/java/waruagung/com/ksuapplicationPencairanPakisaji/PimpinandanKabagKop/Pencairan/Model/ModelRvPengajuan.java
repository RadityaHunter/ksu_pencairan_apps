package waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelRvPengajuan implements Parcelable {
    public static final Creator<ModelRvPengajuan> CREATOR = new Creator<ModelRvPengajuan>() {
        @Override
        public ModelRvPengajuan createFromParcel(Parcel source) {
            return new ModelRvPengajuan(source);
        }

        @Override
        public ModelRvPengajuan[] newArray(int size) {
            return new ModelRvPengajuan[size];
        }
    };

    private String nama, idnasabah;
    private int kodeMenu,gambar1;


    public ModelRvPengajuan(String nama, int gambar1, int kodeMenu, String idnasabah) {
        this.nama = nama;
        this.idnasabah = idnasabah;
        this.kodeMenu = kodeMenu;
        this.gambar1 = gambar1;

    }

    protected ModelRvPengajuan(Parcel in) {
        this.nama = in.readString();
        this.gambar1 = in.readInt();
        this.idnasabah = in.readString();
        this.kodeMenu = in.readInt();
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getIdnasabah() { return idnasabah; }

    public void setIdnasabah(String idnasabah) {
        this.idnasabah = idnasabah;
    }

    public int getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(int kodeMenu) {
        this.kodeMenu = kodeMenu;
    }

    public int getGambar1() { return gambar1; }

    public void setGambar1(int gambar1) {this. gambar1 = gambar1; }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nama);
        dest.writeString(this.idnasabah);
        dest.writeInt(this.kodeMenu);
        dest.writeInt(this.gambar1);

    }
}
