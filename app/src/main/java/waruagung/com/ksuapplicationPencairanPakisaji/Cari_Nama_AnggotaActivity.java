package waruagung.com.ksuapplicationPencairanPakisaji;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelNamaAnggotaNasabah;
import waruagung.com.ksuapplicationPencairanPakisaji.Adapter.AdapterCari2;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;

public class Cari_Nama_AnggotaActivity extends AppCompatActivity implements AdapterCari2.AdapterCari2Callback {

    private static final String TAG = Cari_Nama_AnggotaActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.Rvnamaanggota)
    RecyclerView Rvnamaanggota;
    @BindView(R.id.edtCariNasabah)
    EditText edtCariNasabah;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    private APIInterface apiInterface;
    private APIClient ApiClient;
    private GridLayoutManager gridLayoutManager;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari__nama__anggota);
        ButterKnife.bind(this);
        restoreActionBar();
        rvnamaanggota();
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
        edtCariNasabah.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Rvnamaanggota.setAdapter(null);
                        daftaragunanCari(edtCariNasabah.getText().toString().trim());
                        return true;
                    case EditorInfo.IME_ACTION_NEXT:
                    case EditorInfo.IME_ACTION_PREVIOUS:

                        return true;
                }
                return false;
            }
        });
    }

    private void rvnamaanggota() {
        Rvnamaanggota.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false));
        Rvnamaanggota.setHasFixedSize(true);
        Rvnamaanggota.setNestedScrollingEnabled(false);
        Rvnamaanggota.setLayoutManager(new LinearLayoutManager(this));
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelNamaAnggotaNasabah>> call = apiInterface.doGetAPIAnggotaNasabah(Http.getsCmp());
        call.enqueue(new Callback<List<ModelNamaAnggotaNasabah>>() {
            @Override
            public void onResponse(Call<List<ModelNamaAnggotaNasabah>> call, Response<List<ModelNamaAnggotaNasabah>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.e(TAG, "onResponse: " + response.toString());
                    List<ModelNamaAnggotaNasabah> listAlbum = response.body();
                    AdapterCari2 adapterCari2 = new AdapterCari2(getApplicationContext(), 0, listAlbum, Cari_Nama_AnggotaActivity.this);
                    Rvnamaanggota.setAdapter(adapterCari2);
//                    BigSaleProductListAdapter bigSaleProductListAllAdapter = new BigSaleProductListAdapter(listProduct, getApplicationContext(), 0);
//                    recyclerView.setAdapter(bigSaleProductListAllAdapter);
                } else {
                    Log.e(TAG, "onResponse: " + response.toString());
                }
            }

            @Override
            public void onFailure(Call<List<ModelNamaAnggotaNasabah>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    private void daftaragunanCari(String toString) {
        gridLayoutManager = new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false);
        Rvnamaanggota.setHasFixedSize(true);
        Rvnamaanggota.setLayoutManager(new LinearLayoutManager(this));
        Rvnamaanggota.setNestedScrollingEnabled(false);
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelNamaAnggotaNasabah>> call = apiInterface.doGetAPIDaftarPinjamanSearch2(Http.getsCmp(), toString);
        call.enqueue(new Callback<List<ModelNamaAnggotaNasabah>>() {
            @Override
            public void onResponse(Call<List<ModelNamaAnggotaNasabah>> call, Response<List<ModelNamaAnggotaNasabah>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.e(TAG, "onResponse: " + response.toString());
                    List<ModelNamaAnggotaNasabah> listAlbum = response.body();
                    if (listAlbum.size() > 0) {
                        AdapterCari2 adapterCari2 = new AdapterCari2(getApplicationContext(), -1, listAlbum, Cari_Nama_AnggotaActivity.this);
                        Rvnamaanggota.setAdapter(adapterCari2);
                    } else {
                        Toast.makeText(Cari_Nama_AnggotaActivity.this, "DATA Tidak Ada", Toast.LENGTH_SHORT).show();
                    }
//                    BigSaleProductListAdapter bigSaleProductListAllAdapter = new BigSaleProductListAdapter(listProduct, getApplicationContext(), 0);
//                    recyclerView.setAdapter(bigSaleProductListAllAdapter);
                } else {
                    Log.e(TAG, "onResponse: " + response.toString());
                }
            }

            @Override
            public void onFailure(Call<List<ModelNamaAnggotaNasabah>> call, Throwable t) {
//                Toast.makeText(Cari_Nama_AnggotaActivity.this, t.getMessage() + "", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void onRowAdapterCari2Clicked(ModelNamaAnggotaNasabah modelNamaAnggotaNasabah) {

//        Intent intent = new Intent();
        Intent intent = new Intent(Cari_Nama_AnggotaActivity.this, MapsTagActivity.class);

        intent.putExtra("modelNamaAnggotaNasabah", modelNamaAnggotaNasabah);
        startActivity(intent);

//        intent.putExtra("_DATAID", id);
//        intent.putExtra("_DATANAME", Nama);
//        intent.putExtra("_DATANAMEID", idNasabah);
//        setResult(RESULT_OK, intent);
//        finish();
    }
}
