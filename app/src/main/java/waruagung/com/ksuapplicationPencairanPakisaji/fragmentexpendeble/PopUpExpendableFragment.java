package waruagung.com.ksuapplicationPencairanPakisaji.fragmentexpendeble;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanList;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanListDetail;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelRiwayatDetail;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelRiwayatDetailCustom;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.adapter.adapter_detail_riwayat_pembayaran;
import waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.model.ModelHeader;


public class PopUpExpendableFragment extends DialogFragment {
    public static final String TAG = PopUpExpendableFragment.class.getSimpleName();
    @BindView(R.id.btnclose)
    ImageView btnclose;
    @BindView(R.id.tvidpinjaman)
    TextView tvidpinjaman;
    @BindView(R.id.tvjangkawaktu)
    TextView tvjangkawaktu;
    @BindView(R.id.tvTotalPembayaran)
    TextView tvTotalPembayaran;
    @BindView(R.id.tvSisaPembayaran)
    TextView tvSisaPembayaran;
    @BindView(R.id.ExpandableListView)
    android.widget.ExpandableListView ExpandableListView;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.user_profile_photo)
    ImageView userProfilePhoto;


    private List<ModelHeader> listDataHeader;
    private HashMap<String, List<ModelRiwayatDetailCustom>> listDataChild;
    private APIInterface apiInterface;
    private ModelDaftarPinjamanListDetail mParam1;
    private ModelDaftarPinjamanList mParam2;
    private View view;

    public PopUpExpendableFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelable("params");
            mParam2 = getArguments().getParcelable("params2");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Log.d(TAG, "onCreateView: " + mParam1.getJenisPinjaman());
        view = inflater.inflate(R.layout.fragment_pop_up_expendable, container, false);
        ButterKnife.bind(this, view);

        tvTotalPembayaran.setText("Rp " + MainActivity.nf.format(mParam1.getTrnloanamt()));
        tvSisaPembayaran.setText("Rp " + MainActivity.nf.format(mParam1.getSisa()));
        tvidpinjaman.setText(mParam1.getTrnloanno());
        tvjangkawaktu.setText(String.format("Diangsur dalam jangka waktu %s Bulan", mParam1.getJangkaWaktu()));
        setUpDetailPinjaman(String.valueOf(mParam1.getTrnloanoid()));
        return view;

    }


    private void setUpDetailPinjaman(String valueOf) {
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelRiwayatDetail>> call = apiInterface.doGetAPIModelRiwayatDetail(Http.getsCmp(), valueOf);
        call.enqueue(new Callback<List<ModelRiwayatDetail>>() {
            @Override
            public void onResponse(Call<List<ModelRiwayatDetail>> call, Response<List<ModelRiwayatDetail>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<ModelRiwayatDetail> m = new ArrayList<>();
                    m = response.body();
//                    List<model_detail_riwayat_pembayaran> m = new ArrayList<>();
//                    for (ModelDaftarPinjamanListDetail detailPinjaman : mrespond) {
//                        m.add(new model_detail_riwayat_pembayaran(detailPinjaman.getTrnloanno(), detailPinjaman.getTrnloanopendate(), detailPinjaman.getTrnloanno(), detailPinjaman.getNorek(), String.valueOf(detailPinjaman.getTrnloanamt()), String.valueOf(detailPinjaman.getJangkaWaktu()), String.valueOf(detailPinjaman.getTrnloanamt()), "", detailPinjaman.getKeterangan(), ""));
//
//                    }
                    if (m.size() > 0) {
                        int trnloanamt = mParam1.getTrnloanamt();
                        int jumlahPembayaran = 0;
                        String jenisPencairan = "Transfer " + mParam1.getBankName() + "\n" + mParam1.getNorek() + " a/n " + mParam1.getNasabah();
                        listDataHeader = new ArrayList<ModelHeader>();
                        listDataChild = new HashMap<String, List<ModelRiwayatDetailCustom>>();
                        for (int i = 0; i < m.size(); i++) {
                            ModelRiwayatDetail items = m.get(i);
                            jumlahPembayaran = jumlahPembayaran + items.getTagihan();
                            List<ModelRiwayatDetailCustom> itemsadd = new ArrayList<>();
                            trnloanamt = trnloanamt - items.getAcumamt();
                            Log.d(TAG, "onResponse: JenisPencairan " + jenisPencairan);
                            itemsadd.add(new ModelRiwayatDetailCustom(items.getTrnpaymentdate(), jenisPencairan, items.getTrnloandtlnote(), "-", items.getTagihan(), trnloanamt, mParam1.getJangkaWaktu()));
                            listDataHeader.add(new ModelHeader(m.get(i).getTrnloandtlnote(), R.drawable.check));
                            listDataChild.put(m.get(i).getTrnloandtlnote(), itemsadd);
                        }
                        ExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                            @Override
                            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                return false;
                            }
                        });
                        adapter_detail_riwayat_pembayaran expandableListAdapterPinjaman = new adapter_detail_riwayat_pembayaran(getContext(), listDataHeader, listDataChild);
                        ExpandableListView.setAdapter(expandableListAdapterPinjaman);
                    }
                }
//                List<model_detail_riwayat_pembayaran> m = new ArrayList<>();
//                m.add(new model_detail_riwayat_pembayaran("PB656722", "20 Maret 2019", "PJ5576827", "9849273873 a/n Lusi Aprillia", "Rp 10.750.000", "2", "Rp 15.000.000", "Transfer BCA", "-", "image.jpg"));
//                m.add(new model_detail_riwayat_pembayaran("PB557687", "30 Maret 2019", "PJ656722", "9849273873 a/n Lusi Aprillia", "Rp 1.587.000", "2", "Rp 15.000.000", "Transfer BCA", "-", "image.jpg"));
//
//
//                adapter_detail_riwayat_pembayaran expandableListAdapterPinjaman = new adapter_detail_riwayat_pembayaran(Detail_riwayat_pembayaranActivity.this, listDataHeader, listDataChild);
//                expandableListView1.setAdapter(expandableListAdapterPinjaman);


            }

            @Override
            public void onFailure(Call<List<ModelRiwayatDetail>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });


    }


    @OnClick(R.id.btnclose)
    public void onViewClicked() {
        getDialog().dismiss();

    }
}
