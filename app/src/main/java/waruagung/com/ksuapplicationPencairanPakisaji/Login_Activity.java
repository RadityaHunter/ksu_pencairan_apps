package waruagung.com.ksuapplicationPencairanPakisaji;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelLogin;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;

public class Login_Activity extends AppCompatActivity {

    public static final String TAG = Login_Activity.class.getSimpleName();
    @BindView(R.id.btn_login)
    AppCompatButton btnLogin;
    @BindView(R.id.username)
    TextInputEditText username;
    @BindView(R.id.input_password)
    TextInputEditText inputPassword;
    private Intent intent;
    private SessionManager sessionManagerLogin;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        ButterKnife.bind(this);
        requestStoragePermission();
        sessionManagerLogin = new SessionManager(getApplicationContext());

        if (sessionManagerLogin.isLoggedIn()) {
            Intent intent = new Intent(Login_Activity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = ProgressDialog.show(Login_Activity.this, "Tunggu!!!", "Please Wait...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
                Call<List<ModelLogin>> call = apiInterface.doGetAPILogin(username.getText().toString(), inputPassword.getText().toString(), "Staff", Http.getsCmp());
                call.enqueue(new Callback<List<ModelLogin>>() {
                    @Override
                    public void onResponse(Call<List<ModelLogin>> call, Response<List<ModelLogin>> response) {
                        progressDialog.dismiss();

                        try {
                            if (response.body().toString() != "[]" && response.isSuccessful()) {
                                List<ModelLogin> items = response.body();
                                String _userID = items.get(0).getUserid();
                                if (!_userID.equals("Cek kembali User dan Password !")) {
//                                    sessionManagerLogin.setLogin(true, items.get(0).getUserid(), items.get(0).getUserid());
//                                    intent = new Intent(getApplicationContext(), MainActivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                    startActivity(intent);
                                    settingRole(_userID);
                                } else {
//                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), _userID, Toast.LENGTH_SHORT).show();
//                                    Toast.makeText(getApplicationContext(), "Username dan Password salah", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(Login_Activity.this, "Gagal Terhubung Ke server. silakan coba lagi!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Terjadi Kesalahan Fatal Pada API.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ModelLogin>> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.e(TAG, "onFailure: " + t.getMessage());
                        Toast.makeText(getApplicationContext(), "Gagal Terhubung Dengan API", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void settingRole(String userID) {
        final ProgressDialog progressDialog = ProgressDialog.show(Login_Activity.this, "Tunggu!!!", "Please Wait...");

        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelUserRole>> call = apiInterface.doGetApiUserRole(Http.getsCmp(), userID);
        call.enqueue(new Callback<List<ModelUserRole>>() {
            @Override
            public void onResponse(Call<List<ModelUserRole>> call, Response<List<ModelUserRole>> response) {
                try {
                    if (response.body().toString() != "[]" && response.isSuccessful()) {
                        progressDialog.dismiss();
                        List<ModelUserRole> items = response.body();
                        Boolean dataRole = false;
                        for (ModelUserRole m : items) {
                            if (m.getFORMADDRESS().equals("APP_PENCAIRAN")) {
                                dataRole = true;
                            }
                        }
                        if (dataRole) {
                            String dataROle = new Gson().toJson(items);
                            Log.e(TAG, "onResponse: " + dataROle);
                            sessionManagerLogin.setLogin(true, userID, "STAFF", dataROle);
                            intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }else{
                            Toast.makeText(Login_Activity.this, "tidak ada akses Apps", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Terjadi Kesalahan Fatal Pada API.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ModelUserRole>> call, Throwable t) {
                progressDialog.dismiss();
                call.request();
                Toast.makeText(getApplicationContext(), "Gagal Terhubung Dengan API", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void requestStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Dexter.withActivity(this)
                    .withPermissions(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_MEDIA_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            // check if all permissions are granted
                            if (report.areAllPermissionsGranted()) {
                                // do you work now
                            }

                            // check for permanent denial of any permission
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                // permission is denied permenantly, navigate user to app settings
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    })
                    .onSameThread()
                    .check();
        }else{
            Dexter.withActivity(this)
                    .withPermissions(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            // check if all permissions are granted
                            if (report.areAllPermissionsGranted()) {
                                // do you work now
                            }

                            // check for permanent denial of any permission
                            if (report.isAnyPermissionPermanentlyDenied()) {
                                // permission is denied permenantly, navigate user to app settings
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    })
                    .onSameThread()
                    .check();
        }


    }
}
