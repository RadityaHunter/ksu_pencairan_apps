package waruagung.com.ksuapplicationPencairanPakisaji.agunan.Model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class ModelAttachments implements Parcelable {
    private Uri uri;
    private String path, name;
    private Integer drawable;

    public ModelAttachments(Uri uri, String path, String name, Integer drawable) {
        this.uri = uri;
        this.path = path;
        this.name = name;
        this.drawable = drawable;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDrawable() {
        return drawable;
    }

    public void setDrawable(Integer drawable) {
        this.drawable = drawable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.uri, flags);
        dest.writeString(this.path);
        dest.writeString(this.name);
        dest.writeValue(this.drawable);
    }

    public ModelAttachments() {
    }

    protected ModelAttachments(Parcel in) {
        this.uri = in.readParcelable(Uri.class.getClassLoader());
        this.path = in.readString();
        this.name = in.readString();
        this.drawable = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<ModelAttachments> CREATOR = new Parcelable.Creator<ModelAttachments>() {
        @Override
        public ModelAttachments createFromParcel(Parcel source) {
            return new ModelAttachments(source);
        }

        @Override
        public ModelAttachments[] newArray(int size) {
            return new ModelAttachments[size];
        }
    };
}
