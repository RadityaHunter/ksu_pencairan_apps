package waruagung.com.ksuapplicationPencairanPakisaji.agunan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.agunan.Model.ModelAttachments;

public class AdapterAttachment extends
        RecyclerView.Adapter<AdapterAttachment.ViewHolder> {

    private static final String TAG = AdapterAttachment.class.getSimpleName();

    private Context context;
    private List<ModelAttachments> list;
    private AdapterAttachmentCallback mAdapterCallback;
    private int result = -1;

    public AdapterAttachment(Context context, int result, List<ModelAttachments> list, AdapterAttachmentCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.result = result;
        this.mAdapterCallback = adapterCallback;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_attachments,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelAttachments item = list.get(position);
        holder.imageBanner.setImageResource(item.getDrawable());
        holder.tvNameFile.setText(item.getName());
        holder.tvContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onRowAdapterAttachmentClicked(position);
            }
        });

    }

    public void addItems(List<ModelAttachments> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface AdapterAttachmentCallback {
        void onRowAdapterAttachmentClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageBanner)
        ImageView imageBanner;
        @BindView(R.id.tvNameFile)
        TextView tvNameFile;
        @BindView(R.id.tvContent)
        LinearLayout tvContent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}