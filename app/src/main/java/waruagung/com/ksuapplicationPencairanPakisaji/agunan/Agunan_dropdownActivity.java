package waruagung.com.ksuapplicationPencairanPakisaji.agunan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.agunan.adapter.adapter_listview_agunan_dropdown;

public class Agunan_dropdownActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.listview)
    ListView listview;



    //Data-Data yang Akan dimasukan Pada ListView
    String[] names = {"Perhiasan", "Emas Batangan","Batu Permata"};

    //Data-Data yang Akan dimasukan Pada ListView
    int[] images = {R.drawable.perhiasan, R.drawable.masbatangan, R.drawable.permata};

    //ArrayList digunakan Untuk menampung Data nama
    private ArrayList<String> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agunan_dropdown);
        ButterKnife.bind(this);
        restoreActionBar();


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        data = new ArrayList<>();
        getData();


        adapter_listview_agunan_dropdown adapter_listview_agunan_dropdown = new adapter_listview_agunan_dropdown(this, names, images);
        listview.setAdapter(adapter_listview_agunan_dropdown);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("_DATA", data.get(position));
                setResult(RESULT_OK, returnIntent);
                finish();
//                if (position == 0) {
//                    Intent myIntent = new Intent(view.getContext(), Detail_barang_berhargaActivity.class);
//                    startActivityForResult(myIntent, 0);
//                }
//
//                if (position == 1) {
//                    Intent myIntent = new Intent(view.getContext(), Detail_barang_berhargaActivity.class);
//                    startActivityForResult(myIntent, 0);
//                }
//
//                if (position == 2) {
//                    Intent myIntent = new Intent(view.getContext(), Detail_barang_berhargaActivity.class);
//                    startActivityForResult(myIntent, 0);
//                }

//                if (position == 3) {
//                    Intent myIntent = new Intent(view.getContext(), Tanah_dan_bangunanActivity.class);
//                    startActivityForResult(myIntent, 0);
//                }
//
//                if (position == 4) {
//                    Intent myIntent = new Intent(view.getContext(), ListItemActivity1.class);
//                    startActivityForResult(myIntent, 0);
//                }
//
//                if (position == 5) {
//                    Intent myIntent = new Intent(view.getContext(), ListItemActivity2.class);
//                    startActivityForResult(myIntent, 0);
//                }
//
//                if (position == 6) {
//                    Intent myIntent = new Intent(view.getContext(), ListItemActivity1.class);
//                    startActivityForResult(myIntent, 0);
//                }
//
//                if (position == 7) {
//                    Intent myIntent = new Intent(view.getContext(), ListItemActivity2.class);
//                    startActivityForResult(myIntent, 0);
//                }
            }
        });
    }
//        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                //Mendapatkan Nama pada salah satu item yang diklik, berdasarkan posisinya
//                String names = data.get(position);
//
////                Berpindah Activity dan Mempassing data Nama pada Activity Selanjutnya
////                Intent sendData = new Intent(Merchant2Activity.this, Merchant3Activity.class);
////                sendData.putExtra("MyName", names);
////                sendData.putExtra("MyName", images);
////                startActivity(sendData);
//            }
//        });
//    }

    private void getData() {
        //Memasukan Semua Data nama kedalam ArrayList
//        Collections.addAll(data, names);
        //Memasukan Semua Data nama kedalam ArrayList
        if (getIntent().hasExtra("_DATA")) {
            String dataSelected = getIntent().getStringExtra("_DATA").toLowerCase();

            switch (dataSelected) {
                case "barang berharga":
                    //Data-Data yang Akan dimasukan Pada ListView
                    names = new String[]{"Perhiasan", "Emas Batangan", "Batu Permata"};
                    //Data-Data yang Akan dimasukan Pada ListView
                    images = new int[]{R.drawable.perhiasan, R.drawable.masbatangan, R.drawable.permata};
                    Collections.addAll(data, names);
                    adapter_listview_agunan_dropdown adapter_listview_agunan_dropdown = new adapter_listview_agunan_dropdown(this, names, images);
                    listview.setAdapter(adapter_listview_agunan_dropdown);
                    break;
                case "kendaraan":
                    //Data-Data yang Akan dimasukan Pada ListView
                    names = new String[]{"BPKB Mobil", "BPKB Motor"};
                    //Data-Data yang Akan dimasukan Pada ListView
                    images = new int[]{R.drawable.mobil, R.drawable.kendaraan};
                    Collections.addAll(data, names);
                    adapter_listview_agunan_dropdown = new adapter_listview_agunan_dropdown(this, names, images);
                    listview.setAdapter(adapter_listview_agunan_dropdown);
                    break;
                case "tanah":
                    //Data-Data yang Akan dimasukan Pada ListView
                    names = new String[]{"Tanah SHGB", "Tanah SHM"};

                    //Data-Data yang Akan dimasukan Pada ListView
                    images = new int[]{R.drawable.tanah, R.drawable.tanah};
                    Collections.addAll(data, names);
                    adapter_listview_agunan_dropdown = new adapter_listview_agunan_dropdown(this, names, images);
                    listview.setAdapter(adapter_listview_agunan_dropdown);
                    break;
                case "tanah dan bangunan":
                    //Data-Data yang Akan dimasukan Pada ListView
                    names = new String[]{"Tanah dan Bangunan SHGB", "Tanah dan Bangunan SHM"};

                    //Data-Data yang Akan dimasukan Pada ListView
                    images = new int[]{R.drawable.home2, R.drawable.home2};
                    Collections.addAll(data, names);
                    adapter_listview_agunan_dropdown = new adapter_listview_agunan_dropdown(this, names, images);
                    listview.setAdapter(adapter_listview_agunan_dropdown);
                    break;
                default:
                    finish();
            }

        } else {
            finish();
        }


    }


    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
