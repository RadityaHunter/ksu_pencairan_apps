package waruagung.com.ksuapplicationPencairanPakisaji.agunan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.agunan.adapter.adapter_jenisagunan;

public class JenisagunanActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.listview)
    ListView listview;


    //Data-Data yang Akan dimasukan Pada ListView
    String[] names = {"Barang Berharga", "Kendaraan","Tanah", "Tanah dan Bangunan"};

    //Data-Data yang Akan dimasukan Pada ListView
    int[] images = {R.drawable.barangberharga, R.drawable.kendaraan, R.drawable.tanah, R.drawable.home2};

    //ArrayList digunakan Untuk menampung Data nama
    private ArrayList<String> data;
    private int setResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jenisagunan);
        ButterKnife.bind(this);


        restoreActionBar();



        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        data = new ArrayList<>();
        getData();


        adapter_jenisagunan adapter_jenisagunan = new adapter_jenisagunan(this, names, images);
        listview.setAdapter(adapter_jenisagunan);



                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("_DATA", data.get(position));
                setResult(RESULT_OK, returnIntent);
                finish();
//                if (position == 0) {
//                    Intent myIntent = new Intent(view.getContext(), Agunan_dropdownActivity.class);
//                    startActivityForResult(myIntent, 0);
//                }
//
//                if (position == 1) {
//                    Intent myIntent = new Intent(view.getContext(), Kendaraan_Activity.class);
//                    startActivityForResult(myIntent, 0);
//                }
//
//                if (position == 2) {
//                    Intent myIntent = new Intent(view.getContext(), Tanah_Activity.class);
//                    startActivityForResult(myIntent, 0);
//                }
//
//                if (position == 3) {
//                    Intent myIntent = new Intent(view.getContext(), Tanah_dan_bangunanActivity.class);
//                    startActivityForResult(myIntent, 0);
//                }

            }
    });
    }

    private void getData() {
        //Memasukan Semua Data nama kedalam ArrayList
        Collections.addAll(data, names);

    }


    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
