package waruagung.com.ksuapplicationPencairanPakisaji.agunan;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;

import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AgunanActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 321;
    static final int REQUEST_FILE_MANAGER = 223;
    private static final String TAG = "";
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 1;
    public String base64_image_1 = "", base64_image_2 = "", type_upload = "";
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rel_jenisagunan)
    RelativeLayout relJenisagunan;
    @BindView(R.id.rel_agunan)
    RelativeLayout relAgunan;
    @BindView(R.id.jenisagunan)
    TextView jenisagunan;
    @BindView(R.id.agunan)
    TextView agunan;
    @BindView(R.id.subagunan)
    TextView subagunan;
    @BindView(R.id.mobil)
    LinearLayout mobil;
    @BindView(R.id.sepedamotor)
    LinearLayout sepedamotor;
    @BindView(R.id.tanah_shm)
    LinearLayout tanahSHM;
    @BindView(R.id.tanah_SHGB)
    LinearLayout tanahSHGB;
    @BindView(R.id.tanahdanbangunan_shm)
    LinearLayout tanahdanbangunanSHM;
    @BindView(R.id.tanahdanbangunan_shcb)
    LinearLayout tanahdanbangunanSHGB;
    @BindView(R.id.barangberharga_emasbatangan)
    LinearLayout barangberhargaEmasbatangan;
    @BindView(R.id.barangberharga_permata)
    LinearLayout barangberhargaPermata;
    @BindView(R.id.barangberharga_perhiasan)
    LinearLayout barangberhargaPerhiasan;
    @BindView(R.id.sumbitmobil)
    TextView sumbitmobil;
    @BindView(R.id.sumbitsepedamotor)
    TextView sumbitsepedamotor;
    @BindView(R.id.sumbittanahshgb)
    TextView sumbittanahSHGB;
    @BindView(R.id.sumbittanahshm)
    TextView sumbittanahshm;
    @BindView(R.id.sumbittanahdanbangunan_shm)
    TextView sumbittanahdanbangunanShm;
    @BindView(R.id.sumbittanahdanbangunan_shcb)
    TextView sumbittanahdanbangunanShcb;
    @BindView(R.id.sumbitemasbatang)
    TextView sumbitemasbatang;
    @BindView(R.id.sumbitpermata)
    TextView sumbitpermata;
    @BindView(R.id.sumbitperhiasan)
    TextView sumbitperhiasan;
    @BindView(R.id.m2tvshm)
    TextView m2tvshm;
    @BindView(R.id.m2tvshgb)
    TextView m2tvshgb;
    @BindView(R.id.m2tvtbshgm)
    TextView m2tvtbshgb;
    @BindView(R.id.m2tvtbshm)
    TextView m2tvtbshm;
    //tanggal
    @BindView(R.id.tvData_tanggal_shm)
    TextView tvDataTanggalShm;
    //    @BindView(R.id.tvData_tanggal_shm)
//    TextInputEditText tvDataTanggalShm;
    @BindView(R.id.tvData_tanggal_shgb)
    TextView tvDataTanggalShgb;
    @BindView(R.id.tvData_tanggal_tb_shm)
    TextView tvDataTanggalTbShm;
    @BindView(R.id.tvData_tanggal_tb_shgb)
    TextView tvDataTanggalTbShgb;
    //upload foto
    @BindView(R.id.uploadfoto_mobil)
    TextView uploadfotoMobil;
    @BindView(R.id.lv_listViewItem_mobil)
    RecyclerView lvListViewItemMobil;
    @BindView(R.id.uploadfotoimage_mobil)
    ImageView uploadfotoimageMobil;
    @BindView(R.id.uploadfoto_sepedamotor)
    TextView uploadfotoSepedamotor;
    @BindView(R.id.lv_listViewItem_sepedamotor)
    RecyclerView lvListViewItemSepedamotor;
    @BindView(R.id.uploadfotoimage_sepedamotor)
    ImageView uploadfotoimageSepedamotor;
    @BindView(R.id.uploadfoto_tb_shm)
    TextView uploadfotoTbShm;
    @BindView(R.id.lv_listViewItem_tb_shm)
    RecyclerView lvListViewItemTbShm;
    @BindView(R.id.uploadfotoimage_tb_shm)
    ImageView uploadfotoimageTbShm;
    @BindView(R.id.uploadfoto_tb_shgb)
    TextView uploadfotoTbShgb;
    @BindView(R.id.lv_listViewItem_tb_shgb)
    RecyclerView lvListViewItemTbShgb;
    @BindView(R.id.uploadfotoimage_tb_shgb)
    ImageView uploadfotoimageTbShgb;
    @BindView(R.id.uploadfoto_emasbatangan)
    TextView uploadfotoEmasbatangan;
    @BindView(R.id.lv_listViewItem_emasbatangan)
    RecyclerView lvListViewItemEmasbatangan;
    @BindView(R.id.uploadfotoimage_emasbatangan)
    ImageView uploadfotoimageEmasbatangan;
    @BindView(R.id.uploadfoto_batu_permata)
    TextView uploadfotoBatuPermata;
    @BindView(R.id.lv_listViewItem_batu_permata)
    RecyclerView lvListViewItemBatuPermata;
    @BindView(R.id.uploadfotoimage_batu_permata)
    ImageView uploadfotoimageBatuPermata;
    @BindView(R.id.uploadfoto_perhiasan)
    TextView uploadfotoPerhiasan;
    @BindView(R.id.lv_listViewItem_perhiasan)
    RecyclerView lvListViewItemPerhiasan;
    @BindView(R.id.uploadfotoimage_perhiasan)
    ImageView uploadfotoimagePerhiasan;
    @BindView(R.id.uploadfoto_tanah_shm)
    TextView uploadfoto_tanah_shm;
    @BindView(R.id.uploadfotoimage_tanah_shm)
    ImageView uploadfotoimage_tanah_shm;
    @BindView(R.id.uploadfoto_tanah_shgb)
    TextView uploadfoto_tanah_shgb;
    @BindView(R.id.uploadfotoimage_tanah_shgb)


    ImageView uploadfotoimage_tanah_shgb;
    ImageView uploadfotoimage_tanah_shgb1;
    ImageView uploadfotoimage_tanah_shm2;
    ImageView uploadfotoimagePerhiasan3;
    ImageView uploadfotoimageBatuPermata4;
    ImageView uploadfotoimageEmasbatangan5;
    ImageView uploadfotoimageTbShgb6;
    ImageView uploadfotoimageTbShm7;
    ImageView uploadfotoimageSepedamotor8;
    ImageView uploadfotoimageMobil9;
    @BindView(R.id.spinner_agunan)
    Spinner spinnerAgunan;
    @BindView(R.id.nilaiemasbatangan)
    EditText nilaiemasbatangan;
    @BindView(R.id.deskripsiemasbatangan)
    EditText deskripsiemasbatangan;
    @BindView(R.id.nilaipermata)
    EditText nilaipermata;
    @BindView(R.id.deskripsipermata)
    EditText deskripsipermata;
    @BindView(R.id.nilaiperhiasan)
    EditText nilaiperhiasan;
    @BindView(R.id.deskripsiperhiasan)
    EditText deskripsiperhiasan;
    @BindView(R.id.nomorbpkbmobil)
    EditText nomorbpkbmobil;
    @BindView(R.id.atasnamamobil)
    EditText atasnamamobil;
    @BindView(R.id.alamatmobil)
    EditText alamatmobil;
    @BindView(R.id.merkmobil)
    EditText merkmobil;
    @BindView(R.id.nomorpolisimobil)
    EditText nomorpolisimobil;
    @BindView(R.id.nomorrangkamobil)
    EditText nomorrangkamobil;
    @BindView(R.id.nomormesinmobil)
    EditText nomormesinmobil;
    @BindView(R.id.tahunmobil)
    EditText tahunmobil;
    @BindView(R.id.deskripsimobil)
    EditText deskripsimobil;
    @BindView(R.id.nomorbpkbmotor)
    EditText nomorbpkbmotor;
    @BindView(R.id.atasnamamotor)
    EditText atasnamamotor;
    @BindView(R.id.alamatmotor)
    EditText alamatmotor;
    @BindView(R.id.merkmotor)
    EditText merkmotor;
    @BindView(R.id.nomorpolisimotor)
    EditText nomorpolisimotor;
    @BindView(R.id.nomorrangkamotor)
    EditText nomorrangkamotor;
    @BindView(R.id.nomormesinmotor)
    EditText nomormesinmotor;
    @BindView(R.id.tahunmotor)
    EditText tahunmotor;
    @BindView(R.id.deskripsimotor)
    EditText deskripsimotor;
    @BindView(R.id.nomortanahshm)
    EditText nomortanahshm;
    @BindView(R.id.atasnamatanahshm)
    EditText atasnamatanahshm;
    @BindView(R.id.shmtanah)
    EditText shmtanah;
    @BindView(R.id.lokasitanahshm)
    EditText lokasitanahshm;
    @BindView(R.id.luastanahshm)
    EditText luastanahshm;
    @BindView(R.id.deskripsitanahshm)
    EditText deskripsitanahshm;
    @BindView(R.id.nomortanahshgb)
    EditText nomortanahshgb;
    @BindView(R.id.atasnamatanahshgb)
    EditText atasnamatanahshgb;
    @BindView(R.id.shgbtanah)
    EditText shgbtanah;
    @BindView(R.id.lokasitanahshgb)
    EditText lokasitanahshgb;
    @BindView(R.id.luastanahshgb)
    EditText luastanahshgb;
    @BindView(R.id.deskripsitanahshgb)
    EditText deskripsitanahshgb;
    @BindView(R.id.nomortanahdanbangunanshm)
    EditText nomortanahdanbangunanshm;
    @BindView(R.id.atasnamatanahdanbangunanshm)
    EditText atasnamatanahdanbangunanshm;
    @BindView(R.id.shmtanahdanbangunan)
    EditText shmtanahdanbangunan;
    @BindView(R.id.lokasitanahdanbangunanshm)
    EditText lokasitanahdanbangunanshm;
    @BindView(R.id.luastanahdanbangunanshm)
    EditText luastanahdanbangunanshm;
    @BindView(R.id.deskripsitanahdanbangunanshm)
    EditText deskripsitanahdanbangunanshm;
    @BindView(R.id.nomortanahdanbangunanshgb)
    EditText nomortanahdanbangunanshgb;
    @BindView(R.id.atasnamatanahdanbangunanshgb)
    EditText atasnamatanahdanbangunanshgb;
    @BindView(R.id.shgbtanahdanbangunan)
    EditText shgbtanahdanbangunan;
    @BindView(R.id.lokasitanahdanbangunanshgb)
    EditText lokasitanahdanbangunanshgb;
    @BindView(R.id.luastanahdanbangunanshgb)
    EditText luastanahdanbangunanshgb;
    @BindView(R.id.deskripsitanahdanbangunanshgb)
    EditText deskripsitanahdanbangunanshgb;
    //    @BindView(R.id.lokasi_shm)
//    TextView lokasiShm;
//    @BindView(R.id.lokasi_shgb)
//    TextView lokasiShgb;
//    @BindView(R.id.lokasitb_shm)
//    TextView lokasitbShm;
//    @BindView(R.id.lokasi_tbshgb)
//    TextView lokasiTbshgb;
    private String[] Item = {"Lusi Aprillia", "Prasetya", "Andri", "Olis Dare"};

    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSatlistener;
    //Image request code
    private int PICK_IMAGE_REQUEST = 124;
    //Bitmap to get image from gallery
    private Bitmap bitmap;
    private ArrayList<Uri> arrayList = new ArrayList<>();
    //Uri to store the image uri
    private Uri filePath;
////map
//    public static final String EXTRA_DATA = "EXTRA_DATA";
//    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1022;
//    private static final int RC_CAMERA_AND_LOCATION_AND_STORAGE = 1011;
//    private boolean mPermissionDenied = false;
//    protected LocationManager locationManager;
//    protected LocationListener locationListener;
//
//    private GoogleMap _GoogleMap;
//    private SupportMapFragment _SupportMapFragment;
//    private String lat = "";
//    private String lng = "";
//    private LatLng latLng;
//    private String LatLNG = "";
//    //loading
//    private ProgressDialog progressDialog;

    private void setAwall() {

        mobil.setVisibility(View.GONE);
        sepedamotor.setVisibility(View.GONE);
        tanahSHGB.setVisibility(View.GONE);
        tanahSHM.setVisibility(View.GONE);
        tanahdanbangunanSHGB.setVisibility(View.GONE);
        tanahdanbangunanSHM.setVisibility(View.GONE);
        barangberhargaEmasbatangan.setVisibility(View.GONE);
        barangberhargaPerhiasan.setVisibility(View.GONE);
        barangberhargaPermata.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agunan);
        ButterKnife.bind(this);
//        //loading
//        progressDialog = new ProgressDialog(this);
//        methodRequiresTwoPermission();

        spinnerAgunan = findViewById(R.id.spinner_agunan);
// inisialiasi Array Adapter dengan memasukkan string array di atas
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, Item);

        // mengeset Array Adapter tersebut ke Spinner
        spinnerAgunan.setAdapter(adapter);
        // mengeset listener untuk mengetahui saat item dipilih
        spinnerAgunan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
//                Toast.makeText(AgunanActivity.this, "Selected "+ adapter.getItem(i), Toast.LENGTH_SHORT).show();
                ((TextView) view).setTextSize(13);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        requestStoragePermission();
        relAgunan.setVisibility(View.GONE);
        subagunan.setVisibility(View.GONE);
        uploadfotoimage_tanah_shgb1 = findViewById(R.id.uploadfotoimage_tanah_shgb);
        uploadfotoimage_tanah_shm2 = findViewById(R.id.uploadfotoimage_tanah_shm);
        uploadfotoimagePerhiasan3 = findViewById(R.id.uploadfotoimage_perhiasan);
        uploadfotoimageBatuPermata4 = findViewById(R.id.uploadfotoimage_batu_permata);
        uploadfotoimageEmasbatangan5 = findViewById(R.id.uploadfotoimage_emasbatangan);
        uploadfotoimageTbShgb6 = findViewById(R.id.uploadfotoimage_tb_shgb);
        uploadfotoimageTbShm7 = findViewById(R.id.uploadfotoimage_tb_shm);
        uploadfotoimageSepedamotor8 = findViewById(R.id.uploadfotoimage_sepedamotor);
        uploadfotoimageMobil9 = findViewById(R.id.uploadfotoimage_mobil);
        uploadfotoEmasbatangan = findViewById(R.id.uploadfoto_emasbatangan);
        uploadfotoPerhiasan = findViewById(R.id.uploadfoto_perhiasan);
        uploadfotoBatuPermata = findViewById(R.id.uploadfoto_batu_permata);
        uploadfotoSepedamotor = findViewById(R.id.uploadfoto_sepedamotor);
        uploadfotoMobil = findViewById(R.id.uploadfoto_mobil);
        uploadfotoTbShm = findViewById(R.id.uploadfoto_tb_shm);
        uploadfotoTbShgb = findViewById(R.id.uploadfoto_tb_shgb);
        uploadfoto_tanah_shgb = findViewById(R.id.uploadfoto_tanah_shgb);
        uploadfoto_tanah_shm = findViewById(R.id.uploadfoto_tanah_shm);


        setAwall();
//navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
        restoreActionBar();
        relJenisagunan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), JenisagunanActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        relAgunan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Agunan_dropdownActivity.class);
                intent.putExtra("_DATA", jenisagunan.getText().toString());
                startActivityForResult(intent, 2);
            }
        });

    }


//    private void methodRequiresTwoPermission() {
//        String[] perms = {
//                Manifest.permission.CAMERA,
//                Manifest.permission.ACCESS_FINE_LOCATION,
//                Manifest.permission.INTERNET,
//                Manifest.permission.READ_EXTERNAL_STORAGE,
//                Manifest.permission.ACCESS_NETWORK_STATE,
//                Manifest.permission.ACCESS_COARSE_LOCATION,
//                Manifest.permission.ACCESS_FINE_LOCATION,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE
//        };
//        if (EasyPermissions.hasPermissions(this, perms)) {
//            try {
//                Log.d(TAG, "getLocation: Started:");
//                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 10, this);
//            } catch (SecurityException e) {
//                Log.d(TAG, "getLocation: Error Massage:" + e.getMessage());
////                Crashlytics.logException(e);
//            }
//            Log.d(TAG, "methodRequiresTwoPermission: TRUE");
//            LatLNG = getIntent().getStringExtra("_LatLNG");
////            getSupportActionBar().setTitle("Tambah");
//            _SupportMapFragment = (SupportMapFragment) getSupportFragmentManager()
//                    .findFragmentById(R.id.map);
//            if (_SupportMapFragment != null) {
//                _SupportMapFragment.getMapAsync(this);
//            }
//        } else {
//            // Do not have permissions, request them now
//            EasyPermissions.requestPermissions(this, getString(R.string.camera_and_location_rationale),
//                    RC_CAMERA_AND_LOCATION_AND_STORAGE, perms);
//        }
//    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                setAwall();
                String strEditText = data.getStringExtra("_DATA");
                jenisagunan.setText(strEditText);
                agunan.setText("Agunan");
                agunan.setTextColor(Color.GRAY);

                relAgunan.setVisibility(View.VISIBLE);
                subagunan.setVisibility(View.VISIBLE);

            }
            if (requestCode == 2) {
//                String strEditText = data.getStringExtra("_DATA");
//                agunan.setText(strEditText);
//                setForm(resultData.toLowerCase());
                if (data.hasExtra("_DATA")) {
                    setAwall();
                    String resultData = data.getStringExtra("_DATA");
                    agunan.setText(resultData);
                    agunan.setTextColor(Color.BLACK);
                    Log.e("errortag", "onActivityResult: tvAngunan " + resultData);
                    setForm(resultData.toLowerCase());
                }

            }

            if (requestCode == PICK_IMAGE_REQUEST) {
                filePath = data.getData();
                Bitmap bm = null;
                if (data != null) {
                    try {
                        bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Bitmap selectedImage = getResizedBitmap(bm, 300);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                assert bm != null;
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                if (type_upload.equals("image_1")) {
                    try {
                        base64_image_1 = URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
                        Log.e("base64_image_1", base64_image_1);
                        Log.e("base64_image_1_length", String.valueOf(base64_image_1.length()));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        setImageUpload(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        base64_image_2 = URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
                        Log.e("base64_image_2", base64_image_2);
                        Log.e("base64_image_2_length", String.valueOf(base64_image_2.length()));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        //rl_uploadFotoKtp.setVisibility(View.GONE);
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                        setImageUpload(bitmap);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (requestCode == REQUEST_FILE_MANAGER) {
                try {
                    Uri uri = data.getData();
                    Log.d(TAG, "File Uri: " + uri.toString());
                    String path = FileUtils.getPath(this, uri);
                    String filename = path.substring(path.lastIndexOf("/") + 1);
                    Log.e(TAG, "onActivityResult: " + FileUtils.getExtension(path) + "  " + filename);

                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
                }
            }
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                Bitmap selectedImage = getResizedBitmap(thumbnail, 300);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                assert thumbnail != null;
                selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;

                if (type_upload.equals("image_1")) {
//                base64_image_1 = Base64.encodeToString(bytes.toByteArray(), Base64.NO_WRAP);
                    try {
                        base64_image_1 = URLEncoder.encode(Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT), "UTF-8");
                        Log.e("base64_image_1", base64_image_1);
                        Log.e("base64_image_1_length", String.valueOf(base64_image_1.length()));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Log.e("base64_image_1", base64_image_1);
                    Log.e("base64_image_1_length", String.valueOf(base64_image_1.length()));
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    setImageUpload(imageBitmap);
                } else {
//                base64_image_2 = Base64.encodeToString(bytes.toByteArray(), Base64.NO_WRAP);
                    try {
                        base64_image_2 = URLEncoder.encode(Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT), "UTF-8");
                        Log.e("base64_image_2", base64_image_2);
                        Log.e("base64_image_2_length", String.valueOf(base64_image_2.length()));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Log.e("base64_image_2", base64_image_2);
                    Log.e("base64_image_2_length", String.valueOf(base64_image_2.length()));
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    //rl_uploadFotoKtp.setVisibility(View.GONE);
                    Matrix matrix = new Matrix();

                    matrix.postRotate(90);

                    imageBitmap = Bitmap.createScaledBitmap(imageBitmap, imageBitmap.getWidth(), imageBitmap.getHeight(), true);

                    imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.getWidth(), imageBitmap.getHeight(), matrix, true);
                    setImageUpload(imageBitmap);

                }
            }

        }
    }

    private void setImageUpload(Bitmap imageBitmap) {
        uploadfotoimage_tanah_shgb.setVisibility(View.VISIBLE);
        uploadfotoimage_tanah_shm.setVisibility(View.VISIBLE);
        uploadfotoimageTbShm.setVisibility(View.VISIBLE);
        uploadfotoimageTbShgb.setVisibility(View.VISIBLE);
        uploadfotoimageSepedamotor.setVisibility(View.VISIBLE);
        uploadfotoimageMobil.setVisibility(View.VISIBLE);
        uploadfotoimagePerhiasan.setVisibility(View.VISIBLE);
        uploadfotoimageBatuPermata.setVisibility(View.VISIBLE);
        uploadfotoimageEmasbatangan.setVisibility(View.VISIBLE);


        uploadfotoimage_tanah_shgb1.setImageBitmap(imageBitmap);
        uploadfotoimage_tanah_shm2.setImageBitmap(imageBitmap);
        uploadfotoimagePerhiasan3.setImageBitmap(imageBitmap);
        uploadfotoimageBatuPermata4.setImageBitmap(imageBitmap);
        uploadfotoimageEmasbatangan5.setImageBitmap(imageBitmap);
        uploadfotoimageTbShgb6.setImageBitmap(imageBitmap);
        uploadfotoimageTbShm7.setImageBitmap(imageBitmap);
        uploadfotoimageSepedamotor8.setImageBitmap(imageBitmap);
        uploadfotoimageMobil9.setImageBitmap(imageBitmap);
    }

    private void setOnclik(View btnUpload, View btnSubmit) {
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type_upload = "image_1";
                @SuppressLint("ResourceType")
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AgunanActivity.this);
                LayoutInflater inflater = AgunanActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alert_uploadfoto, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(true);
                dialogBuilder.setTitle("Upload File");
                CardView takePhoto = dialogView.findViewById(R.id.cv_takePhoto);
                CardView selectPhoto = dialogView.findViewById(R.id.cv_selectPhoto);
//                CardView selectFile = dialogView.findViewById(R.id.cv_File);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                alertDialog.setCancelable(true);
                alertDialog.setCanceledOnTouchOutside(true);
                takePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dispatchTakePictureIntent();
                        alertDialog.dismiss();

                    }
                });
                selectPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showFileChooser();
                        alertDialog.dismiss();
                    }
                });


            }

        });

    }

    private void setForm(String resultData) {
        setAwall();
        switch (resultData) {
            case "bpkb mobil":
                mobil.setVisibility(View.VISIBLE);
                //upload foto
                uploadfotoMobil = findViewById(R.id.uploadfoto_mobil);
                uploadfotoimageMobil = findViewById(R.id.uploadfotoimage_mobil);
                sumbitmobil = findViewById(R.id.sumbitmobil);
                sumbitmobil.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (nomorbpkbmobil.getText().toString().isEmpty() && atasnamamobil.getText().toString().isEmpty() && alamatmobil.getText().toString().isEmpty()
                                && merkmobil.getText().toString().isEmpty() && nomorpolisimobil.getText().toString().isEmpty() && nomorrangkamobil.getText().toString().isEmpty()
                                && nomormesinmobil.getText().toString().isEmpty() && tahunmobil.getText().toString().isEmpty() && deskripsimobil.getText().toString().isEmpty() && base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                        } else if (nomorbpkbmobil.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor BPBKB Kosong", Toast.LENGTH_LONG).show();

                        } else if (atasnamamobil.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Atas Nama Kosong", Toast.LENGTH_LONG).show();

                        } else if (alamatmobil.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Alamat Kosong", Toast.LENGTH_LONG).show();

                        } else if (merkmobil.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Merk Kosong", Toast.LENGTH_LONG).show();

                        } else if (nomorpolisimobil.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor Polisi Kosong", Toast.LENGTH_LONG).show();

                        } else if (nomorrangkamobil.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor Rangka Kosong", Toast.LENGTH_LONG).show();

                        } else if (nomormesinmobil.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor Mesin Kosong", Toast.LENGTH_LONG).show();

                        } else if (tahunmobil.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Tahun Kosong", Toast.LENGTH_LONG).show();

                        } else if (deskripsimobil.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Deskripsi Kosong", Toast.LENGTH_LONG).show();

                        } else if (base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "foto Kosong", Toast.LENGTH_LONG).show();
                        } else {
                            Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                            FragmentManager mFragmentManager = getSupportFragmentManager();
                            menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                        }

//                        Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
//                        FragmentManager mFragmentManager = getSupportFragmentManager();
//                        menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                    }
                });
                setOnclik(uploadfotoMobil, sumbitmobil);
                break;
            case "bpkb motor":
                sepedamotor.setVisibility(View.VISIBLE);
                //upload foto
                uploadfotoSepedamotor = findViewById(R.id.uploadfoto_sepedamotor);
                uploadfotoimageSepedamotor = findViewById(R.id.uploadfotoimage_sepedamotor);
                sumbitsepedamotor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (nomorbpkbmotor.getText().toString().isEmpty() && atasnamamotor.getText().toString().isEmpty() && alamatmotor.getText().toString().isEmpty()
                                && merkmotor.getText().toString().isEmpty() && nomorpolisimotor.getText().toString().isEmpty() && nomorrangkamotor.getText().toString().isEmpty()
                                && nomormesinmotor.getText().toString().isEmpty() && tahunmotor.getText().toString().isEmpty() && deskripsimotor.getText().toString().isEmpty() && base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                        } else if (nomorbpkbmotor.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor BPBKB Kosong", Toast.LENGTH_LONG).show();

                        } else if (atasnamamotor.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Atas Nama Kosong", Toast.LENGTH_LONG).show();

                        } else if (alamatmotor.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Alamat Kosong", Toast.LENGTH_LONG).show();

                        } else if (merkmotor.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Merk Kosong", Toast.LENGTH_LONG).show();

                        } else if (nomorpolisimotor.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor Polisi Kosong", Toast.LENGTH_LONG).show();

                        } else if (nomorrangkamotor.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor Rangka Kosong", Toast.LENGTH_LONG).show();

                        } else if (nomormesinmotor.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor Mesin Kosong", Toast.LENGTH_LONG).show();

                        } else if (tahunmotor.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Tahun Kosong", Toast.LENGTH_LONG).show();

                        } else if (deskripsimotor.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Deskripsi Kosong", Toast.LENGTH_LONG).show();

                        } else if (base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "foto Kosong", Toast.LENGTH_LONG).show();
                        } else {
                            Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                            FragmentManager mFragmentManager = getSupportFragmentManager();
                            menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                        }

//                        Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
//                        FragmentManager mFragmentManager = getSupportFragmentManager();
//                        menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                    }
                });
                setOnclik(uploadfotoSepedamotor, sumbitsepedamotor);
                break;
            case "tanah shm":
                tanahSHM.setVisibility(View.VISIBLE);

                //upload foto
                uploadfoto_tanah_shm = findViewById(R.id.uploadfoto_tanah_shm);
                uploadfotoimage_tanah_shm = findViewById(R.id.uploadfotoimage_tanah_shgb);
                //button
                sumbittanahshm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (nomortanahshm.getText().toString().isEmpty() && atasnamatanahshm.getText().toString().isEmpty() && shmtanah.getText().toString().isEmpty()
                                && tvDataTanggalShm.getText().toString().isEmpty() && lokasitanahshm.getText().toString().isEmpty() && luastanahshm.getText().toString().isEmpty()
                                && deskripsitanahshm.getText().toString().isEmpty() && base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                        } else if (nomortanahshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor SHM Kosong ", Toast.LENGTH_LONG).show();

                        } else if (atasnamatanahshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Atas Nama Kosong", Toast.LENGTH_LONG).show();

                        } else if (shmtanah.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "SHM Kosong", Toast.LENGTH_LONG).show();

                        } else if (tvDataTanggalShm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Tanggal Akhir SHM Kosong", Toast.LENGTH_LONG).show();

                        } else if (lokasitanahshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Lokasi Kosong", Toast.LENGTH_LONG).show();

                        } else if (luastanahshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Luas Kosong", Toast.LENGTH_LONG).show();

                        } else if (deskripsitanahshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Deskripsi Kosong", Toast.LENGTH_LONG).show();

                        } else if (base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "foto Kosong", Toast.LENGTH_LONG).show();
                        } else {
                            Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                            FragmentManager mFragmentManager = getSupportFragmentManager();
                            menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                        }
                    }
                });
                setOnclik(uploadfoto_tanah_shm, sumbittanahshm);
                //datepacer
                mDisplayDate = findViewById(R.id.tvData_tanggal_shm);
                mDisplayDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Calendar cal = Calendar.getInstance();
                        int year = cal.get(Calendar.YEAR);
                        int month = cal.get(Calendar.MONTH);
                        int day = cal.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog dialog = new DatePickerDialog(
                                AgunanActivity.this,
                                android.R.style.Theme_Material_Light_Dialog_MinWidth,
                                mDateSatlistener,
                                year, month, day);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                    }
                });
                mDateSatlistener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.d(TAG, "onDateSet: Date: " + dayOfMonth + "/" + month + "/" + year);
                        String date = dayOfMonth + "/" + month + "/" + year;
                        mDisplayDate.setText(date);
                    }
                };
                //bikinkuatdrat
                m2tvshm.setText(Html.fromHtml("m<sup>2</sup>"));

                break;
            case "tanah shgb":
                tanahSHGB.setVisibility(View.VISIBLE);


                //upload foto
                uploadfoto_tanah_shgb = findViewById(R.id.uploadfoto_tanah_shgb);
                uploadfotoimage_tanah_shgb = findViewById(R.id.uploadfotoimage_tanah_shgb);
                //button
                sumbittanahshm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (nomortanahshgb.getText().toString().isEmpty() && atasnamatanahshgb.getText().toString().isEmpty() && shgbtanah.getText().toString().isEmpty()
                                && tvDataTanggalShgb.getText().toString().isEmpty() && lokasitanahshgb.getText().toString().isEmpty() && luastanahshgb.getText().toString().isEmpty()
                                && deskripsitanahshgb.getText().toString().isEmpty() && base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                        } else if (nomortanahshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor SHGB Kosong ", Toast.LENGTH_LONG).show();

                        } else if (atasnamatanahshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Atas Nama Kosong", Toast.LENGTH_LONG).show();

                        } else if (shgbtanah.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "SHGB Kosong", Toast.LENGTH_LONG).show();

                        } else if (tvDataTanggalShgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Tanggal Akhir SHGB Kosong", Toast.LENGTH_LONG).show();

                        } else if (lokasitanahshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Lokasi Kosong", Toast.LENGTH_LONG).show();

                        } else if (luastanahshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Luas Kosong", Toast.LENGTH_LONG).show();

                        } else if (deskripsitanahshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Deskripsi Kosong", Toast.LENGTH_LONG).show();

                        } else if (base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "foto Kosong", Toast.LENGTH_LONG).show();
                        } else {
                            Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                            FragmentManager mFragmentManager = getSupportFragmentManager();
                            menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                        }
                    }
                });
                setOnclik(uploadfoto_tanah_shgb, sumbittanahSHGB);
                //datepacer
                mDisplayDate = findViewById(R.id.tvData_tanggal_shgb);
                mDisplayDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Calendar cal = Calendar.getInstance();
                        int year = cal.get(Calendar.YEAR);
                        int month = cal.get(Calendar.MONTH);
                        int day = cal.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog dialog = new DatePickerDialog(
                                AgunanActivity.this,
                                android.R.style.Theme_Material_Light_Dialog_MinWidth,
                                mDateSatlistener,
                                year, month, day);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                    }
                });
                mDateSatlistener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.d(TAG, "onDateSet: Date: " + dayOfMonth + "/" + month + "/" + year);
                        String date = dayOfMonth + "/" + month + "/" + year;
                        mDisplayDate.setText(date);
                    }
                };
                //bikinkuatdrat
                m2tvshgb.setText(Html.fromHtml("m<sup>2</sup>"));
                break;
            case "tanah dan bangunan shm":
                tanahdanbangunanSHM.setVisibility(View.VISIBLE);


                //upload foto
                uploadfotoTbShm = findViewById(R.id.uploadfoto_tb_shm);
                uploadfotoimageTbShm = findViewById(R.id.uploadfotoimage_tb_shm);
                //button
                sumbittanahdanbangunanShm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (nomortanahdanbangunanshm.getText().toString().isEmpty() && atasnamatanahdanbangunanshm.getText().toString().isEmpty() && shmtanahdanbangunan.getText().toString().isEmpty()
                                && tvDataTanggalTbShm.getText().toString().isEmpty() && lokasitanahdanbangunanshm.getText().toString().isEmpty() && luastanahdanbangunanshm.getText().toString().isEmpty()
                                && deskripsitanahdanbangunanshm.getText().toString().isEmpty() && base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                        } else if (nomortanahdanbangunanshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor SHM Kosong ", Toast.LENGTH_LONG).show();

                        } else if (atasnamatanahdanbangunanshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Atas Nama Kosong", Toast.LENGTH_LONG).show();

                        } else if (shmtanahdanbangunan.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "SHM Kosong", Toast.LENGTH_LONG).show();

                        } else if (tvDataTanggalTbShm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Tanggal Akhir SHM Kosong", Toast.LENGTH_LONG).show();

                        } else if (lokasitanahdanbangunanshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Lokasi Kosong", Toast.LENGTH_LONG).show();

                        } else if (luastanahdanbangunanshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Luas Kosong", Toast.LENGTH_LONG).show();

                        } else if (deskripsitanahdanbangunanshm.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Deskripsi Kosong", Toast.LENGTH_LONG).show();

                        } else if (base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "foto Kosong", Toast.LENGTH_LONG).show();
                        } else {
                            Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                            FragmentManager mFragmentManager = getSupportFragmentManager();
                            menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                        }
                    }
                });
                setOnclik(uploadfotoTbShm, sumbittanahdanbangunanShm);
                //datepacer
                mDisplayDate = findViewById(R.id.tvData_tanggal_tb_shm);
                mDisplayDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Calendar cal = Calendar.getInstance();
                        int year = cal.get(Calendar.YEAR);
                        int month = cal.get(Calendar.MONTH);
                        int day = cal.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog dialog = new DatePickerDialog(
                                AgunanActivity.this,
                                android.R.style.Theme_Material_Light_Dialog_MinWidth,
                                mDateSatlistener,
                                year, month, day);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                    }
                });
                mDateSatlistener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.d(TAG, "onDateSet: Date: " + dayOfMonth + "/" + month + "/" + year);
                        String date = dayOfMonth + "/" + month + "/" + year;
                        mDisplayDate.setText(date);
                    }
                };
                //bikinkuatdrat
                m2tvtbshm.setText(Html.fromHtml("m<sup>2</sup>"));
                break;
            case "tanah dan bangunan shgb":
                tanahdanbangunanSHGB.setVisibility(View.VISIBLE);
                //lokasi


                //upload foto
                uploadfotoTbShgb = findViewById(R.id.uploadfoto_tb_shgb);
                uploadfotoimageTbShgb = findViewById(R.id.uploadfotoimage_tb_shgb);
                //button
                sumbittanahdanbangunanShcb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (nomortanahdanbangunanshgb.getText().toString().isEmpty() && atasnamatanahdanbangunanshgb.getText().toString().isEmpty() &&
                                shgbtanahdanbangunan.getText().toString().isEmpty() && tvDataTanggalTbShgb.getText().toString().isEmpty() && lokasitanahdanbangunanshgb.getText().toString().isEmpty()
                                && luastanahdanbangunanshgb.getText().toString().isEmpty() && deskripsitanahdanbangunanshgb.getText().toString().isEmpty() && base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                        } else if (nomortanahdanbangunanshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nomor SHGB Kosong ", Toast.LENGTH_LONG).show();

                        } else if (atasnamatanahdanbangunanshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Atas Nama Kosong", Toast.LENGTH_LONG).show();

                        } else if (shgbtanahdanbangunan.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "SHGB Kosong", Toast.LENGTH_LONG).show();

                        } else if (tvDataTanggalTbShgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Tanggal Akhir SHGB Kosong", Toast.LENGTH_LONG).show();

                        } else if (lokasitanahdanbangunanshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Lokasi Kosong", Toast.LENGTH_LONG).show();

                        } else if (luastanahdanbangunanshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Luas Kosong", Toast.LENGTH_LONG).show();

                        } else if (deskripsitanahdanbangunanshgb.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Deskripsi Kosong", Toast.LENGTH_LONG).show();

                        } else if (base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "foto Kosong", Toast.LENGTH_LONG).show();
                        } else {
                            Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                            FragmentManager mFragmentManager = getSupportFragmentManager();
                            menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                        }
                    }

                });
                setOnclik(uploadfotoTbShgb, sumbittanahdanbangunanShcb);
                //datepacer
                mDisplayDate = findViewById(R.id.tvData_tanggal_tb_shgb);
                mDisplayDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Calendar cal = Calendar.getInstance();
                        int year = cal.get(Calendar.YEAR);
                        int month = cal.get(Calendar.MONTH);
                        int day = cal.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog dialog = new DatePickerDialog(
                                AgunanActivity.this,
                                android.R.style.Theme_Material_Light_Dialog_MinWidth,
                                mDateSatlistener,
                                year, month, day);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                    }
                });
                mDateSatlistener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.d(TAG, "onDateSet: Date: " + dayOfMonth + "/" + month + "/" + year);
                        String date = dayOfMonth + "/" + month + "/" + year;
                        mDisplayDate.setText(date);
                    }
                };
                //bikinkuatdrat
                m2tvtbshgb.setText(Html.fromHtml("m<sup>2</sup>"));
                break;
            case "batu permata":
                barangberhargaPermata.setVisibility(View.VISIBLE);
                //upload foto
                uploadfotoBatuPermata = findViewById(R.id.uploadfoto_batu_permata);
                uploadfotoimageBatuPermata = findViewById(R.id.uploadfotoimage_batu_permata);
                nilaipermata = findViewById(R.id.nilaipermata);
                deskripsipermata = findViewById(R.id.deskripsipermata);
                sumbitpermata.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (nilaipermata.getText().toString().isEmpty() && deskripsipermata.getText().toString().isEmpty() && base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                        } else if (nilaipermata.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nilai Kosong", Toast.LENGTH_LONG).show();

                        } else if (deskripsipermata.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Deskripsi Kosong", Toast.LENGTH_LONG).show();

                        } else if (base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "foto Kosong", Toast.LENGTH_LONG).show();
                        } else {
                            Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                            FragmentManager mFragmentManager = getSupportFragmentManager();
                            menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                        }

//                        Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
//                        FragmentManager mFragmentManager = getSupportFragmentManager();
//                        menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                    }
                });
                setOnclik(uploadfotoBatuPermata, sumbitpermata);
                break;
            case "perhiasan":
                barangberhargaPerhiasan.setVisibility(View.VISIBLE);
                nilaiperhiasan = findViewById(R.id.nilaiperhiasan);
                deskripsiperhiasan = findViewById(R.id.deskripsiperhiasan);
                sumbitperhiasan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (nilaiperhiasan.getText().toString().isEmpty() && deskripsiperhiasan.getText().toString().isEmpty() && base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                        } else if (nilaiperhiasan.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nilai Kosong", Toast.LENGTH_LONG).show();

                        } else if (deskripsiperhiasan.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Deskripsi Kosong", Toast.LENGTH_LONG).show();

                        } else if (base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "foto Kosong", Toast.LENGTH_LONG).show();
                        } else {
                            Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                            FragmentManager mFragmentManager = getSupportFragmentManager();
                            menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                        }

//                        Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
//                        FragmentManager mFragmentManager = getSupportFragmentManager();
//                        menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                    }
                });
                setOnclik(uploadfotoPerhiasan, sumbitperhiasan);
                break;
            case "emas batangan":
                barangberhargaEmasbatangan.setVisibility(View.VISIBLE);
                //upload foto
                uploadfotoEmasbatangan = findViewById(R.id.uploadfoto_emasbatangan);
                uploadfotoimageEmasbatangan = findViewById(R.id.uploadfotoimage_emasbatangan);
                nilaiemasbatangan = findViewById(R.id.nilaiemasbatangan);
                deskripsiemasbatangan = findViewById(R.id.deskripsiemasbatangan);
                sumbitemasbatang.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (nilaiemasbatangan.getText().toString().isEmpty() && deskripsiemasbatangan.getText().toString().isEmpty() && base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                        } else if (nilaiemasbatangan.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Nilai Kosong", Toast.LENGTH_LONG).show();

                        } else if (deskripsiemasbatangan.getText().toString().isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "Deskripsi Kosong", Toast.LENGTH_LONG).show();

                        } else if (base64_image_1.isEmpty()) {
                            Toast.makeText(AgunanActivity.this, "foto Kosong", Toast.LENGTH_LONG).show();
                        } else {
                            Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                            FragmentManager mFragmentManager = getSupportFragmentManager();
                            menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                        }

//                        Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
//                        FragmentManager mFragmentManager = getSupportFragmentManager();
//                        menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
                    }
                });
                setOnclik(uploadfotoEmasbatangan, sumbitemasbatang);
                break;

        }

    }


    //tanah_shgb
    private void showChooser() {
        // Use the GET_CONTENT intent from the utility class
        Intent intent = new Intent();
        intent.setType("*/*");
        Log.e(TAG, "showChooser: File Start");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Select a File to Upload"),
                REQUEST_FILE_MANAGER);

    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);

    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    //Requesting permission
    private void requestStoragePermission() {
        String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permission, STORAGE_PERMISSION_CODE);

        }

    }


    //This method will be called when the user will tap on allow or deny
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//
//        //Checking the request code of our request
//        if (requestCode == STORAGE_PERMISSION_CODE) {
//
//            //If permission is granted
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
//            } else {
//
//                //Displaying another toast if permission is not granted
//                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
//            }
//        }
//
//
//    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

//    @Override
//    public void onLocationChanged(Location location) {
//        Log.d(TAG, "onLocationChanged: " + location.getLatitude() + location.getLongitude());
//        latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        try {
//            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
//            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
//            Log.d(TAG, "onLocationChanged: Adress " + addresses.get(0).getAddressLine(0));
//            latLng = new LatLng(location.getLatitude(), location.getLongitude());
//            lat = Double.toString(location.getLatitude());
//            lng = Double.toString(location.getLongitude());
//            LatLNG = lat + "," + lng;
//            lokasiShgb.setText(addresses.get(0).getAddressLine(0));
//            lokasiShm.setText(addresses.get(0).getAddressLine(0));
//            lokasiTbshgb.setText(addresses.get(0).getAddressLine(0));
//            lokasitbShm.setText(addresses.get(0).getAddressLine(0));
//            //titik koordinat
////            lokasi_shgb_tanah.setText(LatLNG);
//            Log.d(TAG, "onMapReady: " + LatLNG);
//            _GoogleMap.clear();
//            _GoogleMap.addMarker(new MarkerOptions().position(latLng).title("Kamu Disini..."));
//            _GoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
//            _GoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
//            _GoogleMap.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
//            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
//            CameraPosition cameraPosition = new CameraPosition.Builder()
//                    .target(latLng)      // Sets the center of the map to Mountain View
//                    .zoom(17)                   // Sets the zoom
//                    .bearing(0)                // Sets the orientation of the camera to east
//                    .tilt(45)                   // Sets the tilt of the camera to 30 degrees
//                    .build();                   // Creates a CameraPosition from the builder
//            _GoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//        } catch (Exception e) {
//            Log.d(TAG, "getLocation: Error Massage:" + e.getMessage());
//            e.printStackTrace();
////            Crashlytics.logException(e);
//        }
//
//    }

//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//        progressDialog.setMessage("Peringatan"); // Setting Message
//        progressDialog.setTitle("Mohon Tetap Aktifkan GPS Anda."); // Setting Title
//        progressDialog.setProgressStyle(ProgressDialog.BUTTON_NEGATIVE); // Progress Dialog Style Spinner
//        progressDialog.show(); // Display Progress Dialog
//        progressDialog.setCancelable(false);
//        new Thread(new Runnable() {
//            public void run() {
//                try {
//                    Thread.sleep(10000);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                progressDialog.dismiss();
//            }
//        }).start();
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//        progressDialog.dismiss();
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//        progressDialog.setMessage("Peringatan"); // Setting Message
//        progressDialog.setTitle("Mohon Aktifkan GPS Anda."); // Setting Title
//        progressDialog.setProgressStyle(ProgressDialog.BUTTON_NEGATIVE); // Progress Dialog Style Spinner
//        progressDialog.show(); // Display Progress Dialog
//        progressDialog.setCancelable(false);
//        new Thread(new Runnable() {
//            public void run() {
//                try {
//                    Thread.sleep(10000);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                progressDialog.dismiss();
//            }
//        }).start();
//
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        _GoogleMap = googleMap;
//    }
}
