package waruagung.com.ksuapplicationPencairanPakisaji.agunan;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;

import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.agunan.Model.ModelAttachments;
import waruagung.com.ksuapplicationPencairanPakisaji.agunan.adapter.AdapterAttachment;

public class Detail_tanah_Activity extends AppCompatActivity implements AdapterAttachment.AdapterAttachmentCallback {

    static final int REQUEST_IMAGE_CAPTURE = 321;
    static final int REQUEST_FILE_MANAGER = 223;
    private static final String TAG = "";
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 1;
    public String base64_image_1 = "", base64_image_2 = "", type_upload = "";
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.sumbit)
    TextView sumbit;
    //untukuplod gambar dari camera atau galeri
    TextView lingkaran1;
    ImageView uploadfoto2;
    @BindView(R.id.lv_listViewItem)
    RecyclerView lv_listViewItem;
    @BindView(R.id.m2tvtbshm)
    TextView m2tv;
    //datepacker
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSatlistener;
    //Image request code
    private int PICK_IMAGE_REQUEST = 1;
    //Bitmap to get image from gallery
    private Bitmap bitmap;

    private ArrayList<Uri> arrayList = new ArrayList<>();
    //Uri to store the image uri
    private Uri filePath;


    //untuk toolbar
    private String mTitle = "REVEIW PEMBAYARAN";
    private SessionManager sessionManager;
    private List<String> listDataHeader;
    private HashMap<String, List> listDataChild;
    private List<ModelAttachments> modelAttachmentsList = new ArrayList<>();
    private AdapterAttachment adapterBuyNow;

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
//    @BindView(R.id.sumbit)
//    TextView sumbit;
//
//    //untukuplod gambar dari camera atau galeri
//    TextView lingkaran1;
//    ImageView uploadfoto2;
//    public String base64_image_1 = "", base64_image_2 = "", type_upload = "";
//
//
//
//    static final int REQUEST_IMAGE_CAPTURE = 1;
//    private static final String TAG = "";
//    //storage permission code
//    private static final int STORAGE_PERMISSION_CODE = 1;
//
//
//    //Image request code
//    private int PICK_IMAGE_REQUEST = 1;
//    //Bitmap to get image from gallery
//    private Bitmap bitmap;
//
//    private ArrayList<Uri> arrayList = new ArrayList<>();
//    //Uri to store the image uri
//    private Uri filePath;
//
//
//    //untuk toolbar
//    private String mTitle = "REVEIW PEMBAYARAN";
//    private SessionManager sessionManager;
//    private List<String> listDataHeader;
//    private HashMap<String, List> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tanah_);
        ButterKnife.bind(this);
        //datepacker
        mDisplayDate = findViewById(R.id.tvData_tanggal_tb_shm);
        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        Detail_tanah_Activity.this,
                        android.R.style.Theme_Material_Light_Dialog_MinWidth,
                        mDateSatlistener,
                        year, month, day);
//                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSatlistener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Log.d(TAG, "onDateSet: Date: " + dayOfMonth + "/" + month + "/" + year);
                String date = dayOfMonth + "/" + month + "/" + year;
                mDisplayDate.setText(date);
            }
        };

        //bikinkuatdrat
        m2tv.setText(Html.fromHtml("m<sup>2</sup>"));
        //navigationonclik

        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });


        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Popup_Fragment menuDialogHotelFragment = new Popup_Fragment();
                FragmentManager mFragmentManager = getSupportFragmentManager();
                menuDialogHotelFragment.show(mFragmentManager, Popup_Fragment.class.getSimpleName());
            }


        });

        lv_listViewItem.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));
        lv_listViewItem.setHasFixedSize(true);
        lv_listViewItem.setVisibility(View.GONE);
        restoreActionBar();
//        showChooser();
        restoreActionBar();
        //untukuplod gambar dari camera atau galeri
//        requestStoragePermission();


        lingkaran1 = findViewById(R.id.uploadfoto);
        uploadfoto2 = findViewById(R.id.uploadfotoimage);

        lingkaran1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type_upload = "image_1";
                @SuppressLint("ResourceType")
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Detail_tanah_Activity.this);
                LayoutInflater inflater = Detail_tanah_Activity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alert_uploadfoto, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(true);
                dialogBuilder.setTitle("Upload File");
                CardView takePhoto = dialogView.findViewById(R.id.cv_takePhoto);
                CardView selectPhoto = dialogView.findViewById(R.id.cv_selectPhoto);
//                CardView selectFile = dialogView.findViewById(R.id.cv_File);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                alertDialog.setCancelable(true);
                alertDialog.setCanceledOnTouchOutside(true);
                takePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dispatchTakePictureIntent();
                        alertDialog.dismiss();

                    }
                });
                selectPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showFileChooser();
                        alertDialog.dismiss();
                    }
                });


            }

        });
    }

    private void showChooser() {
        // Use the GET_CONTENT intent from the utility class
        Intent intent = new Intent();
        intent.setType("*/*");
        Log.e(TAG, "showChooser: File Start");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Select a File to Upload"),
                REQUEST_FILE_MANAGER);

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);

    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            Bitmap bm = null;
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Bitmap selectedImage = getResizedBitmap(bm, 300);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            assert bm != null;
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
            if (type_upload.equals("image_1")) {
                try {
                    base64_image_1 = URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
                    Log.e("base64_image_1", base64_image_1);
                    Log.e("base64_image_1_length", String.valueOf(base64_image_1.length()));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    uploadfoto2.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    base64_image_2 = URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
                    Log.e("base64_image_2", base64_image_2);
                    Log.e("base64_image_2_length", String.valueOf(base64_image_2.length()));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    //rl_uploadFotoKtp.setVisibility(View.GONE);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    uploadfoto2.setVisibility(View.VISIBLE);
                    uploadfoto2.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == REQUEST_FILE_MANAGER && resultCode == RESULT_OK) {
            try {
                Uri uri = data.getData();
                Log.d(TAG, "File Uri: " + uri.toString());
                String path = FileUtils.getPath(this, uri);
                String filename = path.substring(path.lastIndexOf("/") + 1);
                Log.e(TAG, "onActivityResult: " + FileUtils.getExtension(path) + "  " + filename);
                modelAttachmentsList.add(new ModelAttachments(uri, path, filename, R.drawable.agunan));
                setupAttachmentFile();
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getApplicationContext(), "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            Bitmap selectedImage = getResizedBitmap(thumbnail, 300);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            assert thumbnail != null;
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;

            if (type_upload.equals("image_1")) {
//                base64_image_1 = Base64.encodeToString(bytes.toByteArray(), Base64.NO_WRAP);
                try {
                    base64_image_1 = URLEncoder.encode(Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT), "UTF-8");
                    Log.e("base64_image_1", base64_image_1);
                    Log.e("base64_image_1_length", String.valueOf(base64_image_1.length()));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Log.e("base64_image_1", base64_image_1);
                Log.e("base64_image_1_length", String.valueOf(base64_image_1.length()));
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                uploadfoto2.setImageBitmap(imageBitmap);
            } else {
//                base64_image_2 = Base64.encodeToString(bytes.toByteArray(), Base64.NO_WRAP);
                try {
                    base64_image_2 = URLEncoder.encode(Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT), "UTF-8");
                    Log.e("base64_image_2", base64_image_2);
                    Log.e("base64_image_2_length", String.valueOf(base64_image_2.length()));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Log.e("base64_image_2", base64_image_2);
                Log.e("base64_image_2_length", String.valueOf(base64_image_2.length()));
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                //rl_uploadFotoKtp.setVisibility(View.GONE);
                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                imageBitmap = Bitmap.createScaledBitmap(imageBitmap, imageBitmap.getWidth(), imageBitmap.getHeight(), true);

                imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.getWidth(), imageBitmap.getHeight(), matrix, true);

                uploadfoto2.setVisibility(View.VISIBLE);
                uploadfoto2.setImageBitmap(imageBitmap);
            }
        }
    }

    private void setupAttachmentFile() {
        if (modelAttachmentsList.size() < 1) {
            lv_listViewItem.setVisibility(View.GONE);
        } else {
            lv_listViewItem.setVisibility(View.VISIBLE);
            lv_listViewItem.setAdapter(null);
            adapterBuyNow = new AdapterAttachment(Detail_tanah_Activity.this, -1, modelAttachmentsList, Detail_tanah_Activity.this);
            lv_listViewItem.setAdapter(adapterBuyNow);
            uploadfoto2.setVisibility(View.GONE);
        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    //Requesting permission
    private void requestStoragePermission() {
        String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permission, STORAGE_PERMISSION_CODE);
        }

    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {

                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }


    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onRowAdapterAttachmentClicked(int position) {
        File file = new File(modelAttachmentsList.get(position).getPath());
        Log.e(TAG, "onRowAdapterAttachmentClicked: " + FileUtils.getMimeType(file));
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(modelAttachmentsList.get(position).getUri(), FileUtils.getMimeType(file));
            PackageManager pm = getPackageManager();
            if (intent.resolveActivity(pm) != null) {
                startActivity(intent);
            }
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "onRowAdapterAttachmentClicked: " + e.getMessage());
            Toast.makeText(Detail_tanah_Activity.this, "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
        }


//
//    private void dispatchTakePictureIntent() {
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//        }
//    }
//
//    private void showFileChooser() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
//    }
//
//    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
//
//        int width = image.getWidth();
//        int height = image.getHeight();
//
//        float bitmapRatio = (float) width / (float) height;
//        if (bitmapRatio > 1) {
//            width = maxSize;
//            height = (int) (width / bitmapRatio);
//        } else {
//            height = maxSize;
//            width = (int) (height * bitmapRatio);
//        }
//        return Bitmap.createScaledBitmap(image, width, height, true);
//
//    }
//
//
//    //handling the image chooser activity result
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
//            filePath = data.getData();
//            Bitmap bm = null;
//            if (data != null) {
//                try {
//                    bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            Bitmap selectedImage = getResizedBitmap(bm, 300);
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            assert bm != null;
//            selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
//            if (type_upload.equals("image_1")) {
//                try {
//                    base64_image_1 = URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
//                    Log.e("base64_image_1", base64_image_1);
//                    Log.e("base64_image_1_length", String.valueOf(base64_image_1.length()));
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
//                    uploadfoto2.setImageBitmap(bitmap);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                try {
//                    base64_image_2 = URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
//                    Log.e("base64_image_2", base64_image_2);
//                    Log.e("base64_image_2_length", String.valueOf(base64_image_2.length()));
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
//                    //rl_uploadFotoKtp.setVisibility(View.GONE);
//                    Matrix matrix = new Matrix();
//                    matrix.postRotate(90);
//                    bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
//                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
//                    uploadfoto2.setVisibility(View.VISIBLE);
//                    uploadfoto2.setImageBitmap(bitmap);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//            Bitmap selectedImage = getResizedBitmap(thumbnail, 300);
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            assert thumbnail != null;
//            selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
//            File destination = new File(Environment.getExternalStorageDirectory(),
//                    System.currentTimeMillis() + ".jpg");
//            FileOutputStream fo;
//
//            if (type_upload.equals("image_1")) {
////                base64_image_1 = Base64.encodeToString(bytes.toByteArray(), Base64.NO_WRAP);
//                try {
//                    base64_image_1 = URLEncoder.encode(Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT), "UTF-8");
//                    Log.e("base64_image_1", base64_image_1);
//                    Log.e("base64_image_1_length", String.valueOf(base64_image_1.length()));
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//                Log.e("base64_image_1", base64_image_1);
//                Log.e("base64_image_1_length", String.valueOf(base64_image_1.length()));
//                Bundle extras = data.getExtras();
//                Bitmap imageBitmap = (Bitmap) extras.get("data");
//                uploadfoto2.setImageBitmap(imageBitmap);
//            } else {
////                base64_image_2 = Base64.encodeToString(bytes.toByteArray(), Base64.NO_WRAP);
//                try {
//                    base64_image_2 = URLEncoder.encode(Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT), "UTF-8");
//                    Log.e("base64_image_2", base64_image_2);
//                    Log.e("base64_image_2_length", String.valueOf(base64_image_2.length()));
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                }
//                Log.e("base64_image_2", base64_image_2);
//                Log.e("base64_image_2_length", String.valueOf(base64_image_2.length()));
//                Bundle extras = data.getExtras();
//                Bitmap imageBitmap = (Bitmap) extras.get("data");
//                //rl_uploadFotoKtp.setVisibility(View.GONE);
//                Matrix matrix = new Matrix();
//
//                matrix.postRotate(90);
//
//                imageBitmap = Bitmap.createScaledBitmap(imageBitmap, imageBitmap.getWidth(), imageBitmap.getHeight(), true);
//
//                imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.getWidth(), imageBitmap.getHeight(), matrix, true);
//
//                uploadfoto2.setVisibility(View.VISIBLE);
//                uploadfoto2.setImageBitmap(imageBitmap);
//            }
//        }
//    }
//
//
//
//    //method to get the file path from uri
//    public String getPath(Uri uri) {
//        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
//        cursor.moveToFirst();
//        String document_id = cursor.getString(0);
//        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
//        cursor.close();
//
//        cursor = getContentResolver().query(
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
//        cursor.moveToFirst();
//        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
//        cursor.close();
//
//        return path;
//    }
//
//
//
//    //Requesting permission
//    private void requestStoragePermission() {
//        String [] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            requestPermissions(permission, STORAGE_PERMISSION_CODE);
//        }
//
//    }
//
//
//    //This method will be called when the user will tap on allow or deny
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//
//        //Checking the request code of our request
//        if (requestCode == STORAGE_PERMISSION_CODE) {
//
//            //If permission is granted
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                //Displaying a toast
//                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
//            } else {
//
//                //Displaying another toast if permission is not granted
//                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
//            }
//        }
//
//
//
//
//
//
//
//    }
//
//
//
//
//
//
//
//    private void restoreActionBar() {
//        setSupportActionBar(toolbar);
////        toolbarTitle.setText("Keranjang");
////        toolbarTitle.setTextColor(Color.BLACK);
//        getSupportActionBar().setTitle("");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
////            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
//        getSupportActionBar().setDisplayShowTitleEnabled(true);
////        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
    }
}
