package waruagung.com.ksuapplicationPencairanPakisaji.agunan.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class adapter_listview_agunan_dropdown extends ArrayAdapter<String> {

    private String[] names;
    private int[] images;
    private Activity context;

    public adapter_listview_agunan_dropdown(@NonNull Activity context, String[] names, int[] images) {
        super(context, R.layout.desain_adapter_agunandropdown, names);
        this.context = context;
        this.names = names;
        this.images = images;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View r = convertView;
        adapter_listview_agunan_dropdown.ViewHolder viewHolder = null;

        LayoutInflater layoutInflater = context.getLayoutInflater();
        r = layoutInflater.inflate(R.layout.desain_adapter_agunandropdown, null, true);
        viewHolder = new adapter_listview_agunan_dropdown.ViewHolder(r);
        r.setTag(viewHolder);

        viewHolder.ivw.setImageResource(images[position]);
        viewHolder.tv1.setText(names[position]);
        return r;

    }

    class ViewHolder {
        TextView tv1;
        ImageView ivw;

        ViewHolder(View v) {
            tv1 = v.findViewById(R.id.txtTitle);
            ivw = v.findViewById(R.id.imgIcon);
        }
    }
}
