package waruagung.com.ksuapplicationPencairanPakisaji.pinjaman.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.DaftarPinjamanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class pop_up_pinjamanFragment extends DialogFragment implements View.OnClickListener {
    private View view;
    private Intent intent;

    public pop_up_pinjamanFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        view = inflater.inflate(R.layout.fragment_pop_uppembayaran, container, false);
        view.findViewById(R.id.lihatdaftar).setOnClickListener(this);
        view.findViewById(R.id.btnclose).setOnClickListener(this);
        return view;


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lihatdaftar:
                intent = new Intent(view.getContext(), DaftarPinjamanActivity.class);
                startActivity(intent);
                break;
//            case R.id.promo1:
//                intent = new Intent(view.getContext(), MarketingActivity.class);
//                startActivity(intent);
//                break;
//            case R.id.bantuan:
//                intent = new Intent(v.getContext(), ToursTravelActivity.class);
//                startActivity(intent);
//                break;
            case R.id.btnclose:
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
                break;
        }
    }
}
