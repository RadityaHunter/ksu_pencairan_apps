package waruagung.com.ksuapplicationPencairanPakisaji.pinjaman;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

import waruagung.com.ksuapplicationPencairanPakisaji.Cari_Nama_AnggotaActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.LaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;

public class PinjamanActivity extends AppCompatActivity {
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.reljenispinjaman)
    RelativeLayout reljenispinjaman;
    @BindView(R.id.reljenispencairan)
    RelativeLayout reljenispencairan;
    @BindView(R.id.sumbit)
    LinearLayout sumbit;
    @BindView(R.id.tvjenispencairan)
    TextView jenispencairan;
    @BindView(R.id.jenispinjaman)
    TextView jenispinjaman;
    private int setResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pinjaman);
        ButterKnife.bind(this);


        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Detail_pinjamanActivity.class);
                startActivity(intent);
            }
        });
        reljenispinjaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), jenis_pinjamanActivity.class);
                startActivityForResult(intent, 2);
            }
        });
        reljenispencairan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Jenis_pencairanActivity.class);
                startActivityForResult(intent, 1);
            }
        });

//navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                                        case R.id.Pembayaran:
                        Intent intent2 = new Intent(getApplicationContext(), Cari_Nama_AnggotaActivity.class);
startActivity(intent2);
                    case R.id.Laporan:
                         Intent intent3 = new Intent(getApplicationContext(), LaporanActivity.class);
startActivity(intent3);
                        break;
//                    case R.id.Riwayat:
//                        Intent intent3 = new Intent(getApplicationContext(), Riwayat_pembayaranActivity.class);
//                        startActivity(intent3);
//                        break;
                }
                return false;
            }
        });
        restoreActionBar();


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                String strEditText = data.getStringExtra("_DATA");
                jenispencairan.setText(strEditText);
            }

            if (requestCode == 2) {
                String strEditText = data.getStringExtra("_DATA");
                jenispinjaman.setText(strEditText);
            }
        }
    }


    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
