package waruagung.com.ksuapplicationPencairanPakisaji.pinjaman;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

import waruagung.com.ksuapplicationPencairanPakisaji.Cari_Nama_AnggotaActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.LaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.pinjaman.fragment.pop_up_pinjamanFragment;

public class Detail_pinjamanActivity extends AppCompatActivity {
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.reljenispinjaman)
    RelativeLayout reljenispinjaman;
    @BindView(R.id.relbank)
    RelativeLayout relbank;
    @BindView(R.id.reljenispencairan)
    RelativeLayout reljenispencairan;
    @BindView(R.id.sumbit)
    LinearLayout sumbit;
    @BindView(R.id.jenispinjaman)
    TextView jenispinjaman;
    @BindView(R.id.transfer)
    TextView jenispencairan;
    @BindView(R.id.bank)
    TextView bank;
    @BindView(R.id.rekening)
    RelativeLayout rekening;
    @BindView(R.id.nama)
    RelativeLayout nama;
    //    @BindView(R.id.tanggalpinjam)
//    TextView tanggalpinjam;
    @BindView(R.id.spinner_pinjaman)
    Spinner spinnerPinjaman;
    private String[] Item = {"Lusi Aprillia", "Prasetya", "Andri", "Olis Dare"};
    //datepacker
//    private TextView mDisplayDate;
//    private DatePickerDialog.OnDateSetListener mDateSatlistener;
//    private static final String TAG = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pinjaman);
        ButterKnife.bind(this);
        //spinner
        spinnerPinjaman = findViewById(R.id.spinner_pinjaman);
// inisialiasi Array Adapter dengan memasukkan string array di atas
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, Item);

        // mengeset Array Adapter tersebut ke Spinner
        spinnerPinjaman.setAdapter(adapter);
        // mengeset listener untuk mengetahui saat item dipilih
        spinnerPinjaman.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
//                Toast.makeText(Detail_pinjamanActivity.this, "Selected "+ adapter.getItem(i), Toast.LENGTH_SHORT).show();
                ((TextView) view).setTextSize(13);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//        //datepacker
//        mDisplayDate = (TextView) findViewById(R.id.tanggalpinjam);
//        mDisplayDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Calendar cal = Calendar.getInstance();
//                int year = cal.get(Calendar.YEAR);
//                int month = cal.get(Calendar.MONTH);
//                int day = cal.get(Calendar.DAY_OF_MONTH);
//                DatePickerDialog dialog = new DatePickerDialog(
//                        Detail_pinjamanActivity.this,
//                        android.R.style.Theme_Material_Light_Dialog_MinWidth,
//                        mDateSatlistener,
//                        year, month, day);
////                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                dialog.show();
//            }
//        });
//        mDateSatlistener = new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//                Log.d(TAG, "onDateSet: Date: " + dayOfMonth + "/" + month + "/" + year);
//                String date = dayOfMonth + "/" + month + "/" + year;
//                mDisplayDate.setText(date);
//            }
//        };
        relbank.setVisibility(View.GONE);
        rekening.setVisibility(View.GONE);
        nama.setVisibility(View.GONE);
        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pop_up_pinjamanFragment pop_up_pinjamanFragment = new pop_up_pinjamanFragment();
                FragmentManager mFragmentManager = getSupportFragmentManager();
                pop_up_pinjamanFragment.show(mFragmentManager, pop_up_pinjamanFragment.class.getSimpleName());
            }


        });

        reljenispinjaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), jenis_pinjamanActivity.class);
                startActivityForResult(intent, 1);
            }
        });
        relbank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), BankActivity.class);
                startActivityForResult(intent, 2);
            }
        });
        reljenispencairan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Jenis_pencairanActivity.class);
                startActivityForResult(intent, 3);
            }
        });


//navigationonclik
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(2).setChecked(true);
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(2).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                                        case R.id.Pembayaran:
                        Intent intent2 = new Intent(getApplicationContext(), Cari_Nama_AnggotaActivity.class);
startActivity(intent2);
                    case R.id.Laporan:
                         Intent intent3 = new Intent(getApplicationContext(), LaporanActivity.class);
startActivity(intent3);
                        break;
//                    case R.id.Riwayat:
//                        Intent intent3 = new Intent(getApplicationContext(), Riwayat_pembayaranActivity.class);
//                        startActivity(intent3);
//                        break;
                }
                return false;
            }
        });
        restoreActionBar();


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                String strEditText = data.getStringExtra("_DATA");
                jenispinjaman.setText(strEditText);
            }

            if (requestCode == 2) {
                String strEditText = data.getStringExtra("_DATA");
                bank.setText(strEditText);
                rekening.setVisibility(View.VISIBLE);
                nama.setVisibility(View.VISIBLE);
            }
            if (requestCode == 3) {
                String strEditText = data.getStringExtra("_DATA");
                jenispencairan.setText(strEditText);
                Log.e("Yuhu", "onActivityResult: " + strEditText);
                if (strEditText.equals("Transfer")) {
                    relbank.setVisibility(View.VISIBLE);
                } else {
                    relbank.setVisibility(View.GONE);
                }
            }
        }
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
