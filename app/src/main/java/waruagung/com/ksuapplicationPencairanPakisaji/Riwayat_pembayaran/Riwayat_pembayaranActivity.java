package waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.Adapter.AdapterRiwayatPembayaran;

import waruagung.com.ksuapplicationPencairanPakisaji.Cari_Nama_AnggotaActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.LaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanList;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class Riwayat_pembayaranActivity extends AppCompatActivity implements AdapterRiwayatPembayaran.AdapterRiwayatPembayaranCallback {
    public static final String TAG = Riwayat_pembayaranActivity.class.getSimpleName();
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.Rvdaftaragunan)
    RecyclerView Rvdaftaragunan;
    @BindView(R.id.CariKeyWord)
    EditText CariKeyWord;
    private APIInterface apiInterface;
    private ModelDaftarPinjamanList modelDaftarPinjamanList;
    private List<ModelDaftarPinjamanList> listNasabah = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_pembayaran);
        ButterKnife.bind(this);
        daftaragunan();
//navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                                        case R.id.Pembayaran:
                        Intent intent2 = new Intent(getApplicationContext(), Cari_Nama_AnggotaActivity.class);
startActivity(intent2);
                    case R.id.Laporan:
                         Intent intent3 = new Intent(getApplicationContext(), LaporanActivity.class);
startActivity(intent3);
                        break;
//                    case R.id.Riwayat:
//                        Intent intent3 = new Intent(getApplicationContext(), Riwayat_pembayaranActivity.class);
//                        startActivity(intent3);
//                        break;
                }
                return false;
            }
        });
        restoreActionBar();
        CariKeyWord.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (CariKeyWord.getText().length() >= 1) {
                    daftaragunanSearch(s.toString());
                    Log.e(TAG, "onTextChanged: CariKeyWord" + s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void daftaragunanSearch(String stringCari) {
        Rvdaftaragunan.setLayoutManager(new LinearLayoutManager(this));
        Rvdaftaragunan.setHasFixedSize(true);
        Rvdaftaragunan.setNestedScrollingEnabled(false);
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelDaftarPinjamanList>> call = apiInterface.doGetAPIPembayaranListCari("KSP11", stringCari);
        call.enqueue(new Callback<List<ModelDaftarPinjamanList>>() {
            @Override
            public void onResponse(Call<List<ModelDaftarPinjamanList>> call, Response<List<ModelDaftarPinjamanList>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.e(TAG, "onResponse: " + response.toString());
                    listNasabah = response.body();
                    AdapterRiwayatPembayaran adapter = new AdapterRiwayatPembayaran(getApplicationContext(), -1, listNasabah, Riwayat_pembayaranActivity.this);
                    Rvdaftaragunan.setAdapter(adapter);
                } else {
                    Log.e(TAG, "onFailure: " + response);
                }
            }

            @Override
            public void onFailure(Call<List<ModelDaftarPinjamanList>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void daftaragunan() {
        Rvdaftaragunan.setLayoutManager(new LinearLayoutManager(this));
        Rvdaftaragunan.setHasFixedSize(true);
        Rvdaftaragunan.setNestedScrollingEnabled(false);
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelDaftarPinjamanList>> call = apiInterface.doGetAPIPembayaranList("KSP11");
        call.enqueue(new Callback<List<ModelDaftarPinjamanList>>() {
            @Override
            public void onResponse(Call<List<ModelDaftarPinjamanList>> call, Response<List<ModelDaftarPinjamanList>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.e(TAG, "onResponse: " + response.toString());
                    listNasabah = response.body();
                    AdapterRiwayatPembayaran adapter = new AdapterRiwayatPembayaran(getApplicationContext(), -1, listNasabah, Riwayat_pembayaranActivity.this);
                    Rvdaftaragunan.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<ModelDaftarPinjamanList>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onRowAdapterRiwayatPembayaranClicked(int position) {
        ModelDaftarPinjamanList m = listNasabah.get(position);
        Intent intent = new Intent(getApplicationContext(), Detail_riwayat_pembayaranActivity.class);
        intent.putExtra("ModelDaftarPinjamanList", m);
        startActivity(intent);
    }
}
