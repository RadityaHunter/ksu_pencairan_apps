package waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelHeader implements Parcelable {

    public static final Creator<ModelHeader> CREATOR = new Creator<ModelHeader>() {
        @Override
        public ModelHeader createFromParcel(Parcel source) {
            return new ModelHeader(source);
        }

        @Override
        public ModelHeader[] newArray(int size) {
            return new ModelHeader[size];
        }
    };
    private String title;
    private Integer date;

    public ModelHeader(String title, int images) {
        this.title = title;
        this.date = images;
    }

    protected ModelHeader(Parcel in) {
        this.title = in.readString();
        this.date = in.readInt();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeInt(this.date);
    }
}
