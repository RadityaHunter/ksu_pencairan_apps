package waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.Adapter.AdapterRecyclerViewRiwayatPembayaran;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.LaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Cari_Nama_AnggotaActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanList;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanListDetail;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.model.ModelHeader;
import waruagung.com.ksuapplicationPencairanPakisaji.fragmentexpendeble.PopUpExpendableFragment;

public class Detail_riwayat_pembayaranActivity extends AppCompatActivity implements AdapterRecyclerViewRiwayatPembayaran.AdapterRecyclerViewRiwayatPembayaranCallback {
    public static final String TAG = Detail_riwayat_pembayaranActivity.class.getSimpleName();
    //    LinearLayout linierisitakeorderplan;
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
//    @BindView(R.id.bn_main)
//    BottomNavigationView bnMain;
//    @BindView(R.id.ExpandableListView)
//    ExpandableListView expandableListView;
//
//
//    private String mTitle = "List Review Plan";
//    private SessionManager sessionManager;
//    private List<String> listDataHeader;
//    private HashMap<String, List<model_detail_riwayat_pembayaran>> listDataChild;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ExpandableListView)
    ExpandableListView expandableListView1;
    @BindView(R.id.nama)
    TextView nama;
    @BindView(R.id.noid1)
    TextView noid1;
    @BindView(R.id.totalpinjam)
    TextView totalpinjam;
    @BindView(R.id.tvTotalPembayaran)
    TextView tvTotalPembayaran;
    @BindView(R.id.tvSisaPembayaran)
    TextView tvSisaPembayaran;
    @BindView(R.id.totalPinjaman)
    TextView totalPinjaman;
    @BindView(R.id.recyclerViewDetail)
    RecyclerView recyclerViewDetail;
    private List<ModelHeader> listDataHeader;
    private HashMap<String, List<ModelDaftarPinjamanListDetail>> listDataChild;
    private APIInterface apiInterface;
    private ModelDaftarPinjamanList modelDaftarPinjamanList;
    private List<ModelDaftarPinjamanListDetail> m = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_riwayat_pembayaran);

        ButterKnife.bind(this);
        if (getIntent().hasExtra("ModelDaftarPinjamanList")) {
            modelDaftarPinjamanList = getIntent().getParcelableExtra("ModelDaftarPinjamanList");
            tvTotalPembayaran.setText("Rp " + MainActivity.nf.format(modelDaftarPinjamanList.getNPay()));
            tvSisaPembayaran.setText("Rp " + MainActivity.nf.format(modelDaftarPinjamanList.getSisa()));
            noid1.setText(modelDaftarPinjamanList.getMembermasterno());
            totalPinjaman.setText(modelDaftarPinjamanList.getNLoan() + " Pinjaman");
            nama.setText(modelDaftarPinjamanList.getMembername());
            setUpDetailPinjaman(String.valueOf(modelDaftarPinjamanList.getMemberloanoid()));
        } else {
            Toast.makeText(this, "Gagal Mendapatkan Data", Toast.LENGTH_SHORT).show();
            finish();
        }


        //navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.Pembayaran:
                        Intent intent2 = new Intent(getApplicationContext(), Cari_Nama_AnggotaActivity.class);
                        startActivity(intent2);
                    case R.id.Laporan:
                        Intent intent3 = new Intent(getApplicationContext(), LaporanActivity.class);
                        startActivity(intent3);
                        break;
//                    case R.id.Riwayat:
//                        Intent intent3 = new Intent(getApplicationContext(), Riwayat_pembayaranActivity.class);
//                        startActivity(intent3);
//                        break;
                }
                return false;
            }
        });


        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setUpDetailPinjaman(String DataId) {


        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelDaftarPinjamanListDetail>> call = apiInterface.doGetAPIDaftarPinjamanListDetail("KSP11", DataId);
        call.enqueue(new Callback<List<ModelDaftarPinjamanListDetail>>() {
            @Override
            public void onResponse(Call<List<ModelDaftarPinjamanListDetail>> call, Response<List<ModelDaftarPinjamanListDetail>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    m = response.body();
                    AdapterRecyclerViewRiwayatPembayaran adapterRecyclerViewRiwayatPembayaran = new AdapterRecyclerViewRiwayatPembayaran(Detail_riwayat_pembayaranActivity.this, -1, m, Detail_riwayat_pembayaranActivity.this);
                    recyclerViewDetail.setLayoutManager(new LinearLayoutManager(Detail_riwayat_pembayaranActivity.this));
                    recyclerViewDetail.setHasFixedSize(false);
                    recyclerViewDetail.setAdapter(adapterRecyclerViewRiwayatPembayaran);
//                    explandableView
//                    List<model_detail_riwayat_pembayaran> m = new ArrayList<>();
//                    for (ModelDaftarPinjamanListDetail detailPinjaman : mrespond) {
//                        m.add(new model_detail_riwayat_pembayaran(detailPinjaman.getTrnloanno(), detailPinjaman.getTrnloanopendate(), detailPinjaman.getTrnloanno(), detailPinjaman.getNorek(), String.valueOf(detailPinjaman.getTrnloanamt()), String.valueOf(detailPinjaman.getJangkaWaktu()), String.valueOf(detailPinjaman.getTrnloanamt()), "", detailPinjaman.getKeterangan(), ""));
//
//                    }
//
//                    listDataHeader = new ArrayList<ModelHeader>();
//                    listDataChild = new HashMap<String, List<ModelDaftarPinjamanListDetail>>();
//                    for (int i = 0; i < m.size(); i++) {
//                        List<ModelDaftarPinjamanListDetail> itemsadd = new ArrayList<>();
//                        itemsadd.add(m.get(i));
//                        listDataHeader.add(new ModelHeader(m.get(i).getTrnloanno(), m.get(i).getTrnloanopendate()));
//                        listDataChild.put(m.get(i).getTrnloanno(), itemsadd);
////            listDataChild.put(m.get(i).getTanggal(), itemsadd);
//                    }
//                    expandableListView1.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//                        @Override
//                        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                            return false;
//                        }
//                    });
//                    adapter_detail_riwayat_pembayaran expandableListAdapterPinjaman = new adapter_detail_riwayat_pembayaran(Detail_riwayat_pembayaranActivity.this, listDataHeader, listDataChild);
//                    expandableListView1.setAdapter(expandableListAdapterPinjaman);
                }


            }

            @Override
            public void onFailure(Call<List<ModelDaftarPinjamanListDetail>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });


    }

    @Override
    public void onRowAdapterRecyclerViewRiwayatPembayaranClicked(int position) {
        ModelDaftarPinjamanListDetail ms = m.get(position);
        Bundle bundle = new Bundle();

        bundle.putParcelable("params", ms);
        bundle.putParcelable("params2", modelDaftarPinjamanList);
        PopUpExpendableFragment popUpExpendableFragment = new PopUpExpendableFragment();
        popUpExpendableFragment.setArguments(bundle);
        FragmentManager mFragmentManager = getSupportFragmentManager();
        popUpExpendableFragment.setCancelable(true);
        popUpExpendableFragment.show(mFragmentManager, PopUpExpendableFragment.class.getSimpleName());
    }
}
