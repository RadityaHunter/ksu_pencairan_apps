package waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanList;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.Detail_riwayat_pembayaranActivity;

public class adapter_riwayat_pembayaran extends RecyclerView.Adapter<adapter_riwayat_pembayaran.myViewHolder> {
  static final String EXTRAS_DATA = "EXTRAS_data";
  private Context context;
  private List<ModelDaftarPinjamanList> listMenu;
  private Intent intent;

  public adapter_riwayat_pembayaran(Context context, List<ModelDaftarPinjamanList> listMenu) {
    this.context = context;
    this.listMenu = listMenu;
  }

  @NonNull
  @Override
  public adapter_riwayat_pembayaran.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.desain_adapter_riwayat_pembayaran, viewGroup, false);
    return new adapter_riwayat_pembayaran.myViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull adapter_riwayat_pembayaran.myViewHolder myViewHolder, final int i) {
    myViewHolder.nama.setText(listMenu.get(i).getMembername());
    myViewHolder.noid.setText(String.valueOf(listMenu.get(i).getMembermasterno()));
    myViewHolder.jumlahuang.setText(MainActivity.nf.format(listMenu.get(i).getNLoanNom()));
    myViewHolder.jumlahpinjaman.setText(listMenu.get(i).getNLoan() + " Pinjaman");

//        Glide.with(context)
//                .load(listMenu.get(i).getImageMenu7()).into(myViewHolder.MenuImages);

    myViewHolder.ListBarang.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ModelDaftarPinjamanList modelDaftarPinjamanList = listMenu.get(i);
        Intent intent = new Intent(context, Detail_riwayat_pembayaranActivity.class);
        intent.putExtra("ModelDaftarPinjamanList", modelDaftarPinjamanList);
        context.startActivity(intent);
      }
    });

  }

  @Override
  public int getItemCount() {
    return listMenu.size();
  }

  class myViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.RLitemsMenu)
    LinearLayout ListBarang;
    @BindView(R.id.nama1)
    TextView nama;
    @BindView(R.id.noid1)
    TextView noid;
    @BindView(R.id.jumlahuang1)
    TextView jumlahuang;
    @BindView(R.id.tvjumlahpinjaman)
    TextView jumlahpinjaman;

    public myViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}