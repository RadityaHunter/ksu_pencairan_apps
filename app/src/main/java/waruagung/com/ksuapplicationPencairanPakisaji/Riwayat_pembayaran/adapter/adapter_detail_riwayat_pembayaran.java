package waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelRiwayatDetailCustom;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.model.ModelHeader;

public class adapter_detail_riwayat_pembayaran extends BaseExpandableListAdapter {
    private static final String TAG = adapter_detail_riwayat_pembayaran.class.getSimpleName();
    private List<ModelHeader> expandableListTitle;
    private HashMap<String, List<ModelRiwayatDetailCustom>> expandableListDetail;
    private Context context;

    public adapter_detail_riwayat_pembayaran(Context context, List<ModelHeader> title, HashMap<String, List<ModelRiwayatDetailCustom>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = title;
        this.expandableListDetail = expandableListDetail;
    }

    @Override

    public int getGroupCount() {
        return expandableListTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition).getTitle()).size();
    }

    @Override
    public ModelHeader getGroup(int groupPosition) {
        return expandableListTitle.get(groupPosition);
    }

    @Override
    public ModelRiwayatDetailCustom getChild(int groupPosition, int childPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition).getTitle())
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ModelHeader listTitle = getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_detail_hider_desain_daftar_pinjaman_new, null);
        }
        TextView listTitleTextView = convertView
                .findViewById(R.id.textView);
//        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle.getTitle());
        ImageView listtanggal = convertView
                .findViewById(R.id.imageView);
//        listtanggal.setTypeface(null, Typeface.BOLD);http://slmb.wais01.com/
//
          listtanggal.setImageResource(listTitle.getDate());

//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        if (isExpanded) {
            listTitleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.panahbawah, 0);
        } else {
            listTitleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.panahatas, 0);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ModelRiwayatDetailCustom jsonInString = getChild(groupPosition, childPosition);


        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.new_desain_expandable_detail_riwayatpembayaran, null);
        }
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(jsonInString.getTanggalPembayaran());
            String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
            TextView trnloanno = convertView.findViewById(R.id.tvKodePinjaman);
            trnloanno.setText(formattedDate );
        } catch (Exception e) {
            Log.d(TAG, "getChildView: Error"+e.getMessage());
        }

        TextView JangkaWaktu = convertView.findViewById(R.id.tvAngsuran);
        JangkaWaktu.setText(String.valueOf(jsonInString.getJangkaWatu())+" Bulan");
        TextView tvJumlahPembayaran = convertView.findViewById(R.id.tvJumlahDibayar);
        tvJumlahPembayaran.setText("Rp " + MainActivity.nf.format(Integer.parseInt(String.valueOf(jsonInString.getJumlahPembayaran()))));
        TextView tvjumlahpinjaman = convertView.findViewById(R.id.tvjumlahpinjaman);
        tvjumlahpinjaman.setText("Rp " + MainActivity.nf.format(Integer.parseInt(String.valueOf(jsonInString.getSisaTagihan()))));
        TextView tvRekening = convertView.findViewById(R.id.tvRekening);
        tvRekening.setText(jsonInString.getJenisPencairan());
        TextView tvKeterangan = convertView.findViewById(R.id.tvKeterangan);
        tvKeterangan.setText(jsonInString.getKeterangan());
        TextView tvgambar = convertView.findViewById(R.id.tvgambar);
        tvgambar.setText(jsonInString.getBuktiPembayaran());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
