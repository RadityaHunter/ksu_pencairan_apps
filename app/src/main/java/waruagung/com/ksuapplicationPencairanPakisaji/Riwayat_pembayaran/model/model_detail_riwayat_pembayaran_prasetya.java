package waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.model;

public class model_detail_riwayat_pembayaran_prasetya {
    /**
     * nameToko : Toko
     * takeOrder : 09.30-10.30
     * LastOrder : 1000000
     * Piutang : 0
     * Piutang Terlama : 0
     * SisaLimitKredit : 0
     * Status : belum jalan
     */
    private String kodenama;
    private String tanggal;
    private String kodepinjam;
    private String regkening;
    private String jumlahpembayaran;
    private String angsuranke;
    private String jumlahpinjaman;
    private String jenispencairan;
    private String isiketerangan;
    private String namagambar;


    public model_detail_riwayat_pembayaran_prasetya(String kodenama, String tanggal, String kodepinjam, String regkening, String jumlahpembayaran, String angsuranke, String jumlahpinjaman, String jenispencairan, String isiketerangan, String namagambar
    ) {
        this.kodenama = kodenama;
        this.tanggal = tanggal;
        this.kodepinjam = kodepinjam;
        this.regkening = regkening;
        this.jumlahpembayaran = jumlahpembayaran;
        this.angsuranke = angsuranke;
        this.jumlahpinjaman = jumlahpinjaman;
        this.jenispencairan = jenispencairan;
        this.namagambar = namagambar;
        this.isiketerangan = isiketerangan;

    }
    public String getKodenama() { return kodenama; }

    public void setKodenama(String kodenama) { this.kodenama = kodenama; }


    public String getTanggal() { return tanggal; }

    public void setTanggal(String tanggal) { this.tanggal = tanggal; }



    public String getKodepinjam() { return kodepinjam; }

    public void setKodepinjam(String kodepinjam) { this.kodepinjam = kodepinjam; }



    public String getJumlahpembayaran() { return jumlahpembayaran; }

    public void setJumlahpembayaran(String jumlahpembayaran) { this.jumlahpembayaran = jumlahpembayaran; }



    public String getAngsuranke() {
        return angsuranke;
    }

    public void setAngsuranke(String sisapembayaran) { this.angsuranke = angsuranke; }



    public String getJumlahpinjaman() {
        return jumlahpinjaman;
    }

    public void setJumlahpinjaman(String jumlahpinjaman) { this.jumlahpinjaman = jumlahpinjaman; }




    public String getNamagambar() {
        return namagambar;
    }

    public void setNamagambar(String namagambar) { this.namagambar = namagambar; }




    public String getJenispencairan() {
        return jenispencairan;
    }

    public void setJenispencairan(String jenispencairan) { this.jenispencairan = jenispencairan; }




    public String getRegkening() {
        return regkening;
    }

    public void setRegkening(String regkening) { this.regkening = regkening; }


}
