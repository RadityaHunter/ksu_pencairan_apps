package waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.model;

import android.os.Parcel;
import android.os.Parcelable;

public class model_riwayat_pembayaran implements Parcelable {
    public static final Parcelable.Creator<model_riwayat_pembayaran> CREATOR = new Parcelable.Creator<model_riwayat_pembayaran>() {
        @Override
        public model_riwayat_pembayaran createFromParcel(Parcel source) {
            return new model_riwayat_pembayaran(source);
        }

        @Override
        public model_riwayat_pembayaran[] newArray(int size) {
            return new model_riwayat_pembayaran[size];
        }
    };

    private String nama1, noid1, jumlahuang1,jumlahpinjaman;
    private int kodeMenu;


    public model_riwayat_pembayaran(String nama1, String noid1, int kodeMenu, String jumlahuang1, String jumlahpinjaman) {
        this.nama1 = nama1;
        this.noid1 = noid1;
        this.jumlahuang1 = jumlahuang1;
        this.jumlahpinjaman = jumlahpinjaman;
        this.kodeMenu = kodeMenu;

    }

    protected model_riwayat_pembayaran(Parcel in) {
        this.nama1 = in.readString();
        this.noid1 = in.readString();
        this.jumlahuang1 = in.readString();
        this.kodeMenu = in.readInt();
        this.jumlahpinjaman = in.readString();
    }

    public String getNama1() {
        return nama1;
    }

    public void setNama1(String nama1) {
        this.nama1 = nama1;
    }


    public String getNoid1() { return noid1; }

    public void setNoid1(String noid1) {
        this.noid1 = noid1;
    }


    public String getJumlahuang1() {
        return jumlahuang1;
    }

    public void setJumlahuang1(String jumlahuang1) {
        this.jumlahuang1 = jumlahuang1;
    }


    public String getJumlahpinjaman() {
        return jumlahpinjaman;
    }

    public void setJumlahpinjaman(String jumlahagunan1) {
        this.jumlahpinjaman = jumlahpinjaman;
    }

    public int getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(int kodeMenu) {
        this.kodeMenu = kodeMenu;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nama1);
        dest.writeString(this.noid1);
        dest.writeInt(this.kodeMenu);
        dest.writeString(this.jumlahuang1);
        dest.writeString(this.jumlahpinjaman);

    }
}
