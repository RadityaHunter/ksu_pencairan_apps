package waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import waruagung.com.ksuapplicationPencairanPakisaji.Cari_Nama_AnggotaActivity;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.LaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.adapter.adapter_detail_riwayat_pembayaran_prasetya;
import waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.model.ModelHeader;
import waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.model.model_detail_riwayat_pembayaran_prasetya;

public class Detail_riwayat_pembayaran_prasetyaActivity extends AppCompatActivity {
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ExpandableListView)
    ExpandableListView expandableListView1;

    private List<ModelHeader> listDataHeader;
    private HashMap<String, List<model_detail_riwayat_pembayaran_prasetya>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_riwayat_pembayaran_prasetya);
        ButterKnife.bind(this);
        List<model_detail_riwayat_pembayaran_prasetya> m = new ArrayList<>();
        m.add(new model_detail_riwayat_pembayaran_prasetya("PB143283", "23 Mei 2019", "PJ656722", "9849273873 a/n Lusi Aprillia", "Rp 2.150.000", "2", "Rp 15.000.000", "Transfer BCA", "-", "image.jpg"));
        m.add(new model_detail_riwayat_pembayaran_prasetya("PB656722", "20 Maret 2019", "PJ5576827", "9849273873 a/n Lusi Aprillia", "Rp 10.750.000", "2", "Rp 15.000.000", "Transfer BCA", "-", "image.jpg"));
        m.add(new model_detail_riwayat_pembayaran_prasetya("PB557687", "30 Maret 2019", "PJ656722", "9849273873 a/n Lusi Aprillia", "Rp 1.587.000", "2", "Rp 15.000.000", "Transfer BCA", "-", "image.jpg"));

        listDataHeader = new ArrayList<ModelHeader>();
        listDataChild = new HashMap<String, List<model_detail_riwayat_pembayaran_prasetya>>();
        for (int i = 0; i < m.size(); i++) {
            List<model_detail_riwayat_pembayaran_prasetya> itemsadd = new ArrayList<>();
            itemsadd.add(m.get(i));
            listDataHeader.add(new ModelHeader(m.get(i).getKodenama(), R.drawable.agunan));
            listDataChild.put(m.get(i).getKodenama(), itemsadd);
//            listDataChild.put(m.get(i).getTanggal(), itemsadd);
        }

        adapter_detail_riwayat_pembayaran_prasetya expandableListAdapterPinjaman = new adapter_detail_riwayat_pembayaran_prasetya(this, listDataHeader, listDataChild);
        expandableListView1.setAdapter(expandableListAdapterPinjaman);

        //navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                                        case R.id.Pembayaran:
                        Intent intent2 = new Intent(getApplicationContext(), Cari_Nama_AnggotaActivity.class);
startActivity(intent2);
                    case R.id.Laporan:
                         Intent intent3 = new Intent(getApplicationContext(), LaporanActivity.class);
startActivity(intent3);
                        break;
//                    case R.id.Riwayat:
//                        Intent intent3 = new Intent(getApplicationContext(), Riwayat_pembayaranActivity.class);
//                        startActivity(intent3);
//                        break;
                }
                return false;
            }
        });


        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
