package waruagung.com.ksuapplicationPencairanPakisaji.pembayaran;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelNamaAnggotaNasabah;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.JanjiBayar.JanjiBayarActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class CheckInActivity extends AppCompatActivity {

    public static final String TAG = CheckInActivity.class.getSimpleName();
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
    private static final long MIN_TIME_BW_UPDATES = 0;
    protected LocationManager locationManager;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    //    @BindView(R.id.lat_now_checkin)
//    TextView latNowCheckin;
//    @BindView(R.id.lgt_now_checkin)
//    TextView lgtNowCheckin;
//    @BindView(R.id.label_check)
//    TextView labelCheck;
//    @BindView(R.id.label_check1)
//    TextView labelCheck1;
//    @BindView(R.id.name_mahasiswa_checkin)
//    TextView nameMahasiswaCheckin;
//    @BindView(R.id.tvLatitude)
//    TextView tvLatitude;
//    @BindView(R.id.tvLongitude)
//    TextView tvLongitude;
//    @BindView(R.id.distance_between)
//    TextView distanceBetween;
//    @BindView(R.id.button)
//    TextView button;
//    @BindView(R.id.btn_checkin_peserta)
//    LinearLayout btnCheckinPeserta;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.btn_tidak)
    Button btnTidak;
    @BindView(R.id.btn_ya)
    Button btnYa;
    private ModelNamaAnggotaNasabah modelNamaAnggotaNasabah;
    private Double GEO_LAT = Double.valueOf("-7.3312211");
    private Double GEO_LNG = Double.valueOf("112.7802326");
    private GoogleMap mMap;
    private String Latitude, Longitude;
    private ProgressDialog progressDialog;
    private LatLng latLng;
    private String LatLNG = "";
    private Location loc;
    private float distanceInMeters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        ButterKnife.bind(this);
        restoreActionBar();


        btnYa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Detail_pembayaranActivity.class);
                intent.putExtra("modelNamaAnggotaNasabah", modelNamaAnggotaNasabah);
                startActivity(intent);
            }
        });
        btnTidak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), JanjiBayarActivity.class);
                startActivity(intent);
            }
        });


        if (getIntent().hasExtra("modelNamaAnggotaNasabah")) {
            modelNamaAnggotaNasabah = getIntent().getParcelableExtra("modelNamaAnggotaNasabah");
        }
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(CheckInActivity.this, Detail_pembayaranActivity.class);
//                intent.putExtra("modelNamaAnggotaNasabah", modelNamaAnggotaNasabah);
//                startActivity(intent);
//
//            }
//        });
//        button.setEnabled(false);

//        requestStoragePermission();
    }

//    @AfterPermissionGranted(123)
//    private void requestStoragePermission() {
//
//        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(CheckInActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(CheckInActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//        } else {
//            button.setText("Tunggu...");
////            GPSTracker finder;
////            double longitude = 0.0, latitude = 0.0;
////            finder = new GPSTracker(this);
////            if (finder.canGetLocation()) {
////                latitude = finder.getLatitude();
////                longitude = finder.getLongitude();
////                Log.e(TAG, "onLocationChanged: " + latitude +","+ longitude);
////                setupJarak(latitude, longitude);
////                Toast.makeText(this, "lat-lng " + latitude + " - " + longitude, Toast.LENGTH_LONG).show();
////            } else {
////                finder.showSettingsAlert();
////            }
//            setupAwal();
//        }
//
//    }

//    private void showSettingsDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(CheckInActivity.this);
//        builder.setTitle("Butuh Akses");
//        builder.setMessage("aplikasi ini membutuhkan akses permission GPS, Internet");
//        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//                openSettings();
//            }
//        });
//        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        builder.show();
//
//    }

    // navigating user to app settings
//    private void openSettings() {
//        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//        Uri uri = Uri.fromParts("package", getPackageName(), null);
//        intent.setData(uri);
//        startActivityForResult(intent, 101);
//    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

//    private void setupAwal() {
//        try {
//
//            Log.d(TAG, "getLocation: Started:");
//            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_DISTANCE_CHANGE_FOR_UPDATES, MIN_TIME_BW_UPDATES, this);
//
//            if (locationManager != null) {
//                loc = locationManager
//                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                if (loc != null) {
//                    Latitude = String.valueOf(loc.getLatitude());
//                    Longitude = String.valueOf(loc.getLongitude());
//                }
//            }
//        } catch (SecurityException e) {
//            Log.d(TAG, "getLocation: Error Massage:" + e.getMessage());
//        }
//
//    }

//    @Override
//    public void onLocationChanged(Location location) {
//        Log.d(TAG, "onLocationChanged: " + location.getLatitude() + location.getLongitude());
//        latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        try {
//            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
//            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 10);
//            Log.d(TAG, "onLocationChanged: Adress " + addresses.get(0).getAddressLine(0));
//            latLng = new LatLng(location.getLatitude(), location.getLongitude());
//            Latitude = Double.toString(location.getLatitude());
//            Longitude = Double.toString(location.getLongitude());
//            LatLNG = Latitude + "," + Longitude;
//            Log.e(TAG, "onLocationChanged: " + Latitude + Longitude);
//            tvLongitude.setText(Longitude);
//            tvLongitude.setVisibility(View.VISIBLE);
//            tvLatitude.setVisibility(View.VISIBLE);
//            tvLatitude.setText(Latitude);
//            Log.d(TAG, "onMapReady: " + LatLNG);
//            setupJarak(location.getLatitude(), location.getLongitude());
//            button.setEnabled(true);
//            button.setText("Yuk Check In");
//
//        } catch (Exception e) {
//            Log.d(TAG, "getLocation: Error Massage:" + e.getMessage());
//            e.printStackTrace();
//            //Crashlytics.logException(e);
//        }
//    }

//    private void setupJarak(Double latitude, Double longitude) {
//        Location loc1 = new Location("");
//        loc1.setLatitude(latitude);
//        loc1.setLongitude(longitude);
//        Location loc2 = new Location("");
//        loc2.setLatitude(GEO_LAT);
//        loc2.setLongitude(GEO_LNG);
//
//        distanceInMeters = loc1.distanceTo(loc2);
//        //char plus_minus = (int) 241;
//
//        Log.e(TAG, "setupJarak: " + distanceInMeters);
//        distanceBetween.setVisibility(View.VISIBLE);
//        float konversi_km = distanceInMeters / 1000;
//        if (distanceInMeters > 50) {
//
//            Log.e("konversi_km: ", String.valueOf(konversi_km));
//
//
//            if (konversi_km > 0.5) {
//                AlertDialog.Builder builder1 = new AlertDialog.Builder(CheckInActivity.this);
//                builder1.setTitle("Konfirmasi");
//                builder1.setMessage("Jarak anda jauh dari target!");
//                builder1.setCancelable(false);
//
//                builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                        finish();
////                                finish();
//                    }
//                });
//                final AlertDialog alert11 = builder1.create();
//                alert11.setOnShowListener(new DialogInterface.OnShowListener() {
//                    @Override
//                    public void onShow(DialogInterface dialogInterface) {
//                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
//                        alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
//                    }
//                });
//                alert11.show();
//            }
//
//            distanceBetween.setText("Jarak Perkiraan  ± " + new DecimalFormat("##.##").format(konversi_km) + " km");
//        } else {
//            distanceBetween.setText("Jarak Perkiraan  ± " + new DecimalFormat("##.##").format(konversi_km) + " km");
//        }
//    }

//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//        progressDialog = new ProgressDialog(CheckInActivity.this);
//
//        progressDialog.setMessage("Mohon Aktifkan GPS Anda. dan Data Aktif"); // Setting Message
//        progressDialog.setTitle("Peringatan"); // Setting Title
//        progressDialog.setProgressStyle(ProgressDialog.BUTTON_NEGATIVE); // Progress Dialog Style Spinner
//        progressDialog.show(); // Display Progress Dialog
//        progressDialog.setCancelable(false);
//        new Thread(new Runnable() {
//            public void run() {
//                try {
//                    Thread.sleep(10000);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                progressDialog.dismiss();
//            }
//        }).start();
//    }

//    @Override
//    public void onProviderEnabled(String provider) {
//        progressDialog.dismiss();
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//        progressDialog = new ProgressDialog(CheckInActivity.this);
//
//        progressDialog.setMessage("Mohon Aktifkan GPS Anda. dan Data Aktif"); // Setting Message
//        progressDialog.setTitle("Peringatan"); // Setting Title
//        progressDialog.setProgressStyle(ProgressDialog.BUTTON_NEGATIVE); // Progress Dialog Style Spinner
//        progressDialog.show(); // Display Progress Dialog
//        progressDialog.setCancelable(false);
//        new Thread(new Runnable() {
//            public void run() {
//                try {
//                    Thread.sleep(10000);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                progressDialog.dismiss();
//            }
//        }).start();
//    }
}
