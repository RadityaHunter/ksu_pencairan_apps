package waruagung.com.ksuapplicationPencairanPakisaji.pembayaran;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.Adapter.AdapterNoPinjamanPembayaran;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelFormPinjaman;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class No_pimjamanActivity extends AppCompatActivity implements AdapterNoPinjamanPembayaran.AdapterNoPinjamanPembayaranCallback {
    public static final String TAG = No_pimjamanActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.Rvnomorpinjaman)
    RecyclerView Rvnomorpinjaman;
    private APIInterface apiInterface;
    private List<ModelFormPinjaman> listCategory = new ArrayList<>();


    //    @BindView(R.id.listview)
//    ListView listview;
//
//
//
//    //Data-Data yang Akan dimasukan Pada ListView
//    String[] names = {"PJ27364325", "PJ82242652"};
//
//    //Data-Data yang Akan dimasukan Pada ListView
//    int[] images = {R.drawable.pinjaman, R.drawable.pinjaman};
//
//    //ArrayList digunakan Untuk menampung Data nama
//    private ArrayList<String> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_pimjaman);
        ButterKnife.bind(this);
        restoreActionBar();
        if (getIntent().hasExtra("DATA_ID")) {
            rvnomorpinjaman(getIntent().getStringExtra("DATA_ID"));
        } else {
            finish();
        }


//        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        data = new ArrayList<>();
//        getData();
//
//
//        adapter_nomor_pinjaman adapter_nomor_pinjaman = new adapter_nomor_pinjaman(this, names, images);
//        listview.setAdapter(adapter_nomor_pinjaman);
//
//        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//                Intent returnIntent = new Intent();
//                returnIntent.putExtra("_DATA", data.get(position));
//                setResult(RESULT_OK, returnIntent);
//                finish();
////                if (position == 0) {
////                    Intent myIntent = new Intent(view.getContext(), Detail_tanah_Activity.class);
////                    startActivityForResult(myIntent, 0);
////                }
////
////                if (position == 1) {
////                    Intent myIntent = new Intent(view.getContext(), Detail_tanah_Activity.class);
////                    startActivityForResult(myIntent, 0);
////                }
////
////                if (position == 2) {
////                    Intent myIntent = new Intent(view.getContext(), Tanah_Activity.class);
////                    startActivityForResult(myIntent, 0);
////                }
////
////                if (position == 3) {
////                    Intent myIntent = new Intent(view.getContext(), Tanah_dan_bangunanActivity.class);
////                    startActivityForResult(myIntent, 0);
////                }
////
////                if (position == 4) {
////                    Intent myIntent = new Intent(view.getContext(), ListItemActivity1.class);
////                    startActivityForResult(myIntent, 0);
////                }
////
////                if (position == 5) {
////                    Intent myIntent = new Intent(view.getContext(), ListItemActivity2.class);
////                    startActivityForResult(myIntent, 0);
////                }
////
////                if (position == 6) {
////                    Intent myIntent = new Intent(view.getContext(), ListItemActivity1.class);
////                    startActivityForResult(myIntent, 0);
////                }
////
////                if (position == 7) {
////                    Intent myIntent = new Intent(view.getContext(), ListItemActivity2.class);
////                    startActivityForResult(myIntent, 0);
////                }
//            }
//        });
//    }
//
//    private void getData() {
//        //Memasukan Semua Data nama kedalam ArrayList
//        Collections.addAll(data, names);
//
    }

    private void rvnomorpinjaman(String idAnggota) {
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelFormPinjaman>> call = apiInterface.doGetAPIForm_Pembayaran(Http.getsCmp(), idAnggota);
        call.enqueue(new Callback<List<ModelFormPinjaman>>() {
            @Override
            public void onResponse(Call<List<ModelFormPinjaman>> call, Response<List<ModelFormPinjaman>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Rvnomorpinjaman.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false));
                    Rvnomorpinjaman.setHasFixedSize(true);
                    Rvnomorpinjaman.setNestedScrollingEnabled(false);
                    listCategory = response.body();
                    AdapterNoPinjamanPembayaran adapter_nomorpinjaman = new AdapterNoPinjamanPembayaran(getApplicationContext(), -1, listCategory, No_pimjamanActivity.this);
                    Rvnomorpinjaman.setAdapter(adapter_nomorpinjaman);
                } else {
                    Toast.makeText(No_pimjamanActivity.this, "Error Code " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ModelFormPinjaman>> call, Throwable t) {
                Log.e(TAG, "onFailure: Error Code " + t.getMessage());
                call.cancel();
            }
        });

//        listCategory.add(new model_nomorpinjaman("PJ27364325","Rp 50.000.000",R.drawable.pinjaman,1, "12 September 2019"));
//        listCategory.add(new model_nomorpinjaman("PJ82242652", "Rp 10.000.000", R.drawable.pinjaman,2, "30 November 2019"));

    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onRowAdapterNoPinjamanPembayaranClicked(int position) {
        ModelFormPinjaman m = listCategory.get(position);
        Intent intent = new Intent();
        intent.putExtra("ModelFormPinjaman", m);
        setResult(RESULT_OK, intent);
        finish();

    }
}
