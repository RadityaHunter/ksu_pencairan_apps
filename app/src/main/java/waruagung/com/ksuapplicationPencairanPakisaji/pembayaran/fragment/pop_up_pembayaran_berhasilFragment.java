package waruagung.com.ksuapplicationPencairanPakisaji.pembayaran.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelPopUpPembayaran;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.Riwayat_pembayaran.Riwayat_pembayaranActivity;


public class pop_up_pembayaran_berhasilFragment extends DialogFragment implements View.OnClickListener {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvIdNasabah)
    TextView tvIdNasabah;
    @BindView(R.id.tvTanggal)
    TextView tvTanggal;
    @BindView(R.id.tvJumlahPembayaran)
    TextView tvJumlahPembayaran;
    @BindView(R.id.tvNotedPayment)
    TextView tvNotedPayment;
    private View view;
    private Intent intent;
    private APIInterface apiInterface;

    public pop_up_pembayaran_berhasilFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        view = inflater.inflate(R.layout.fragment_pop_up_pembayaran_berhasil, container, false);
        ButterKnife.bind(this, view);

        view.findViewById(R.id.lihatdaftarpembayaran).setOnClickListener(this);
        view.findViewById(R.id.btnclose).setOnClickListener(this);
        String strtext = getArguments().getString("paymentno");
        setupPopup(strtext);
        return view;


    }

    private void setupPopup(String strtext) {
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelPopUpPembayaran>> call = apiInterface.PopUp_Pembayaran("KSP11", strtext);
        call.enqueue(new Callback<List<ModelPopUpPembayaran>>() {
            @Override
            public void onResponse(Call<List<ModelPopUpPembayaran>> call, Response<List<ModelPopUpPembayaran>> response) {
                if (response.isSuccessful()) {
                    ModelPopUpPembayaran m = response.body().get(0);
                    tvName.setText(m.getMembername());
                    try {
                        Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(m.getTrnpaymentdate());
                        String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
                        tvTanggal.setText(formattedDate);

                    } catch (Exception e) {
                        Log.d("Konfert Date", "getChildView: Error"+e.getMessage());
                    }

                    tvIdNasabah.setText(m.getMembermasterno());
                    tvNotedPayment.setText(m.getTrnloandtlnote());
                    tvJumlahPembayaran.setText("Rp " + MainActivity.nf.format(m.getTrnloanamt()));
                } else {
                    Toast.makeText(getContext(), "Error Code " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ModelPopUpPembayaran>> call, Throwable t) {
                Toast.makeText(getContext(), "Error Code " + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lihatdaftarpembayaran:
                intent = new Intent(view.getContext(), Riwayat_pembayaranActivity.class);
                startActivity(intent);
                break;
//            case R.id.promo1:
//                intent = new Intent(view.getContext(), MarketingActivity.class);
//                startActivity(intent);
//                break;
//            case R.id.bantuan:
//                intent = new Intent(v.getContext(), ToursTravelActivity.class);
//                startActivity(intent);
//                break;
            case R.id.btnclose:
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
                break;
        }
    }
}
