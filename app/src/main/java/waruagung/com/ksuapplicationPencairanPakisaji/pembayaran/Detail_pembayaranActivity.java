package waruagung.com.ksuapplicationPencairanPakisaji.pembayaran;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ipaulpro.afilechooser.utils.FileUtils;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelNamaAnggotaNasabah;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;

import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelFormPinjaman;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.agunan.Model.ModelAttachments;
import waruagung.com.ksuapplicationPencairanPakisaji.agunan.adapter.AdapterAttachment;
import waruagung.com.ksuapplicationPencairanPakisaji.pembayaran.fragment.pop_up_pembayaran_berhasilFragment;

public class Detail_pembayaranActivity extends AppCompatActivity implements AdapterAttachment.AdapterAttachmentCallback {
    static final int REQUEST_IMAGE_CAPTURE = 321;
    //    @BindView(R.id.toolbar)
//    Toolbar toolbar;
    static final int REQUEST_FILE_MANAGER = 223;
    private static final String TAG = "";
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 12345;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    @BindView(R.id.ygharusdibayar)
    TextView ygharusdibayar;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.relnomorpinjaman)
    RelativeLayout relnomorpinjaman;
    @BindView(R.id.reltipebayar)
    RelativeLayout reltipebayar;
    @BindView(R.id.NomorPinjaman)
    TextView nomorpinjaman;
    @BindView(R.id.tipebayar)
    TextView tipebayar;
    @BindView(R.id.sumbitdpb)
    TextView sumbit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    //untukuplod gambar dari camera atau galeri
    TextView lingkaran1;
    ImageView uploadfoto2;
    @BindView(R.id.lv_listViewItem)
    RecyclerView lv_listViewItem;
    @BindView(R.id.jumlahbayar)
    EditText jumlahbayar;
    @BindView(R.id.edtSimpananPokok)
    EditText edtSimpananPokok;
    @BindView(R.id.titip)
    TextView titip;
    @BindView(R.id.liniertitip)
    LinearLayout liniertitip;
    //    @BindView(R.id.spinner_pembayaran)
//    Spinner spinnerPembayaran;
    @BindView(R.id.idangotadetailbayar)
    TextView idangotadetailbayar;
    @BindView(R.id.jml_pinjam)
    TextView jmlPinjam;
    @BindView(R.id.tvAngsuran)
    TextView angsuranke;
    @BindView(R.id.tanggalbayar)
    TextView tanggalbayar;
    @BindView(R.id.view6)
    View view6;
    @BindView(R.id.view7)
    View view7;
    @BindView(R.id.keterangandetailpembayaran)
    EditText keterangandetailpembayaran;
    @BindView(R.id.uploadfoto)
    TextView uploadfoto;
    @BindView(R.id.uploadfotoimage)
    ImageView uploadfotoimage;
    @BindView(R.id.spinner_pembayaran)
    TextView spinnerPembayaran;
    @BindView(R.id.uploadfoto4)
    TextView uploadfoto4;
    @BindView(R.id.uploadfotoimage4)
    ImageView uploadfotoimage4;
    //    @BindView(R.id.tanggalbayar)
//    TextView tanggalbayar;
    @BindView(R.id.uploadfoto3)
    TextView uploadfoto3;
    @BindView(R.id.uploadfotoimage3)
    ImageView uploadfotoimage3;
    //
//    @BindView(R.id.Tvpokok)
//    TextView Tvpokok;
//    @BindView(R.id.lnFlat)
//    LinearLayout lnFlat;
//    @BindView(R.id.edtPokok)
//    EditText edtPokok;
//    @BindView(R.id.lnSliding)
//    LinearLayout lnSliding;
//    @BindView(R.id.jasa1)
//    TextView jasa1;
//    @BindView(R.id.linierjasa1)
//    LinearLayout linierjasa1;
//    @BindView(R.id.switchsanksi)
//    Switch switchsanksi;
//    @BindView(R.id.switchtitip)
//    Switch switchtitip;
//
    @BindView(R.id.switchtitip)
    Switch switchtitip;
    @BindView(R.id.switchsanksi)
    Switch switchsanksi;
    @BindView(R.id.edtBayarSanksi)
    EditText edtBayarSanksi;
    @BindView(R.id.Linier_pokok)
    LinearLayout LinierPokok;
    @BindView(R.id.linierjasa1)
    LinearLayout linierjasa1;
    @BindView(R.id.edtTitipanSaldo)
    TextView edtTitipanSaldo;
    @BindView(R.id.linierpokoksanksi)
    LinearLayout linierpokoksanksi;
    @BindView(R.id.Tvpokok)
    TextView Tvpokok;
    @BindView(R.id.tipe_byr)
    TextView tipe_byr;
    @BindView(R.id.lnFlat)
    LinearLayout lnFlat;
    @BindView(R.id.edtPokok)
    EditText edtPokok;
    @BindView(R.id.lnSliding)
    LinearLayout lnSliding;
    @BindView(R.id.jasa1)
    TextView jasa1;
    @BindView(R.id.tvTagihanSanksi)
    TextView tvTagihanSanksi;
    @BindView(R.id.tvSaldoTitipan)
    TextView tvSaldoTitipan;
    @BindView(R.id.lnPembayaranDetail)
    LinearLayout lnPembayaranDetail;
    @BindView(R.id.clear_button)
    TextView clearButton;
    @BindView(R.id.signature_pad_description)
    TextView signaturePadDescription;
    @BindView(R.id.signature_pad)
    SignaturePad signaturePad;
    @BindView(R.id.signature_pad_container)
    RelativeLayout signaturePadContainer;
    @BindView(R.id.clear_button2)
    TextView clearButton2;
    @BindView(R.id.signature_pad_description2)
    TextView signaturePadDescription2;
    @BindView(R.id.signature_pad2)
    SignaturePad signaturePad2;
    @BindView(R.id.signature_pad_container2)
    RelativeLayout signaturePadContainer2;
    @BindView(R.id.save_button)
    Button saveButton;
    @BindView(R.id.buttons_container)
    LinearLayout buttonsContainer;
    @BindView(R.id.save_button2)
    Button saveButton2;
    @BindView(R.id.buttons_container2)
    LinearLayout buttonsContainer2;
    private String base64_image_1 = "", base64_image_2 = "", type_upload = "";
    //Image request code
    private int PICK_IMAGE_REQUEST = 10006;
    //Bitmap to get image from gallery
    private Bitmap bitmap;
    private ArrayList<Uri> arrayList = new ArrayList<>();
    //Uri to store the image uri
    private Uri filePath;
    private String current = "";
    //    //datepacker
//    private TextView mDisplayDate;
//    private DatePickerDialog.OnDateSetListener mDateSatlistener;


//spinner

    //    private String[] Item = {"Pilih Nama Anggota","Lusi Aprillia", "Prasetya", "Andri", "Olis Dare"};
    private Integer jumlahpembayaran = 0, yangharusdibayar = 0;
    private Double titipan = 0.0;
    private String spinnerPembayaranvalue;
    //untuk toolbar
    private String mTitle = "REVEIW PEMBAYARAN";
    private SessionManager sessionManager;
    private List<String> listDataHeader;
    private HashMap<String, List> listDataChild;
    private List<ModelAttachments> modelAttachmentsList = new ArrayList<>();
    private AdapterAttachment adapterBuyNow;
    private APIInterface apiInterface;
    private APIClient ApiClient;
    private GridLayoutManager gridLayoutManager;
    private Intent intent;
    //ttd
    private byte[] byteArray;
    private String base64_signature;
    private SignaturePad mSignaturePad;
    private TextView mClearButton;
    private Button mSaveButton;
    private SignaturePad mSignaturePad2;
    private TextView mClearButton2;
    private Button mSaveButton2;

    private String dataTipePinjaman = "";
    private ModelFormPinjaman modelFormPinjamanData;
    private String strIdNasabah = "";
    private String beforeText = "";
    private int hitungBiaya = 0, biayajasaEdit = 0, simpananPokok = 0, totalBiaya = 0, dibayar = 0, dititipkan = 0, biayaPokok = 0, biayajasa = 0, bayarsanksi = 0, bayartitipan = 0;
    private ModelNamaAnggotaNasabah modelNamaAnggotaNasabah;

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pembayaran);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        //hitungan
        //set the switch to ON
        switchtitip.setChecked(false);
        LinierPokok.setVisibility(View.GONE);
        lnPembayaranDetail.setVisibility(View.GONE);
        if (getIntent().hasExtra("modelNamaAnggotaNasabah")) {
            modelNamaAnggotaNasabah = getIntent().getParcelableExtra("modelNamaAnggotaNasabah");
            String strEditTextId = String.valueOf(modelNamaAnggotaNasabah.getMemberloanoid() + "");
            String strEditTextName = String.valueOf(modelNamaAnggotaNasabah.getMembername());
            strIdNasabah = String.valueOf(modelNamaAnggotaNasabah.getMemberloanoid());
            idangotadetailbayar.setText(strEditTextId);
            spinnerPembayaran.setText(strEditTextName);
            setAwal();

        }
//        spinnerPembayaran.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), Cari_Nama_AnggotaActivity.class);
//                startActivityForResult(intent, 3);
//            }
//        });
//        CompoundButton.OnCheckedChangeListener()
        switchtitip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    edtBayarSanksi.setText("Status: ON");
                    LinierPokok.setVisibility(View.VISIBLE);
                    edtTitipanSaldo.setText(MainActivity.nf.format(modelFormPinjamanData.getSaldotitipan()));

                } else {
//                    edtBayarSanksi.setText("Status: OFF");
                    LinierPokok.setVisibility(View.GONE);
                    edtTitipanSaldo.setText("");
                    edtTitipanSaldo.setHint("Rp 0");

                }
                setupHitung();

            }
        });
        //set the switch to ON
        switchsanksi.setChecked(false);
        linierpokoksanksi.setVisibility(View.GONE);

//        CompoundButton.OnCheckedChangeListener()
        switchsanksi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    edtBayarSanksi.setText("Status: ON");
                    linierpokoksanksi.setVisibility(View.VISIBLE);
                    edtBayarSanksi.setText(MainActivity.nf.format(modelFormPinjamanData.getTtgSanksi()));

                } else {
//                    edtBayarSanksi.setText("Status: OFF");
                    edtBayarSanksi.setText("");
                    linierpokoksanksi.setVisibility(View.GONE);

                }
                setupHitung();


            }
        });

        //tanda tangan 1
        mSignaturePad = findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
//                Toast.makeText(Detail_pinjamanActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        mClearButton = findViewById(R.id.clear_button);
        mSaveButton = findViewById(R.id.save_button);

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bitmap bitmap = mSignaturePad.getSignatureBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
////                keterangandetailpembayaran.setText(encoded);
                Log.e("emcode", "onClick: " + encoded);
                try {
                    base64_signature = encoded;
                    Log.e("Base64 Encode ", "onClick: " + encoded);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        //tanda tangan 2
        mSignaturePad2 = findViewById(R.id.signature_pad2);
        mSignaturePad2.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
//                Toast.makeText(Detail_pinjamanActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                mSaveButton2.setEnabled(true);
                mClearButton2.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton2.setEnabled(false);
                mClearButton2.setEnabled(false);
            }
        });

        mClearButton2 = findViewById(R.id.clear_button2);
        mSaveButton2 = findViewById(R.id.save_button2);

        mClearButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad2.clear();
            }
        });

        mSaveButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bitmap bitmap = mSignaturePad.getSignatureBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
////                keterangandetailpembayaran.setText(encoded);
                Log.e("emcode", "onClick: " + encoded);
                try {
                    base64_signature = encoded;
                    Log.e("Base64 Encode ", "onClick: " + encoded);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        //hitungan
//        ygharusdibayar.setText("100000");
//        jumlahbayar.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (count > 0) {
//                    if (!s.toString().equals(current) && !s.equals("") | jumlahpembayaran != 0 | s.length() > 0) {
//                        //setuptotal();
//                        int formatted = 0; // or any appllication default value
//                        try {
//                            Log.e(TAG, "onTextChanged: " + s.toString());
//                            jumlahbayar.removeTextChangedListener(this);
//                            String cleanString = s.toString().replaceAll("[$,.Rp ]", "");
//                            //double parsed = Double.parseDouble(cleanString);
//                            formatted = Integer.parseInt(cleanString.replace(".", ""));
//                            Log.e(TAG, "onTextChanged: " + formatted);
//                            current = String.valueOf(formatted);
//                            jumlahbayar.setText(MainActivity.nf.format(formatted));
//                            setupPembayaran(current);
//
//
//                            jumlahbayar.setSelection(jumlahbayar.length());
//
//                        } catch (NumberFormatException nfe) {
//                            nfe.printStackTrace();
//                        }
//
//                        jumlahbayar.addTextChangedListener(this);
////                } else {
////                    jumlahbayar.setText("0");
//                    }
//
//                } else {
//                    jumlahbayar.setHint("0");
//                    titip.setText("0");
//                    titipan = 0.0;
////                            jumlahbayar.setError("Min. Rp. 10.000");
//                }
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//
//            }
//        });
//        //spinner
//        spinnerPembayaran = (Spinner) findViewById(R.id.spinner_pembayaran);
//// inisialiasi Array Adapter dengan memasukkan string array di atas
//        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, Item);
//
//        // mengeset Array Adapter tersebut ke Spinner
//        spinnerPembayaran.setAdapter(adapter);
//        spinnerPembayaran.setSelection(0);
//        // mengeset listener untuk mengetahui saat item dipilih
//        spinnerPembayaran.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
////                Toast.makeText(Detail_pembayaranActivity.this, "Selected "+ adapter.getItem(i), Toast.LENGTH_SHORT).show();
//                ((TextView) view).setTextSize(13);
//                spinnerPembayaranvalue = Item[i];
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//        //datepacker
        Calendar calendar = Calendar.getInstance();
        String currentDate = new SimpleDateFormat("MM/dd/yyyy").format(calendar.getTime());
        TextView textViewDate = findViewById(R.id.tanggalbayar);
        textViewDate.setText(currentDate);

        //navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
        edtSimpananPokok.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s.toString().trim())) {
                    setupHitung();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtPokok.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s.toString().trim())) {
                    setupHitung();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        relnomorpinjaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (idangotadetailbayar.getText() == "" || TextUtils.isEmpty(idangotadetailbayar.getText().toString())) {
                    Toast.makeText(Detail_pembayaranActivity.this, "Pilih Nasabah dalulu", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getApplicationContext(), No_pimjamanActivity.class);
//                    intent.putExtra("DATA_ID", idangotadetailbayar.getText());
                    intent.putExtra("DATA_ID", strIdNasabah);
                    startActivityForResult(intent, 1);
                }

            }
        });
        reltipebayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Tipe_bayarActivity.class);
                startActivityForResult(intent, 2);
            }
        });


        lv_listViewItem.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));
        lv_listViewItem.setHasFixedSize(true);
        lv_listViewItem.setVisibility(View.GONE);
        restoreActionBar();
        //untukuplod gambar dari camera atau galeri
//        requestStoragePermission();
        lingkaran1 = findViewById(R.id.uploadfoto);
        uploadfoto2 = findViewById(R.id.uploadfotoimage);
        onclickBtnUpload(lingkaran1, "image_1");
        onclickBtnUpload(uploadfoto4, "image_2");
        onclickBtnUpload(uploadfoto3, "image_3");
        addTextWathcer(edtPokok);
        addTextWathcer(edtBayarSanksi);
        liniertitip.setVisibility(View.GONE);
        jumlahbayar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (!TextUtils.isEmpty(s.toString().trim())) {
//                    Log.e(TAG, "onTextChanged: jumlahbayar " + s.toString());
//                    setupPembayaran(s.toString().trim());
//                }
                if (count > 0) {
                    if (!s.toString().equals(current) && !s.equals("") | jumlahpembayaran != 0 | s.length() > 0) {
                        //setuptotal();
                        int formatted = 0; // or any appllication default value
                        try {
                            Log.e(TAG, "onTextChanged: " + s.toString());
                            jumlahbayar.removeTextChangedListener(this);
                            String cleanString = s.toString().replaceAll("[$,.Rp ]", "");
                            //double parsed = Double.parseDouble(cleanString);
                            formatted = Integer.parseInt(cleanString.replace(".", ""));
                            Log.e(TAG, "onTextChanged: " + formatted);
                            current = String.valueOf(formatted);
                            jumlahbayar.setText(MainActivity.nf.format(formatted));
                            setupPembayaran(current);


                            jumlahbayar.setSelection(jumlahbayar.length());

                        } catch (NumberFormatException nfe) {
                            nfe.printStackTrace();
                        }

                        jumlahbayar.addTextChangedListener(this);
                    }

                } else {
                    jumlahbayar.setHint("0");
                    titip.setText("0");
                    titipan = 0.0;
//                            jumlahbayar.setError("Min. Rp. 10.000");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                // setupPembayaran(s.toString());
            }
        });
        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = mSignaturePad.getSignatureBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
////                keterangandetailpembayaran.setText(encoded);
                try {
                    String encoded = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                    Log.e("emcode", "onClick: " + encoded);

                    base64_signature = encoded;
                    Log.e("Base64 Encode ", "onClick: " + encoded);
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                Log.e(TAG, "SendDataToCloud: base64_image_1 " + base64_image_1);
//                Log.e(TAG, "SendDataToCloud: base64_signature " + base64_signature);
//                Log.e(TAG, "SendDataToCloud: base64_image_2 " + base64_image_2);

                if (spinnerPembayaran.getText().toString().isEmpty() && idangotadetailbayar.getText().toString().isEmpty() &&
                        nomorpinjaman.getText().toString().isEmpty() && jmlPinjam.getText().toString().isEmpty() &&
                        angsuranke.getText().toString().isEmpty() && tanggalbayar.getText().toString().isEmpty() &&
                        tipebayar.getText().toString().isEmpty() && ygharusdibayar.getText().toString().isEmpty() &&
                        jumlahbayar.getText().toString().isEmpty() && titip.getText().toString().isEmpty() && keterangandetailpembayaran.getText().toString().isEmpty() &&
                        base64_image_1.isEmpty()) {
                    Toast.makeText(Detail_pembayaranActivity.this, "Data Belum di Isi ", Toast.LENGTH_LONG).show();

                } else if (spinnerPembayaran.getText().toString().isEmpty()) {
                    Toast.makeText(Detail_pembayaranActivity.this, "Nama Anggota Belum di Pilih", Toast.LENGTH_LONG).show();

                }
//                else if (idangotadetailbayar.getText().toString().isEmpty()) {
//                    Toast.makeText(Detail_pembayaranActivity.this, "ID Anggota Belum di Isi", Toast.LENGTH_LONG).show();
//
//                }
                else if (nomorpinjaman.getText().toString().isEmpty()) {
                    Toast.makeText(Detail_pembayaranActivity.this, "Nomor Pinjam Belum di Pilih", Toast.LENGTH_LONG).show();

                }
//                else if (jmlPinjam.getText().toString().isEmpty()) {
//                    Toast.makeText(Detail_pembayaranActivity.this, "Jumlah Pinjaman Belum di Isi", Toast.LENGTH_LONG).show();
//
//                }
//                else if (angsuranke.getText().toString().isEmpty()) {
//                    Toast.makeText(Detail_pembayaranActivity.this, "Angsuran ke Belum di Isi", Toast.LENGTH_LONG).show();
//
//            }
//            else if (tanggalbayar.getText().toString().isEmpty()) {
//                Toast.makeText(Detail_pembayaranActivity.this, "Tanggal Bayar Belum di Isi", Toast.LENGTH_LONG).show();
//
//            }
                else if (tipebayar.getText().toString().isEmpty()) {
                    Toast.makeText(Detail_pembayaranActivity.this, "Tipe Bayar Belum di Pilih", Toast.LENGTH_LONG).show();

                }
//            else if (ygharusdibayar.getText().toString().isEmpty()) {
//                Toast.makeText(Detail_pembayaranActivity.this, "Yang Harus di Bayar Belum di Isi", Toast.LENGTH_LONG).show();
//
//            }
                else if (jumlahbayar.getText().toString().isEmpty()) {
                    Toast.makeText(Detail_pembayaranActivity.this, "Jumlah Bayar Belum di Isi", Toast.LENGTH_LONG).show();

                }
//            else if (titip.getText().toString().isEmpty()) {
//                Toast.makeText(Detail_pembayaranActivity.this, "titip Belum di Isi", Toast.LENGTH_LONG).show();
//
//            }
//                else if (keterangandetailpembayaran.getText().toString().isEmpty()) {
//                    Toast.makeText(Detail_pembayaranActivity.this, "Keterangan Belum di Isi", Toast.LENGTH_LONG).show();

//                }
                else if (base64_signature.isEmpty()) {
                    Toast.makeText(Detail_pembayaranActivity.this, "belum ada ttd", Toast.LENGTH_LONG).show();
                } else if (base64_image_1.isEmpty()) {
                    Toast.makeText(Detail_pembayaranActivity.this, "foto Belum di Upload", Toast.LENGTH_LONG).show();
                } else {
                    int cokjumlahbayar = Integer.parseInt(jumlahbayar.getText().toString().trim().replaceAll("\\D+", ""));
                    int cokyangharusdibayar = Integer.parseInt(ygharusdibayar.getText().toString().trim().replaceAll("\\D+", ""));
                    if (cokjumlahbayar >= cokyangharusdibayar) {
                        SendDataToCloud();
                    } else {
                        SendDataToCloudKurang();
                    }
                }

            }


        });


    }

    private void sendData(JSONObject json) {

        String url = Http.getUrl() + "InsertPayment";
        final ProgressDialog dialog1 = new ProgressDialog(Detail_pembayaranActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Sedang Menyimpan...");
        dialog1.show();
        RequestQueue mQueue = Volley.newRequestQueue(Detail_pembayaranActivity.this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, json,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog1.dismiss();
                        pop_up_pembayaran_berhasilFragment menuDialogHotelFragment = new pop_up_pembayaran_berhasilFragment();
                        FragmentManager mFragmentManager = getSupportFragmentManager();
                        menuDialogHotelFragment.setCancelable(false);
                        menuDialogHotelFragment.show(mFragmentManager, pop_up_pembayaran_berhasilFragment.class.getSimpleName());
                        Log.e(TAG, "onResponse: " + response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();

                String responseBody = null;
                responseBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);
                Log.d(TAG, "onErrorResponse() called with: error = [" + responseBody + "]");
                Toast.makeText(Detail_pembayaranActivity.this, responseBody, Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                //params.put("Authorization", "Bearer " + sessionManager.getKeyToken());
                return params;
            }

            @Override
            protected com.android.volley.Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjectRequest);
    }

    private void SendDataToCloud() {
        final ProgressDialog dialog1 = new ProgressDialog(Detail_pembayaranActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Sedang Menyimpan...");
        dialog1.show();
        String amtPokok = "0", simpananPokok = "0", useTitipanSaldo = "0", sisaBayar = "0", paysanksi = "0", payinterestamt = "0";
        if (modelFormPinjamanData.getLoandesc().equals("Flat")) {
            amtPokok = Tvpokok.getText().toString().trim().replaceAll("\\D+", "");
        } else {
            amtPokok = edtPokok.getText().toString().trim().replaceAll("\\D+", "");
        }
        if (!edtSimpananPokok.getText().toString().trim().replaceAll("\\D+", "").equals("")) {
            simpananPokok = edtSimpananPokok.getText().toString().trim().replaceAll("\\D+", "");
        } else {
            simpananPokok = "0";
        }
        if (!jasa1.getText().toString().trim().replaceAll("\\D+", "").equals("")) {
            payinterestamt = jasa1.getText().toString().trim().replaceAll("\\D+", "");
        } else {
            payinterestamt = "0";
        }
        if (!titip.getText().toString().trim().replaceAll("\\D+", "").equals("")) {
            sisaBayar = titip.getText().toString().trim().replaceAll("\\D+", "");
        } else {
            sisaBayar = "0";
        }
        if (!edtBayarSanksi.getText().toString().trim().replaceAll("\\D+", "").equals("") && switchsanksi.isChecked()) {
            paysanksi = edtBayarSanksi.getText().toString().trim().replaceAll("\\D+", "");
        } else {
            paysanksi = "0";
        }
        if (switchtitip.isChecked()) {
            if (!edtTitipanSaldo.getText().toString().trim().replaceAll("\\D+", "").equals("")) {
                useTitipanSaldo = edtTitipanSaldo.getText().toString().trim().replaceAll("\\D+", "");
            }
        } else {
            useTitipanSaldo = "0";
        }

        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<String> call = apiInterface.doPostPembayaran(
                Http.getsCmp(), //scmp
                String.valueOf(modelFormPinjamanData.getTrnloandtloid()), //loandtloid
                tanggalbayar.getText().toString().trim(), //paydate
                modelFormPinjamanData.getLoandesc(), //paytype
                "0", //cashbankacctgoid
                amtPokok, //payloanamt
                payinterestamt, //payinterestamt
                paysanksi, //paysanksi
                sisaBayar, //paytitipan
                simpananPokok, //paysaving
                keterangandetailpembayaran.getText().toString(), //paynote
                useTitipanSaldo, //titipanamtpay
                sessionManager.getPID(), //createuser
                base64_image_1, //rawimgrefpay
                base64_signature //rawimgttd
        );

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    dialog1.dismiss();
                    String responseBody = response.body().trim();
                    String[] responseOK = responseBody.split(";");
                    Log.e(TAG, "onResponse: " + response.body() + " " + responseOK[1]);

                    if (responseOK[0].equals("OK")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("paymentno", String.valueOf(responseOK[1]));
                        pop_up_pembayaran_berhasilFragment menuDialogHotelFragment = new pop_up_pembayaran_berhasilFragment();
                        FragmentManager mFragmentManager = getSupportFragmentManager();
                        menuDialogHotelFragment.setArguments(bundle);
                        menuDialogHotelFragment.setCancelable(false);
                        menuDialogHotelFragment.show(mFragmentManager, pop_up_pembayaran_berhasilFragment.class.getSimpleName());
                        Log.e(TAG, "onResponse: " + response.toString());
                    } else {
                        Toast.makeText(Detail_pembayaranActivity.this, "Pembayaran Gagal", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "onResponse: " + t.getMessage());
                dialog1.dismiss();

                call.cancel();
                Toast.makeText(Detail_pembayaranActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void SendDataToCloudKurang() {
        final ProgressDialog dialog1 = new ProgressDialog(Detail_pembayaranActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Sedang Menyimpan...");
        dialog1.show();
        String payygharusdibayar = "0";

        if (!jumlahbayar.getText().toString().trim().replaceAll("\\D+", "").equals("")) {
            payygharusdibayar = jumlahbayar.getText().toString().trim().replaceAll("\\D+", "");
        } else {
            payygharusdibayar = "0";
        }

        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<String> call = apiInterface.doPostPembayaran(
                Http.getsCmp(), //scmp
                String.valueOf(modelFormPinjamanData.getTrnloandtloid()), //loandtloid
                tanggalbayar.getText().toString().trim(), //paydate
                modelFormPinjamanData.getLoandesc(), //paytype
                "0", //cashbankacctgoid
                "0", //payloanamt
                "0", //payinterestamt
                "0", //paysanksi
                payygharusdibayar, //paytitipan
                "0", //paysaving
                keterangandetailpembayaran.getText().toString(), //paynote
                "0", //titipanamtpay
                sessionManager.getPID(), //createuser
                base64_image_1, //rawimgrefpay
                base64_signature //rawimgttd
        );

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG, "onResponse: " + response.body());
                    dialog1.dismiss();
                    Log.e(TAG, "onResponse: " + response.body());
                    String responseBody = response.body().trim();
                    String[] responseOK = responseBody.split(";");
                    Log.e(TAG, "onResponse: " + response.body() + " " + responseOK[1]);

                    if (responseOK[0].equals("OK")) {
                        Bundle bundle = new Bundle();
                        bundle.putString("paymentno", String.valueOf(responseOK[1]));
                        pop_up_pembayaran_berhasilFragment menuDialogHotelFragment = new pop_up_pembayaran_berhasilFragment();
                        FragmentManager mFragmentManager = getSupportFragmentManager();
                        menuDialogHotelFragment.setArguments(bundle);
                        menuDialogHotelFragment.setCancelable(false);
                        menuDialogHotelFragment.show(mFragmentManager, pop_up_pembayaran_berhasilFragment.class.getSimpleName());
                        Log.e(TAG, "onResponse: " + response.toString());
                    } else {
                        Toast.makeText(Detail_pembayaranActivity.this, "Pembayaran Gagal", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "onResponse: " + t.getMessage());
                call.cancel();
                dialog1.dismiss();

                Toast.makeText(Detail_pembayaranActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void addTextWathcer(EditText editTextWathc) {
        editTextWathc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s.toString().trim())) {
                    Log.e(TAG, "onTextChanged: edtBayarSanksi " + s.toString());
                    setupHitung();
                }
                if (count > 0) {
                    if (!s.toString().equals(current) && !s.equals("") | jumlahpembayaran != 0 | s.length() > 0) {
                        //setuptotal();
                        int formatted = 0; // or any appllication default value
                        try {
                            Log.e(TAG, "onTextChanged: " + s.toString());
                            editTextWathc.removeTextChangedListener(this);
                            String cleanString = s.toString().replaceAll("[$,.Rp ]", "");
                            //double parsed = Double.parseDouble(cleanString);
                            formatted = Integer.parseInt(cleanString.replace(".", ""));
                            Log.e(TAG, "onTextChanged: " + formatted);
                            current = String.valueOf(formatted);
                            editTextWathc.setText(MainActivity.nf.format(formatted));
                            setupHitung();
                            setupPembayaran(current);
                            editTextWathc.setSelection(editTextWathc.length());
                        } catch (NumberFormatException nfe) {
                            nfe.printStackTrace();
                        }

                        editTextWathc.addTextChangedListener(this);
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void onclickBtnUpload(TextView lingkaran1, String type_uploads) {
        lingkaran1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type_upload = type_uploads;
                @SuppressLint("ResourceType")
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Detail_pembayaranActivity.this);
                LayoutInflater inflater = Detail_pembayaranActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alert_uploadfoto, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(true);
                dialogBuilder.setTitle("Upload File");
                CardView takePhoto = dialogView.findViewById(R.id.cv_takePhoto);
                CardView selectPhoto = dialogView.findViewById(R.id.cv_selectPhoto);
//                CardView selectFile = dialogView.findViewById(R.id.cv_File);

                final AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
                alertDialog.setCancelable(true);
                alertDialog.setCanceledOnTouchOutside(true);
                takePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dispatchTakePictureIntent();
                        alertDialog.dismiss();
                    }
                });
                selectPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showFileChooser();
                        alertDialog.dismiss();
                    }
                });


            }

        });

    }

    private void setupPembayaran(String naruto) {
        double pembayaran = Double.parseDouble(naruto.trim().replaceAll("\\D+", ""));
        double tagihan = Double.parseDouble(ygharusdibayar.getText().toString().replaceAll("\\D+", ""));

        titipan = pembayaran - tagihan;
//untuk hilang dan muncul untuk titip
        titip.setText("Rp " + MainActivity.nf.format(titipan.intValue()) + "");
        Log.e(TAG, "setupJumlahPembayaran: " + titipan.intValue() + " " + pembayaran + " " + tagihan);
        if (!titip.getText().toString().equals("0") && titipan.intValue() >= 0 || titipan.intValue() == 0) {
//            sumbit.setEnabled(true);
            if (titipan.intValue() >= 1) {

                liniertitip.setVisibility(View.VISIBLE);
            }

        } else {
            liniertitip.setVisibility(View.GONE);
//            sumbit.setEnabled(false);

        }

    }

    private void showChooser() {
        // Use the GET_CONTENT intent from the utility class
        Intent intent = new Intent();
        intent.setType("*/*");
        Log.e(TAG, "showChooser: File Start");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Select a File to Upload"),
                REQUEST_FILE_MANAGER);

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);

    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            modelFormPinjamanData = data.getParcelableExtra("ModelFormPinjaman");
            nomorpinjaman.setText(String.valueOf(modelFormPinjamanData.getTrnloanno()));
            jmlPinjam.setText("Rp " + MainActivity.nf.format(modelFormPinjamanData.getTrnloanamt()));
            angsuranke.setText(String.valueOf(modelFormPinjamanData.getTrnloandtlseq()));
//            edtBayarSanksi.setText(MainActivity.nf.format(modelFormPinjamanData.getTtgSanksi()));
            tvTagihanSanksi.setText("Total Tagihan Sanksi Rp " + MainActivity.nf.format(modelFormPinjamanData.getTtgSanksi()));
            tvSaldoTitipan.setText("Saldo Titipan Tersisa Rp " + MainActivity.nf.format(modelFormPinjamanData.getSaldotitipan()));
            setupForm();
            lnPembayaranDetail.setVisibility(View.VISIBLE);

        }

        if (requestCode == 2 && resultCode == RESULT_OK && data != null) {
            String strEditText = data.getStringExtra("_DATA");
            tipebayar.setText(strEditText);
        }
        if (requestCode == 3 && resultCode == RESULT_OK && data != null) {
            String strEditTextId = data.getStringExtra("_DATAID");
            String strEditTextName = data.getStringExtra("_DATANAME");
            strIdNasabah = data.getStringExtra("_DATANAMEID");
            idangotadetailbayar.setText(strEditTextId);
            spinnerPembayaran.setText(strEditTextName);
            setAwal();
        }
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            Bitmap bm = null;
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Bitmap selectedImage = getResizedBitmap(bm, 300);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            assert bm != null;
            Log.e(TAG, "onActivityResult PICK_IMAGE_REQUEST: " + type_upload);
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            try {
                base64_image_1 = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("sdsad", base64_image_1);
            if (type_upload.equals("image_1")) {
                selectImageViewREQUEST_Pick_CAPTURE("base64_image_1", filePath, byteArrayOutputStream, uploadfotoimage);
            } else if (type_upload.equals("image_2")) {
                selectImageViewREQUEST_Pick_CAPTURE("base64_image_2", filePath, byteArrayOutputStream, uploadfotoimage3);
            } else if (type_upload.equals("image_3")) {
                selectImageViewREQUEST_Pick_CAPTURE("base64_image_3", filePath, byteArrayOutputStream, uploadfotoimage4);
            } else {
                selectImageViewREQUEST_Pick_CAPTURE("base64_image_1", filePath, byteArrayOutputStream, uploadfoto2);
            }
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            Log.e(TAG, "onActivityResult REQUEST_IMAGE_CAPTURE: " + type_upload);
            Bitmap selectedImage = getResizedBitmap(thumbnail, 300);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            assert thumbnail != null;
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            if (type_upload.equals("image_1")) {
                try {
                    base64_image_1 = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                selectImageViewREQUEST_IMAGE_CAPTURE("base64_image_1", uploadfotoimage, selectedImage, byteArrayOutputStream);
            } else if (type_upload.equals("image_2")) {
                try {
                    base64_image_2 = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                selectImageViewREQUEST_IMAGE_CAPTURE("base64_image_2", uploadfotoimage4, selectedImage, byteArrayOutputStream);
            } else if (type_upload.equals("image_3")) {
                selectImageViewREQUEST_IMAGE_CAPTURE("base64_image_3", uploadfotoimage3, selectedImage, byteArrayOutputStream);
            } else {
                selectImageViewREQUEST_IMAGE_CAPTURE("base64_image_1", uploadfoto2, selectedImage, byteArrayOutputStream);
            }
        }

        if (requestCode == REQUEST_FILE_MANAGER && resultCode == RESULT_OK) {
            try {
                Uri uri = data.getData();
                Log.d(TAG, "File Uri: " + uri.toString());
                String path = FileUtils.getPath(this, uri);
                String filename = path.substring(path.lastIndexOf("/") + 1);
                Log.e(TAG, "onActivityResult: " + FileUtils.getExtension(path) + "  " + filename);
                modelAttachmentsList.add(new ModelAttachments(uri, path, filename, R.drawable.agunan));
                setupAttachmentFile();
            } catch (ActivityNotFoundException e) {
                Log.e(TAG, "onActivityResult: " + e.getMessage());
//                Toast.makeText(getApplicationContext(), "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
            }
        }
        nomorpinjaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), No_pimjamanActivity.class);
                startActivityForResult(intent, 1);
            }
        });
        tipebayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Tipe_bayarActivity.class);
                startActivityForResult(intent, 2);
            }
        });

    }

    private void setAwal() {
        edtTitipanSaldo.setText("");
        edtBayarSanksi.setText("");
        nomorpinjaman.setText("");
        nomorpinjaman.setText("");
        jmlPinjam.setText("");
        angsuranke.setText("");
        nomorpinjaman.setHint("Nomor Pinjaman");
        Tvpokok.setText("");
        jasa1.setText("");
        Tvpokok.setHint("Tagihan Pokok");
        jasa1.setText("");
        jasa1.setHint("Biaya Jasa");
        keterangandetailpembayaran.setText("");
        keterangandetailpembayaran.setHint("Masukkan Keterangan");
        jumlahbayar.setText("");
        jumlahbayar.setHint("Jumlah yang harus di bayar");
        titip.setText("");
        titip.setHint("Saldo Titipan");
        ygharusdibayar.setText("");
        ygharusdibayar.setHint("Total yang harus di balas");
    }

    private void setupForm() {
        Log.e(TAG, "setupForm: " + modelFormPinjamanData.getLoandesc());
        if (modelFormPinjamanData.getLoandesc().equals("Flat")) {
            Tvpokok.setText(MainActivity.nf.format(modelFormPinjamanData.getTrnloandtlamt()));
            jasa1.setText("Rp " + MainActivity.nf.format(modelFormPinjamanData.getTrnloandtlinterest()));
//            edtBayarSanksi.setText("Rp " + MainActivity.nf.format(modelFormPinjamanData.getTrnloandtlinterest()));
//            edtTitipanSaldo.setText("Rp " + MainActivity.nf.format(modelFormPinjamanData.getTrnloandtlinterest()));
            lnFlat.setVisibility(View.VISIBLE);
            lnSliding.setVisibility(View.GONE);
        } else {
            lnFlat.setVisibility(View.GONE);
            lnSliding.setVisibility(View.VISIBLE);
            edtPokok.setText(MainActivity.nf.format(modelFormPinjamanData.getTrnloandtlamt()));
            jasa1.setText(MessageFormat.format("Rp {0}", MainActivity.nf.format(modelFormPinjamanData.getTrnloandtlinterest())));
//            edtBayarSanksi.setText("Rp " + MainActivity.nf.format(modelFormPinjamanData.getTrnloandtlinterest()));
//            edtTitipanSaldo.setText("Rp " + MainActivity.nf.format(modelFormPinjamanData.getTrnloandtlinterest()));
        }

        setupHitung();
    }

    private void setupHitung() {
        hitungBiaya = 0;
        biayaPokok = 0;
        biayajasa = 0;
        bayarsanksi = 0;
        bayartitipan = 0;
        biayajasaEdit = 0;
        simpananPokok = 0;
        if (!Tvpokok.getText().toString().equals("") || !TextUtils.isEmpty(Tvpokok.getText().toString())) {
            biayaPokok = Integer.parseInt(Tvpokok.getText().toString().replaceAll("\\D+", ""));
        }
        if (!edtPokok.getText().toString().equals("") || !TextUtils.isEmpty(edtPokok.getText().toString())) {
            biayajasaEdit = Integer.parseInt(edtPokok.getText().toString().replaceAll("\\D+", ""));
        }
        if (!jasa1.getText().toString().equals("") || !TextUtils.isEmpty(jasa1.getText().toString())) {
            biayajasa = Integer.parseInt(jasa1.getText().toString().replaceAll("\\D+", ""));
        }

        if (!TextUtils.isEmpty(edtBayarSanksi.getText().toString()) && switchsanksi.isChecked()) {
            bayarsanksi = Integer.parseInt(edtBayarSanksi.getText().toString().replaceAll("\\D+", ""));
        }
        if (!TextUtils.isEmpty(edtTitipanSaldo.getText().toString()) && switchtitip.isChecked()) {
            bayartitipan = Integer.parseInt(edtTitipanSaldo.getText().toString().replaceAll("\\D+", ""));
        }
        if (!edtSimpananPokok.getText().toString().equals("") || !TextUtils.isEmpty(edtSimpananPokok.getText().toString())) {
            simpananPokok = Integer.parseInt(edtSimpananPokok.getText().toString().replaceAll("\\D+", ""));
        }
        hitungBiaya = biayaPokok + biayajasa + biayajasaEdit + bayarsanksi + simpananPokok - bayartitipan;
        Log.e(TAG, "setupHitung: " + hitungBiaya);
        ygharusdibayar.setText("Rp " + MainActivity.nf.format(hitungBiaya));
    }


    private void selectImageViewREQUEST_Pick_CAPTURE(String base642, Uri filePath, ByteArrayOutputStream byteArrayOutputStream, ImageView viewImage) {

        try {
            base642 = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            Log.e("sdsad", base642);
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
            viewImage.setImageBitmap(bitmap);
            Log.e("sdasd", String.valueOf(base642.length()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void selectImageViewREQUEST_IMAGE_CAPTURE(String base64232, ImageView xuploadfotoimage, Bitmap selectedImage, ByteArrayOutputStream bytes) {

        Log.e(base64232, base64232);
        Log.e(base64232 + "_length", String.valueOf(base64232.length()));
        xuploadfotoimage.setImageBitmap(selectedImage);
    }

    private void setupAttachmentFile() {
        if (modelAttachmentsList.size() < 1) {
            lv_listViewItem.setVisibility(View.GONE);
        } else {
            lv_listViewItem.setVisibility(View.VISIBLE);
            lv_listViewItem.setAdapter(null);
            adapterBuyNow = new AdapterAttachment(Detail_pembayaranActivity.this, -1, modelAttachmentsList, Detail_pembayaranActivity.this);
            lv_listViewItem.setAdapter(adapterBuyNow);
            uploadfoto2.setVisibility(View.GONE);
        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    //Requesting permission
    private void requestStoragePermission() {
        String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permission, STORAGE_PERMISSION_CODE);
        }

    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    //ttd

    @Override
    public void onRowAdapterAttachmentClicked(int position) {
        File file = new File(modelAttachmentsList.get(position).getPath());
        Log.e(TAG, "onRowAdapterAttachmentClicked: " + FileUtils.getMimeType(file));
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(modelAttachmentsList.get(position).getUri(), FileUtils.getMimeType(file));
            PackageManager pm = getPackageManager();
            if (intent.resolveActivity(pm) != null) {
                startActivity(intent);
            }
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "onRowAdapterAttachmentClicked: " + e.getMessage());
            Toast.makeText(Detail_pembayaranActivity.this, "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(Detail_pembayaranActivity.this, "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        Detail_pembayaranActivity.this.sendBroadcast(mediaScanIntent);
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()));
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}
