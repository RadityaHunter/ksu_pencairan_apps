package waruagung.com.ksuapplicationPencairanPakisaji.pembayaran.model;

import android.os.Parcel;
import android.os.Parcelable;

public class model_nomorpinjaman implements Parcelable {
    public static final Creator<model_nomorpinjaman> CREATOR = new Creator<model_nomorpinjaman>() {
        @Override
        public model_nomorpinjaman createFromParcel(Parcel source) {
            return new model_nomorpinjaman(source);
        }

        @Override
        public model_nomorpinjaman[] newArray(int size) {
            return new model_nomorpinjaman[size];
        }
    };

    private String txtTitle, tgl_jatuhtempo, jumlahpinjamanuang;
    private int kodeMenu,imgIcon;


    public model_nomorpinjaman(String txtTitle, String jumlahpinjamanuang, int ImageMenudeveloper, int kodeMenu, String tgl_jatuhtempo) {
        this.txtTitle = txtTitle;
        this.jumlahpinjamanuang = jumlahpinjamanuang;
        this.tgl_jatuhtempo = tgl_jatuhtempo;
        this.kodeMenu = kodeMenu;
        this.imgIcon = ImageMenudeveloper;

    }

    protected model_nomorpinjaman(Parcel in) {
        this.txtTitle = in.readString();
        this.imgIcon = in.readInt();
        this.jumlahpinjamanuang = in.readString();
        this.tgl_jatuhtempo = in.readString();
        this.kodeMenu = in.readInt();
    }

    public String getTxtTitle() {
        return txtTitle;
    }

    public void setTxtTitle(String txtTitle) {
        this.txtTitle = txtTitle;
    }
    public String getJumlahpinjamanuang() { return jumlahpinjamanuang; }

    public void setJumlahpinjamanuang(String jumlahpinjamanuang) { this.jumlahpinjamanuang = jumlahpinjamanuang; }
    public String getTgl_jatuhtempo() { return tgl_jatuhtempo; }

    public void setTgl_jatuhtempo(String tgl_jatuhtempo) {
        this.tgl_jatuhtempo = tgl_jatuhtempo;
    }

    public int getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(int kodeMenu) {
        this.kodeMenu = kodeMenu;
    }

    public int getImgIcon() { return imgIcon; }

    public void setImgIcon(int imgIcon) {this. imgIcon = imgIcon; }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.txtTitle);
        dest.writeString(this.jumlahpinjamanuang);
        dest.writeString(this.tgl_jatuhtempo);
        dest.writeInt(this.kodeMenu);
        dest.writeInt(this.imgIcon);

    }
}
