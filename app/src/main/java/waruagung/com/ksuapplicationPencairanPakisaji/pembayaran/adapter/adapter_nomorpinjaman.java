package waruagung.com.ksuapplicationPencairanPakisaji.pembayaran.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.pembayaran.model.model_nomorpinjaman;

public class adapter_nomorpinjaman extends RecyclerView.Adapter<adapter_nomorpinjaman.myViewHolder> {
    static final String EXTRAS_DATA = "EXTRAS_data";
    private Context context;
    private ArrayList<model_nomorpinjaman> listMenu;
    private Intent intent;

    public adapter_nomorpinjaman(Context context, ArrayList<model_nomorpinjaman> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @NonNull
    @Override
    public adapter_nomorpinjaman.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.desain_adapter_nomor_pinjaman, viewGroup, false);
        return new adapter_nomorpinjaman.myViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull adapter_nomorpinjaman.myViewHolder myViewHolder, final int i) {
        myViewHolder.txtTitle.setText(listMenu.get(i).getTxtTitle());
        myViewHolder.jumlahpinjamanuang.setText(listMenu.get(i).getJumlahpinjamanuang());
        myViewHolder.tgl_jatuhtempo.setText(listMenu.get(i).getTgl_jatuhtempo());

        Glide.with(context)
                .load(listMenu.get(i).getImgIcon()).into(myViewHolder.MenuImages);

        myViewHolder.ListBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              MenuKategori(listMenu.get(i).getTxtTitle());
            }
        });

    }

    private void MenuKategori(String names) {
        Intent intent = new Intent();
        intent.putExtra("_DATA", names);
        if (context instanceof Activity)
            ((Activity)context).setResult(Activity.RESULT_OK, intent);
        ((Activity)context).finish();

        }


    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgIcon)
        ImageView MenuImages;
        @BindView(R.id.ListNomorPinjaman)
        RelativeLayout ListBarang;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.jumlahpinjamanuang)
        TextView jumlahpinjamanuang;
        @BindView(R.id.tgl_jatuhtempo)
        TextView tgl_jatuhtempo;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
