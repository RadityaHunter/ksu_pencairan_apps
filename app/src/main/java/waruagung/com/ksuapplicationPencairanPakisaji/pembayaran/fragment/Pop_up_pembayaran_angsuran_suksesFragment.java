package waruagung.com.ksuapplicationPencairanPakisaji.pembayaran.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.fragment.app.DialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;


public class Pop_up_pembayaran_angsuran_suksesFragment extends DialogFragment implements View.OnClickListener {

    @BindView(R.id.btnclose)
    ImageView btnclose;
    @BindView(R.id.profil)
    ImageView profil;
    @BindView(R.id.btn_tutup)
    LinearLayout btnTutup;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    private View view;
    private Intent intent;

    public Pop_up_pembayaran_angsuran_suksesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_pop_up_pembayaran_angsuran_sukses, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        view = inflater.inflate(R.layout.fragment_pop_up_pembayaran_angsuran_sukses, container, false);
        ButterKnife.bind(this, view);

        view.findViewById(R.id.btn_tutup).setOnClickListener(this);
        view.findViewById(R.id.btnclose).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_tutup:
                intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
                break;
            case R.id.btnclose:
                Intent intent = new Intent(view.getContext(), MainActivity.class);
                startActivity(intent);
                break;
        }
    }

}
