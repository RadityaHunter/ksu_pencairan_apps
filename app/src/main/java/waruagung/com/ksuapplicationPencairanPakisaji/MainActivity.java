package waruagung.com.ksuapplicationPencairanPakisaji;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.Adapter.AdapterMenuHome;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.LaporanPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanTabelActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelMenuHome;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.NotifikasiPencairanActivity;

public class MainActivity extends AppCompatActivity implements AdapterMenuHome.AdapterMenuHomeCallback {
    public static final String TAG = MainActivity.class.getSimpleName();
    public static NumberFormat nf = NumberFormat.getInstance(Locale.GERMAN);
    //    @BindView(R.id.angunan)
//    LinearLayout angunan;
//    @BindView(R.id.pinjaman)
//    LinearLayout pinjaman;
    @BindView(R.id.LaporanPencairan)
    LinearLayout LaporanPencairan;
    //    @BindView(R.id.Daftaragunan)
//    LinearLayout Daftaragunan;
//    @BindView(R.id.daftarpengajuan)
//    LinearLayout daftarpengajuan;
    @BindView(R.id.daftarpencairan)
    LinearLayout daftarpencairan;
    //    @BindView(R.id.riwayatpembayaran)
//    LinearLayout riwayatpembayaran;
    @BindView(R.id.keluar)
    ImageView keluar;
    @BindView(R.id.notif)
    ImageView notif;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvId)
    TextView tvId;
    @BindView(R.id.rvMenu)
    RecyclerView rvMenu;
    @BindView(R.id.laporanpenagihan)
    LinearLayout laporanpenagihan;
    private SessionManager sessionManager;
    private Intent intent;
    private Boolean APP_PENCAIRAN_LAPANGAN = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        keluar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showDialog();
//            }
//        });
        sessionManager = new SessionManager(MainActivity.this);
        tvName.setText(sessionManager.getPID());
        tvId.setText("Staff");
        keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Apakah Anda yakin untuk keluar ?")
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sessionManager.destroySession();
                                Intent intent = new Intent(MainActivity.this, Login_Activity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
        notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), NotifikasiPencairanActivity.class);
                startActivity(intent);
            }
        });
        List<ModelMenuHome> menu = new ArrayList<>();
        Log.e(TAG, "onCreate: " + sessionManager.getRole());
        try {
            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    APP_PENCAIRAN_LAPANGAN = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (APP_PENCAIRAN_LAPANGAN) {
            menu.clear();
            menu.add(new ModelMenuHome(1, "Daftar<br>Pencairan", R.drawable.icon_daftar_pencairan1));
            menu.add(new ModelMenuHome(0, "Laporan<br>Pencairan", R.drawable.icon_laporan_pencairan1));
            menu.add(new ModelMenuHome(2, "Pencairan", R.drawable.icon_pencairan));
        } else {
            menu.clear();
            menu.add(new ModelMenuHome(1, "Daftar<br>Pencairan", R.drawable.icon_daftar_pencairan1));
            menu.add(new ModelMenuHome(0, "Laporan<br>Pencairan", R.drawable.icon_laporan_pencairan1));
        }
        Log.e(TAG, "onCreate: " + menu.size());
        if (menu.size() > 0) {
            AdapterMenuHome adapterMenuHome = new AdapterMenuHome(MainActivity.this, -1, menu, MainActivity.this);
            rvMenu.setLayoutManager(new GridLayoutManager(this, 3));
            rvMenu.setHasFixedSize(true);
            rvMenu.setItemAnimator(new DefaultItemAnimator());
            rvMenu.setVisibility(View.VISIBLE);
            rvMenu.setAdapter(adapterMenuHome);
        }

        //laporan lama
//        LaporanPencairan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), CariLaporanActivity.class);
//                startActivity(intent);
//            }
//        });
         //laporan baru
        LaporanPencairan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LaporanPencairanActivity.class);
                startActivity(intent);
            }
        });



        daftarpencairan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                startActivity(intent1);
            }
        });


    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            exitBybackKey();
            //moveTaskToBack(False);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onRowAdapterMenuHomeClicked(int position) {
        switch (position) {
            case 0:
//                intent = new Intent(getApplicationContext(), CariLaporanActivity.class);
//                startActivity(intent);
//                break;
                intent = new Intent(getApplicationContext(), LaporanPencairanActivity.class);
                startActivity(intent);
                break;


//            case 1:
//                intent = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
//                startActivity(intent);
//                break;

                 case 1:
                intent = new Intent(getApplicationContext(), DaftarPencairanTabelActivity.class);
                startActivity(intent);
                break;


            case 2:
                intent = new Intent(getApplicationContext(), PencairanActivity.class);
                startActivity(intent);
                break;
        }
    }
}
