package waruagung.com.ksuapplicationPencairanPakisaji;

import android.os.Parcel;
import android.os.Parcelable;

public class Modelcari implements Parcelable {
    public static final Parcelable.Creator<Modelcari> CREATOR = new Parcelable.Creator<Modelcari>() {
        @Override
        public Modelcari createFromParcel(Parcel source) {
            return new Modelcari(source);
        }

        @Override
        public Modelcari[] newArray(int size) {
            return new Modelcari[size];
        }
    };

    private String nama;
    private int kodeMenu;


    public Modelcari(String nama, int kodeMenu) {
        this.nama = nama;
        this.kodeMenu = kodeMenu;

    }

    protected Modelcari(Parcel in) {
        this.nama = in.readString();

        this.kodeMenu = in.readInt();
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }


    public int getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(int kodeMenu) {
        this.kodeMenu = kodeMenu;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nama);
        dest.writeInt(this.kodeMenu);

    }
}
