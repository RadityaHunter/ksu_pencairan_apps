
package waruagung.com.ksuapplicationPencairanPakisaji.API.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelNamaAnggotaNasabah implements Parcelable {

    /**
     * memberloanoid : 194
     * membermasterno : KRD/KSP11/1710/000194.
     * membername : DEWI RETNOWATI
     * nLoan : 1
     * nLoanNom : 9500000
     */

    private int memberloanoid;
    private String membermasterno;
    private String membername;
    private int nLoan;
    private int nLoanNom;

    public int getMemberloanoid() {
        return memberloanoid;
    }

    public void setMemberloanoid(int memberloanoid) {
        this.memberloanoid = memberloanoid;
    }

    public String getMembermasterno() {
        return membermasterno;
    }

    public void setMembermasterno(String membermasterno) {
        this.membermasterno = membermasterno;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public int getNLoan() {
        return nLoan;
    }

    public void setNLoan(int nLoan) {
        this.nLoan = nLoan;
    }

    public int getNLoanNom() {
        return nLoanNom;
    }

    public void setNLoanNom(int nLoanNom) {
        this.nLoanNom = nLoanNom;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.memberloanoid);
        dest.writeString(this.membermasterno);
        dest.writeString(this.membername);
        dest.writeInt(this.nLoan);
        dest.writeInt(this.nLoanNom);
    }

    public ModelNamaAnggotaNasabah() {
    }

    protected ModelNamaAnggotaNasabah(Parcel in) {
        this.memberloanoid = in.readInt();
        this.membermasterno = in.readString();
        this.membername = in.readString();
        this.nLoan = in.readInt();
        this.nLoanNom = in.readInt();
    }

    public static final Parcelable.Creator<ModelNamaAnggotaNasabah> CREATOR = new Parcelable.Creator<ModelNamaAnggotaNasabah>() {
        @Override
        public ModelNamaAnggotaNasabah createFromParcel(Parcel source) {
            return new ModelNamaAnggotaNasabah(source);
        }

        @Override
        public ModelNamaAnggotaNasabah[] newArray(int size) {
            return new ModelNamaAnggotaNasabah[size];
        }
    };
}