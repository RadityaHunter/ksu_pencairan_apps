 package waruagung.com.ksuapplicationPencairanPakisaji.API.model;

 public class ModelNotifPenagihan {

     /**
      * memberoid : 825
      * membername : SUJIATI
      * membermasterno : KRD/KSP11/1901/000810
      * trnloanno : 1120.01/KRD/FR/KSP11/01/2019
      * trnloanamt : 5500000
      * trnloandtldate : 2020-01-31T00:00:00
      * trnloandtlinterest : 93500
      * trnloandtlamt : 229200
      * trnloandtlseq : 12
      * noAngsuran : 7
      */

     private int memberoid;
     private String membername;
     private String membermasterno;
     private String trnloanno;
     private int trnloanamt;
     private String trnloandtldate;
     private int trnloandtlinterest;
     private int trnloandtlamt;
     private int trnloandtlseq;
     private int noAngsuran;

     public int getMemberoid() {
         return memberoid;
     }

     public void setMemberoid(int memberoid) {
         this.memberoid = memberoid;
     }

     public String getMembername() {
         return membername;
     }

     public void setMembername(String membername) {
         this.membername = membername;
     }

     public String getMembermasterno() {
         return membermasterno;
     }

     public void setMembermasterno(String membermasterno) {
         this.membermasterno = membermasterno;
     }

     public String getTrnloanno() {
         return trnloanno;
     }

     public void setTrnloanno(String trnloanno) {
         this.trnloanno = trnloanno;
     }

     public int getTrnloanamt() {
         return trnloanamt;
     }

     public void setTrnloanamt(int trnloanamt) {
         this.trnloanamt = trnloanamt;
     }

     public String getTrnloandtldate() {
         return trnloandtldate;
     }

     public void setTrnloandtldate(String trnloandtldate) {
         this.trnloandtldate = trnloandtldate;
     }

     public int getTrnloandtlinterest() {
         return trnloandtlinterest;
     }

     public void setTrnloandtlinterest(int trnloandtlinterest) {
         this.trnloandtlinterest = trnloandtlinterest;
     }

     public int getTrnloandtlamt() {
         return trnloandtlamt;
     }

     public void setTrnloandtlamt(int trnloandtlamt) {
         this.trnloandtlamt = trnloandtlamt;
     }

     public int getTrnloandtlseq() {
         return trnloandtlseq;
     }

     public void setTrnloandtlseq(int trnloandtlseq) {
         this.trnloandtlseq = trnloandtlseq;
     }

     public int getNoAngsuran() {
         return noAngsuran;
     }

     public void setNoAngsuran(int noAngsuran) {
         this.noAngsuran = noAngsuran;
     }
 }