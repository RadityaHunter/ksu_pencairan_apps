 package waruagung.com.ksuapplicationPencairanPakisaji.API.model;

 import android.os.Parcel;
 import android.os.Parcelable;

 public class ModelJatuhTempo implements Parcelable {


     /**
      * memberoid : 825
      * membername : SUJIATI
      * membermasterno : KRD/KSP11/1901/000810
      * trnloanno : 1120.01/KRD/FR/KSP11/01/2019
      * trnloanamt : 5500000
      * trnloandtldate : 2020-01-31T00:00:00
      * trnloandtlinterest : 93500
      * trnloandtlamt : 229200
      * trnloandtlseq : 12
      * noAngsuran : 7
      */

     private int memberoid;
     private String membername;
     private String membermasterno;
     private String trnloanno;
     private int trnloanamt;
     private String trnloandtldate;
     private int trnloandtlinterest;
     private int trnloandtlamt;
     private int trnloandtlseq;
     private int noAngsuran;

     public int getMemberoid() {
         return memberoid;
     }

     public void setMemberoid(int memberoid) {
         this.memberoid = memberoid;
     }

     public String getMembername() {
         return membername;
     }

     public void setMembername(String membername) {
         this.membername = membername;
     }

     public String getMembermasterno() {
         return membermasterno;
     }

     public void setMembermasterno(String membermasterno) {
         this.membermasterno = membermasterno;
     }

     public String getTrnloanno() {
         return trnloanno;
     }

     public void setTrnloanno(String trnloanno) {
         this.trnloanno = trnloanno;
     }

     public int getTrnloanamt() {
         return trnloanamt;
     }

     public void setTrnloanamt(int trnloanamt) {
         this.trnloanamt = trnloanamt;
     }

     public String getTrnloandtldate() {
         return trnloandtldate;
     }

     public void setTrnloandtldate(String trnloandtldate) {
         this.trnloandtldate = trnloandtldate;
     }

     public int getTrnloandtlinterest() {
         return trnloandtlinterest;
     }

     public void setTrnloandtlinterest(int trnloandtlinterest) {
         this.trnloandtlinterest = trnloandtlinterest;
     }

     public int getTrnloandtlamt() {
         return trnloandtlamt;
     }

     public void setTrnloandtlamt(int trnloandtlamt) {
         this.trnloandtlamt = trnloandtlamt;
     }

     public int getTrnloandtlseq() {
         return trnloandtlseq;
     }

     public void setTrnloandtlseq(int trnloandtlseq) {
         this.trnloandtlseq = trnloandtlseq;
     }

     public int getNoAngsuran() {
         return noAngsuran;
     }

     public void setNoAngsuran(int noAngsuran) {
         this.noAngsuran = noAngsuran;
     }

     @Override
     public int describeContents() {
         return 0;
     }

     @Override
     public void writeToParcel(Parcel dest, int flags) {
         dest.writeInt(this.memberoid);
         dest.writeString(this.membername);
         dest.writeString(this.membermasterno);
         dest.writeString(this.trnloanno);
         dest.writeInt(this.trnloanamt);
         dest.writeString(this.trnloandtldate);
         dest.writeInt(this.trnloandtlinterest);
         dest.writeInt(this.trnloandtlamt);
         dest.writeInt(this.trnloandtlseq);
         dest.writeInt(this.noAngsuran);
     }

     public ModelJatuhTempo() {
     }

     protected ModelJatuhTempo(Parcel in) {
         this.memberoid = in.readInt();
         this.membername = in.readString();
         this.membermasterno = in.readString();
         this.trnloanno = in.readString();
         this.trnloanamt = in.readInt();
         this.trnloandtldate = in.readString();
         this.trnloandtlinterest = in.readInt();
         this.trnloandtlamt = in.readInt();
         this.trnloandtlseq = in.readInt();
         this.noAngsuran = in.readInt();
     }

     public static final Parcelable.Creator<ModelJatuhTempo> CREATOR = new Parcelable.Creator<ModelJatuhTempo>() {
         @Override
         public ModelJatuhTempo createFromParcel(Parcel source) {
             return new ModelJatuhTempo(source);
         }

         @Override
         public ModelJatuhTempo[] newArray(int size) {
             return new ModelJatuhTempo[size];
         }
     };
 }