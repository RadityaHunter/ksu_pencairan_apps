
package waruagung.com.ksuapplicationPencairanPakisaji.API.model;

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelLogin implements Serializable, Parcelable
{

    @SerializedName("userid")
    @Expose
    private String userid;
    public final static Parcelable.Creator<ModelLogin> CREATOR = new Creator<ModelLogin>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ModelLogin createFromParcel(Parcel in) {
            return new ModelLogin(in);
        }

        public ModelLogin[] newArray(int size) {
            return (new ModelLogin[size]);
        }

    }
            ;
    private final static long serialVersionUID = -5603486326373424473L;

    protected ModelLogin(Parcel in) {
        this.userid = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public ModelLogin() {
    }

    /**
     *
     * @param userid
     */
    public ModelLogin(String userid) {
        super();
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userid);
    }

    public int describeContents() {
        return 0;
    }

}
