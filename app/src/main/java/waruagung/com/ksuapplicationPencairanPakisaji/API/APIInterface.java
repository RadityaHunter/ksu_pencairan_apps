package waruagung.com.ksuapplicationPencairanPakisaji.API;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelDaftarPinjaman;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelExpendatebelDaftarPinjam;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelJatuhTempo;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelLogin;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelNamaAnggotaNasabah;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelNotifPenagihan;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanList;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanListDetail;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelFormPinjaman;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelPopUpPembayaran;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelReportPencairanResponse;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelRiwayatDetail;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;

public interface APIInterface {
    @Headers({"Accept: application/json"})
    @GET("LoginMobile")
    Call<List<ModelLogin>> doGetAPILogin(@Query("sUser") String sUserID, @Query("sPass") String sPassID, @Query("sType") String sTypeID, @Query("sCmp") String sCmpID);


    @Headers({"Accept: application/json"})
    @GET("DaftarPinjaman_List")
    Call<List<ModelDaftarPinjaman>> doGetAPIDaftarPinjaman(@Query("sCmp") String sCmpID);

    @Headers({"Accept: application/json"})
    @GET("DaftarPinjaman_List_Detail")
    Call<List<ModelExpendatebelDaftarPinjam>> doGetAPIDaftarPinjamanDetail(@Query("sCmp") String sCmpID, @Query("memberoid") String smemberoidID);

    @Headers({"Accept: application/json"})
    @GET("DaftarPinjaman_List")
    Call<List<ModelNamaAnggotaNasabah>> doGetAPIAnggotaNasabah(@Query("sCmp") String sCmpID);

    @Headers({"Accept: application/json"})
    @GET("Search_DaftarPinjaman_List")
    Call<List<ModelDaftarPinjaman>> doGetAPIDaftarPinjamanSearch(@Query("sCmp") String sCmpID, @Query("membername") String membername);

    @Headers({"Accept: application/json"})
    @GET("Search_DaftarPinjaman_List")
    Call<List<ModelNamaAnggotaNasabah>> doGetAPIDaftarPinjamanSearch2(@Query("sCmp") String sCmpID, @Query("membername") String membername);

    @Headers({"Accept: application/json"})
    @GET("DaftarPembayaran_List")
    Call<List<ModelDaftarPinjamanList>> doGetAPIPembayaranList(@Query("sCmp") String sCmpID);


    @Headers({"Accept: application/json"})
    @GET("Notifikasi_JadwalPenagihan")
    Call<List<ModelNotifPenagihan>> doGetAPINotifikasi_JadwalPenagihan(@Query("sCmp") String sCmpID);

    @Headers({"Accept: application/json"})
    @GET("Notifikasi_JatuhTempo")
    Call<List<ModelJatuhTempo>> doGetAPINotifikasi_JatuhTempo(@Query("sCmp") String sCmpID, @Query("tgl") String tgl);

    @Headers({"Accept: application/json"})
    @GET("Search_Daftar_Riwayat")
    Call<List<ModelDaftarPinjamanList>> doGetAPIPembayaranListCari(@Query("sCmp") String sCmpID, @Query("membername") String stringCari);

    @Headers({"Accept: application/json"})
    @GET("DaftarPinjaman_List_Detail")
    Call<List<ModelDaftarPinjamanListDetail>> doGetAPIDaftarPinjamanListDetail(@Query("sCmp") String sCmpID, @Query("memberoid") String memberoid);

    @FormUrlEncoded
    @POST("Riwayatpembayaran_List_Detail")
    Call<List<ModelRiwayatDetail>> doGetAPIModelRiwayatDetail(@Field("sCmp") String sCmpID, @Field("trnloanoid") String memberloanoid);

    @Headers({"Accept: application/json"})
    @GET("Form_Pembayaran")
    Call<List<ModelFormPinjaman>> doGetAPIForm_Pembayaran(@Query("sCmp") String sCmpID, @Query("memberloanoid") String membermasterno);

    @Headers({"Accept: application/json"})
    @GET("PopUp_Pembayaran")
    Call<List<ModelPopUpPembayaran>> PopUp_Pembayaran(@Query("sCmp") String sCmpID, @Query("paymentno") String paymentno);

    @FormUrlEncoded
    @POST("InsertPayment")
    Call<String> doPostPembayaran(
            @Field("sCmp") String sCmp,
            @Field("loandtloid") String loandtloid,
            @Field("paydate") String paydate,
            @Field("paytype") String paytype,
            @Field("cashbankacctgoid") String cashbankacctgoid,
            @Field("payloanamt") String payloanamt,
            @Field("payinterestamt") String payinterestamt,
            @Field("paysanksi") String paysanksi,
            @Field("paytitipan") String paytitipan,
            @Field("paysaving") String paysaving,
            @Field("paynote") String paynote,
            @Field("titipanamtpay") String titipanamtpay,
            @Field("createuser") String createuser,
            @Field("rawimgrefpay") String rawimgrefpay,
            @Field("rawimgttd") String rawimgttd
    );

    @FormUrlEncoded
    @POST("UserRole")
    Call<List<ModelUserRole>> doGetApiUserRole(@Field("sCmp") String toString, @Field("sUser") String toString1);

    @Headers({"Accept: application/json"})
    @GET("Report_Pencairan_List")
    Call<List<ModelReportPencairanResponse>> doGetPencairanReport(
            @Query("sCmp") String sCmp,
            @Query("datestart") String datestart,
            @Query("dateend") String dateend,
            @Query("membername") String membername
    );
}
