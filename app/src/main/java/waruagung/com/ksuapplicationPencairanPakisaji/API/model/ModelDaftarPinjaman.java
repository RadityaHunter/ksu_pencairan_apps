
package waruagung.com.ksuapplicationPencairanPakisaji.API.model;

public class ModelDaftarPinjaman {

    /**
     * memberloanoid : 834
     * membermasterno : KRD/KSP11/1902/000762.
     * membername : ABDUL MAKRUS
     * nLoan : 1
     * nLoanNom : 3000000
     */

    private int memberloanoid;
    private String membermasterno;
    private String membername;
    private int nLoan;
    private int nLoanNom;

    public int getMemberloanoid() {
        return memberloanoid;
    }

    public void setMemberloanoid(int memberloanoid) {
        this.memberloanoid = memberloanoid;
    }

    public String getMembermasterno() {
        return membermasterno;
    }

    public void setMembermasterno(String membermasterno) {
        this.membermasterno = membermasterno;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public int getNLoan() {
        return nLoan;
    }

    public void setNLoan(int nLoan) {
        this.nLoan = nLoan;
    }

    public int getNLoanNom() {
        return nLoanNom;
    }

    public void setNLoanNom(int nLoanNom) {
        this.nLoanNom = nLoanNom;
    }
}
