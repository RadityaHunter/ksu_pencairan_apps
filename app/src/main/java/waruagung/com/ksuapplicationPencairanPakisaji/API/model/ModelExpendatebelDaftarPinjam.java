
package waruagung.com.ksuapplicationPencairanPakisaji.API.model;

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelExpendatebelDaftarPinjam implements Serializable, Parcelable
{

    @SerializedName("trnloanoid")
    @Expose
    private Integer trnloanoid;
    @SerializedName("trnloanno")
    @Expose
    private String trnloanno;
    @SerializedName("trnloanopendate")
    @Expose
    private String trnloanopendate;
    @SerializedName("trnloanamt")
    @Expose
    private Double trnloanamt;
    @SerializedName("sisa")
    @Expose
    private Double sisa;
    @SerializedName("JenisPinjaman")
    @Expose
    private String jenisPinjaman;
    @SerializedName("JangkaWaktu")
    @Expose
    private Double jangkaWaktu;
    @SerializedName("JatuhTempo")
    @Expose
    private String jatuhTempo;
    @SerializedName("Bunga")
    @Expose
    private Double bunga;
    @SerializedName("BankName")
    @Expose
    private String bankName;
    @SerializedName("Norek")
    @Expose
    private String norek;
    @SerializedName("Nasabah")
    @Expose
    private Object nasabah;
    @SerializedName("Keterangan")
    @Expose
    private String keterangan;
    public final static Parcelable.Creator<ModelExpendatebelDaftarPinjam> CREATOR = new Creator<ModelExpendatebelDaftarPinjam>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ModelExpendatebelDaftarPinjam createFromParcel(Parcel in) {
            return new ModelExpendatebelDaftarPinjam(in);
        }

        public ModelExpendatebelDaftarPinjam[] newArray(int size) {
            return (new ModelExpendatebelDaftarPinjam[size]);
        }

    }
            ;
    private final static long serialVersionUID = -2612022650474346873L;

    protected ModelExpendatebelDaftarPinjam(Parcel in) {
        this.trnloanoid = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.trnloanno = ((String) in.readValue((String.class.getClassLoader())));
        this.trnloanopendate = ((String) in.readValue((String.class.getClassLoader())));
        this.trnloanamt = ((Double) in.readValue((Double.class.getClassLoader())));
        this.sisa = ((Double) in.readValue((Double.class.getClassLoader())));
        this.jenisPinjaman = ((String) in.readValue((String.class.getClassLoader())));
        this.jangkaWaktu = ((Double) in.readValue((Double.class.getClassLoader())));
        this.jatuhTempo = ((String) in.readValue((String.class.getClassLoader())));
        this.bunga = ((Double) in.readValue((Double.class.getClassLoader())));
        this.bankName = ((String) in.readValue((String.class.getClassLoader())));
        this.norek = ((String) in.readValue((String.class.getClassLoader())));
        this.nasabah = ((Object) in.readValue((Object.class.getClassLoader())));
        this.keterangan = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public ModelExpendatebelDaftarPinjam() {
    }

    /**
     *
     * @param keterangan
     * @param jangkaWaktu
     * @param trnloanno
     * @param bankName
     * @param bunga
     * @param sisa
     * @param jatuhTempo
     * @param norek
     * @param jenisPinjaman
     * @param trnloanoid
     * @param nasabah
     * @param trnloanopendate
     * @param trnloanamt
     */
    public ModelExpendatebelDaftarPinjam(Integer trnloanoid, String trnloanno, String trnloanopendate, Double trnloanamt, Double sisa, String jenisPinjaman, Double jangkaWaktu, String jatuhTempo, Double bunga, String bankName, String norek, Object nasabah, String keterangan) {
        super();
        this.trnloanoid = trnloanoid;
        this.trnloanno = trnloanno;
        this.trnloanopendate = trnloanopendate;
        this.trnloanamt = trnloanamt;
        this.sisa = sisa;
        this.jenisPinjaman = jenisPinjaman;
        this.jangkaWaktu = jangkaWaktu;
        this.jatuhTempo = jatuhTempo;
        this.bunga = bunga;
        this.bankName = bankName;
        this.norek = norek;
        this.nasabah = nasabah;
        this.keterangan = keterangan;
    }

    public Integer getTrnloanoid() {
        return trnloanoid;
    }

    public void setTrnloanoid(Integer trnloanoid) {
        this.trnloanoid = trnloanoid;
    }

    public String getTrnloanno() {
        return trnloanno;
    }

    public void setTrnloanno(String trnloanno) {
        this.trnloanno = trnloanno;
    }

    public String getTrnloanopendate() {
        return trnloanopendate;
    }

    public void setTrnloanopendate(String trnloanopendate) {
        this.trnloanopendate = trnloanopendate;
    }

    public Double getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(Double trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public Double getSisa() {
        return sisa;
    }

    public void setSisa(Double sisa) {
        this.sisa = sisa;
    }

    public String getJenisPinjaman() {
        return jenisPinjaman;
    }

    public void setJenisPinjaman(String jenisPinjaman) {
        this.jenisPinjaman = jenisPinjaman;
    }

    public Double getJangkaWaktu() {
        return jangkaWaktu;
    }

    public void setJangkaWaktu(Double jangkaWaktu) {
        this.jangkaWaktu = jangkaWaktu;
    }

    public String getJatuhTempo() {
        return jatuhTempo;
    }

    public void setJatuhTempo(String jatuhTempo) {
        this.jatuhTempo = jatuhTempo;
    }

    public Double getBunga() {
        return bunga;
    }

    public void setBunga(Double bunga) {
        this.bunga = bunga;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getNorek() {
        return norek;
    }

    public void setNorek(String norek) {
        this.norek = norek;
    }

    public Object getNasabah() {
        return nasabah;
    }

    public void setNasabah(Object nasabah) {
        this.nasabah = nasabah;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnloanoid);
        dest.writeValue(trnloanno);
        dest.writeValue(trnloanopendate);
        dest.writeValue(trnloanamt);
        dest.writeValue(sisa);
        dest.writeValue(jenisPinjaman);
        dest.writeValue(jangkaWaktu);
        dest.writeValue(jatuhTempo);
        dest.writeValue(bunga);
        dest.writeValue(bankName);
        dest.writeValue(norek);
        dest.writeValue(nasabah);
        dest.writeValue(keterangan);
    }

    public int describeContents() {
        return 0;
    }

}