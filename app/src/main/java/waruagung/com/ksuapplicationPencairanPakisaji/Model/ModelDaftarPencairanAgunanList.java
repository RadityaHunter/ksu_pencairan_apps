package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelDaftarPencairanAgunanList implements Parcelable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModelDaftarPencairanAgunanList(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
    }

    protected ModelDaftarPencairanAgunanList(Parcel in) {
        this.name = in.readString();
    }

    public static final Parcelable.Creator<ModelDaftarPencairanAgunanList> CREATOR = new Parcelable.Creator<ModelDaftarPencairanAgunanList>() {
        @Override
        public ModelDaftarPencairanAgunanList createFromParcel(Parcel source) {
            return new ModelDaftarPencairanAgunanList(source);
        }

        @Override
        public ModelDaftarPencairanAgunanList[] newArray(int size) {
            return new ModelDaftarPencairanAgunanList[size];
        }
    };
}
