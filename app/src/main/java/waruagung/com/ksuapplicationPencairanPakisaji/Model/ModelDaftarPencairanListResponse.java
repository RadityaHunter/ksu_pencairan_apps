package waruagung.com.ksuapplicationPencairanPakisaji.Model;

public class ModelDaftarPencairanListResponse {

    /**
     * trnloanoid : 937
     * trnloanno : 1867.01/KRD/SR/KSP11/02/2020
     * membermasterno : KRD/KSP11/2002/001298
     * membername : suryatest
     * trnloanamt : 00020323
     * trnloantime : 21
     * trnloanamtrev : 1905000
     * loandesc : Sliding
     * jenisagunan : BPKB Motor BMW 3201 MT
     */

    private int trnloanoid;
    private String trnloanno;
    private String membermasterno;
    private String membername;
    private String trnloanamt;
    private int trnloantime;
    private int trnloanamtrev;
    private String loandesc;
    private String jenisagunan;

    public ModelDaftarPencairanListResponse(int trnloanoid, String trnloanno, String membermasterno, String membername, String trnloanamt, int trnloantime, int trnloanamtrev, String loandesc, String jenisagunan) {
        this.trnloanoid = trnloanoid;
        this.trnloanno = trnloanno;
        this.membermasterno = membermasterno;
        this.membername = membername;
        this.trnloanamt = trnloanamt;
        this.trnloantime = trnloantime;
        this.trnloanamtrev = trnloanamtrev;
        this.loandesc = loandesc;
        this.jenisagunan = jenisagunan;
    }


    public int getTrnloanoid() {
        return trnloanoid;
    }

    public void setTrnloanoid(int trnloanoid) {
        this.trnloanoid = trnloanoid;
    }

    public String getTrnloanno() {
        return trnloanno;
    }

    public void setTrnloanno(String trnloanno) {
        this.trnloanno = trnloanno;
    }

    public String getMembermasterno() {
        return membermasterno;
    }

    public void setMembermasterno(String membermasterno) {
        this.membermasterno = membermasterno;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(String trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public int getTrnloantime() {
        return trnloantime;
    }

    public void setTrnloantime(int trnloantime) {
        this.trnloantime = trnloantime;
    }

    public int getTrnloanamtrev() {
        return trnloanamtrev;
    }

    public void setTrnloanamtrev(int trnloanamtrev) {
        this.trnloanamtrev = trnloanamtrev;
    }

    public String getLoandesc() {
        return loandesc;
    }

    public void setLoandesc(String loandesc) {
        this.loandesc = loandesc;
    }

    public String getJenisagunan() {
        return jenisagunan;
    }

    public void setJenisagunan(String jenisagunan) {
        this.jenisagunan = jenisagunan;
    }
}
