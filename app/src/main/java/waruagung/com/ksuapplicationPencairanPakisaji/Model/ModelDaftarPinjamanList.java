package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelDaftarPinjamanList implements Parcelable {

  public static final Parcelable.Creator<ModelDaftarPinjamanList> CREATOR = new Parcelable.Creator<ModelDaftarPinjamanList>() {
    @Override
    public ModelDaftarPinjamanList createFromParcel(Parcel source) {
      return new ModelDaftarPinjamanList(source);
    }

    @Override
    public ModelDaftarPinjamanList[] newArray(int size) {
      return new ModelDaftarPinjamanList[size];
    }
  };
  /**
   * memberloanoid : 834
   * membermasterno : KRD/KSP11/1902/000762.
   * membername : ABDUL MAKRUS
   * nLoan : 1
   * Sisa : 1500000
   * nPay : 1500000
   * nLoanNom : 3000000
   */

  private int memberloanoid;
  private String membermasterno;
  private String membername;
  private int nLoan;
  private int Sisa;
  private int nPay;
  private int nLoanNom;

  public ModelDaftarPinjamanList() {
  }

  protected ModelDaftarPinjamanList(Parcel in) {
    this.memberloanoid = in.readInt();
    this.membermasterno = in.readString();
    this.membername = in.readString();
    this.nLoan = in.readInt();
    this.Sisa = in.readInt();
    this.nPay = in.readInt();
    this.nLoanNom = in.readInt();
  }

  public int getMemberloanoid() {
    return memberloanoid;
  }

  public void setMemberloanoid(int memberloanoid) {
    this.memberloanoid = memberloanoid;
  }

  public String getMembermasterno() {
    return membermasterno;
  }

  public void setMembermasterno(String membermasterno) {
    this.membermasterno = membermasterno;
  }

  public String getMembername() {
    return membername;
  }

  public void setMembername(String membername) {
    this.membername = membername;
  }

  public int getNLoan() {
    return nLoan;
  }

  public void setNLoan(int nLoan) {
    this.nLoan = nLoan;
  }

  public int getSisa() {
    return Sisa;
  }

  public void setSisa(int Sisa) {
    this.Sisa = Sisa;
  }

  public int getNPay() {
    return nPay;
  }

  public void setNPay(int nPay) {
    this.nPay = nPay;
  }

  public int getNLoanNom() {
    return nLoanNom;
  }

  public void setNLoanNom(int nLoanNom) {
    this.nLoanNom = nLoanNom;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(this.memberloanoid);
    dest.writeString(this.membermasterno);
    dest.writeString(this.membername);
    dest.writeInt(this.nLoan);
    dest.writeInt(this.Sisa);
    dest.writeInt(this.nPay);
    dest.writeInt(this.nLoanNom);
  }
}
