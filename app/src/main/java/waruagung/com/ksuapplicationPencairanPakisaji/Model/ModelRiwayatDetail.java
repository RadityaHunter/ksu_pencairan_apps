package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelRiwayatDetail implements Parcelable {

    /**
     * memberloanoid : 834
     * membername : ABDUL MAKRUS
     * trnloanno : 1037.01/KRD/FR/KSP11/02/2019
     * trnloanamt : 3000000
     * trnloandtlnote : Angsuran Ke-1
     * trnpaymentdate : 2019-03-15T00:00:00
     * tagihan : 298000
     * titipanamt : 0
     */

    private int memberloanoid;
    private String membername;
    private String trnloanno;
    private int trnloanamt;
    private String trnloandtlnote;
    private String trnpaymentdate;
    private int tagihan;
    private int titipanamt;
    private int acumamt;
    public ModelRiwayatDetail() {
    }

    public int getAcumamt() {
        return acumamt;
    }

    public void setAcumamt(int acumamt) {
        this.acumamt = acumamt;
    }

    public int getMemberloanoid() {
        return memberloanoid;
    }

    public void setMemberloanoid(int memberloanoid) {
        this.memberloanoid = memberloanoid;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getTrnloanno() {
        return trnloanno;
    }

    public void setTrnloanno(String trnloanno) {
        this.trnloanno = trnloanno;
    }

    public int getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(int trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public String getTrnloandtlnote() {
        return trnloandtlnote;
    }

    public void setTrnloandtlnote(String trnloandtlnote) {
        this.trnloandtlnote = trnloandtlnote;
    }

    public String getTrnpaymentdate() {
        return trnpaymentdate;
    }

    public void setTrnpaymentdate(String trnpaymentdate) {
        this.trnpaymentdate = trnpaymentdate;
    }

    public int getTagihan() {
        return tagihan;
    }

    public void setTagihan(int tagihan) {
        this.tagihan = tagihan;
    }

    public int getTitipanamt() {
        return titipanamt;
    }

    public void setTitipanamt(int titipanamt) {
        this.titipanamt = titipanamt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.memberloanoid);
        dest.writeString(this.membername);
        dest.writeString(this.trnloanno);
        dest.writeInt(this.trnloanamt);
        dest.writeString(this.trnloandtlnote);
        dest.writeString(this.trnpaymentdate);
        dest.writeInt(this.tagihan);
        dest.writeInt(this.titipanamt);
        dest.writeInt(this.acumamt);
    }

    protected ModelRiwayatDetail(Parcel in) {
        this.memberloanoid = in.readInt();
        this.membername = in.readString();
        this.trnloanno = in.readString();
        this.trnloanamt = in.readInt();
        this.trnloandtlnote = in.readString();
        this.trnpaymentdate = in.readString();
        this.tagihan = in.readInt();
        this.titipanamt = in.readInt();
        this.acumamt = in.readInt();
    }

    public static final Creator<ModelRiwayatDetail> CREATOR = new Creator<ModelRiwayatDetail>() {
        @Override
        public ModelRiwayatDetail createFromParcel(Parcel source) {
            return new ModelRiwayatDetail(source);
        }

        @Override
        public ModelRiwayatDetail[] newArray(int size) {
            return new ModelRiwayatDetail[size];
        }
    };
}
