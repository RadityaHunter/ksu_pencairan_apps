package waruagung.com.ksuapplicationPencairanPakisaji.Model;

public class ModelUserRole {

    /**
     * FORMTYPE : ANDROID
     * FORMNAME : APPROVAL PENAGIHAN KOLEKTOR
     * FORMADDRESS : APP_APPROVAL_KOL
     */

    private String FORMTYPE;
    private String FORMNAME;
    private String FORMADDRESS;

    public String getFORMTYPE() {
        return FORMTYPE;
    }

    public void setFORMTYPE(String FORMTYPE) {
        this.FORMTYPE = FORMTYPE;
    }

    public String getFORMNAME() {
        return FORMNAME;
    }

    public void setFORMNAME(String FORMNAME) {
        this.FORMNAME = FORMNAME;
    }

    public String getFORMADDRESS() {
        return FORMADDRESS;
    }

    public void setFORMADDRESS(String FORMADDRESS) {
        this.FORMADDRESS = FORMADDRESS;
    }
}
