package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelSimpanPembayaran implements Parcelable {
    private String sCmp;
    private String loandtloid;
    private String paydate;
    private String paytype;
    private String cashbankacctgoid;
    private String payloanamt;
    private String payinterestamt;
    private String paysanksi;
    private String paytitipan;
    private String paysaving;
    private String paynote;
    private String titipanamtpay;
    private String createuser;
    public ModelSimpanPembayaran(String sCmp, String loandtloid, String paydate, String paytype, String cashbankacctgoid, String payloanamt, String payinterestamt, String paysanksi, String paytitipan, String paysaving, String paynote, String titipanamtpay, String createuser) {
        this.sCmp = sCmp;
        this.loandtloid = loandtloid;
        this.paydate = paydate;
        this.paytype = paytype;
        this.cashbankacctgoid = cashbankacctgoid;
        this.payloanamt = payloanamt;
        this.payinterestamt = payinterestamt;
        this.paysanksi = paysanksi;
        this.paytitipan = paytitipan;
        this.paysaving = paysaving;
        this.paynote = paynote;
        this.titipanamtpay = titipanamtpay;
        this.createuser = createuser;
    }

    public String getsCmp() {
        return sCmp;
    }

    public void setsCmp(String sCmp) {
        this.sCmp = sCmp;
    }

    public String getLoandtloid() {
        return loandtloid;
    }

    public void setLoandtloid(String loandtloid) {
        this.loandtloid = loandtloid;
    }

    public String getPaydate() {
        return paydate;
    }

    public void setPaydate(String paydate) {
        this.paydate = paydate;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public String getCashbankacctgoid() {
        return cashbankacctgoid;
    }

    public void setCashbankacctgoid(String cashbankacctgoid) {
        this.cashbankacctgoid = cashbankacctgoid;
    }

    public String getPayloanamt() {
        return payloanamt;
    }

    public void setPayloanamt(String payloanamt) {
        this.payloanamt = payloanamt;
    }

    public String getPayinterestamt() {
        return payinterestamt;
    }

    public void setPayinterestamt(String payinterestamt) {
        this.payinterestamt = payinterestamt;
    }

    public String getPaysanksi() {
        return paysanksi;
    }

    public void setPaysanksi(String paysanksi) {
        this.paysanksi = paysanksi;
    }

    public String getPaytitipan() {
        return paytitipan;
    }

    public void setPaytitipan(String paytitipan) {
        this.paytitipan = paytitipan;
    }

    public String getPaysaving() {
        return paysaving;
    }

    public void setPaysaving(String paysaving) {
        this.paysaving = paysaving;
    }

    public String getPaynote() {
        return paynote;
    }

    public void setPaynote(String paynote) {
        this.paynote = paynote;
    }

    public String getTitipanamtpay() {
        return titipanamtpay;
    }

    public void setTitipanamtpay(String titipanamtpay) {
        this.titipanamtpay = titipanamtpay;
    }

    public String getCreateuser() {
        return createuser;
    }

    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sCmp);
        dest.writeString(this.loandtloid);
        dest.writeString(this.paydate);
        dest.writeString(this.paytype);
        dest.writeString(this.cashbankacctgoid);
        dest.writeString(this.payloanamt);
        dest.writeString(this.payinterestamt);
        dest.writeString(this.paysanksi);
        dest.writeString(this.paytitipan);
        dest.writeString(this.paysaving);
        dest.writeString(this.paynote);
        dest.writeString(this.titipanamtpay);
        dest.writeString(this.createuser);
    }

    protected ModelSimpanPembayaran(Parcel in) {
        this.sCmp = in.readString();
        this.loandtloid = in.readString();
        this.paydate = in.readString();
        this.paytype = in.readString();
        this.cashbankacctgoid = in.readString();
        this.payloanamt = in.readString();
        this.payinterestamt = in.readString();
        this.paysanksi = in.readString();
        this.paytitipan = in.readString();
        this.paysaving = in.readString();
        this.paynote = in.readString();
        this.titipanamtpay = in.readString();
        this.createuser = in.readString();
    }

    public static final Parcelable.Creator<ModelSimpanPembayaran> CREATOR = new Parcelable.Creator<ModelSimpanPembayaran>() {
        @Override
        public ModelSimpanPembayaran createFromParcel(Parcel source) {
            return new ModelSimpanPembayaran(source);
        }

        @Override
        public ModelSimpanPembayaran[] newArray(int size) {
            return new ModelSimpanPembayaran[size];
        }
    };
}
