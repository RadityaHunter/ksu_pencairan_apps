package waruagung.com.ksuapplicationPencairanPakisaji.Model;

public class ModelDaftarPencairan2 {

    /**
     * membername : test coba2
     * trnloanamt : 1.0E7
     * trnrequestno : 0040.02/REQ/FR/KSP11/MM/yyyy
     * apploancashnote : In Approval Pengawas
     * loandesc : Flat
     * trnloantime : 36
     * apploancashstatus : In Approval
     * agunan :  BPKB MOTOR
     */

    private String membername;
    private double trnloanamt;
    private String trnrequestno;
    private String apploancashnote;
    private String loandesc;
    private int trnloantime;
    private String apploancashstatus;
    private String agunan;

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public double getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(double trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public String getTrnrequestno() {
        return trnrequestno;
    }

    public void setTrnrequestno(String trnrequestno) {
        this.trnrequestno = trnrequestno;
    }

    public String getApploancashnote() {
        return apploancashnote;
    }

    public void setApploancashnote(String apploancashnote) {
        this.apploancashnote = apploancashnote;
    }

    public String getLoandesc() {
        return loandesc;
    }

    public void setLoandesc(String loandesc) {
        this.loandesc = loandesc;
    }

    public int getTrnloantime() {
        return trnloantime;
    }

    public void setTrnloantime(int trnloantime) {
        this.trnloantime = trnloantime;
    }

    public String getApploancashstatus() {
        return apploancashstatus;
    }

    public void setApploancashstatus(String apploancashstatus) {
        this.apploancashstatus = apploancashstatus;
    }

    public String getAgunan() {
        return agunan;
    }

    public void setAgunan(String agunan) {
        this.agunan = agunan;
    }
}
