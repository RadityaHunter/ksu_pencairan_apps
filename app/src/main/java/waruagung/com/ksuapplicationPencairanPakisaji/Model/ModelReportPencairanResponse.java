package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelReportPencairanResponse implements Parcelable {

    /**
     * apploancashoid : 1
     * membername : test coba2
     * trnloanamt : 1.0E7
     * trnloanamtrev : 9794000
     * noac: "001300"
     * nospk: "1869.01",
     * trnrequestno :0035.02/REQ/FR/KSP11/MM/yyyy
     * apploancashnote : Berkas Pencairan sudah di terima oleh kabag
     * loandesc : Flat
     * trnloantime : 36
     * apploancashstatus : Approved
     * agunan :  BPKB MOTOR
     * tglcair : 2020-02-26T15:22:19.43
     * pencair : pimpinan
     * geolat": "-7.966857"
     * geolng": "112.638373"
     */

    private int apploancashoid;
    private String membername;
    private double trnloanamt;
    private int trnloanamtrev;
    private String noac;
    private String nospk;
    private String trnrequestno;
    private String apploancashnote;
    private String loandesc;
    private int trnloantime;
    private String apploancashstatus;
    private String agunan;
    private String tglcair;
    private String pencair;
    private String geolat;
    private String geolng;



    public int getApploancashoid() {
        return apploancashoid;
    }

    public void setApploancashoid(int apploancashoid) {
        this.apploancashoid = apploancashoid;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public double getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(double trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public int getTrnloanamtrev() {
        return trnloanamtrev;
    }

    public void setTrnloanamtrev(int trnloanamtrev) {
        this.trnloanamtrev = trnloanamtrev;
    }

    public String getNoac() {
        return noac;
    }

    public void setNoac(String noac) {
        this.noac = noac;
    }

    public String getNospk() {
        return nospk;
    }

    public void setNospk(String nospk) {
        this.nospk = nospk;
    }

    public String getTrnrequestno() {
        return trnrequestno;
    }

    public void setTrnrequestno(String trnrequestno) {
        this.trnrequestno = trnrequestno;
    }

    public String getApploancashnote() {
        return apploancashnote;
    }

    public void setApploancashnote(String apploancashnote) {
        this.apploancashnote = apploancashnote;
    }

    public String getLoandesc() {
        return loandesc;
    }

    public void setLoandesc(String loandesc) {
        this.loandesc = loandesc;
    }

    public int getTrnloantime() {
        return trnloantime;
    }

    public void setTrnloantime(int trnloantime) {
        this.trnloantime = trnloantime;
    }

    public String getApploancashstatus() {
        return apploancashstatus;
    }

    public void setApploancashstatus(String apploancashstatus) {
        this.apploancashstatus = apploancashstatus;
    }

    public String getAgunan() {
        return agunan;
    }

    public void setAgunan(String agunan) {
        this.agunan = agunan;
    }

    public String getTglcair() {
        return tglcair;
    }

    public void setTglcair(String tglcair) {
        this.tglcair = tglcair;
    }

    public String getPencair() {
        return pencair;
    }

    public void setPencair(String pencair) {
        this.pencair = pencair;
    }

    public String getGeolat() {
        return geolat;
    }

    public void setGeolat(String geolat) {
        this.geolat = geolat;
    }

    public String getGeolng() {
        return geolng;
    }

    public void setGeolng(String geolng) {
        this.geolng = geolng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.apploancashoid);
        dest.writeString(this.membername);
        dest.writeDouble(this.trnloanamt);
        dest.writeInt(this.trnloanamtrev);
        dest.writeValue(this.noac);
        dest.writeValue(this.nospk);
        dest.writeString(this.trnrequestno);
        dest.writeString(this.apploancashnote);
        dest.writeString(this.loandesc);
        dest.writeInt(this.trnloantime);
        dest.writeString(this.apploancashstatus);
        dest.writeString(this.agunan);
        dest.writeString(this.tglcair);
        dest.writeString(this.pencair);
        dest.writeValue(this.geolat);
        dest.writeValue(this.geolng);
    }

    public ModelReportPencairanResponse() {
    }

    protected ModelReportPencairanResponse(Parcel in) {
        this.apploancashoid = in.readInt();
        this.membername = in.readString();
        this.trnloanamt = in.readDouble();
        this.trnloanamtrev = in.readInt();
        this.noac = in.readString();
        this.nospk = in.readString();
        this.trnrequestno = in.readString();
        this.apploancashnote = in.readString();
        this.loandesc = in.readString();
        this.trnloantime = in.readInt();
        this.apploancashstatus = in.readString();
        this.agunan = in.readString();
        this.tglcair = in.readString();
        this.pencair = in.readString();
        this.geolat = in.readString();
        this.geolng = in.readString();
    }

    public static final Parcelable.Creator<ModelReportPencairanResponse> CREATOR = new Parcelable.Creator<ModelReportPencairanResponse>() {
        @Override
        public ModelReportPencairanResponse createFromParcel(Parcel source) {
            return new ModelReportPencairanResponse(source);
        }

        @Override
        public ModelReportPencairanResponse[] newArray(int size) {
            return new ModelReportPencairanResponse[size];
        }
    };
}