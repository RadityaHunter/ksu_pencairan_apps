package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ModelReportPencairanList implements Parcelable {


    private int trnloanoid;
    private String trnloanno;
    private String membername;
    private double trnloanamt;
    private double trnloanamtrev;
    private String cashbankdate;
    private String pencair;
    private int trnloantime;
    private String loandesc;
    private List<ModelDaftarPencairanAgunanList> agunanList;

    public ModelReportPencairanList(int trnloanoid, String trnloanno, String membername, double trnloanamt, double trnloanamtrev, String cashbankdate, String pencair, int trnloantime, String loandesc, List<ModelDaftarPencairanAgunanList> agunanList) {
        this.trnloanoid = trnloanoid;
        this.trnloanno = trnloanno;
        this.membername = membername;
        this.trnloanamt = trnloanamt;
        this.trnloanamtrev = trnloanamtrev;
        this.cashbankdate = cashbankdate;
        this.pencair = pencair;
        this.trnloantime = trnloantime;
        this.loandesc = loandesc;
        this.agunanList = agunanList;
    }

    public int getTrnloanoid() {
        return trnloanoid;
    }

    public void setTrnloanoid(int trnloanoid) {
        this.trnloanoid = trnloanoid;
    }

    public String getTrnloanno() {
        return trnloanno;
    }

    public void setTrnloanno(String trnloanno) {
        this.trnloanno = trnloanno;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public double getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(double trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public double getTrnloanamtrev() {
        return trnloanamtrev;
    }

    public void setTrnloanamtrev(double trnloanamtrev) {
        this.trnloanamtrev = trnloanamtrev;
    }

    public String getCashbankdate() {
        return cashbankdate;
    }

    public void setCashbankdate(String cashbankdate) {
        this.cashbankdate = cashbankdate;
    }

    public String getPencair() {
        return pencair;
    }

    public void setPencair(String pencair) {
        this.pencair = pencair;
    }

    public int getTrnloantime() {
        return trnloantime;
    }

    public void setTrnloantime(int trnloantime) {
        this.trnloantime = trnloantime;
    }

    public String getLoandesc() {
        return loandesc;
    }

    public void setLoandesc(String loandesc) {
        this.loandesc = loandesc;
    }

    public List<ModelDaftarPencairanAgunanList> getAgunanList() {
        return agunanList;
    }

    public void setAgunanList(List<ModelDaftarPencairanAgunanList> agunanList) {
        this.agunanList = agunanList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.trnloanoid);
        dest.writeString(this.trnloanno);
        dest.writeString(this.membername);
        dest.writeDouble(this.trnloanamt);
        dest.writeDouble(this.trnloanamtrev);
        dest.writeString(this.cashbankdate);
        dest.writeString(this.pencair);
        dest.writeInt(this.trnloantime);
        dest.writeString(this.loandesc);
        dest.writeTypedList(this.agunanList);
    }

    public ModelReportPencairanList() {
    }

    protected ModelReportPencairanList(Parcel in) {
        this.trnloanoid = in.readInt();
        this.trnloanno = in.readString();
        this.membername = in.readString();
        this.trnloanamt = in.readDouble();
        this.trnloanamtrev = in.readDouble();
        this.cashbankdate = in.readString();
        this.pencair = in.readString();
        this.trnloantime = in.readInt();
        this.loandesc = in.readString();
        this.agunanList = in.createTypedArrayList(ModelDaftarPencairanAgunanList.CREATOR);
    }

    public static final Parcelable.Creator<ModelReportPencairanList> CREATOR = new Parcelable.Creator<ModelReportPencairanList>() {
        @Override
        public ModelReportPencairanList createFromParcel(Parcel source) {
            return new ModelReportPencairanList(source);
        }

        @Override
        public ModelReportPencairanList[] newArray(int size) {
            return new ModelReportPencairanList[size];
        }
    };
}
