package waruagung.com.ksuapplicationPencairanPakisaji.Model;

public class ModelMenuHome {
    private String menuname;
    private int menuImage,menuId;

    public ModelMenuHome(int menuId, String menuname, int menuImage) {
        this.menuname = menuname;
        this.menuId = menuId;
        this.menuImage = menuImage;
    }

    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public int getMenuImage() {
        return menuImage;
    }

    public void setMenuImage(int menuImage) {
        this.menuImage = menuImage;
    }
}
