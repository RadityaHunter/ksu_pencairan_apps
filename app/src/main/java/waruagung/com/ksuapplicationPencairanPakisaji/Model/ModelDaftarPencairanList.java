package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ModelDaftarPencairanList implements Parcelable {

    private int trnloanoid;
    private String trnloanno;
    private String membermasterno;
    private String membername;
    private String trnloanamt;
    private int trnloantime;
    private int trnloanamtrev;
    private String loandesc;
    private List<ModelDaftarPencairanAgunanList> agunanList;

    public ModelDaftarPencairanList(int trnloanoid, String trnloanno, String membermasterno, String membername, String trnloanamt, int trnloantime, int trnloanamtrev, String loandesc, List<ModelDaftarPencairanAgunanList> agunanList) {
        this.trnloanoid = trnloanoid;
        this.trnloanno = trnloanno;
        this.membermasterno = membermasterno;
        this.membername = membername;
        this.trnloanamt = trnloanamt;
        this.trnloantime = trnloantime;
        this.trnloanamtrev = trnloanamtrev;
        this.loandesc = loandesc;
        this.agunanList = agunanList;
    }

    public int getTrnloanoid() {
        return trnloanoid;
    }

    public void setTrnloanoid(int trnloanoid) {
        this.trnloanoid = trnloanoid;
    }

    public String getTrnloanno() {
        return trnloanno;
    }

    public void setTrnloanno(String trnloanno) {
        this.trnloanno = trnloanno;
    }

    public String getMembermasterno() {
        return membermasterno;
    }

    public void setMembermasterno(String membermasterno) {
        this.membermasterno = membermasterno;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(String trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public int getTrnloantime() {
        return trnloantime;
    }

    public void setTrnloantime(int trnloantime) {
        this.trnloantime = trnloantime;
    }

    public int getTrnloanamtrev() {
        return trnloanamtrev;
    }

    public void setTrnloanamtrev(int trnloanamtrev) {
        this.trnloanamtrev = trnloanamtrev;
    }

    public String getLoandesc() {
        return loandesc;
    }

    public void setLoandesc(String loandesc) {
        this.loandesc = loandesc;
    }

    public List<ModelDaftarPencairanAgunanList> getAgunanList() {
        return agunanList;
    }

    public void setAgunanList(List<ModelDaftarPencairanAgunanList> agunanList) {
        this.agunanList = agunanList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.trnloanoid);
        dest.writeString(this.trnloanno);
        dest.writeString(this.membermasterno);
        dest.writeString(this.membername);
        dest.writeString(this.trnloanamt);
        dest.writeInt(this.trnloantime);
        dest.writeInt(this.trnloanamtrev);
        dest.writeString(this.loandesc);
        dest.writeTypedList(this.agunanList);
    }

    protected ModelDaftarPencairanList(Parcel in) {
        this.trnloanoid = in.readInt();
        this.trnloanno = in.readString();
        this.membermasterno = in.readString();
        this.membername = in.readString();
        this.trnloanamt = in.readString();
        this.trnloantime = in.readInt();
        this.trnloanamtrev = in.readInt();
        this.loandesc = in.readString();
        this.agunanList = in.createTypedArrayList(ModelDaftarPencairanAgunanList.CREATOR);
    }

    public static final Parcelable.Creator<ModelDaftarPencairanList> CREATOR = new Parcelable.Creator<ModelDaftarPencairanList>() {
        @Override
        public ModelDaftarPencairanList createFromParcel(Parcel source) {
            return new ModelDaftarPencairanList(source);
        }

        @Override
        public ModelDaftarPencairanList[] newArray(int size) {
            return new ModelDaftarPencairanList[size];
        }
    };
}

