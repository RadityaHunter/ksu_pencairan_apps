package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelPopUpPembayaran implements Parcelable {

    /**
     * cmpcode : KSP11
     * trnpaymentoid : 14738
     * trnpaymentno : PM/KSP11/2001/0035
     * trnpaymentdate : 2020-01-18T00:00:00
     * memberoid : 6
     * membermasterno : KRD/KSP11/1710/000002.
     * membername : DIDIK SUPRIYADI
     * trnloanno : 1083.02/KRD/SR/KSP11/06/2019
     * trnloanstatus : Approved
     * trnloancashflag : CLOSED
     * trnloanamt : 1500000
     * trnloandtloid : 45267
     * trnloandtlseq : 3
     * trnloandtlnote : Angsuran Ke-3
     * paymentamt : 0
     * interestamt : 27000
     * penaltyamt : 15000
     * titipanamt : 8000
     * titipanamtpay : 0
     * cashbankamt : 50000
     * cashbanktype : BKM
     * trnpaymentstatus : POST
     * count_print : 1
     */

    private String cmpcode;
    private int trnpaymentoid;
    private String trnpaymentno;
    private String trnpaymentdate;
    private int memberoid;
    private String membermasterno;
    private String membername;
    private String trnloanno;
    private String trnloanstatus;
    private String trnloancashflag;
    private int trnloanamt;
    private int trnloandtloid;
    private int trnloandtlseq;
    private String trnloandtlnote;
    private int paymentamt;
    private int interestamt;
    private int penaltyamt;
    private int titipanamt;
    private int titipanamtpay;
    private int cashbankamt;
    private String cashbanktype;
    private String trnpaymentstatus;
    private int count_print;

    public ModelPopUpPembayaran(String cmpcode, int trnpaymentoid, String trnpaymentno, String trnpaymentdate, int memberoid, String membermasterno, String membername, String trnloanno, String trnloanstatus, String trnloancashflag, int trnloanamt, int trnloandtloid, int trnloandtlseq, String trnloandtlnote, int paymentamt, int interestamt, int penaltyamt, int titipanamt, int titipanamtpay, int cashbankamt, String cashbanktype, String trnpaymentstatus, int count_print) {
        this.cmpcode = cmpcode;
        this.trnpaymentoid = trnpaymentoid;
        this.trnpaymentno = trnpaymentno;
        this.trnpaymentdate = trnpaymentdate;
        this.memberoid = memberoid;
        this.membermasterno = membermasterno;
        this.membername = membername;
        this.trnloanno = trnloanno;
        this.trnloanstatus = trnloanstatus;
        this.trnloancashflag = trnloancashflag;
        this.trnloanamt = trnloanamt;
        this.trnloandtloid = trnloandtloid;
        this.trnloandtlseq = trnloandtlseq;
        this.trnloandtlnote = trnloandtlnote;
        this.paymentamt = paymentamt;
        this.interestamt = interestamt;
        this.penaltyamt = penaltyamt;
        this.titipanamt = titipanamt;
        this.titipanamtpay = titipanamtpay;
        this.cashbankamt = cashbankamt;
        this.cashbanktype = cashbanktype;
        this.trnpaymentstatus = trnpaymentstatus;
        this.count_print = count_print;
    }

    public String getCmpcode() {
        return cmpcode;
    }

    public void setCmpcode(String cmpcode) {
        this.cmpcode = cmpcode;
    }

    public int getTrnpaymentoid() {
        return trnpaymentoid;
    }

    public void setTrnpaymentoid(int trnpaymentoid) {
        this.trnpaymentoid = trnpaymentoid;
    }

    public String getTrnpaymentno() {
        return trnpaymentno;
    }

    public void setTrnpaymentno(String trnpaymentno) {
        this.trnpaymentno = trnpaymentno;
    }

    public String getTrnpaymentdate() {
        return trnpaymentdate;
    }

    public void setTrnpaymentdate(String trnpaymentdate) {
        this.trnpaymentdate = trnpaymentdate;
    }

    public int getMemberoid() {
        return memberoid;
    }

    public void setMemberoid(int memberoid) {
        this.memberoid = memberoid;
    }

    public String getMembermasterno() {
        return membermasterno;
    }

    public void setMembermasterno(String membermasterno) {
        this.membermasterno = membermasterno;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public String getTrnloanno() {
        return trnloanno;
    }

    public void setTrnloanno(String trnloanno) {
        this.trnloanno = trnloanno;
    }

    public String getTrnloanstatus() {
        return trnloanstatus;
    }

    public void setTrnloanstatus(String trnloanstatus) {
        this.trnloanstatus = trnloanstatus;
    }

    public String getTrnloancashflag() {
        return trnloancashflag;
    }

    public void setTrnloancashflag(String trnloancashflag) {
        this.trnloancashflag = trnloancashflag;
    }

    public int getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(int trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public int getTrnloandtloid() {
        return trnloandtloid;
    }

    public void setTrnloandtloid(int trnloandtloid) {
        this.trnloandtloid = trnloandtloid;
    }

    public int getTrnloandtlseq() {
        return trnloandtlseq;
    }

    public void setTrnloandtlseq(int trnloandtlseq) {
        this.trnloandtlseq = trnloandtlseq;
    }

    public String getTrnloandtlnote() {
        return trnloandtlnote;
    }

    public void setTrnloandtlnote(String trnloandtlnote) {
        this.trnloandtlnote = trnloandtlnote;
    }

    public int getPaymentamt() {
        return paymentamt;
    }

    public void setPaymentamt(int paymentamt) {
        this.paymentamt = paymentamt;
    }

    public int getInterestamt() {
        return interestamt;
    }

    public void setInterestamt(int interestamt) {
        this.interestamt = interestamt;
    }

    public int getPenaltyamt() {
        return penaltyamt;
    }

    public void setPenaltyamt(int penaltyamt) {
        this.penaltyamt = penaltyamt;
    }

    public int getTitipanamt() {
        return titipanamt;
    }

    public void setTitipanamt(int titipanamt) {
        this.titipanamt = titipanamt;
    }

    public int getTitipanamtpay() {
        return titipanamtpay;
    }

    public void setTitipanamtpay(int titipanamtpay) {
        this.titipanamtpay = titipanamtpay;
    }

    public int getCashbankamt() {
        return cashbankamt;
    }

    public void setCashbankamt(int cashbankamt) {
        this.cashbankamt = cashbankamt;
    }

    public String getCashbanktype() {
        return cashbanktype;
    }

    public void setCashbanktype(String cashbanktype) {
        this.cashbanktype = cashbanktype;
    }

    public String getTrnpaymentstatus() {
        return trnpaymentstatus;
    }

    public void setTrnpaymentstatus(String trnpaymentstatus) {
        this.trnpaymentstatus = trnpaymentstatus;
    }

    public int getCount_print() {
        return count_print;
    }

    public void setCount_print(int count_print) {
        this.count_print = count_print;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cmpcode);
        dest.writeInt(this.trnpaymentoid);
        dest.writeString(this.trnpaymentno);
        dest.writeString(this.trnpaymentdate);
        dest.writeInt(this.memberoid);
        dest.writeString(this.membermasterno);
        dest.writeString(this.membername);
        dest.writeString(this.trnloanno);
        dest.writeString(this.trnloanstatus);
        dest.writeString(this.trnloancashflag);
        dest.writeInt(this.trnloanamt);
        dest.writeInt(this.trnloandtloid);
        dest.writeInt(this.trnloandtlseq);
        dest.writeString(this.trnloandtlnote);
        dest.writeInt(this.paymentamt);
        dest.writeInt(this.interestamt);
        dest.writeInt(this.penaltyamt);
        dest.writeInt(this.titipanamt);
        dest.writeInt(this.titipanamtpay);
        dest.writeInt(this.cashbankamt);
        dest.writeString(this.cashbanktype);
        dest.writeString(this.trnpaymentstatus);
        dest.writeInt(this.count_print);
    }

    protected ModelPopUpPembayaran(Parcel in) {
        this.cmpcode = in.readString();
        this.trnpaymentoid = in.readInt();
        this.trnpaymentno = in.readString();
        this.trnpaymentdate = in.readString();
        this.memberoid = in.readInt();
        this.membermasterno = in.readString();
        this.membername = in.readString();
        this.trnloanno = in.readString();
        this.trnloanstatus = in.readString();
        this.trnloancashflag = in.readString();
        this.trnloanamt = in.readInt();
        this.trnloandtloid = in.readInt();
        this.trnloandtlseq = in.readInt();
        this.trnloandtlnote = in.readString();
        this.paymentamt = in.readInt();
        this.interestamt = in.readInt();
        this.penaltyamt = in.readInt();
        this.titipanamt = in.readInt();
        this.titipanamtpay = in.readInt();
        this.cashbankamt = in.readInt();
        this.cashbanktype = in.readString();
        this.trnpaymentstatus = in.readString();
        this.count_print = in.readInt();
    }

    public static final Parcelable.Creator<ModelPopUpPembayaran> CREATOR = new Parcelable.Creator<ModelPopUpPembayaran>() {
        @Override
        public ModelPopUpPembayaran createFromParcel(Parcel source) {
            return new ModelPopUpPembayaran(source);
        }

        @Override
        public ModelPopUpPembayaran[] newArray(int size) {
            return new ModelPopUpPembayaran[size];
        }
    };
}
