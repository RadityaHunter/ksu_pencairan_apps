package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelFormPinjaman implements Parcelable {


    /**
     * trnloanno : 1043.01/KRD/FR/KSP11/03/2019
     * trnloanoid : 719
     * trnloandtloid : 39415
     * trnloanamt : 3000000
     * loancode : FR
     * loandesc : Flat
     * trnloandtlamt : 250000
     * acumamt : 0
     * trnloandtlinterest : 51000
     * acuminterest : 0
     * trnloandtldate : 2019-08-15T00:00:00
     * trnloandtlseq : 5
     * ttgpokok : 1500000
     * ttgjasa : 306000
     * ttgSanksi : 18100
     * saldotitipan : 0
     * trnloandue : 2020-03-15T00:00:00
     */

    private String trnloanno;
    private int trnloanoid;
    private int trnloandtloid;
    private int trnloanamt;
    private String loancode;
    private String loandesc;
    private int trnloandtlamt;
    private int acumamt;
    private int trnloandtlinterest;
    private int acuminterest;
    private String trnloandtldate;
    private int trnloandtlseq;
    private int ttgpokok;
    private int ttgjasa;
    private int ttgSanksi;
    private int saldotitipan;
    private String trnloandue;

    public ModelFormPinjaman(String trnloanno, int trnloanoid, int trnloandtloid, int trnloanamt, String loancode, String loandesc, int trnloandtlamt, int acumamt, int trnloandtlinterest, int acuminterest, String trnloandtldate, int trnloandtlseq, int ttgpokok, int ttgjasa, int ttgSanksi, int saldotitipan, String trnloandue) {
        this.trnloanno = trnloanno;
        this.trnloanoid = trnloanoid;
        this.trnloandtloid = trnloandtloid;
        this.trnloanamt = trnloanamt;
        this.loancode = loancode;
        this.loandesc = loandesc;
        this.trnloandtlamt = trnloandtlamt;
        this.acumamt = acumamt;
        this.trnloandtlinterest = trnloandtlinterest;
        this.acuminterest = acuminterest;
        this.trnloandtldate = trnloandtldate;
        this.trnloandtlseq = trnloandtlseq;
        this.ttgpokok = ttgpokok;
        this.ttgjasa = ttgjasa;
        this.ttgSanksi = ttgSanksi;
        this.saldotitipan = saldotitipan;
        this.trnloandue = trnloandue;
    }

    public String getTrnloanno() {
        return trnloanno;
    }

    public void setTrnloanno(String trnloanno) {
        this.trnloanno = trnloanno;
    }

    public int getTrnloanoid() {
        return trnloanoid;
    }

    public void setTrnloanoid(int trnloanoid) {
        this.trnloanoid = trnloanoid;
    }

    public int getTrnloandtloid() {
        return trnloandtloid;
    }

    public void setTrnloandtloid(int trnloandtloid) {
        this.trnloandtloid = trnloandtloid;
    }

    public int getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(int trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public String getLoancode() {
        return loancode;
    }

    public void setLoancode(String loancode) {
        this.loancode = loancode;
    }

    public String getLoandesc() {
        return loandesc;
    }

    public void setLoandesc(String loandesc) {
        this.loandesc = loandesc;
    }

    public int getTrnloandtlamt() {
        return trnloandtlamt;
    }

    public void setTrnloandtlamt(int trnloandtlamt) {
        this.trnloandtlamt = trnloandtlamt;
    }

    public int getAcumamt() {
        return acumamt;
    }

    public void setAcumamt(int acumamt) {
        this.acumamt = acumamt;
    }

    public int getTrnloandtlinterest() {
        return trnloandtlinterest;
    }

    public void setTrnloandtlinterest(int trnloandtlinterest) {
        this.trnloandtlinterest = trnloandtlinterest;
    }

    public int getAcuminterest() {
        return acuminterest;
    }

    public void setAcuminterest(int acuminterest) {
        this.acuminterest = acuminterest;
    }

    public String getTrnloandtldate() {
        return trnloandtldate;
    }

    public void setTrnloandtldate(String trnloandtldate) {
        this.trnloandtldate = trnloandtldate;
    }

    public int getTrnloandtlseq() {
        return trnloandtlseq;
    }

    public void setTrnloandtlseq(int trnloandtlseq) {
        this.trnloandtlseq = trnloandtlseq;
    }

    public int getTtgpokok() {
        return ttgpokok;
    }

    public void setTtgpokok(int ttgpokok) {
        this.ttgpokok = ttgpokok;
    }

    public int getTtgjasa() {
        return ttgjasa;
    }

    public void setTtgjasa(int ttgjasa) {
        this.ttgjasa = ttgjasa;
    }

    public int getTtgSanksi() {
        return ttgSanksi;
    }

    public void setTtgSanksi(int ttgSanksi) {
        this.ttgSanksi = ttgSanksi;
    }

    public int getSaldotitipan() {
        return saldotitipan;
    }

    public void setSaldotitipan(int saldotitipan) {
        this.saldotitipan = saldotitipan;
    }

    public String getTrnloandue() {
        return trnloandue;
    }

    public void setTrnloandue(String trnloandue) {
        this.trnloandue = trnloandue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.trnloanno);
        dest.writeInt(this.trnloanoid);
        dest.writeInt(this.trnloandtloid);
        dest.writeInt(this.trnloanamt);
        dest.writeString(this.loancode);
        dest.writeString(this.loandesc);
        dest.writeInt(this.trnloandtlamt);
        dest.writeInt(this.acumamt);
        dest.writeInt(this.trnloandtlinterest);
        dest.writeInt(this.acuminterest);
        dest.writeString(this.trnloandtldate);
        dest.writeInt(this.trnloandtlseq);
        dest.writeInt(this.ttgpokok);
        dest.writeInt(this.ttgjasa);
        dest.writeInt(this.ttgSanksi);
        dest.writeInt(this.saldotitipan);
        dest.writeString(this.trnloandue);
    }

    public ModelFormPinjaman() {
    }

    protected ModelFormPinjaman(Parcel in) {
        this.trnloanno = in.readString();
        this.trnloanoid = in.readInt();
        this.trnloandtloid = in.readInt();
        this.trnloanamt = in.readInt();
        this.loancode = in.readString();
        this.loandesc = in.readString();
        this.trnloandtlamt = in.readInt();
        this.acumamt = in.readInt();
        this.trnloandtlinterest = in.readInt();
        this.acuminterest = in.readInt();
        this.trnloandtldate = in.readString();
        this.trnloandtlseq = in.readInt();
        this.ttgpokok = in.readInt();
        this.ttgjasa = in.readInt();
        this.ttgSanksi = in.readInt();
        this.saldotitipan = in.readInt();
        this.trnloandue = in.readString();
    }

    public static final Creator<ModelFormPinjaman> CREATOR = new Creator<ModelFormPinjaman>() {
        @Override
        public ModelFormPinjaman createFromParcel(Parcel source) {
            return new ModelFormPinjaman(source);
        }

        @Override
        public ModelFormPinjaman[] newArray(int size) {
            return new ModelFormPinjaman[size];
        }
    };
}
