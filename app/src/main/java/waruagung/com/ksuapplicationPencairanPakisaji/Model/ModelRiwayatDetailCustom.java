package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelRiwayatDetailCustom implements Parcelable {
    private String tanggalPembayaran, jenisPencairan, keterangan, buktiPembayaran;
    private Integer jumlahPembayaran, sisaTagihan, jangkaWatu;
    public ModelRiwayatDetailCustom(String tanggalPembayaran, String jenisPencairan, String keterangan, String buktiPembayaran, Integer jumlahPembayaran, Integer sisaTagihan, Integer jangkaWatu) {
        this.tanggalPembayaran = tanggalPembayaran;
        this.jenisPencairan = jenisPencairan;
        this.keterangan = keterangan;
        this.buktiPembayaran = buktiPembayaran;
        this.jumlahPembayaran = jumlahPembayaran;
        this.sisaTagihan = sisaTagihan;
        this.jangkaWatu = jangkaWatu;
    }

    public String getTanggalPembayaran() {
        return tanggalPembayaran;
    }

    public void setTanggalPembayaran(String tanggalPembayaran) {
        this.tanggalPembayaran = tanggalPembayaran;
    }

    public String getJenisPencairan() {
        return jenisPencairan;
    }

    public void setJenisPencairan(String jenisPencairan) {
        this.jenisPencairan = jenisPencairan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getBuktiPembayaran() {
        return buktiPembayaran;
    }

    public void setBuktiPembayaran(String buktiPembayaran) {
        this.buktiPembayaran = buktiPembayaran;
    }

    public Integer getJumlahPembayaran() {
        return jumlahPembayaran;
    }

    public void setJumlahPembayaran(Integer jumlahPembayaran) {
        this.jumlahPembayaran = jumlahPembayaran;
    }

    public Integer getSisaTagihan() {
        return sisaTagihan;
    }

    public void setSisaTagihan(Integer sisaTagihan) {
        this.sisaTagihan = sisaTagihan;
    }

    public Integer getJangkaWatu() {
        return jangkaWatu;
    }

    public void setJangkaWatu(Integer jangkaWatu) {
        this.jangkaWatu = jangkaWatu;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.tanggalPembayaran);
        dest.writeString(this.jenisPencairan);
        dest.writeString(this.keterangan);
        dest.writeString(this.buktiPembayaran);
        dest.writeValue(this.jumlahPembayaran);
        dest.writeValue(this.sisaTagihan);
        dest.writeValue(this.jangkaWatu);
    }

    protected ModelRiwayatDetailCustom(Parcel in) {
        this.tanggalPembayaran = in.readString();
        this.jenisPencairan = in.readString();
        this.keterangan = in.readString();
        this.buktiPembayaran = in.readString();
        this.jumlahPembayaran = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sisaTagihan = (Integer) in.readValue(Integer.class.getClassLoader());
        this.jangkaWatu = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<ModelRiwayatDetailCustom> CREATOR = new Parcelable.Creator<ModelRiwayatDetailCustom>() {
        @Override
        public ModelRiwayatDetailCustom createFromParcel(Parcel source) {
            return new ModelRiwayatDetailCustom(source);
        }

        @Override
        public ModelRiwayatDetailCustom[] newArray(int size) {
            return new ModelRiwayatDetailCustom[size];
        }
    };
}
