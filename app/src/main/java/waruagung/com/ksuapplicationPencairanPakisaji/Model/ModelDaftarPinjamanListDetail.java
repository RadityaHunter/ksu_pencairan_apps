package waruagung.com.ksuapplicationPencairanPakisaji.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelDaftarPinjamanListDetail implements Parcelable {


    /**
     * trnloanoid : 651
     * trnloanno : 1024.01/KRD/FR/KSP11/01/2019
     * trnloanopendate : 2019-01-29T00:00:00
     * trnloanamt : 4000000
     * sisa : 1332800
     * JenisPinjaman : Flat
     * JangkaWaktu : 12
     * JatuhTempo : 2020-01-29T00:00:00
     * Bunga : 1.6
     * BankName :
     * Norek :
     * Nasabah :
     * Keterangan :
     */

    private int trnloanoid;
    private String trnloanno;
    private String trnloanopendate;
    private int trnloanamt;
    private int sisa;
    private String JenisPinjaman;
    private int JangkaWaktu;
    private String JatuhTempo;
    private double Bunga;
    private String BankName;
    private String Norek;
    private String Nasabah;
    private String Keterangan;

    public int getTrnloanoid() {
        return trnloanoid;
    }

    public void setTrnloanoid(int trnloanoid) {
        this.trnloanoid = trnloanoid;
    }

    public String getTrnloanno() {
        return trnloanno;
    }

    public void setTrnloanno(String trnloanno) {
        this.trnloanno = trnloanno;
    }

    public String getTrnloanopendate() {
        return trnloanopendate;
    }

    public void setTrnloanopendate(String trnloanopendate) {
        this.trnloanopendate = trnloanopendate;
    }

    public int getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(int trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public int getSisa() {
        return sisa;
    }

    public void setSisa(int sisa) {
        this.sisa = sisa;
    }

    public String getJenisPinjaman() {
        return JenisPinjaman;
    }

    public void setJenisPinjaman(String JenisPinjaman) {
        this.JenisPinjaman = JenisPinjaman;
    }

    public int getJangkaWaktu() {
        return JangkaWaktu;
    }

    public void setJangkaWaktu(int JangkaWaktu) {
        this.JangkaWaktu = JangkaWaktu;
    }

    public String getJatuhTempo() {
        return JatuhTempo;
    }

    public void setJatuhTempo(String JatuhTempo) {
        this.JatuhTempo = JatuhTempo;
    }

    public double getBunga() {
        return Bunga;
    }

    public void setBunga(double Bunga) {
        this.Bunga = Bunga;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String BankName) {
        this.BankName = BankName;
    }

    public String getNorek() {
        return Norek;
    }

    public void setNorek(String Norek) {
        this.Norek = Norek;
    }

    public String getNasabah() {
        return Nasabah;
    }

    public void setNasabah(String Nasabah) {
        this.Nasabah = Nasabah;
    }

    public String getKeterangan() {
        return Keterangan;
    }

    public void setKeterangan(String Keterangan) {
        this.Keterangan = Keterangan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.trnloanoid);
        dest.writeString(this.trnloanno);
        dest.writeString(this.trnloanopendate);
        dest.writeInt(this.trnloanamt);
        dest.writeInt(this.sisa);
        dest.writeString(this.JenisPinjaman);
        dest.writeInt(this.JangkaWaktu);
        dest.writeString(this.JatuhTempo);
        dest.writeDouble(this.Bunga);
        dest.writeString(this.BankName);
        dest.writeString(this.Norek);
        dest.writeString(this.Nasabah);
        dest.writeString(this.Keterangan);
    }

    public ModelDaftarPinjamanListDetail() {
    }

    protected ModelDaftarPinjamanListDetail(Parcel in) {
        this.trnloanoid = in.readInt();
        this.trnloanno = in.readString();
        this.trnloanopendate = in.readString();
        this.trnloanamt = in.readInt();
        this.sisa = in.readInt();
        this.JenisPinjaman = in.readString();
        this.JangkaWaktu = in.readInt();
        this.JatuhTempo = in.readString();
        this.Bunga = in.readDouble();
        this.BankName = in.readString();
        this.Norek = in.readString();
        this.Nasabah = in.readString();
        this.Keterangan = in.readString();
    }

    public static final Creator<ModelDaftarPinjamanListDetail> CREATOR = new Creator<ModelDaftarPinjamanListDetail>() {
        @Override
        public ModelDaftarPinjamanListDetail createFromParcel(Parcel source) {
            return new ModelDaftarPinjamanListDetail(source);
        }

        @Override
        public ModelDaftarPinjamanListDetail[] newArray(int size) {
            return new ModelDaftarPinjamanListDetail[size];
        }
    };
}
