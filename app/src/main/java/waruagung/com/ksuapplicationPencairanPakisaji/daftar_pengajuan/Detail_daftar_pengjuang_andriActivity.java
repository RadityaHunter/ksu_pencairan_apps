package waruagung.com.ksuapplicationPencairanPakisaji.daftar_pengajuan;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.daftar_pengajuan.adapter.adapter_detail_daftar_pengajuan_andri;
import waruagung.com.ksuapplicationPencairanPakisaji.daftar_pengajuan.model.ModelHeader;
import waruagung.com.ksuapplicationPencairanPakisaji.daftar_pengajuan.model.model_detail_daftar_pengajuan_andri;

import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;

public class Detail_daftar_pengjuang_andriActivity extends AppCompatActivity {
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ExpandableListView)
    ExpandableListView expandableListView1;

    private List<ModelHeader> listDataHeader;
    private HashMap<String, List<model_detail_daftar_pengajuan_andri>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_daftar_pengjuang_andri);
        ButterKnife.bind(this);
        List<model_detail_daftar_pengajuan_andri> m = new ArrayList<>();
        m.add(new model_detail_daftar_pengajuan_andri("PJ547867", "23 Mei 2019", "Rp 15.000.000", "Rp 15.000.000", "Flat", "12 Bulan", "12 September 2019", "0.25%", "Transfer BCA", "98324634 a/n Lusi Aprillia"));

        listDataHeader = new ArrayList<ModelHeader>();
        listDataChild = new HashMap<String, List<model_detail_daftar_pengajuan_andri>>();
        for (int i = 0; i < m.size(); i++) {
            List<model_detail_daftar_pengajuan_andri> itemsadd = new ArrayList<>();
            itemsadd.add(m.get(i));
            listDataHeader.add(new ModelHeader(m.get(i).getKodenama(), m.get(i).getTanggal()));
            listDataChild.put(m.get(i).getKodenama(), itemsadd);
//            listDataChild.put(m.get(i).getTanggal(), itemsadd);
        }

        adapter_detail_daftar_pengajuan_andri expandableListAdapterPinjaman = new adapter_detail_daftar_pengajuan_andri(this, listDataHeader, listDataChild);
        expandableListView1.setAdapter(expandableListAdapterPinjaman);

        //navigationonclik
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(1).setChecked(true);
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
