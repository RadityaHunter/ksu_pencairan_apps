package waruagung.com.ksuapplicationPencairanPakisaji.daftar_pengajuan.model;

public class model_detail_daftar_pengajuan {

    /**
     * nameToko : Toko
     * takeOrder : 09.30-10.30
     * LastOrder : 1000000
     * Piutang : 0
     * Piutang Terlama : 0
     * SisaLimitKredit : 0
     * Status : belum jalan
     */
    private String kodenama;
    private String jumlahpinjaman;
    private String sisapembayaran;
    private String jnspinjaman;
    private String jangkawaktu;
    private String tanggal;
    private String pensenbunga;
    private String isiketerangan;
    private String jenispencairan;
    private String regkening;

    public model_detail_daftar_pengajuan(String kodenama, String tanggal, String jumlahpinjaman, String sisapembayaran, String jnspinjaman, String jangkawaktu, String persenbunga, String isiketerangan, String jenispencairan, String regkening
    ) {
        this.kodenama = kodenama;
        this.tanggal = tanggal;
        this.jumlahpinjaman = jumlahpinjaman;
        this.sisapembayaran = sisapembayaran;
        this.jnspinjaman = jnspinjaman;
        this.jangkawaktu = jangkawaktu;
        this.pensenbunga = persenbunga;
        this.isiketerangan = isiketerangan;
        this.jenispencairan = jenispencairan;
        this.regkening = regkening;
    }
    public String getKodenama() { return kodenama; }

    public void setKodenama(String kodenama) { this.kodenama = kodenama; }


    public String getTanggal() { return tanggal; }

    public void setTanggal(String tanggal) { this.tanggal = tanggal; }



    public String getJumlahpinjaman() { return jumlahpinjaman; }

    public void setJumlahpinjaman(String jumlahpinjaman) { this.jumlahpinjaman = jumlahpinjaman; }



    public String getSisapembayaran() {
        return sisapembayaran;
    }

    public void setSisapembayaran(String sisapembayaran) { this.sisapembayaran = sisapembayaran; }



    public String getJnspinjaman() {
        return jnspinjaman;
    }

    public void setJnspinjaman(String jnspinjaman) { this.jnspinjaman = jnspinjaman; }




    public String getJangkawaktu() {
        return jangkawaktu;
    }

    public void setJangkawaktu(String jangkawaktu) { this.jangkawaktu = jangkawaktu; }




    public String getJenispencairan() {
        return jenispencairan;
    }

    public void setJenispencairan(String jnspinjaman) { this.jenispencairan = jenispencairan; }




    public String getRegkening() {
        return regkening;
    }

    public void setRegkening(String regkening) { this.regkening = regkening; }



}
