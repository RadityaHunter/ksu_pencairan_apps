package waruagung.com.ksuapplicationPencairanPakisaji;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelHeaderNotif implements Parcelable {
    private String title,membername;


    public ModelHeaderNotif(String title, String membername) {
        this.title = title;
        this.membername = membername;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.membername);
    }

    public ModelHeaderNotif(Parcel in) {
        this.title = in.readString();
        this.membername = in.readString();
    }

    public static final Parcelable.Creator<ModelHeaderNotif> CREATOR = new Parcelable.Creator<ModelHeaderNotif>() {
        @Override
        public ModelHeaderNotif createFromParcel(Parcel source) {
            return new ModelHeaderNotif(source);
        }

        @Override
        public ModelHeaderNotif[] newArray(int size) {
            return new ModelHeaderNotif[size];
        }
    };
}
