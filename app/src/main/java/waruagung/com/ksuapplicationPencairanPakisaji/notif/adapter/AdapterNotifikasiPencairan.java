package waruagung.com.ksuapplicationPencairanPakisaji.notif.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.model.ModelNotifCustom;

public class AdapterNotifikasiPencairan extends
        RecyclerView.Adapter<AdapterNotifikasiPencairan.ViewHolder> {

    private static final String TAG = AdapterNotifikasiPencairan.class.getSimpleName();


    private Context context;
    private List<ModelNotifCustom> list;
    private AdapterNotifikasiPencairanCallback mAdapterCallback;
    private int result = -1;

    public AdapterNotifikasiPencairan(Context context, int result, List<ModelNotifCustom> list, AdapterNotifikasiPencairanCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.result = result;
        this.mAdapterCallback = adapterCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notifikasi_pencairan,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder myViewHolder, int position) {
        ModelNotifCustom item = list.get(position);
        myViewHolder.judul.setText(item.getTitle());
        myViewHolder.isi.setText(item.getMassage());
        myViewHolder.tanggal.setVisibility(View.GONE);
//        myViewHolder.jam.setText(listMenu.get(i).getSubmenu2().substring(0,53)+"...");
        myViewHolder.jam.setVisibility(View.GONE);

        Glide.with(context)
                .load( R.drawable.angsuranjatuhtempo).into(myViewHolder.imgIcon);

        myViewHolder.RLitemsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onRowAdapterNotifikasiPencairanClicked(position);
            }
        });
    }

    public void addItems(List<ModelNotifCustom> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgIcon)
        ImageView imgIcon;
        @BindView(R.id.judul)
        TextView judul;
        @BindView(R.id.isi)
        TextView isi;
        @BindView(R.id.tanggal)
        TextView tanggal;
        @BindView(R.id.jam)
        TextView jam;
        @BindView(R.id.liniertanggal)
        LinearLayout liniertanggal;
        @BindView(R.id.text)
        LinearLayout text;
        @BindView(R.id.RLitemsMenu)
        RelativeLayout RLitemsMenu;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AdapterNotifikasiPencairanCallback {
        void onRowAdapterNotifikasiPencairanClicked(int position);
    }
}