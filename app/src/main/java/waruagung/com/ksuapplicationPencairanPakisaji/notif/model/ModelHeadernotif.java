package waruagung.com.ksuapplicationPencairanPakisaji.notif.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelHeadernotif implements Parcelable {

    private String title;


    public ModelHeadernotif(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
    }

    public ModelHeadernotif(Parcel in) {
        this.title = in.readString();
    }


        public static final Creator<ModelHeadernotif> CREATOR = new Creator<ModelHeadernotif>() {
            @Override
            public ModelHeadernotif createFromParcel(Parcel source) {
                return new ModelHeadernotif(source);
            }

            @Override
            public ModelHeadernotif[] newArray(int size) {
                return new ModelHeadernotif[size];
            }
    };
}
