package waruagung.com.ksuapplicationPencairanPakisaji.notif.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.PermintaanPersetujuanPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.model.model_notif;

public class adapter_notifikasi_pencairan extends RecyclerView.Adapter<adapter_notifikasi_pencairan.myViewHolder>{
    static final String EXTRAS_DATA = "EXTRAS_data";
    private Context context;
    private ArrayList<model_notif> listMenu;
    private Intent intent;

    public adapter_notifikasi_pencairan(Context context, ArrayList<model_notif> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @NonNull
    @Override
    public adapter_notifikasi_pencairan.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_notifikasi_pencairan, viewGroup, false);
        return new adapter_notifikasi_pencairan.myViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull adapter_notifikasi_pencairan.myViewHolder myViewHolder, final int i) {
        myViewHolder.judul.setText(listMenu.get(i).getJudul());
        myViewHolder.isi.setText(listMenu.get(i).getIsi());
        myViewHolder.tanggal.setText(listMenu.get(i).getTanggal());
//        myViewHolder.jam.setText(listMenu.get(i).getSubmenu2().substring(0,53)+"...");
        myViewHolder.jam.setText(listMenu.get(i).getJam());

        Glide.with(context)
                .load(listMenu.get(i).getImgIcon()).into(myViewHolder.MenuImages);

        myViewHolder.RLitemsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = listMenu.get(i).getKodeMenu();
                MenuKategori(id, v);
            }
        });

    }

    private void MenuKategori(int id, View v) {
        switch (id) {
            case 1:
//            Langsung Laku
                intent = new Intent(context, PermintaanPersetujuanPencairanActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
//
//            case 2:
//                //Toko Member
//                intent = new Intent(context, Detail_notifActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;


        }
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.RLitemsMenu)
        RelativeLayout RLitemsMenu;
//        @BindView(R.id.RLitemsMenu2)
//        LinearLayout RLitemsMenu2;
        @BindView(R.id.judul)
        TextView judul;
        @BindView(R.id.isi)
        TextView isi;
        @BindView(R.id.tanggal)
        TextView tanggal;
        @BindView(R.id.jam)
        TextView jam;
        @BindView(R.id.imgIcon)
        ImageView MenuImages;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
