package waruagung.com.ksuapplicationPencairanPakisaji.notif;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelNotifPenagihan;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;

import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.ModelHeaderNotif;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.adapter.adapter_detail_notif;

public class Detail_notifActivity extends AppCompatActivity {
    private static final String TAG = "";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.ExpandableListView)
    ExpandableListView expandableListView;
    @BindView(R.id.submenu1)
    TextView submenu1;
    @BindView(R.id.tgl_toolbar)
    TextView tglToolbar;

    //    private List<ModelHeadernotif> listDataHeader;
//    private HashMap<String, List<model_detail_notif>> listDataChild;
    private APIInterface apiInterface;
    private APIClient ApiClient;
    private GridLayoutManager gridLayoutManager;
    private SessionManager sessionManager;
    private List<ModelHeaderNotif> listDataHeader;
    private HashMap<String, List<ModelNotifPenagihan>> listDataChild;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_notif);
        ButterKnife.bind(this);
        //        //datepacker
        Calendar calendar = Calendar.getInstance();
        String currentDate = new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime());
        TextView textViewDate = findViewById(R.id.submenu1);
        textViewDate.setText("Berikut adalah jadwal penagihan angsuran nasabah.");


//        //        //datepacker

        Calendar c1 = Calendar.getInstance();
        SimpleDateFormat sdf1 = new SimpleDateFormat("d/M/yyyy h:m:s a");
//SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        String strdate1 = sdf1.format(c1.getTime());
        TextView txtdate1 = findViewById(R.id.tgl_toolbar);
        txtdate1.setText(strdate1);
        String dt = currentDate;  // Start date

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(currentDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        c.add(Calendar.DATE, 2);  // number of days to add
        dt = sdf.format(c.getTime());  // dt is now the new date
        submenu1.setText(String.format("Berikut adalah jadwal penagihan angsuran nasabah. Sampai tanggal %s", dt));
        restoreActionBar();
//        List<model_detail_notif> m = new ArrayList<>();
//        m.add(new model_detail_notif("LA782682 - LUSI APRILLIA",": Lusi Aprillia", ": LA728682", ": PJ67575", ": Rp 12.000.000", ": 3", ": Rp 2.000.000", ": Senin, 07 Juni 2019", ": BCA 79867687 - KSU"));
//        m.add(new model_detail_notif("EP118762 - EKA PUTRI",": Eka Putri", ": EP118762", ": PJ67675", ": Rp 12.000.000", ": 3", ": Rp 2.000.000", ": Senin, 07 Juni 2019", ": BCA 79867687 - KSU"));
//        m.add(new model_detail_notif("UL219282 - ULIL LATIFAH",": Ulil Latifah", ": UL219282", ": PJ67576", ": Rp 12.000.000", ": 3", ": Rp 2.000.000", ": Senin, 07 Juni 2019", ": BCA 79867687 - KSU"));
//
//        listDataHeader = new ArrayList<ModelHeadernotif>();
//        listDataChild = new HashMap<String, List<model_detail_notif>>();
//        for (int i = 0; i < m.size(); i++) {
//            List<model_detail_notif> itemsadd = new ArrayList<>();
//            itemsadd.add(m.get(i));
//            listDataHeader.add(new ModelHeadernotif(m.get(i).getKodedannama()));
//            listDataChild.put(m.get(i).getKodedannama(), itemsadd);
//        }
//
//        adapter_detail_notif expandableListAdapternotif= new adapter_detail_notif(this, listDataHeader, listDataChild);
//        expandableListView.setAdapter(expandableListAdapternotif);
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        intent = getIntent();
//        String contents = intent.getStringExtra("catId");
//        String namaId = intent.getStringExtra("namaId");
//        String Id = intent.getStringExtra("Id");
//        String hargaId = intent.getStringExtra("hargaId");
//        String pinjamanId = intent.getStringExtra("pinjamanId");
////        nama.setText(namaId);
//        kodeid.setText(Id);
//        jumlahuang.setText("Rp " + MainActivity.nf.format(Integer.parseInt(hargaId)));
//        pinjaman.setText(pinjamanId);
//        Log.e(TAG, "onCreate: " + contents);

        Call<List<ModelNotifPenagihan>> call = apiInterface.doGetAPINotifikasi_JadwalPenagihan(Http.getsCmp());
        call.enqueue(new Callback<List<ModelNotifPenagihan>>() {
            @Override
            public void onResponse(Call<List<ModelNotifPenagihan>> call, Response<List<ModelNotifPenagihan>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    try {
                        List<ModelNotifPenagihan> m = response.body();
                        if (m.size() >= 1) {

                            listDataHeader = new ArrayList<ModelHeaderNotif>();
                            listDataChild = new HashMap<String, List<ModelNotifPenagihan>>();

                            for (int i = 0; i < m.size(); i++) {
                                List<ModelNotifPenagihan> itemsadd = new ArrayList<>();
                                itemsadd.add(m.get(i));
                                listDataHeader.add(new ModelHeaderNotif(m.get(i).getMembermasterno(), m.get(i).getMembername()));
                                listDataChild.put(m.get(i).getMembermasterno(), itemsadd);
                            }

                            adapter_detail_notif adapter_detail_notif = new adapter_detail_notif(getApplicationContext(), listDataHeader, listDataChild);
                            expandableListView.setAdapter(adapter_detail_notif);
                        }
                    } catch (Exception e) {
                        Toast.makeText(Detail_notifActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(Detail_notifActivity.this, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<ModelNotifPenagihan>> call, Throwable t) {

            }
        });
        //navigationonclik
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(1).setChecked(true);
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });

    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
