package waruagung.com.ksuapplicationPencairanPakisaji.notif.model;

import android.os.Parcel;
import android.os.Parcelable;

public class model_notif implements Parcelable {
    public static final Parcelable.Creator<model_notif> CREATOR = new Parcelable.Creator<model_notif>() {
        @Override
        public model_notif createFromParcel(Parcel source) {
            return new model_notif(source);
        }

        @Override
        public model_notif[] newArray(int size) {
            return new model_notif[size];
        }
    };

    private String judul, isi, tanggal, jam;
    private int kodeMenu,imgIcon;


    public model_notif(String judul,int imgIcon, String isi, String tanggal, int kodeMenu, String jam) {
        this.judul = judul;
        this.isi = isi;
        this.tanggal = tanggal;
        this.jam = jam;
        this.kodeMenu = kodeMenu;
        this.imgIcon = imgIcon;

    }

    protected model_notif(Parcel in) {
        this.judul = in.readString();
        this.isi = in.readString();
        this.tanggal = in.readString();
        this.jam = in.readString();
        this.kodeMenu = in.readInt();
        this.imgIcon = in.readInt();
    }

    public String getJudul() { return judul; }

    public void setJudul(String judul) {
        this.judul = judul;
    }



    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }



    public String getTanggal() { return tanggal; }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }



    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }



    public int getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(int kodeMenu) {
        this.kodeMenu = kodeMenu;
    }


    public int getImgIcon() { return imgIcon; }

    public void setImgIcon(int imgIcon) {this. imgIcon = imgIcon; }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.judul);
        dest.writeString(this.isi);
        dest.writeString(this.tanggal);
        dest.writeInt(this.kodeMenu);
        dest.writeString(this.jam);
        dest.writeInt(this.imgIcon);

    }
}
