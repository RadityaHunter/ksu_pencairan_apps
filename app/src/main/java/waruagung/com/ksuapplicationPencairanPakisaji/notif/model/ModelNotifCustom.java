package waruagung.com.ksuapplicationPencairanPakisaji.notif.model;


import android.os.Parcel;
import android.os.Parcelable;

public class ModelNotifCustom implements Parcelable {
    public static final Parcelable.Creator<ModelNotifCustom> CREATOR = new Parcelable.Creator<ModelNotifCustom>() {
        @Override
        public ModelNotifCustom createFromParcel(Parcel source) {
            return new ModelNotifCustom(source);
        }

        @Override
        public ModelNotifCustom[] newArray(int size) {
            return new ModelNotifCustom[size];
        }
    };
    private String title, massage;
    private ModelNotifPencairan modelNotifPencairan;

    public ModelNotifCustom(String title, String massage, ModelNotifPencairan modelNotifPencairan) {
        this.title = title;
        this.massage = massage;
        this.modelNotifPencairan = modelNotifPencairan;
    }

    protected ModelNotifCustom(Parcel in) {
        this.title = in.readString();
        this.massage = in.readString();
        this.modelNotifPencairan = in.readParcelable(ModelNotifPencairan.class.getClassLoader());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public ModelNotifPencairan getModelNotifPencairan() {
        return modelNotifPencairan;
    }

    public void setModelNotifPencairan(ModelNotifPencairan modelNotifPencairan) {
        this.modelNotifPencairan = modelNotifPencairan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.massage);
        dest.writeParcelable(this.modelNotifPencairan, flags);
    }
}
