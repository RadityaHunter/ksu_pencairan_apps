package waruagung.com.ksuapplicationPencairanPakisaji.notif;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.text.MessageFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.model.ModelNotifCustom;

public class PermintaanPersetujuanPencairanActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tanggal)
    TextView tanggal;
    @BindView(R.id.nama_nasabah)
    TextView namaNasabah;
    @BindView(R.id.nomor_pinjaman)
    TextView nomorPinjaman;
    @BindView(R.id.jumlah_pinjaman)
    TextView jumlahPinjaman;
    @BindView(R.id.jumlahTerima)
    TextView jumlahTerima;
    private ModelNotifCustom modelNotifCustom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permintaan_persetujuan_pencairan);
        ButterKnife.bind(this);
        restoreActionBar();
        if (getIntent().hasExtra("DATA")) {
            modelNotifCustom = getIntent().getParcelableExtra("DATA");
            if (modelNotifCustom != null) {
                namaNasabah.setText(modelNotifCustom.getModelNotifPencairan().getMembername());
                nomorPinjaman.setText(modelNotifCustom.getModelNotifPencairan().getTrnrequestno());
                jumlahPinjaman.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format(modelNotifCustom.getModelNotifPencairan().getTrnloanamt())));
                jumlahTerima.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format(modelNotifCustom.getModelNotifPencairan().getTrnloanamtrev())));

            }

        } else {
            finish();
        }
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
