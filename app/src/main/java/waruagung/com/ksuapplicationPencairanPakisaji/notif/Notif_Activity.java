package waruagung.com.ksuapplicationPencairanPakisaji.notif;

import android.content.Intent;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.adapter.adapter_notif;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.model.model_notif;

public class Notif_Activity extends AppCompatActivity {
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.RvNotif)
    RecyclerView RvNotif;
    private Intent intent;
    private RecyclerView.Adapter adapter;
    private GridLayoutManager gridLayoutManager;
    private List<Icon> iconList;
    private int spanCount = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif_);
        ButterKnife.bind(this);
        notiflonceng();
        restoreActionBar();

        //navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setChecked(false);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setCheckable(false);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });

    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void notiflonceng() {
        RvNotif.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false));
        RvNotif.setHasFixedSize(true);
        RvNotif.setNestedScrollingEnabled(false);
        ArrayList<model_notif> listCategory14 = new ArrayList<>();
        listCategory14.add(new model_notif("Angsuran Jatuh Tempo", R.drawable.angsuranjatuhtempo, "Berikut adalah daftar nasabah Anda yang memasuki jatuh tempo, besok pada hari Senin, 07 Juni 2019", "6 Juni 2019,", 1, "15:30"));
        listCategory14.add(new model_notif("Angsuran Jatuh Tempo", R.drawable.angsuranjatuhtempo, "Berikut adalah daftar nasabah Anda yang memasuki jatuh tempo, besok pada hari Senin, 07 Mei 2019", "6 Mei 2019,", 2, "15:30"));
        listCategory14.add(new model_notif("Angsuran Jatuh Tempo", R.drawable.angsuranjatuhtempo, "Berikut adalah daftar nasabah Anda yang memasuki jatuh tempo, besok pada hari Senin, 07 April 2019", "6 April 2019,", 3, "15:30"));
        listCategory14.add(new model_notif("Angsuran Jatuh Tempo", R.drawable.angsuranjatuhtempo, "Berikut adalah daftar nasabah Anda yang memasuki jatuh tempo, besok pada hari Senin, 07 Maret 2019", "6 Maret 2019,", 4, "15:30"));
        adapter_notif adapter_notif = new adapter_notif(this, listCategory14);
        RvNotif.setAdapter(adapter_notif);
    }
}
