package waruagung.com.ksuapplicationPencairanPakisaji.notif.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.AngsuranJatuhTempoActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.Detail_notifActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.model.model_notif;

public class adapter_notif extends RecyclerView.Adapter<adapter_notif.myViewHolder>{
    static final String EXTRAS_DATA = "EXTRAS_data";
    private Context context;
    private ArrayList<model_notif> listMenu;
    private Intent intent;

    public adapter_notif(Context context, ArrayList<model_notif> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @NonNull
    @Override
    public adapter_notif.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.desain_adapternotif, viewGroup, false);
        return new adapter_notif.myViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull adapter_notif.myViewHolder myViewHolder, final int i) {
        myViewHolder.judul.setText(listMenu.get(i).getJudul());
        myViewHolder.isi.setText(listMenu.get(i).getIsi());
        myViewHolder.tanggal.setText(listMenu.get(i).getTanggal());
//        myViewHolder.jam.setText(listMenu.get(i).getSubmenu2().substring(0,53)+"...");
        myViewHolder.jam.setText(listMenu.get(i).getJam());

        Glide.with(context)
                .load(listMenu.get(i).getImgIcon()).into(myViewHolder.MenuImages);

        myViewHolder.RLitemsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = listMenu.get(i).getKodeMenu();
                MenuKategori(id, v);
            }
        });

    }

    private void MenuKategori(int id, View v) {
        switch (id) {
            case 1:
//            Langsung Laku
                intent = new Intent(context, AngsuranJatuhTempoActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
//
            case 2:
                //Toko Member
                intent = new Intent(context, Detail_notifActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
//            case 3:
            //Pinjaman Online
//                intent = new Intent(context, ProfilMemberdddActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 4:
            //Tukar Tambah
//                intent = new Intent(context, ProfilMemberActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 5:
//                //Hotel
//                intent = new Intent(context, HotelActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 6:
            //Tiket Event
//                intent = new Intent(context, ProfilMemberActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 7:
////            Komisi
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 8:
////            TokoCabang
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 9:
////            Power Merchant
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 10:
////            Seller Center
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 11:
////            TopAds
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 12:
////            Pulsa
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 1);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 13:
////            Paket Data
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 2);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 14:
//                //Pascabayar
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 3);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 15:
//                //Roaming
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 4);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 16:
//                //Air PDAM
//                intent = new Intent(context, AirPDAMActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 17:
//                //Angsuran Kredit
//                intent = new Intent(context, AngsuranKreditActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 18:
//                //Belajar
//                intent = new Intent(context, BelajarActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 22:
//                //Topup OVO
//                intent = new Intent(context, OVOActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 23:
//                //Donasi
//                intent = new Intent(context, DonasiActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 25:
//                //Penerimaan negara
//                intent = new Intent(context, PenerimaannegaraActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;


        }
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.RLitemsMenu)
        RelativeLayout RLitemsMenu;
//        @BindView(R.id.RLitemsMenu2)
//        LinearLayout RLitemsMenu2;
        @BindView(R.id.judul)
        TextView judul;
        @BindView(R.id.isi)
        TextView isi;
        @BindView(R.id.tanggal)
        TextView tanggal;
        @BindView(R.id.jam)
        TextView jam;
        @BindView(R.id.imgIcon)
        ImageView MenuImages;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
