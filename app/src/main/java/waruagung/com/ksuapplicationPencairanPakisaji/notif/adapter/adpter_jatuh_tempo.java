package waruagung.com.ksuapplicationPencairanPakisaji.notif.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelJatuhTempo;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.ModelHeaderNotif;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class adpter_jatuh_tempo extends BaseExpandableListAdapter {
    private static final String TAG = adapter_detail_notif.class.getSimpleName();
    //    private List<waruagung.com.ksuapplicationKolektor.notif.model.ModelHeadernotif> expandableListTitle;
//    private HashMap<String, List<model_detail_notif>> expandableListDetail;
    private List<ModelHeaderNotif> expandableListTitle;
    private HashMap<String, List<ModelJatuhTempo>> expandableListDetail;
    private Context context;


    public adpter_jatuh_tempo(Context context, List<ModelHeaderNotif> title, HashMap<String, List<ModelJatuhTempo>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = title;
        this.expandableListDetail = expandableListDetail;
    }




    @Override

    public int getGroupCount() {
        return expandableListTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition).getTitle()).size();
    }

    //    @Override
//    public waruagung.com.ksuapplicationKolektor.notif.model.ModelHeadernotif getGroup(int groupPosition) {
//        return expandableListTitle.get(groupPosition);
//    }
    @Override
    public ModelHeaderNotif getGroup(int groupPosition) {
        return expandableListTitle.get(groupPosition);
    }



    @Override
    public ModelJatuhTempo getChild(int groupPosition, int childPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition).getTitle())
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }






    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ModelHeaderNotif listTitle = getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_detail_hider_desain_notif, null);
        }
//        TextView listTitleTextView = convertView
//                .findViewById(R.id.kodedannama);
//
//
////       listTitleTextView.setTypeface(null, Typeface.BOLD);
//        listTitleTextView.setText(listTitle.getTitle());


        TextView listTitleTextView = convertView
                .findViewById(R.id.kodedannama);
        listTitleTextView.setText(listTitle.getTitle());

        TextView listnama = convertView
                .findViewById(R.id.nama);
        listnama.setText(listTitle.getMembername());
//        listtanggal.setTypeface(null, Typeface.BOLD);
//        try {
//            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(listTitle.getDate());
//            String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
//            listtanggal.setText(formattedDate);
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        if (isExpanded) {
            listTitleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.panahbawah, 0, 0, 0);
        } else {
            listTitleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.panahatas, 0, 0, 0);
        }
        return convertView;

    }





    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ModelJatuhTempo jsonInString = getChild(groupPosition, childPosition);
        JSONObject jsonObject = null;


        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.desain_expandable_detail_notif, null);
        }

        TextView nama = convertView
                .findViewById(R.id.nama);
        nama.setText(String.valueOf(jsonInString.getMembername()));
        TextView idanggota = convertView
                .findViewById(R.id.idangota);
        idanggota.setText(jsonInString.getMembermasterno());
        TextView nomorpinjaman = convertView
                .findViewById(R.id.NomorPinjaman);
        nomorpinjaman.setText(jsonInString.getTrnloanno());

        TextView sisapinjaman = convertView
                .findViewById(R.id.sisapinjaman);
        sisapinjaman.setText("Rp " + MainActivity.nf.format(jsonInString.getTrnloanamt()));

        TextView tvAngsuran = convertView
                .findViewById(R.id.tvAngsuran);
//            tvAngsuran.setText(jsonInString.getTrnloandtlseq());
        tvAngsuran.setText(String.valueOf(jsonInString.getNoAngsuran()));
        TextView jumlahangsuran = convertView
                .findViewById(R.id.jumlahangsuran);
        jumlahangsuran.setText("Rp " + MainActivity.nf.format(jsonInString.getTrnloandtlinterest()));

        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(jsonInString.getTrnloandtldate());
            String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
            TextView jatuhtempo = convertView
                    .findViewById(R.id.jatuhtempo);
            jatuhtempo.setText(formattedDate);
        } catch (Exception e) {
            Log.d(TAG, "getChildView: Error "+e.getMessage());
        }



        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
