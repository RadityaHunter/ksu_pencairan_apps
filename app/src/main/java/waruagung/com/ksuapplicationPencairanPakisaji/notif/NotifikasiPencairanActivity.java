package waruagung.com.ksuapplicationPencairanPakisaji.notif;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.VolleyErrorHelper;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.adapter.AdapterNotifikasiPencairan;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.model.ModelNotifCustom;
import waruagung.com.ksuapplicationPencairanPakisaji.notif.model.ModelNotifPencairan;

public class NotifikasiPencairanActivity extends AppCompatActivity implements AdapterNotifikasiPencairan.AdapterNotifikasiPencairanCallback {

    public static final String TAG = NotifikasiPencairanActivity.class.getSimpleName();
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.RvNotifPencairan)
    RecyclerView RvNotifPencairan;
    @BindView(R.id.swap)
    SwipeRefreshLayout swap;
    RecyclerView.LayoutManager layoutManager;
    private Intent intent;
    private RecyclerView.Adapter adapter;
    private GridLayoutManager gridLayoutManager;
    private List<Icon> iconList;
    private int spanCount = 1;
    private SessionManager sessionManager;
    private List<ModelNotifPencairan> modelNotifPencairan;
    private String ACTIONAPPROVE;
    private List<ModelNotifCustom> c = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifikasi_pencairan);
        ButterKnife.bind(this);
        restoreActionBar();
        sessionManager = new SessionManager(this);
        try {
            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    ACTIONAPPROVE = "APP_PENCAIRAN_LAPANGAN";
                }
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_PENGAWAS")) {
                    ACTIONAPPROVE = "APP_PENCAIRAN_PENGAWAS";
                }
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_KABAG")) {
                    ACTIONAPPROVE = "APP_PENCAIRAN_KABAG";
                }
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_CASHIER")) {
                    ACTIONAPPROVE = "APP_PENCAIRAN_CASHIER";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        swap.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                DaftarPencairan("");
                swap.setRefreshing(false);

            }
        });
//        NotifikasiPencairan();
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void DaftarPencairan(String s) {
//        Log.e(TAG, "onResponse: " + modelNotifPencairan.size());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NotifikasiPencairanActivity.this);
        RvNotifPencairan.setLayoutManager(linearLayoutManager);
        RvNotifPencairan.setHasFixedSize(true);
        RvNotifPencairan.setNestedScrollingEnabled(false);
        RvNotifPencairan.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                swap.setEnabled(linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0);
            }
        });
        RvNotifPencairan.setAdapter(null);
//        String url = Http.getUrl() + "Pencairan_List?sCmp=" + Http.getsCmp() + "&membername=" + s;
        String url = Http.getUrl() + "GetNotifApprovalPencairan?sCmp=" + Http.getsCmp() + "&useroid=" + sessionManager.getPID();
        Log.e(TAG, "rvlaporan: " + url);
        final ProgressDialog dialog1 = new ProgressDialog(NotifikasiPencairanActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Memuat Data...");
        dialog1.show();
        RequestQueue mQueue = Volley.newRequestQueue(NotifikasiPencairanActivity.this);
        //        Log.e(TAG, "accessWebService: Katalog Start with Token " + sessionManager.getKeyToken());
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onResponse(String response) {
                        dialog1.dismiss();

                        try {

                            Log.d(TAG, "onResponse() called with: response = " + response);
                            if (response != null && !response.equals("[]")) {
//                                versi1(response);
                                setUpNotif(response);
                            } else {
                                Toast.makeText(NotifikasiPencairanActivity.this, "Tidak Ada Notifikasi.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse() called with: response  Exception= [" + e.getMessage() + "]");
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                String responseBodyError = VolleyErrorHelper.getMessage(error, NotifikasiPencairanActivity.this);
                Toast.makeText(NotifikasiPencairanActivity.this, responseBodyError, Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/json");
                //                params.put("Authorization", "Bearer " + sessionManager.getKeyToken());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjectRequest);
    }

    private void setUpNotif(String response) {
        modelNotifPencairan = new Gson().fromJson(response, new TypeToken<List<ModelNotifPencairan>>() {
        }.getType());
        c.clear();
        for (ModelNotifPencairan m : modelNotifPencairan) {
            c.add(new ModelNotifCustom("Permintaan Persetujuan Pencairan",
                    "Berikut adalah daftar nasabah " + m.getMembername() + " yang meminta persetujuan pencairan, " +
                            "dengan No Request " + m.getTrnrequestno() + " dengan nilai pinjaman Rp. " + MainActivity.nf.format(m.getTrnloanamt()), m));
        }
        AdapterNotifikasiPencairan adapterNotifikasiPencairan = new AdapterNotifikasiPencairan(NotifikasiPencairanActivity.this,
                -1, c, NotifikasiPencairanActivity.this);
        RvNotifPencairan.setAdapter(adapterNotifikasiPencairan);
    }

    @Override
    public void onRowAdapterNotifikasiPencairanClicked(int position) {
        ModelNotifCustom modelNotifCustom = c.get(position);
        Intent intent = new Intent(NotifikasiPencairanActivity.this, PermintaanPersetujuanPencairanActivity.class);
        intent.putExtra("DATA", modelNotifCustom);
        startActivity(intent);
    }
}
