package waruagung.com.ksuapplicationPencairanPakisaji.notif.model;

public class model_detail_notif {

    /**
     * nameToko : Toko
     * takeOrder : 09.30-10.30
     * LastOrder : 1000000
     * Piutang : 0
     * Piutang Terlama : 0
     * SisaLimitKredit : 0
     * Status : belum jalan
     */
    private String kodedannama;
    private String nama;
    private String idanggota;
    private String nomorpinjaman;
    private String sisapinjaman;
    private String angsuranke;
    private String jumlahangsuran;
    private String jatuhtempo;
    private String rekeningbayar;


    public model_detail_notif(String kodedannama,String nama, String idanggota, String nomorpinjaman, String sisapinjaman, String angsuranke, String jumlahangsuran, String jatuhtempo, String rekeningbayar
    ) {
        this.kodedannama = kodedannama;
        this.nama = nama;
        this.idanggota = idanggota;
        this.nomorpinjaman = nomorpinjaman;
        this.sisapinjaman = sisapinjaman;
        this.angsuranke = angsuranke;
        this.jumlahangsuran = jumlahangsuran;
        this.jatuhtempo = jatuhtempo;
        this.rekeningbayar = rekeningbayar;
    }



    public String getKodedannama() { return kodedannama; }

    public void setKodedannama(String kodedannama) { this.kodedannama = kodedannama; }


    public String getNama() { return nama; }

    public void setNama(String nama) { this.nama = nama; }


    public String getIdanggota() { return idanggota; }

    public void setIdanggota(String idanggota) { this.idanggota = idanggota; }



    public String getNomorpinjaman() { return nomorpinjaman; }

    public void setNomorpinjaman(String nomorpinjaman) { this.nomorpinjaman = nomorpinjaman; }



    public String getSisapinjaman() {
        return sisapinjaman;
    }

    public void setSisapinjaman(String sisapinjaman) { this.sisapinjaman = sisapinjaman; }



    public String getAngsuranke() {
        return angsuranke;
    }

    public void setAngsuranke(String angsuranke) { this.angsuranke = angsuranke; }




    public String getJumlahangsuran() {
        return jumlahangsuran;
    }

    public void setJumlahangsuran(String jumlahangsuran) { this.jumlahangsuran = jumlahangsuran; }




    public String getJatuhtempo() {
        return jatuhtempo;
    }

    public void setJatuhtempo(String jatuhtempo) { this.jatuhtempo = jatuhtempo; }




    public String getRekeningbayar() { return rekeningbayar; }

    public void setRekeningbayar(String rekeningbayar) { this.rekeningbayar = rekeningbayar; }



}
