package waruagung.com.ksuapplicationPencairanPakisaji.notif.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelNotifPencairan implements Parcelable {


    /**
     * apploancashoid : 7
     * membername : LASTINI
     * trnloanamt : 8855000
     * trnloanamtrev : 8855000
     * trnrequestno : 0035.02/REQ/FR/KSP11/MM/yyyy
     * apploancashnote : Menunggu Persetujuan Pengawas
     * loandesc : Flat
     * trnloantime : 10
     * apploancashstatus : In Approval
     * agunan :  BPKB MOTOR, BPKB MOTOR
     */

    private int apploancashoid;
    private String membername;
    private int trnloanamt;
    private int trnloanamtrev;
    private String trnrequestno;
    private String apploancashnote;
    private String loandesc;
    private int trnloantime;
    private String apploancashstatus;
    private String agunan;

    public int getApploancashoid() {
        return apploancashoid;
    }

    public void setApploancashoid(int apploancashoid) {
        this.apploancashoid = apploancashoid;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public int getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(int trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public int getTrnloanamtrev() {
        return trnloanamtrev;
    }

    public void setTrnloanamtrev(int trnloanamtrev) {
        this.trnloanamtrev = trnloanamtrev;
    }

    public String getTrnrequestno() {
        return trnrequestno;
    }

    public void setTrnrequestno(String trnrequestno) {
        this.trnrequestno = trnrequestno;
    }

    public String getApploancashnote() {
        return apploancashnote;
    }

    public void setApploancashnote(String apploancashnote) {
        this.apploancashnote = apploancashnote;
    }

    public String getLoandesc() {
        return loandesc;
    }

    public void setLoandesc(String loandesc) {
        this.loandesc = loandesc;
    }

    public int getTrnloantime() {
        return trnloantime;
    }

    public void setTrnloantime(int trnloantime) {
        this.trnloantime = trnloantime;
    }

    public String getApploancashstatus() {
        return apploancashstatus;
    }

    public void setApploancashstatus(String apploancashstatus) {
        this.apploancashstatus = apploancashstatus;
    }

    public String getAgunan() {
        return agunan;
    }

    public void setAgunan(String agunan) {
        this.agunan = agunan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.apploancashoid);
        dest.writeString(this.membername);
        dest.writeInt(this.trnloanamt);
        dest.writeInt(this.trnloanamtrev);
        dest.writeString(this.trnrequestno);
        dest.writeString(this.apploancashnote);
        dest.writeString(this.loandesc);
        dest.writeInt(this.trnloantime);
        dest.writeString(this.apploancashstatus);
        dest.writeString(this.agunan);
    }

    public ModelNotifPencairan() {
    }

    protected ModelNotifPencairan(Parcel in) {
        this.apploancashoid = in.readInt();
        this.membername = in.readString();
        this.trnloanamt = in.readInt();
        this.trnloanamtrev = in.readInt();
        this.trnrequestno = in.readString();
        this.apploancashnote = in.readString();
        this.loandesc = in.readString();
        this.trnloantime = in.readInt();
        this.apploancashstatus = in.readString();
        this.agunan = in.readString();
    }

    public static final Parcelable.Creator<ModelNotifPencairan> CREATOR = new Parcelable.Creator<ModelNotifPencairan>() {
        @Override
        public ModelNotifPencairan createFromParcel(Parcel source) {
            return new ModelNotifPencairan(source);
        }

        @Override
        public ModelNotifPencairan[] newArray(int size) {
            return new ModelNotifPencairan[size];
        }
    };
}
