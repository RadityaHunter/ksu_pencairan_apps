package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelDaftarPencairan implements Parcelable {
    public static final Creator<ModelDaftarPencairan> CREATOR = new Creator<ModelDaftarPencairan>() {
        @Override
        public ModelDaftarPencairan createFromParcel(Parcel source) {
            return new ModelDaftarPencairan(source);
        }

        @Override
        public ModelDaftarPencairan[] newArray(int size) {
            return new ModelDaftarPencairan[size];
        }
    };

    private String namanastaf, idnasabah, total, keterangan;
    private int kodeMenu;


    public ModelDaftarPencairan(String namanastaf, String idnasabah, String total, String keterangan, int kodeMenu) {
        this.namanastaf = namanastaf;
        this.idnasabah = idnasabah;
        this.total = total;
        this.keterangan = keterangan;
        this.kodeMenu = kodeMenu;
    }

    protected ModelDaftarPencairan(Parcel in) {
        this.namanastaf = in.readString();
        this.idnasabah = in.readString();
        this.total = in.readString();
        this.keterangan = in.readString();
        this.kodeMenu = in.readInt();
    }

    public String getNamanastaf() {
        return namanastaf;
    }

    public void setNamanastaf(String namanastaf) {
        this.namanastaf = namanastaf;
    }

    public String getIdnasabah() {
        return idnasabah;
    }

    public void setIdnasabah(String idnasabah) {
        this.idnasabah = idnasabah;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public int getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(int kodeMenu) {
        this.kodeMenu = kodeMenu;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.namanastaf);
        dest.writeString(this.idnasabah);
        dest.writeString(this.total);
        dest.writeString(this.keterangan);
        dest.writeInt(this.kodeMenu);

    }
}
