package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.LaporanPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.PersetujuanLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.Adapter.AdapterDaftarPencairanList;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.VolleyErrorHelper;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPencairanList;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class DaftarPencairanActivity extends AppCompatActivity implements AdapterDaftarPencairanList.AdapterDaftarPencairanListCallback {
    private static final String TAG = DaftarPencairanActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cari)
    EditText cari;
    @BindView(R.id.rvdaftar_pencairan)
    RecyclerView rvdaftar_pencairan;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.swap)
    SwipeRefreshLayout swap;
    private List<ModelDaftarPencairanList> modelDaftarPencairanLists = new ArrayList<>();
    private SessionManager sessionManager;
    private Boolean APP_PENCAIRAN_LAPANGAN = false;
    //    private List<ModelDaftarPencairanListResponse> modelDaftarPencairanListResponses;
    private List<ModelDaftarPencairanListResponse2> modelDaftarPencairanListResponses;
    private Intent intent;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pencairan);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        restoreActionBar();
        DaftarPencairan("");
        swap.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                DaftarPencairan(cari.getText().toString().trim());
                swap.setRefreshing(false);
            }
        });
        cari.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    DaftarPencairan(v.getText().toString().trim());
                    //keyboard otomatis hilang jika sudah cari nama/item selesai
                    InputMethodManager inputMethodManager1 = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager1.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        DaftarPencairan("");
    }

    private void DaftarPencairan(String s) {
        Log.e(TAG, "onResponse: " + modelDaftarPencairanLists.size());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DaftarPencairanActivity.this);
        rvdaftar_pencairan.setLayoutManager(linearLayoutManager);
        rvdaftar_pencairan.setHasFixedSize(true);
        rvdaftar_pencairan.setNestedScrollingEnabled(false);
        rvdaftar_pencairan.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                swap.setEnabled(linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0);
            }
        });
        rvdaftar_pencairan.setAdapter(null);
//        String url = Http.getUrl() + "Pencairan_List?sCmp=" + Http.getsCmp() + "&membername=" + s;
        String url = Http.getUrl() + "GetListApprovalPencairan?sCmp=" + Http.getsCmp() + "&useroid=" + sessionManager.getPID() + "&membername=" + s;
        Log.e(TAG, "rvlaporan: " + url);
        final ProgressDialog dialog1 = new ProgressDialog(DaftarPencairanActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Harap Menunggu...");
        dialog1.show();
        RequestQueue mQueue = Volley.newRequestQueue(DaftarPencairanActivity.this);
        //        Log.e(TAG, "accessWebService: Katalog Start with Token " + sessionManager.getKeyToken());
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onResponse(String response) {
                        try {

                            dialog1.dismiss();
                            Log.d(TAG, "onResponse() called with: response = " + response);
                            if (response != null) {
//                                versi1(response);
                                versi2(response);
                            } else {
                                Toast.makeText(DaftarPencairanActivity.this, "Data Kosong.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse() called with: response = [" + e.getMessage() + "]");
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                String responseBodyError = VolleyErrorHelper.getMessage(error, DaftarPencairanActivity.this);
                Toast.makeText(DaftarPencairanActivity.this, responseBodyError, Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/json");
                //                params.put("Authorization", "Bearer " + sessionManager.getKeyToken());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjectRequest);
    }

    private void versi2(String response) {
        Log.e(TAG, "versi2: start");
        modelDaftarPencairanListResponses = new Gson().fromJson(response, new TypeToken<List<ModelDaftarPencairanListResponse2>>() {
        }.getType());
        Log.e(TAG, "versi2: " + modelDaftarPencairanListResponses.size());
        if (modelDaftarPencairanListResponses.isEmpty()) {
            Toast.makeText(DaftarPencairanActivity.this, "Data Kosong!", Toast.LENGTH_SHORT).show();
        } else {
            AdapterDaftarPencairanList adapterListLaporan = new AdapterDaftarPencairanList(DaftarPencairanActivity.this, -1, modelDaftarPencairanListResponses, DaftarPencairanActivity.this);
            rvdaftar_pencairan.setAdapter(adapterListLaporan);
        }
    }

//    private void versi1(String response) {
//        Gson gson = new Gson();
//        modelDaftarPencairanListResponses = gson.fromJson(response, new TypeToken<List<ModelDaftarPencairanListResponse>>() {
//        }.getType());
//        if (!modelDaftarPencairanListResponses.isEmpty()) {
//            modelDaftarPencairanLists.clear();
//            int trnagunanoid = 0;
//            for (ModelDaftarPencairanListResponse m : modelDaftarPencairanListResponses) {
//                if (trnagunanoid != m.getTrnloanoid()) {
//                    trnagunanoid = m.getTrnloanoid();
//                    List<ModelDaftarPencairanAgunanList> modelDaftarPencairanAgunanLists = new ArrayList<>();
//                    for (ModelDaftarPencairanListResponse c : modelDaftarPencairanListResponses) {
//                        if (trnagunanoid == m.getTrnloanoid()) {
//                            modelDaftarPencairanAgunanLists.add(new ModelDaftarPencairanAgunanList(c.getJenisagunan()));
//                        }
//                    }
//                    modelDaftarPencairanLists.add(new ModelDaftarPencairanList(m.getTrnloanoid(), m.getTrnloanno(), m.getMembermasterno(), m.getMembername(), m.getTrnloanamt(), m.getTrnloantime(), m.getTrnloanamtrev(), m.getLoandesc(), modelDaftarPencairanAgunanLists));
//                }
//
//            }
//
//            AdapterDaftarPencairanList adapterListLaporan = new AdapterDaftarPencairanList(DaftarPencairanActivity.this, -1, modelDaftarPencairanLists, DaftarPencairanActivity.this);
//            rvdaftar_pencairan.setAdapter(adapterListLaporan);
//        } else {
//            Toast.makeText(DaftarPencairanActivity.this, "Data Kosong!", Toast.LENGTH_SHORT).show();
//        }
//    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //navigationonclik

        sessionManager = new SessionManager(this);
        try {
            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    APP_PENCAIRAN_LAPANGAN = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        bnMain.getMenu().clear();
        if (APP_PENCAIRAN_LAPANGAN) {
            bnMain.inflateMenu(R.menu.menu_page_pencairan4);
        } else {
            bnMain.inflateMenu(R.menu.menu_page_pencairan);
        }
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
//                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
//                        startActivity(intent3);
//                        break;
                        Intent intent3 = new Intent(getApplicationContext(), LaporanPencairanActivity.class);
                        startActivity(intent3);
                        break;


                }
                return false;

            }
        });
    }

    @Override
    public void onRowAdapterDaftarPencairanListClicked(int position) {
        ModelDaftarPencairanListResponse2 m = modelDaftarPencairanListResponses.get(position);
        intent = new Intent(DaftarPencairanActivity.this, PersetujuanLaporanActivity.class);
        intent.putExtra("_DATA", (Parcelable) m);
        startActivity(intent);
    }
}
