package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanTabelActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.ModelDaftarPencairanListResponse2;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.VolleyErrorHelper;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.pembayaran.fragment.Pop_up_perjanjian_bayar_berhasilFragment;

public class AdapterDaftarPencairanTabel extends
        RecyclerView.Adapter<AdapterDaftarPencairanTabel.ViewHolder> {

    private static final String TAG = AdapterDaftarPencairanTabel.class.getSimpleName();
    private SessionManager sessionManager;
    private Context context;
    private String ACTIONAPPROVE = "";
    private String MASSAGE_FRAGMENT = "";
    private Boolean APP_PENCAIRAN_LAPANGAN = false;
    private List<ModelDaftarPencairanListResponse2> list;
    private int result = -1;

    public AdapterDaftarPencairanTabel(Context context, int result, List<ModelDaftarPencairanListResponse2> list) {
        this.context = context;
        this.list = list;
        this.result = result;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_daftar_pencairan_tabel,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelDaftarPencairanListResponse2 item = list.get(position);
        if (item.getAPPPENCAIRANPENGAWAS().equals("show")){
            holder.lnAproval.setVisibility(View.VISIBLE);
        }else if (item.getAPPPENCAIRANKABAG().equals("show") || item.getAPPPENCAIRANKABAG2().equals("show")){
            holder.lnAprovalKabagOps.setVisibility(View.VISIBLE);
        }else if (item.getAPPPENCAIRANCASHIER().equals("show") || item.getAPPPENCAIRANCASHIER2().equals("show")){
            holder.lnAprovalCashier.setVisibility(View.VISIBLE);
        }else if (item.getAPPPENCAIRANLAPANGAN().equals("show")){
            holder.lnAprovalLapangan.setVisibility(View.VISIBLE);
        }
        holder.namaStaf.setText(item.getMembername());
        holder.nomorPinjaman.setText(item.getTrnrequestno());
        holder.jumlahDiterima.setText(item.getLoandesc());
        holder.jangkaWaktu.setText(MessageFormat.format("{0} Angsuran", item.getTrnloantime()));
        holder.agunan.setText(item.getAgunan());
        Double dt = Double.valueOf(item.getTrnloanamt());
        Double dt2 = Double.valueOf(item.getTrnloanamtrev());
        holder.jumlahPinjaman.setText("Rp. " + MainActivity.nf.format(dt.intValue()));
        holder.jenisPinjaman.setText("Rp. " + MainActivity.nf.format(dt2.intValue()));
        holder.StatusPencairan.setText(item.getApploancashnote());

        holder.tolak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendApproved("Rejected", String.valueOf(item.getApploancashoid()));
            }
        });

        holder.tolak_kabag_ops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendApproved("Rejected", String.valueOf(item.getApploancashoid()));
            }
        });

        holder.tolak_cashier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendApproved("Rejected", String.valueOf(item.getApploancashoid()));
            }
        });

        holder.tolak_lapangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendApproved("Rejected", String.valueOf(item.getApploancashoid()));
            }
        });
        // Action Approved
        holder.setuju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendApproved("Approved", String.valueOf(item.getApploancashoid()));
            }
        });

        holder.setuju_kabag_ops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendApproved("Approved", String.valueOf(item.getApploancashoid()));
            }
        });

        holder.setuju_cashier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendApproved("Approved", String.valueOf(item.getApploancashoid()));
            }
        });

        holder.setuju_lapangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendApproved("Approved", String.valueOf(item.getApploancashoid()));
            }
        });
//        holder.lnAproval.setVisibility(View.GONE);
//
//        Log.e(TAG, "onCreate: " + sessionManager.getRole());
//        try {
//            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
//            }.getType());
//
//            for (ModelUserRole m : modelRole) {
//                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
//                    APP_PENCAIRAN_LAPANGAN = true;
//                    ACTIONAPPROVE = "APP_PENCAIRAN_LAPANGAN";
//                }
//                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_PENGAWAS")) {
//                    ACTIONAPPROVE = "APP_PENCAIRAN_PENGAWAS";
//                }
//                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_KABAG")) {
//                    ACTIONAPPROVE = "APP_PENCAIRAN_KABAG";
//                }
//                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_CASHIER")) {
//                    ACTIONAPPROVE = "APP_PENCAIRAN_CASHIER";
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//        setupApproval();
//        holder.tolak.setOnClickListener(new View.OnClickListener() { // KOMENBARU
//            @Override
//            public void onClick(View view) {
//                ((Activity) context).finish();
////                sendApproved("Rejected");
//            }
//        });
//        holder.setuju.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                sendApproved("Approved");
//                Log.e(TAG, "onCreate: " + " getAppspvstatus:" + item.getAppspvstatus() +
//                        " getAppkabagstatus:" + item.getAppkabagstatus() +
//                        " getAppcashierstatus:" + item.getAppcashierstatus() +
//                        " getApplapstatus:" + item.getApplapstatus() +
//                        " getCheckinstatus:" + item.getCheckinstatus() +
//                        " getLoancashstatus:" + item.getLoancashstatus() +
//                        " getAppcashierstatus2:" + item.getAppcashierstatus2() +
//                        " appkabagstatus2:" + item.getAppkabagstatus2()
//                );
//                if (item.getApploancashstatus().equals("In Approval")) {
//                    if (item.getAppspvstatus().equals("Dalam Proses") &&
//                            ACTIONAPPROVE.equals("APP_PENCAIRAN_PENGAWAS")) {
//                        //menunggu persetujuan Pengawas
//                        holder.lnAproval.setVisibility(View.VISIBLE);
//                        MASSAGE_FRAGMENT = "Pencairan Berhasil di ";
//                    } else if (item.getAppkabagstatus().equals("Dalam Proses") &&
//                            item.getAppspvstatus().equals("Approved") &&
//                            ACTIONAPPROVE.equals("APP_PENCAIRAN_KABAG")) {
//                        //menunggu persetujuan KABAG
//                        holder.lnAproval.setVisibility(View.VISIBLE);
//                        MASSAGE_FRAGMENT = "Pencairan Berhasil di ";
//
//                    } else if (item.getAppspvstatus().equals("Approved") &&
//                            item.getAppkabagstatus().equals("Approved") &&
//                            item.getAppcashierstatus().equals("Dalam Proses") &&
//                            ACTIONAPPROVE.equals("APP_PENCAIRAN_CASHIER")) {
//                        //menunggu penyerahan uang
//                        MASSAGE_FRAGMENT = "Pencairan Berhasil Diserahkan";
//                        holder.lnAproval.setVisibility(View.VISIBLE);
//                        holder.setuju.setText("Serahkan");
//                        holder.tolak.setText("Belum");
//                    } else if (item.getApplapstatus().equals("Dalam Proses") &&
//                            ACTIONAPPROVE.equals("APP_PENCAIRAN_LAPANGAN")) {
//                        //uang sudah di serahkan
//                        holder.lnAproval.setVisibility(View.VISIBLE);
//                        MASSAGE_FRAGMENT = "Uang Telah Di terima.";
//                        holder.setuju.setText("Terima");
//                        holder.tolak.setText("Belum");
//                    } else if (item.getAppspvstatus().equals("Approved") &&
//                            item.getAppkabagstatus().equals("Approved") &&
//                            item.getAppcashierstatus().equals("Approved") &&
//                            item.getApplapstatus().equals("Approved") &&
//                            item.getCheckinstatus().equals("Tidak Ada") &&
//                            item.getLoancashstatus().equals("") &&
//                            item.getAppcashierstatus2().equals("Dalam Proses") &&
//                            ACTIONAPPROVE.equals("APP_PENCAIRAN_CASHIER")) {
//                        //menunggu pembatalan Pencairan Kasir
//                        holder.lnAproval.setVisibility(View.VISIBLE);
//                        holder.setuju.setText("Terima");
//                        holder.tolak.setText("Belum");
//                    } else if (item.getAppspvstatus().equals("Approved") &&
//                            item.getAppkabagstatus().equals("Approved") &&
//                            item.getAppcashierstatus().equals("Approved") &&
//                            item.getApplapstatus().equals("Approved") &&
//                            item.getCheckinstatus().equals("Tidak Ada") &&
//                            item.getLoancashstatus().equals("") &&
//                            item.getAppcashierstatus2().equals("Approved") &&
//                            item.getAppkabagstatus2().equals("Dalam Proses") &&
//                            ACTIONAPPROVE.equals("APP_PENCAIRAN_KABAG")) {
//                        //menunggu pembatalan Pencairan Kabag
//                        holder.lnAproval.setVisibility(View.VISIBLE);
//                        holder.setuju.setText("Terima");
//                        holder.tolak.setText("Belum");
//                    } else if (item.getAppspvstatus().equals("Approved") &&
//                            item.getAppkabagstatus().equals("Approved") &&
//                            item.getAppcashierstatus().equals("Approved") &&
//                            item.getApplapstatus().equals("Approved") &&
//                            item.getCheckinstatus().equals("Ada") &&
//                            item.getLoancashstatus().equals("Closed") &&
//                            item.getAppcashierstatus2().equals("") &&
//                            item.getAppkabagstatus2().equals("Dalam Proses") &&
//                            ACTIONAPPROVE.equals("APP_PENCAIRAN_KABAG")) {
//                        //menunggu penyerahan berkas
//                        holder.lnAproval.setVisibility(View.VISIBLE);
//                        holder.setuju.setText("Terima");
//                        holder.tolak.setText("Belum");
//                    }
//                }
//                if (item.getApploancashstatus().equals("Rejected")) {
//
//                }
////                sendApproved("Approved");
//
//
//            }
//        }); //KOMEBBARU


//        if (item.getTrnrequestno() != null) {
//            if (item.getTrnrequestno().length() > 0) {
//                holder.idnasabah.setText(item.getTrnrequestno());
//            } else {
//                holder.idnasabah.setText("-");
//            }
//        } else {
//            holder.idnasabah.setText("-");
//        }
//        Double dt = item.getTrnloanamt();
//        holder.total.setText("Rp. " + MainActivity.nf.format(dt.intValue()));
//        holder.keterangan.setText(item.getApploancashstatus());
    }


    private void sendApproved(String appactionss, String apploancashoidd) {
        String url = Http.getUrl() + "InsertAprovalPencairan";
        Log.e(TAG, "sendCheckIn: " + url);
        final ProgressDialog dialog1 = new ProgressDialog(context);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Sedang Menyimpan...");
        dialog1.show();
        RequestQueue mQueue = Volley.newRequestQueue(context);
        //        Log.e(TAG, "accessWebService: Katalog Start with Token " + sessionManager.getKeyToken());
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog1.dismiss();
                        try {

                            Log.d(TAG, "onResponse() called with: response = " + response);
                            if (response != null) {
                                //                                Gson gson = new Gson();
                                //                                ModelName modelName = gson.fromJson(String.valueOf(response), ModelName.class)
                                try {
                                    String[] respondde = response.split(";");
                                    if (respondde[0].equals("OK")) {
                                        Bundle bundle = new Bundle();
                                        if (appactionss.equals("Rejected")) {
                                            bundle.putString("DATA", MASSAGE_FRAGMENT);
                                        } else {
                                            bundle.putString("DATA", MASSAGE_FRAGMENT);
                                        }
                                        Pop_up_perjanjian_bayar_berhasilFragment pop_up_perjanjian_bayar_berhasilFragment = new Pop_up_perjanjian_bayar_berhasilFragment();
                                        FragmentManager mFragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                                        pop_up_perjanjian_bayar_berhasilFragment.setArguments(bundle);
                                        pop_up_perjanjian_bayar_berhasilFragment.setCancelable(false);
                                        pop_up_perjanjian_bayar_berhasilFragment.show(mFragmentManager, Pop_up_perjanjian_bayar_berhasilFragment.class.getSimpleName());
                                        Intent intent = new Intent(context, DaftarPencairanTabelActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        context.startActivity(intent);

                                    } else {
                                        Toast.makeText(context, respondde[1], Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(context, "Gagal Menyimpan.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }

                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse() called with: response = [" + e.getMessage() + "]");
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                try {
                    String bodyError = VolleyErrorHelper.getMessage(error, context);
                    Toast.makeText(context, bodyError, Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    Log.e(TAG, "onErrorResponse: " + e.getMessage());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sCmp", Http.getsCmp());
                params.put("apploancashoid", apploancashoidd);
                params.put("appaction", appactionss);
                params.put("createuser", sessionManager.getPID());
                Log.e(TAG, "getParams: " + params.toString());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjectRequest);
    }


    public void addItems(List<ModelDaftarPencairanListResponse2> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface AdapterDaftarPencairanListCallback {
        void onRowAdapterDaftarPencairanListClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tolak)
        Button tolak;
        @BindView(R.id.setuju)
        Button setuju;
        @BindView(R.id.tolak_kabag_ops)
        Button tolak_kabag_ops;
        @BindView(R.id.setuju_kabag_ops)
        Button setuju_kabag_ops;
        @BindView(R.id.tolak_cashier)
        Button tolak_cashier;
        @BindView(R.id.setuju_cashier)
        Button setuju_cashier;
        @BindView(R.id.tolak_lapangan)
        Button tolak_lapangan;
        @BindView(R.id.setuju_lapangan)
        Button setuju_lapangan;
        @BindView(R.id.nama_staf)
        TextView namaStaf;
        @BindView(R.id.nomor_pinjaman)
        TextView nomorPinjaman;
        @BindView(R.id.jumlah_pinjaman)
        TextView jumlahPinjaman;
        @BindView(R.id.jenis_pinjaman)
        TextView jenisPinjaman;
        @BindView(R.id.jumlahDiterima)
        TextView jumlahDiterima;
        @BindView(R.id.jangka_waktu)
        TextView jangkaWaktu;
        @BindView(R.id.agunan)
        TextView agunan;
        @BindView(R.id.Status_Pencairan)
        TextView StatusPencairan;
        @BindView(R.id.lnAproval)
        LinearLayout lnAproval;
        @BindView(R.id.lnAprovalKabagOps)
        LinearLayout lnAprovalKabagOps;
        @BindView(R.id.lnAprovalCashier)
        LinearLayout lnAprovalCashier;
        @BindView(R.id.lnAprovalLapangan)
        LinearLayout lnAprovalLapangan;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            sessionManager = new SessionManager(context);
        }
    }
}