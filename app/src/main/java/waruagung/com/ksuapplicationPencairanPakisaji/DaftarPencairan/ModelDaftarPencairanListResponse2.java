package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan;

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelDaftarPencairanListResponse2 implements Serializable, Parcelable
{

    @SerializedName("apploancashoid")
    @Expose
    private Integer apploancashoid;
    @SerializedName("membername")
    @Expose
    private String membername;
    @SerializedName("trnloanamt")
    @Expose
    private Double trnloanamt;
    @SerializedName("trnloanamtrev")
    @Expose
    private Double trnloanamtrev;
    @SerializedName("trnrequestno")
    @Expose
    private String trnrequestno;
    @SerializedName("apploancashnote")
    @Expose
    private String apploancashnote;
    @SerializedName("loandesc")
    @Expose
    private String loandesc;
    @SerializedName("trnloantime")
    @Expose
    private Double trnloantime;
    @SerializedName("apploancashstatus")
    @Expose
    private String apploancashstatus;
    @SerializedName("agunan")
    @Expose
    private String agunan;
    @SerializedName("APP_PENCAIRAN_PENGAWAS")
    @Expose
    private String aPPPENCAIRANPENGAWAS;
    @SerializedName("APP_PENCAIRAN_KABAG")
    @Expose
    private String aPPPENCAIRANKABAG;
    @SerializedName("APP_PENCAIRAN_CASHIER")
    @Expose
    private String aPPPENCAIRANCASHIER;
    @SerializedName("APP_PENCAIRAN_LAPANGAN")
    @Expose
    private String aPPPENCAIRANLAPANGAN;
    @SerializedName("checkinstatus")
    @Expose
    private String checkinstatus;
    @SerializedName("APP_PENCAIRAN_CASHIER2")
    @Expose
    private String aPPPENCAIRANCASHIER2;
    @SerializedName("APP_PENCAIRAN_KABAG2")
    @Expose
    private String aPPPENCAIRANKABAG2;
    public final static Parcelable.Creator<ModelDaftarPencairanListResponse2> CREATOR = new Creator<ModelDaftarPencairanListResponse2>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ModelDaftarPencairanListResponse2 createFromParcel(Parcel in) {
            return new ModelDaftarPencairanListResponse2(in);
        }

        public ModelDaftarPencairanListResponse2 [] newArray(int size) {
            return (new ModelDaftarPencairanListResponse2[size]);
        }

    }
            ;
    private final static long serialVersionUID = 6174843824196668851L;

    protected ModelDaftarPencairanListResponse2(Parcel in) {
        this.apploancashoid = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.membername = ((String) in.readValue((String.class.getClassLoader())));
        this.trnloanamt = ((Double) in.readValue((Double.class.getClassLoader())));
        this.trnloanamtrev = ((Double) in.readValue((Double.class.getClassLoader())));
        this.trnrequestno = ((String) in.readValue((String.class.getClassLoader())));
        this.apploancashnote = ((String) in.readValue((String.class.getClassLoader())));
        this.loandesc = ((String) in.readValue((String.class.getClassLoader())));
        this.trnloantime = ((Double) in.readValue((Double.class.getClassLoader())));
        this.apploancashstatus = ((String) in.readValue((String.class.getClassLoader())));
        this.agunan = ((String) in.readValue((String.class.getClassLoader())));
        this.aPPPENCAIRANPENGAWAS = ((String) in.readValue((String.class.getClassLoader())));
        this.aPPPENCAIRANKABAG = ((String) in.readValue((String.class.getClassLoader())));
        this.aPPPENCAIRANCASHIER = ((String) in.readValue((String.class.getClassLoader())));
        this.aPPPENCAIRANLAPANGAN = ((String) in.readValue((String.class.getClassLoader())));
        this.checkinstatus = ((String) in.readValue((String.class.getClassLoader())));
        this.aPPPENCAIRANCASHIER2 = ((String) in.readValue((String.class.getClassLoader())));
        this.aPPPENCAIRANKABAG2 = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ModelDaftarPencairanListResponse2() {
    }

    public Integer getApploancashoid() {
        return apploancashoid;
    }

    public void setApploancashoid(Integer apploancashoid) {
        this.apploancashoid = apploancashoid;
    }

    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    public Double getTrnloanamt() {
        return trnloanamt;
    }

    public void setTrnloanamt(Double trnloanamt) {
        this.trnloanamt = trnloanamt;
    }

    public Double getTrnloanamtrev() {
        return trnloanamtrev;
    }

    public void setTrnloanamtrev(Double trnloanamtrev) {
        this.trnloanamtrev = trnloanamtrev;
    }

    public String getTrnrequestno() {
        return trnrequestno;
    }

    public void setTrnrequestno(String trnrequestno) {
        this.trnrequestno = trnrequestno;
    }

    public String getApploancashnote() {
        return apploancashnote;
    }

    public void setApploancashnote(String apploancashnote) {
        this.apploancashnote = apploancashnote;
    }

    public String getLoandesc() {
        return loandesc;
    }

    public void setLoandesc(String loandesc) {
        this.loandesc = loandesc;
    }

    public Double getTrnloantime() {
        return trnloantime;
    }

    public void setTrnloantime(Double trnloantime) {
        this.trnloantime = trnloantime;
    }

    public String getApploancashstatus() {
        return apploancashstatus;
    }

    public void setApploancashstatus(String apploancashstatus) {
        this.apploancashstatus = apploancashstatus;
    }

    public String getAgunan() {
        return agunan;
    }

    public void setAgunan(String agunan) {
        this.agunan = agunan;
    }

    public String getAPPPENCAIRANPENGAWAS() {
        return aPPPENCAIRANPENGAWAS;
    }

    public void setAPPPENCAIRANPENGAWAS(String aPPPENCAIRANPENGAWAS) {
        this.aPPPENCAIRANPENGAWAS = aPPPENCAIRANPENGAWAS;
    }

    public String getAPPPENCAIRANKABAG() {
        return aPPPENCAIRANKABAG;
    }

    public void setAPPPENCAIRANKABAG(String aPPPENCAIRANKABAG) {
        this.aPPPENCAIRANKABAG = aPPPENCAIRANKABAG;
    }

    public String getAPPPENCAIRANCASHIER() {
        return aPPPENCAIRANCASHIER;
    }

    public void setAPPPENCAIRANCASHIER(String aPPPENCAIRANCASHIER) {
        this.aPPPENCAIRANCASHIER = aPPPENCAIRANCASHIER;
    }

    public String getAPPPENCAIRANLAPANGAN() {
        return aPPPENCAIRANLAPANGAN;
    }

    public void setAPPPENCAIRANLAPANGAN(String aPPPENCAIRANLAPANGAN) {
        this.aPPPENCAIRANLAPANGAN = aPPPENCAIRANLAPANGAN;
    }

    public String getCheckinstatus() {
        return checkinstatus;
    }

    public void setCheckinstatus(String checkinstatus) {
        this.checkinstatus = checkinstatus;
    }

    public String getAPPPENCAIRANCASHIER2() {
        return aPPPENCAIRANCASHIER2;
    }

    public void setAPPPENCAIRANCASHIER2(String aPPPENCAIRANCASHIER2) {
        this.aPPPENCAIRANCASHIER2 = aPPPENCAIRANCASHIER2;
    }

    public String getAPPPENCAIRANKABAG2() {
        return aPPPENCAIRANKABAG2;
    }

    public void setAPPPENCAIRANKABAG2(String aPPPENCAIRANKABAG2) {
        this.aPPPENCAIRANKABAG2 = aPPPENCAIRANKABAG2;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(apploancashoid);
        dest.writeValue(membername);
        dest.writeValue(trnloanamt);
        dest.writeValue(trnloanamtrev);
        dest.writeValue(trnrequestno);
        dest.writeValue(apploancashnote);
        dest.writeValue(loandesc);
        dest.writeValue(trnloantime);
        dest.writeValue(apploancashstatus);
        dest.writeValue(agunan);
        dest.writeValue(aPPPENCAIRANPENGAWAS);
        dest.writeValue(aPPPENCAIRANKABAG);
        dest.writeValue(aPPPENCAIRANCASHIER);
        dest.writeValue(aPPPENCAIRANLAPANGAN);
        dest.writeValue(checkinstatus);
        dest.writeValue(aPPPENCAIRANCASHIER2);
        dest.writeValue(aPPPENCAIRANKABAG2);
    }

    public int describeContents() {
        return 0;
    }

}