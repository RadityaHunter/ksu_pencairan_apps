package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.PersetujuanLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.Model.ModelDaftarPencairan;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterDaftarPencairan extends RecyclerView.Adapter<AdapterDaftarPencairan.myViewHolder> {
    static final String EXTRAS_DATA = "EXTRAS_data";
    private Context context;
    private ArrayList<ModelDaftarPencairan> listMenu;
    private Intent intent;

    public AdapterDaftarPencairan(Context context, ArrayList<ModelDaftarPencairan> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @NonNull
    @Override
    public AdapterDaftarPencairan.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_daftar_pencairan, viewGroup, false);
        return new AdapterDaftarPencairan.myViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterDaftarPencairan.myViewHolder myViewHolder, final int i) {
        myViewHolder.namastaf.setText(listMenu.get(i).getNamanastaf());
        myViewHolder.idnasabah.setText(listMenu.get(i).getIdnasabah());
        myViewHolder.total.setText(listMenu.get(i).getTotal());
        myViewHolder.keterangan.setText(listMenu.get(i).getKeterangan());

//        Glide.with(context)
//                .load(listMenu.get(i).getImageMenudeveloper()).into(myViewHolder.MenuImages);

        myViewHolder.ListBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = listMenu.get(i).getKodeMenu();
                MenuKategori(id, v);
            }
        });

    }

    private void MenuKategori(int id, View v) {
        switch (id) {
            case 1:
//            PersetujuanLaporan
                intent = new Intent(context, PersetujuanLaporanActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
//
            case 2:
                //JanjiBayarLaporan
                intent = new Intent(context, PersetujuanLaporanActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
//            case 3:
////            Pinjaman Online
//                intent = new Intent(context, JanjiBayarLaporanActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 4:
////            Tukar Tambah
//                intent = new Intent(context, PersetujuanLaporanActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

        }
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.Rvdesaincari)
        LinearLayout ListBarang;
        @BindView(R.id.namastaf)
        TextView namastaf;
        @BindView(R.id.idnasabah)
        TextView idnasabah;
        @BindView(R.id.total)
        TextView total;
        @BindView(R.id.keterangan)
        TextView keterangan;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
