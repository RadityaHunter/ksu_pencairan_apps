package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.ModelDaftarPencairanListResponse2;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterDaftarPencairanList extends
        RecyclerView.Adapter<AdapterDaftarPencairanList.ViewHolder> {

    private static final String TAG = AdapterDaftarPencairanList.class.getSimpleName();

    private Context context;
    private List<ModelDaftarPencairanListResponse2> list;
    private AdapterDaftarPencairanListCallback mAdapterCallback;
    private int result = -1;

    public AdapterDaftarPencairanList(Context context, int result, List<ModelDaftarPencairanListResponse2> list, AdapterDaftarPencairanListCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.result = result;
        this.mAdapterCallback = adapterCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_daftar_pencairan,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelDaftarPencairanListResponse2 item = list.get(position);
        holder.namastaf.setText(item.getMembername());
        if (item.getTrnrequestno() != null) {
            if (item.getTrnrequestno().length() > 0) {
                holder.idnasabah.setText(item.getTrnrequestno());
            } else {
                holder.idnasabah.setText("-");
            }
        } else {
            holder.idnasabah.setText("-");
        }
        Double dt = item.getTrnloanamt();
        holder.total.setText("Rp. " + MainActivity.nf.format(dt.intValue()));
        holder.keterangan.setText(item.getApploancashstatus());
        holder.ListBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onRowAdapterDaftarPencairanListClicked(position);
            }
        });
    }

    public void addItems(List<ModelDaftarPencairanListResponse2> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface AdapterDaftarPencairanListCallback {
        void onRowAdapterDaftarPencairanListClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.Rvdesaincari)
        LinearLayout ListBarang;
        @BindView(R.id.namastaf)
        TextView namastaf;
        @BindView(R.id.idnasabah)
        TextView idnasabah;
        @BindView(R.id.total)
        TextView total;
        @BindView(R.id.keterangan)
        TextView keterangan;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}