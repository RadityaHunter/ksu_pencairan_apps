package waruagung.com.ksuapplicationPencairanPakisaji.JanjiBayar;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.pembayaran.fragment.Pop_up_perjanjian_bayar_berhasilFragment;

public class JanjiBayarActivity extends AppCompatActivity {
    private static final String TAG = "";
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.Nama_Nasabah)
    TextView NamaNasabah;
    @BindView(R.id.idnasabah)
    TextView idnasabah;
    @BindView(R.id.lnNoNasabah)
    LinearLayout lnNoNasabah;
    @BindView(R.id.jmlhnomorpinjaman)
    TextView jmlhnomorpinjaman;
    @BindView(R.id.jenisnomorpinjaman)
    TextView jenisnomorpinjaman;
    @BindView(R.id.reljenisnomorpinjaman)
    RelativeLayout reljenisnomorpinjaman;
    @BindView(R.id.keteranganpinjam)
    EditText keteranganpinjam;
    @BindView(R.id.tanggaljanji)
    TextView tanggaljanji;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.reltgl)
    ConstraintLayout reltgl;
    @BindView(R.id.clear_button)
    TextView clearButton;
    @BindView(R.id.signature_pad_description)
    TextView signaturePadDescription;
    @BindView(R.id.signature_pad)
    SignaturePad signaturePad;
    @BindView(R.id.signature_pad_container)
    RelativeLayout signaturePadContainer;
    @BindView(R.id.save_button)
    Button saveButton;
    @BindView(R.id.buttons_container)
    LinearLayout buttonsContainer;
    @BindView(R.id.btn_simpan)
    LinearLayout btn_simpan;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;

    private ConstraintLayout mDisplayDate;
    //    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSatlistener;
    //    private String spinnerPinjamaValn;
    private DatePickerDialog picker;
    private String bulanjankawaktu;
    //ttd
    private byte[] byteArray;
    private String base64_signature = "";
    private SignaturePad mSignaturePad;
    private TextView mClearButton;
    private Button mSaveButton;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_janji_bayar);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        lnNoNasabah.setVisibility(View.GONE);
        restoreActionBar();
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = mSignaturePad.getSignatureBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                base64_signature = encoded;

                if (jenisnomorpinjaman.getText().toString().isEmpty()) {
                    Toast.makeText(JanjiBayarActivity.this, "Nomor Pinjaman Belum di Pilih", Toast.LENGTH_LONG).show();
                } else if (signaturePad.isEmpty()) {
                    Toast.makeText(JanjiBayarActivity.this, "Tanda Tangan Belum di Upload", Toast.LENGTH_LONG).show();
                } else if (tanggaljanji.getText().toString().isEmpty()) {
                    Toast.makeText(JanjiBayarActivity.this, "Tanggal Janji Belum di Pilih", Toast.LENGTH_LONG).show();
                } else {
                    Bundle bundle = new Bundle();
                    Pop_up_perjanjian_bayar_berhasilFragment pop_up_perjanjian_bayar_berhasilFragment = new Pop_up_perjanjian_bayar_berhasilFragment();
                    FragmentManager mFragmentManager = getSupportFragmentManager();
                    pop_up_perjanjian_bayar_berhasilFragment.setArguments(bundle);
                    pop_up_perjanjian_bayar_berhasilFragment.setCancelable(false);
                    pop_up_perjanjian_bayar_berhasilFragment.show(mFragmentManager, Pop_up_perjanjian_bayar_berhasilFragment.class.getSimpleName());
                }
            }
        });


        //tanda tangan
        mSignaturePad = findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
//                Toast.makeText(Detail_pinjamanActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        mClearButton = findViewById(R.id.clear_button);
        mSaveButton = findViewById(R.id.save_button);

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        //        //datepacker
        Calendar calendar = Calendar.getInstance();
        String currentDate = new SimpleDateFormat("MM/dd/yyyy").format(calendar.getTime());
        TextView textViewDate = findViewById(R.id.tanggaljanji);
        textViewDate.setText(currentDate);
        mDisplayDate = findViewById(R.id.reltgl);
        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);


                picker = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, monthOfYear, dayOfMonth);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
                                String formatted = dateFormat.format(newDate.getTime());
                                tanggaljanji.setText(formatted);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        //navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;


                }
                return false;
            }
        });

    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(JanjiBayarActivity.this, "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        JanjiBayarActivity.this.sendBroadcast(mediaScanIntent);
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()));
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }


}
