package waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelHeader implements Parcelable {

    private String title,date;


    public ModelHeader(String title, String date) {
        this.title = title;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.date);
    }

    protected ModelHeader(Parcel in) {
        this.title = in.readString();
        this.date = in.readString();
    }

    public static final Creator<ModelHeader> CREATOR = new Creator<ModelHeader>() {
        @Override
        public ModelHeader createFromParcel(Parcel source) {
            return new ModelHeader(source);
        }

        @Override
        public ModelHeader[] newArray(int size) {
            return new ModelHeader[size];
        }
    };
}
