package waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.DetailDaftarAgunan1PrasetyaActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.detail_daftar_agunan1_Activity;
import waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.model.model_daftar_agunan;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class adapter_agunan extends RecyclerView.Adapter<adapter_agunan.myViewHolder>{
    static final String EXTRAS_DATA = "EXTRAS_data";
    private Context context;
    private ArrayList<model_daftar_agunan> listMenu;
    private Intent intent;

    public adapter_agunan(Context context, ArrayList<model_daftar_agunan> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @NonNull
    @Override
    public adapter_agunan.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.desain_adapter_daftar_agunan, viewGroup, false);
        return new adapter_agunan.myViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull adapter_agunan.myViewHolder myViewHolder, final int i) {
        myViewHolder.nama.setText(listMenu.get(i).getNama1());
        myViewHolder.noid.setText(listMenu.get(i).getNoid1());
        myViewHolder.jumlahuang.setText(listMenu.get(i).getJumlahuang1());
        myViewHolder.jumlahagunan.setText(listMenu.get(i).getJumlahagunan1());

//        Glide.with(context)
//                .load(listMenu.get(i).getImageMenu7()).into(myViewHolder.MenuImages);

        myViewHolder.ListBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = listMenu.get(i).getKodeMenu();
                MenuKategori(id, v);
            }
        });

    }

    private void MenuKategori(int id, View v) {
        switch (id) {
            case 1:
//            Langsung Laku
                intent = new Intent(context, detail_daftar_agunan1_Activity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;

//            case 2:
//                //Toko Member
//                intent = new Intent(context, adapter_detail_daftaragunan_prasetya.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
            case 2:
//            Pinjaman Online
                intent = new Intent(context, DetailDaftarAgunan1PrasetyaActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
//            case 4:
            //Tukar Tambah
//                intent = new Intent(context, ProfilMemberActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 5:
//                //Hotel
//                intent = new Intent(context, HotelActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 6:
            //Tiket Event
//                intent = new Intent(context, ProfilMemberActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 7:
////            Komisi
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 8:
////            TokoCabang
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 9:
////            Power Merchant
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 10:
////            Seller Center
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 11:
////            TopAds
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 12:
////            Pulsa
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 1);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 13:
////            Paket Data
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 2);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 14:
//                //Pascabayar
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 3);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 15:
//                //Roaming
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 4);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 16:
//                //Air PDAM
//                intent = new Intent(context, AirPDAMActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 17:
//                //Angsuran Kredit
//                intent = new Intent(context, AngsuranKreditActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 18:
//                //Belajar
//                intent = new Intent(context, BelajarActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 22:
//                //Topup OVO
//                intent = new Intent(context, OVOActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 23:
//                //Donasi
//                intent = new Intent(context, DonasiActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 25:
//                //Penerimaan negara
//                intent = new Intent(context, PenerimaannegaraActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;


        }
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.RLitemsMenu)
        LinearLayout ListBarang;
        @BindView(R.id.nama1)
        TextView nama;
        @BindView(R.id.noid1)
        TextView noid;
        @BindView(R.id.jumlahuang1)
        TextView jumlahuang;
        @BindView(R.id.jumlahagunan1)
        TextView jumlahagunan;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
