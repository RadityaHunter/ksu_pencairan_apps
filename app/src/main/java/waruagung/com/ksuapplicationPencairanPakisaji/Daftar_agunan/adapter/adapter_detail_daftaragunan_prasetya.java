package waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.model.model_detail_daftar_agunan1_prasetya;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.daftar_pengajuan.model.ModelHeader;

public class adapter_detail_daftaragunan_prasetya extends BaseExpandableListAdapter {
//    private static final String TAG = adapter_detail_daftaragunan.class.getSimpleName();
//    private List<String> expandableListTitle;
//    private HashMap<String, List<model_detail_daftar_agunan1>> expandableListDetail;
//    private Context context;
//
//    public adapter_detail_daftaragunan(Context context, List<String> title, HashMap<String, List<model_detail_daftar_agunan1>> expandableListDetail) {
//        this.context = context;
//        this.expandableListTitle = title;
//        this.expandableListDetail = expandableListDetail;
//    }
//
//    @Override
//
//    public int getGroupCount() {
//        return expandableListTitle.size();
//    }
//
//    @Override
//    public int getChildrenCount(int groupPosition) {
//        return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition))
//                .size();
//    }
//
//    @Override
//    public String getGroup(int groupPosition) {
//        return expandableListTitle.get(groupPosition);
//    }
//
//    @Override
//    public model_detail_daftar_agunan1 getChild(int groupPosition, int childPosition) {
//        return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition))
//                .get(childPosition);
//    }
//
//    @Override
//    public long getGroupId(int groupPosition) {
//        return groupPosition;
//    }
//
//    @Override
//    public long getChildId(int groupPosition, int childPosition) {
//        return childPosition;
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return false;
//    }
//
//    @Override
//    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
//        String listTitle = getGroup(groupPosition);
//        if (convertView == null) {
//            LayoutInflater layoutInflater = (LayoutInflater) this.context.
//                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = layoutInflater.inflate(R.layout.adapter_detail_hider_desain_daftar_agunan, null);
//        }
//        TextView listTitleTextView = convertView
//                .findViewById(R.id.listTitleTextView);
//        listTitleTextView.setTypeface(null, Typeface.BOLD);
//        listTitleTextView.setText(listTitle);
//        TextView listtanggal = convertView
//                .findViewById(R.id.listTitleTextView);
//        listtanggal.setTypeface(null, Typeface.BOLD);
//        listTitleTextView.setText(listTitle);
//        if (isExpanded){
//            listTitleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.panahbawah, 0,0,0);
//        }else {
//            listTitleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.panahatas, 0,0,0);
//        }
//        return convertView;
//    }
//
//    @Override
//    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
//        model_detail_daftar_agunan1 jsonInString =getChild(groupPosition, childPosition);
//        JSONObject jsonObject = null;
//
//
//        if (convertView == null) {
//            LayoutInflater layoutInflater = (LayoutInflater) this.context
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = layoutInflater.inflate(R.layout.desain_expandable_deatil_daftar_agunan1, null);
//        }
////        TextView timeTakeOrder = convertView
////                .findViewById(R.id.tanggal);
////        timeTakeOrder.setText(jsonInString.getTanggal()+"");
////        TextView last_order = convertView
////                .findViewById(R.id.last_order);
////        last_order.setText("Rp." + jsonInString.getLastOrder());
////        TextView piutang = convertView
////                .findViewById(R.id.piutang);
////        piutang.setText("Rp." + jsonInString.getPiutang());
//
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, MainActivity.class);
//                context.startActivity(intent);
//            }
//        });
//
//        return convertView;
//    }
//
//    @Override
//    public boolean isChildSelectable(int groupPosition, int childPosition) {
//        return false;
//    }
private static final String TAG = adapter_detail_daftaragunan_prasetya.class.getSimpleName();
    private List<ModelHeader> expandableListTitle;
    private HashMap<String, List<model_detail_daftar_agunan1_prasetya>> expandableListDetail;
    private Context context;

    public adapter_detail_daftaragunan_prasetya(Context context, List<ModelHeader> title, HashMap<String, List<model_detail_daftar_agunan1_prasetya>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = title;
        this.expandableListDetail = expandableListDetail;
    }

    @Override

    public int getGroupCount() {
        return expandableListTitle.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition).getTitle()).size();
    }

    @Override
    public ModelHeader getGroup(int groupPosition) {
        return expandableListTitle.get(groupPosition);
    }

    @Override
    public model_detail_daftar_agunan1_prasetya getChild(int groupPosition, int childPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition).getTitle())
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ModelHeader listTitle = getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_detail_hider_desain_daftar_agunan_prasetya, null);
        }
        TextView listTitleTextView = convertView
                .findViewById(R.id.listTitleTextView);
//        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle.getTitle());
        TextView listtanggal = convertView
                .findViewById(R.id.tanggal);
//        listtanggal.setTypeface(null, Typeface.BOLD);
        listtanggal.setText(listTitle.getDate());

        if (isExpanded) {
            listTitleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.panahbawah, 0);
        } else {
            listTitleTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.panahatas, 0);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        model_detail_daftar_agunan1_prasetya jsonInString = getChild(groupPosition, childPosition);
        JSONObject jsonObject = null;


        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.desain_expandable_deatil_daftar_agunan1_prasetya, null);
        }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
