package waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.model;

public class model_detail_daftar_agunan1 {

    /**
     * nameToko : Toko
     * takeOrder : 09.30-10.30
     * LastOrder : 1000000
     * Piutang : 0
     * Piutang Terlama : 0
     * SisaLimitKredit : 0
     * Status : belum jalan
     */
    private String kodenama;
    private String tanggal;
    private String namajenisagunan;
    private String jumlahnilaiuang;
    private String namaagunan;
    private String namagambar;

    public model_detail_daftar_agunan1(String kodenama, String tanggal, String namajenisagunan, String jumlahnilaiuang, String namaagunan, String namagambar) {
        this.kodenama = kodenama;
        this.tanggal = tanggal;
        this.namajenisagunan = namajenisagunan;
        this.jumlahnilaiuang = jumlahnilaiuang;
        this.namaagunan = namaagunan;
        this.namagambar = namagambar;
    }
    public String getKodenama() { return kodenama; }

    public void setKodenama(String kodenama) { this.kodenama = kodenama; }


    public String getTanggal() { return tanggal; }

    public void setTanggal(String tanggal) { this.tanggal = tanggal; }



    public String getNamajenisagunan() { return namajenisagunan; }

    public void setNamajenisagunan(String namajenisagunan) { this.namajenisagunan = namajenisagunan; }



    public String getJumlahnilaiuang() {
        return jumlahnilaiuang;
    }

    public void setJumlahnilaiuang(String jumlahnilaiuang) { this.jumlahnilaiuang = jumlahnilaiuang; }



    public String getNamaagunan() {
        return namaagunan;
    }

    public void setNamaagunan(String namaagunan) { this.namaagunan = namaagunan; }




    public String getNamagambar() {
        return namagambar;
    }

    public void setNamagambar(String namagambar) { this.namagambar = namagambar; }



}
