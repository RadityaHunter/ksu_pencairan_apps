package waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;

import waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.adapter.adapter_agunan;
import waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.model.model_daftar_agunan;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class Daftar_agunanActivity extends AppCompatActivity {
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.Rvdaftaragunan)
    RecyclerView Rvdaftaragunan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_agunan);
        ButterKnife.bind(this);
        daftaragunan();
//navigationonclik
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(1).setChecked(true);
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
        restoreActionBar();
    }

    private void daftaragunan() {
        Rvdaftaragunan.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false));
        Rvdaftaragunan.setHasFixedSize(true);
        Rvdaftaragunan.setNestedScrollingEnabled(false);
        ArrayList<model_daftar_agunan> listCategory = new ArrayList<>();
        listCategory.add(new model_daftar_agunan("LUSI APRILLIA", "9849273873", 1, "RP 70.000.000", "3 Agunan"));
        listCategory.add(new model_daftar_agunan("PRASETYA", "13124234677", 2, "RP 100.000.000", "5 Agunan"));
        adapter_agunan adapter_agunan = new adapter_agunan(this, listCategory);
        Rvdaftaragunan.setAdapter(adapter_agunan);
    }


    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
