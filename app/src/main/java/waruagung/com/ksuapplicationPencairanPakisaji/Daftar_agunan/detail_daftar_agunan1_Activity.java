package waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;

import waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.adapter.adapter_detail_daftaragunan;
import waruagung.com.ksuapplicationPencairanPakisaji.Daftar_agunan.model.model_detail_daftar_agunan1;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

import waruagung.com.ksuapplicationPencairanPakisaji.daftar_pengajuan.model.ModelHeader;

public class detail_daftar_agunan1_Activity extends AppCompatActivity {
    //    LinearLayout linierisitakeorderplan;
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
//    @BindView(R.id.bn_main)
//    BottomNavigationView bnMain;
//    @BindView(R.id.ExpandableListView)
//    ExpandableListView expandableListView;
//
//
//    private String mTitle = "List Review Plan";
//    private SessionManager sessionManager;
//    private List<String> listDataHeader;
//    private HashMap<String, List<model_detail_daftar_agunan1>> listDataChild;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ExpandableListView)
    ExpandableListView expandableListView1;

    private List<ModelHeader> listDataHeader;
    private HashMap<String, List<model_detail_daftar_agunan1>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_daftar_agunan1_);
//        ButterKnife.bind(this);
//        //navigationonclik
////        bnMain.getMenu().getItem(0).setCheckable(false);
////        bnMain.getMenu().getItem(1).setChecked(true);
////        bnMain.getMenu().getItem(0).setCheckable(false);
////        bnMain.getMenu().getItem(1).setCheckable(true);
//        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//                switch (menuItem.getItemId()) {
//                    case R.id.home_menu:
//                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//                        startActivity(intent);
//                        break;
//                    case R.id.agunan:
//                        Intent intent1 = new Intent(getApplicationContext(), AgunanActivity.class);
//                        startActivity(intent1);
//                        break;
//                    case R.id.pinjaman:
//                        Intent intent2 = new Intent(getApplicationContext(), PinjamanActivity.class);
//                        startActivity(intent2);
//                        break;
//                    case R.id.Pembayaran:
//                        Intent intent3 = new Intent(getApplicationContext(), Pembayaran_Activity.class);
//                        startActivity(intent3);
//                        break;
//                }
//                return false;
//            }
//        });
//
//        restoreActionBar();
//
//        sessionManager = new SessionManager(detail_daftar_agunan1_Activity.this);
////        restoreActionBar();
//        List<model_detail_daftar_agunan1> m = new ArrayList<>();
//        m.add(new model_detail_daftar_agunan1("AC143283","23 Mei 2019","Barang Berharga","Rp 45.000.000", "image.jpg", "Perhiasan" ));
//        m.add(new model_detail_daftar_agunan1("AC557687","30 Maret 2019","Kendaraan", "Rp 10.000.000", "image.jpg", "mobil"));
//        m.add(new model_detail_daftar_agunan1("AC675878","23 Februari 2019","Kendaraan", "Rp 15.000.000", "image.jpg",  "Sepeda Motor"));
//
//        listDataHeader = new ArrayList<String>();
//        listDataChild = new HashMap<String, List<model_detail_daftar_agunan1>>();
//        for (int i = 0; i < m.size(); i++) {
//            List<model_detail_daftar_agunan1> itemsadd = new ArrayList<>();
//            itemsadd.add(m.get(i));
//            listDataHeader.add(m.get(i).getKodenama());
//            listDataChild.put(m.get(i).getKodenama(), itemsadd);
//        }
//        adapter_detail_daftaragunan expandableListAdapter = new adapter_detail_daftaragunan(this, listDataHeader, listDataChild);
//        expandableListView.setAdapter(expandableListAdapter);
//
//
//    }
//    private void restoreActionBar() {
//        setSupportActionBar(toolbar);
////        toolbarTitle.setText("Keranjang");
////        toolbarTitle.setTextColor(Color.BLACK);
//        getSupportActionBar().setTitle("");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
////            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
//        getSupportActionBar().setDisplayShowTitleEnabled(true);
////        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
        ButterKnife.bind(this);
        List<model_detail_daftar_agunan1> m = new ArrayList<>();
        m.add(new model_detail_daftar_agunan1("AC143283", "23 Mei 2019", "Barang Berharga", "Rp 45.000.000", "Perhiasan", "image.jpg"));
        m.add(new model_detail_daftar_agunan1("AC557687", "30 Maret 2019", "Kendaraan", "Rp 10.000.000", "Mobil", "image.jpg"));
        m.add(new model_detail_daftar_agunan1("AC675878", "23 Februari 2019", "Kendaraan", "Rp 15.000.000", "Sepeda Motor", "image.jpg"));

        listDataHeader = new ArrayList<ModelHeader>();
        listDataChild = new HashMap<String, List<model_detail_daftar_agunan1>>();
        for (int i = 0; i < m.size(); i++) {
            List<model_detail_daftar_agunan1> itemsadd = new ArrayList<>();
            itemsadd.add(m.get(i));
            listDataHeader.add(new ModelHeader(m.get(i).getKodenama(), m.get(i).getTanggal()));
            listDataChild.put(m.get(i).getKodenama(), itemsadd);
//            listDataChild.put(m.get(i).getTanggal(), itemsadd);
        }

        adapter_detail_daftaragunan expandableListAdapterPinjaman = new adapter_detail_daftaragunan(this, listDataHeader, listDataChild);
        expandableListView1.setAdapter(expandableListAdapterPinjaman);

        //navigationonclik
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(1).setChecked(true);
//        bnMain.getMenu().getItem(0).setCheckable(false);
//        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
