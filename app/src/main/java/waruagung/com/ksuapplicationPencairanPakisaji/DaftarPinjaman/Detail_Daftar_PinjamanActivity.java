package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelExpendatebelDaftarPinjam;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.adapter.adapter_detail_daftar_pinjaman;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.model.ModelHeader;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class Detail_Daftar_PinjamanActivity extends AppCompatActivity {
  private static final String TAG = Detail_Daftar_PinjamanActivity.class.getSimpleName();
  //    LinearLayout linierisitakeorderplan;
//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
//    @BindView(R.id.bn_main)
//    BottomNavigationView bnMain;
//    @BindView(R.id.ExpandableListView)
//    ExpandableListView expandableListView;
//
//
//    private String mTitle = "List Review Plan";
//    private SessionManager sessionManager;
//    private List<String> listDataHeader;
//    private HashMap<String, List<model_detail_daftar_pinjaman>> listDataChild;
  @BindView(R.id.bn_main)
  BottomNavigationView bnMain;
  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.ExpandableListView)
  ExpandableListView expandableListView1;
  @BindView(R.id.nama)
  TextView nama;
  @BindView(R.id.kodeid)
  TextView kodeid;
  @BindView(R.id.jumlahuang)
  TextView jumlahuang;
  @BindView(R.id.pinjaman)
  TextView pinjaman;
  private APIInterface apiInterface;
  private APIClient ApiClient;
  private GridLayoutManager gridLayoutManager;
  private SessionManager sessionManager;
  private List<ModelHeader> listDataHeader;
  private HashMap<String, List<ModelExpendatebelDaftarPinjam>> listDataChild;
  private Intent intent;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail__daftar__pinjaman);
    ButterKnife.bind(this);
    //navigationonclik
    bnMain.getMenu().getItem(0).setCheckable(false);
    bnMain.getMenu().getItem(1).setChecked(true);
    bnMain.getMenu().getItem(0).setCheckable(false);
    bnMain.getMenu().getItem(1).setCheckable(true);
    bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
      @Override
      public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
          case R.id.home_menu:
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            break;
          case R.id.pencairan:
            Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
            startActivity(intent1);
            break;
          case R.id.DaftarPencairan:
            Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
            startActivity(intent2);
            break;
          case R.id.LaporanPencairan:
            Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
            startActivity(intent3);
            break;
        }
        return false;
      }
    });

//        m.add(new model_detail_daftar_pinjaman("PJ766889","23 Mei 2019","Rp 15.000.000","Rp 15.000.000", "Flat", "12 Bulan","0,5%","-","Transfer BCA","9849273873 a/n Lusi Aprillia" ));
//        m.add(new model_detail_daftar_pinjaman("PJ656722","12 April 2019","Rp 29.000.000","Rp 25.000.000", "Flat", "12 Bulan","0,5%","-","Transfer BCA","9849273873 a/n Lusi Aprillia" ));
//        m.add(new model_detail_daftar_pinjaman("PJ557687","19 Februari 2019","Rp 20.000.000","Rp 10.000.000", "Flat", "12 Bulan","0,5%","-","Transfer BCA","9849273873 a/n Lusi Aprillia" ));

    apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
    intent = getIntent();
    String contents = intent.getStringExtra("catId");
    String namaId = intent.getStringExtra("namaId");
    String Id = intent.getStringExtra("Id");
    String hargaId = intent.getStringExtra("hargaId");
    String pinjamanId = intent.getStringExtra("pinjamanId");
    nama.setText(namaId);
    kodeid.setText(Id);
    jumlahuang.setText("Rp " + MainActivity.nf.format(Integer.parseInt(hargaId)));
    pinjaman.setText(pinjamanId);
    Log.e(TAG, "onCreate: " + contents);

    Call<List<ModelExpendatebelDaftarPinjam>> call = apiInterface.doGetAPIDaftarPinjamanDetail(Http.getsCmp(), contents);
    call.enqueue(new Callback<List<ModelExpendatebelDaftarPinjam>>() {
      @Override
      public void onResponse(Call<List<ModelExpendatebelDaftarPinjam>> call, Response<List<ModelExpendatebelDaftarPinjam>> response) {
        List<ModelExpendatebelDaftarPinjam> m = response.body();
        if (m.size() >= 1) {

          listDataHeader = new ArrayList<ModelHeader>();
          listDataChild = new HashMap<String, List<ModelExpendatebelDaftarPinjam>>();

          for (int i = 0; i < m.size(); i++) {
            List<ModelExpendatebelDaftarPinjam> itemsadd = new ArrayList<>();
            itemsadd.add(m.get(i));
            listDataHeader.add(new ModelHeader(m.get(i).getTrnloanno(), m.get(i).getTrnloanopendate()));
            listDataChild.put(m.get(i).getTrnloanno(), itemsadd);
//            listDataChild.put(m.get(i).getTanggal(), itemsadd);

          }

          adapter_detail_daftar_pinjaman expandableListAdapterPinjaman = new adapter_detail_daftar_pinjaman(getApplicationContext(), listDataHeader, listDataChild);
          expandableListView1.setAdapter(expandableListAdapterPinjaman);
        }
      }

      @Override
      public void onFailure(Call<List<ModelExpendatebelDaftarPinjam>> call, Throwable t) {

      }
    });


//        //        bnMain.getMenu().getItem(0).setCheckable(false);
////        bnMain.getMenu().getItem(1).setChecked(true);
////        bnMain.getMenu().getItem(0).setCheckable(false);
////        bnMain.getMenu().getItem(1).setCheckable(true);
//        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//                switch (menuItem.getItemId()) {
//                    case R.id.NavHome:
////                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
////                        startActivity(intent);
////                        break;
//                    case R.id.NavAgunan:
////                        Intent intent1 = new Intent(getApplicationContext(), AgunanActivity.class);
////                        startActivity(intent1);
////                        break;
//                    case R.id.NavPinjaman:
////                        Intent intent2 = new Intent(getApplicationContext(), PinjamanActivity.class);
////                        startActivity(intent2);
////                        break;
//                    case R.id.NavPembayaran:
////                        Intent intent3 = new Intent(getApplicationContext(), PembayaranActivity.class);
////                        startActivity(intent3);
////                        break;
//                }
//                return false;
//            }
//        });

    toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });


//        ButterKnife.bind(this);
//        //navigationonclik
////        bnMain.getMenu().getItem(0).setCheckable(false);
////        bnMain.getMenu().getItem(1).setChecked(true);
////        bnMain.getMenu().getItem(0).setCheckable(false);
////        bnMain.getMenu().getItem(1).setCheckable(true);
//        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//                switch (menuItem.getItemId()) {
//                    case R.id.home_menu:
//                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//                        startActivity(intent);
//                        break;
//                    case R.id.agunan:
//                        Intent intent1 = new Intent(getApplicationContext(), AgunanActivity.class);
//                        startActivity(intent1);
//                        break;
//                    case R.id.pinjaman:
//                        Intent intent2 = new Intent(getApplicationContext(), PinjamanActivity.class);
//                        startActivity(intent2);
//                        break;
//                    case R.id.Pembayaran:
//                        Intent intent3 = new Intent(getApplicationContext(), Pembayaran_Activity.class);
//                        startActivity(intent3);
//                        break;
//                }
//                return false;
//            }
//        });
//
//        restoreActionBar();
//
//        sessionManager = new SessionManager(Detail_Daftar_PinjamanActivity.this);
////        restoreActionBar();
//        List<model_detail_daftar_pinjaman> m = new ArrayList<>();
//        m.add(new model_detail_daftar_pinjaman("PJ766889","23 Mei 2019","Rp 15.000.000","Rp 15.000.000", "Flat", "12 Bulan","0,5%","-","Transfer BCA","9849273873 a/n Lusi Aprillia" ));
//        m.add(new model_detail_daftar_pinjaman("PJ656722","12 April 2019","Rp 29.000.000","Rp 25.000.000", "Flat", "12 Bulan","0,5%","-","Transfer BCA","9849273873 a/n Lusi Aprillia" ));
//        m.add(new model_detail_daftar_pinjaman("PJ557687","19 Februari 2019","Rp 20.000.000","Rp 10.000.000", "Flat", "12 Bulan","0,5%","-","Transfer BCA","9849273873 a/n Lusi Aprillia" ));

//        listDataHeader = new ArrayList<String>();
//        listDataChild = new HashMap<String, List<model_detail_daftar_pinjaman>>();
//        for (int i = 0; i < m.size(); i++) {
//            List<model_detail_daftar_pinjaman> itemsadd = new ArrayList<>();
//            itemsadd.add(m.get(i));
//            listDataHeader.add(m.get(i).getKodenama());
//            listDataChild.put(m.get(i).getKodenama(), itemsadd);
//        }
//        adapter_detail_daftar_pinjaman expandableListAdapter = new adapter_detail_daftar_pinjaman(this, listDataHeader, listDataChild);
//        expandableListView.setAdapter(expandableListAdapter);
//
//
//    }
//    private void restoreActionBar() {
//        setSupportActionBar(toolbar);
////        toolbarTitle.setText("Keranjang");
////        toolbarTitle.setTextColor(Color.BLACK);
//        getSupportActionBar().setTitle("");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
////            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
//        getSupportActionBar().setDisplayShowTitleEnabled(true);
////        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
  }
}
