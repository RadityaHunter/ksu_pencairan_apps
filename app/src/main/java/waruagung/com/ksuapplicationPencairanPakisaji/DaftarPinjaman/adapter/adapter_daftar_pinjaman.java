package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelDaftarPinjaman;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.Detail_Daftar_PinjamanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class adapter_daftar_pinjaman extends RecyclerView.Adapter<adapter_daftar_pinjaman.myViewHolder> {
  public static final String TAG = adapter_daftar_pinjaman.class.getSimpleName();
  static final String EXTRAS_DATA = "EXTRAS_data";
  private Context context;
  //    private ArrayList<model_daftar_pinjaman> listMenu;
  private Intent intent;
  private List<ModelDaftarPinjaman> listMenu;

  public adapter_daftar_pinjaman(List<ModelDaftarPinjaman> listMenu, Context context, int result) {
    this.context = context;
    this.listMenu = listMenu;
  }

  @NonNull
  @Override
  public adapter_daftar_pinjaman.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.desain_adapter_daftar_pinjaman, viewGroup, false);
    return new adapter_daftar_pinjaman.myViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull adapter_daftar_pinjaman.myViewHolder myViewHolder, final int i) {
    Log.e(TAG, "onBindViewHolder: " + listMenu.get(i));
    myViewHolder.nama.setText(listMenu.get(i).getMembername());
    myViewHolder.noid.setText(listMenu.get(i).getMembermasterno());
    myViewHolder.jumlahuang.setText("Rp " + MainActivity.nf.format(listMenu.get(i).getNLoanNom()));
    myViewHolder.jumlahpinjaman.setText(listMenu.get(i).getNLoan() + " Pinjaman");

//intent
    myViewHolder.ListBarang.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(context, Detail_Daftar_PinjamanActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("catId", String.valueOf(listMenu.get(i).getMemberloanoid()));
        intent.putExtra("namaId", listMenu.get(i).getMembername());
        intent.putExtra("Id", listMenu.get(i).getMembermasterno());
        intent.putExtra("hargaId", String.valueOf(listMenu.get(i).getNLoanNom()));
        intent.putExtra("pinjamanId", listMenu.get(i).getNLoan() + "");
        context.getApplicationContext().startActivity(intent);
      }
    });

//        myViewHolder.ListBarang.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int id = listMenu.get(i).getKodeMenu();
//                MenuKategori(id, v);
//            }
//        });
//
  }

//    private void MenuKategori(int id, View v) {
//        switch (id) {
//            case 1:
////            Langsung Laku
//                intent = new Intent(context, Detail_Daftar_PinjamanActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 2:
//                //Toko Member
//                intent = new Intent(context, DaftarPinjamPrasetyaActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 3:
////            Pinjaman Online
//                intent = new Intent(context, DaftarPinjamOlisActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 4:
  //Tukar Tambah
//                intent = new Intent(context, ProfilMemberActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 5:
//                //Hotel
//                intent = new Intent(context, HotelActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 6:
  //Tiket Event
//                intent = new Intent(context, ProfilMemberActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 7:
////            Komisi
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 8:
////            TokoCabang
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 9:
////            Power Merchant
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 10:
////            Seller Center
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 11:
////            TopAds
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 12:
////            Pulsa
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 1);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 13:
////            Paket Data
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 2);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 14:
//                //Pascabayar
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 3);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 15:
//                //Roaming
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 4);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 16:
//                //Air PDAM
//                intent = new Intent(context, AirPDAMActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 17:
//                //Angsuran Kredit
//                intent = new Intent(context, AngsuranKreditActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 18:
//                //Belajar
//                intent = new Intent(context, BelajarActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 22:
//                //Topup OVO
//                intent = new Intent(context, OVOActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 23:
//                //Donasi
//                intent = new Intent(context, DonasiActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 25:
//                //Penerimaan negara
//                intent = new Intent(context, PenerimaannegaraActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;


//        }
//    }

  @Override
  public int getItemCount() {
    return listMenu.size();
  }

  class myViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.RLitemsMenu)
    LinearLayout ListBarang;
    @BindView(R.id.nama1)
    TextView nama;
    @BindView(R.id.noid1)
    TextView noid;
    @BindView(R.id.jumlahuang1)
    TextView jumlahuang;
    @BindView(R.id.tvjumlahpinjaman)
    TextView jumlahpinjaman;

    public myViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
