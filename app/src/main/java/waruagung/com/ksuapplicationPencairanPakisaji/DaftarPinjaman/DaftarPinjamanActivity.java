package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelDaftarPinjaman;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.adapter.adapter_daftar_pinjaman;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class DaftarPinjamanActivity extends AppCompatActivity {
    private static final String TAG = DaftarPinjamanActivity.class.getSimpleName();
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.Rvdaftaragunan)
    RecyclerView Rvdaftaragunan;
    @BindView(R.id.cari)
    EditText cari;
    private APIInterface apiInterface;
    private APIClient ApiClient;
    private GridLayoutManager gridLayoutManager;
    private SessionManager sessionManager;
    private String dataCari = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pinjaman2);
        ButterKnife.bind(this);
        daftaragunan();
        cari.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (cari.getText().length() >= 1) {
                    Log.e(TAG, "onTextChanged: Cari" + s.toString() + " " + dataCari.toLowerCase());
                    dataCari = s.toString();
                    daftaragunanCari(s.toString());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
        restoreActionBar();
    }

    private void daftaragunanCari(String toString) {
        gridLayoutManager = new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false);
        Rvdaftaragunan.setHasFixedSize(true);
        Rvdaftaragunan.setLayoutManager(new LinearLayoutManager(this));
        Rvdaftaragunan.setNestedScrollingEnabled(false);
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelDaftarPinjaman>> call = apiInterface.doGetAPIDaftarPinjamanSearch(Http.getsCmp(), toString);
        call.enqueue(new Callback<List<ModelDaftarPinjaman>>() {
            @Override
            public void onResponse(Call<List<ModelDaftarPinjaman>> call, Response<List<ModelDaftarPinjaman>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.e(TAG, "onResponse: " + response.toString());
                    List<ModelDaftarPinjaman> listAlbum = response.body();
                    adapter_daftar_pinjaman adapter_daftar_pinjaman2 = new adapter_daftar_pinjaman(listAlbum, getApplicationContext(), 0);
                    Rvdaftaragunan.setAdapter(adapter_daftar_pinjaman2);
//                    BigSaleProductListAdapter bigSaleProductListAllAdapter = new BigSaleProductListAdapter(listProduct, getApplicationContext(), 0);
//                    recyclerView.setAdapter(bigSaleProductListAllAdapter);
                } else {
                    Log.e(TAG, "onResponse: " + response.toString());
                }
            }

            @Override
            public void onFailure(Call<List<ModelDaftarPinjaman>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void daftaragunan() {
        gridLayoutManager = new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false);
        Rvdaftaragunan.setHasFixedSize(true);
        Rvdaftaragunan.setLayoutManager(new LinearLayoutManager(this));
        Rvdaftaragunan.setNestedScrollingEnabled(false);
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelDaftarPinjaman>> call = apiInterface.doGetAPIDaftarPinjaman(Http.getsCmp());
        call.enqueue(new Callback<List<ModelDaftarPinjaman>>() {
            @Override
            public void onResponse(Call<List<ModelDaftarPinjaman>> call, Response<List<ModelDaftarPinjaman>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.e(TAG, "onResponse: " + response.toString());
                    List<ModelDaftarPinjaman> listAlbum = response.body();
                    adapter_daftar_pinjaman adapter_daftar_pinjaman2 = new adapter_daftar_pinjaman(listAlbum, getApplicationContext(), 0);
                    Rvdaftaragunan.setAdapter(adapter_daftar_pinjaman2);
//                    BigSaleProductListAdapter bigSaleProductListAllAdapter = new BigSaleProductListAdapter(listProduct, getApplicationContext(), 0);
//                    recyclerView.setAdapter(bigSaleProductListAllAdapter);
                } else {
                    Log.e(TAG, "onResponse: " + response.toString());
                }
            }

            @Override
            public void onFailure(Call<List<ModelDaftarPinjaman>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });


//        Rvdaftaragunan.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false));
//        Rvdaftaragunan.setHasFixedSize(true);
//        Rvdaftaragunan.setNestedScrollingEnabled(false);
//        ArrayList<model_daftar_pinjaman> listCategory = new ArrayList<>();
//        listCategory.add(new model_daftar_pinjaman("LUSI APRILLIA", "9849273873", 1, "RP 50.000.000", "3 Pinjaman"));
//        listCategory.add(new model_daftar_pinjaman("PRASETYA", "13124234677", 2, "RP 10.000.000", "5 Pinjaman"));
//        listCategory.add(new model_daftar_pinjaman("OLIS DARE", "5467685876", 3, "RP 5.000.000", "1 Pinjaman"));
//        adapter_daftar_pinjaman adapter_daftar_pinjaman = new adapter_daftar_pinjaman(this, listCategory);
//        Rvdaftaragunan.setAdapter(adapter_daftar_pinjaman);

    }


}

