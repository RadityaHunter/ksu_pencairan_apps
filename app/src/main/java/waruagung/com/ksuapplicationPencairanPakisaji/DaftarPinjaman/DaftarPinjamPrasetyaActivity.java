package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;

import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.adapter.adapter_detail_daftar_pinjaman_prasetya;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.model.ModelHeader;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.model.model_detail_daftar_pinjaman_prasetya;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class DaftarPinjamPrasetyaActivity extends AppCompatActivity {
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ExpandableListView)
    ExpandableListView expandableListView1;

    private List<ModelHeader> listDataHeader;
    private HashMap<String, List<model_detail_daftar_pinjaman_prasetya>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pinjam_prasetya);
        ButterKnife.bind(this);
        //navigationonclik
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
        List<model_detail_daftar_pinjaman_prasetya> m = new ArrayList<>();
        m.add(new model_detail_daftar_pinjaman_prasetya("PJ766889", "23 Mei 2019", "Rp 15.000.000", "Rp 15.000.000", "Flat", "12 Bulan", "0,5%", "-", "Transfer BCA", "9849273873 a/n Lusi Aprillia"));
        m.add(new model_detail_daftar_pinjaman_prasetya("PJ656722", "12 April 2019", "Rp 29.000.000", "Rp 25.000.000", "Flat", "12 Bulan", "0,5%", "-", "Transfer BCA", "9849273873 a/n Lusi Aprillia"));
        m.add(new model_detail_daftar_pinjaman_prasetya("PJ557687", "19 Februari 2019", "Rp 20.000.000", "Rp 10.000.000", "Flat", "12 Bulan", "0,5%", "-", "Transfer BCA", "9849273873 a/n Lusi Aprillia"));

        listDataHeader = new ArrayList<ModelHeader>();
        listDataChild = new HashMap<String, List<model_detail_daftar_pinjaman_prasetya>>();
        for (int i = 0; i < m.size(); i++) {
            List<model_detail_daftar_pinjaman_prasetya> itemsadd = new ArrayList<>();
            itemsadd.add(m.get(i));
            listDataHeader.add(new ModelHeader(m.get(i).getKodenama(), m.get(i).getTanggal()));
            listDataChild.put(m.get(i).getKodenama(), itemsadd);
//            listDataChild.put(m.get(i).getTanggal(), itemsadd);
        }

        adapter_detail_daftar_pinjaman_prasetya expandableListAdapterPinjaman = new adapter_detail_daftar_pinjaman_prasetya(this, listDataHeader, listDataChild);
        expandableListView1.setAdapter(expandableListAdapterPinjaman);

//        //        bnMain.getMenu().getItem(0).setCheckable(false);
////        bnMain.getMenu().getItem(1).setChecked(true);
////        bnMain.getMenu().getItem(0).setCheckable(false);
////        bnMain.getMenu().getItem(1).setCheckable(true);
//        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//                switch (menuItem.getItemId()) {
//                    case R.id.NavHome:
////                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
////                        startActivity(intent);
////                        break;
//                    case R.id.NavAgunan:
////                        Intent intent1 = new Intent(getApplicationContext(), AgunanActivity.class);
////                        startActivity(intent1);
////                        break;
//                    case R.id.NavPinjaman:
////                        Intent intent2 = new Intent(getApplicationContext(), PinjamanActivity.class);
////                        startActivity(intent2);
////                        break;
//                    case R.id.NavPembayaran:
////                        Intent intent3 = new Intent(getApplicationContext(), PembayaranActivity.class);
////                        startActivity(intent3);
////                        break;
//                }
//                return false;
//            }
//        });

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
