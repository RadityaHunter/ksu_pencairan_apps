package waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelExpendatebelDaftarPinjam;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPinjaman.model.ModelHeader;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class adapter_detail_daftar_pinjaman extends BaseExpandableListAdapter {

  private static final String TAG = adapter_detail_daftar_pinjaman.class.getSimpleName();
  private List<ModelHeader> expandableListTitle;
  private HashMap<String, List<ModelExpendatebelDaftarPinjam>> expandableListDetail;
  private Context context;

  public adapter_detail_daftar_pinjaman(Context context, List<ModelHeader> title, HashMap<String, List<ModelExpendatebelDaftarPinjam>> expandableListDetail) {
    this.context = context;
    this.expandableListTitle = title;
    this.expandableListDetail = expandableListDetail;
  }

  @Override

  public int getGroupCount() {
    return expandableListTitle.size();
  }

  @Override
  public int getChildrenCount(int groupPosition) {
    return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition).getTitle()).size();
  }

  @Override
  public ModelHeader getGroup(int groupPosition) {
    return expandableListTitle.get(groupPosition);
  }

  @Override
  public ModelExpendatebelDaftarPinjam getChild(int groupPosition, int childPosition) {
    return this.expandableListDetail.get(this.expandableListTitle.get(groupPosition).getTitle())
        .get(childPosition);
  }

  @Override
  public long getGroupId(int groupPosition) {
    return groupPosition;
  }

  @Override
  public long getChildId(int groupPosition, int childPosition) {
    return childPosition;
  }

  @Override
  public boolean hasStableIds() {
    return false;
  }

  @Override
  public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
    ModelHeader listTitle = getGroup(groupPosition);
    if (convertView == null) {
      LayoutInflater layoutInflater = (LayoutInflater) this.context.
          getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = layoutInflater.inflate(R.layout.adapter_detail_hider_desain_daftar_pinjaman, null);
    }
    TextView listTitleTextView = convertView
        .findViewById(R.id.listTitleTextView);
//        listTitleTextView.setTypeface(null, Typeface.BOLD);
    listTitleTextView.setText(listTitle.getTitle());
    TextView listtanggal = convertView
        .findViewById(R.id.tanggal);
//        listtanggal.setTypeface(null, Typeface.BOLD);
    try {
      Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(listTitle.getDate());
      String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
      listtanggal.setText(formattedDate);

    } catch (ParseException e) {
      e.printStackTrace();
    }

    if (isExpanded) {
      listtanggal.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.panahbawah, 0);
    } else {
      listtanggal.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.panahatas, 0);
    }
    return convertView;
  }

  @Override
  public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
    ModelExpendatebelDaftarPinjam jsonInString = getChild(groupPosition, childPosition);
    JSONObject jsonObject = null;


    if (convertView == null) {
      LayoutInflater layoutInflater = (LayoutInflater) this.context
          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = layoutInflater.inflate(R.layout.desain_expandable_detail_daftar_pinjaman, null);
    }


    TextView jumlahpinjam = convertView
        .findViewById(R.id.jumlahpinjam);
    jumlahpinjam.setText("Rp " + MainActivity.nf.format(jsonInString.getTrnloanamt().intValue()));
    TextView last_sisapembayaran = convertView
        .findViewById(R.id.sisapembayaran);
    last_sisapembayaran.setText("Rp " + MainActivity.nf.format(jsonInString.getSisa().intValue()));
    TextView jenispinjaman = convertView
        .findViewById(R.id.jnspinjaman);
    jenispinjaman.setText(jsonInString.getJenisPinjaman());
    TextView jangkawaktu = convertView
        .findViewById(R.id.jangakawaktu);
    jangkawaktu.setText(String.valueOf(jsonInString.getJangkaWaktu().intValue()));
    try {

      Date dateTempo = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(jsonInString.getJatuhTempo());
      String formatteddateTempo = new SimpleDateFormat("dd/MM/yyyy").format(dateTempo);

      TextView jatuhtempo = convertView
          .findViewById(R.id.tanggal);
      jatuhtempo.setText(formatteddateTempo);

    } catch (ParseException e) {
      e.printStackTrace();
    }
    TextView bunga = convertView
        .findViewById(R.id.persenbunga);
    bunga.setText(String.valueOf(jsonInString.getBunga()));
    TextView jenispencairan = convertView
        .findViewById(R.id.tvjenispencairan);
    jenispencairan.setText(jsonInString.getBankName());
    TextView norek = convertView
        .findViewById(R.id.tvRekening);
    norek.setText(jsonInString.getNorek());
    TextView ket = convertView
        .findViewById(R.id.tvKeterangan);
    ket.setText(jsonInString.getKeterangan());

//        TextView timeTakeOrder = convertView
//                .findViewById(R.id.tanggal);
//        timeTakeOrder.setText(jsonInString.getTanggal()+"");
//        TextView last_order = convertView
//                .findViewById(R.id.last_order);
//        last_order.setText("Rp." + jsonInString.getLastOrder());
//        TextView piutang = convertView
//                .findViewById(R.id.piutang);
//        piutang.setText("Rp." + jsonInString.getPiutang());

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context, MainActivity.class);
//                context.startActivity(intent);
//            }
//        });

    return convertView;
  }

  @Override
  public boolean isChildSelectable(int groupPosition, int childPosition) {
    return false;
  }
}
