package waruagung.com.ksuapplicationPencairanPakisaji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanListDetail;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterRecyclerViewRiwayatPembayaran extends
        RecyclerView.Adapter<AdapterRecyclerViewRiwayatPembayaran.ViewHolder> {

    private static final String TAG = AdapterRecyclerViewRiwayatPembayaran.class.getSimpleName();


    private Context context;
    private List<ModelDaftarPinjamanListDetail> list;
    private AdapterRecyclerViewRiwayatPembayaranCallback mAdapterCallback;
    private int result = -1;

    public AdapterRecyclerViewRiwayatPembayaran(Context context, int result, List<ModelDaftarPinjamanListDetail> list, AdapterRecyclerViewRiwayatPembayaranCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.result = result;
        this.mAdapterCallback = adapterCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_recyclerview_detail_riwayat,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelDaftarPinjamanListDetail item = list.get(position);
        holder.listTitleTextView.setText(item.getTrnloanno());
        holder.tvJumlahPinjaman.setText("Rp " + MainActivity.nf.format(item.getTrnloanamt()));
        holder.tvJumlahDibayar.setText("Rp " + MainActivity.nf.format(item.getSisa()));
        holder.recyclerViewDetailRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onRowAdapterRecyclerViewRiwayatPembayaranClicked(position);

            }
        });

    }

    public void addItems(List<ModelDaftarPinjamanListDetail> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    @OnClick(R.id.recyclerViewDetailRow)
    public void onViewClicked() {
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.listTitleTextView)
        TextView listTitleTextView;
        @BindView(R.id.tanggal)
        TextView tanggal;
        @BindView(R.id.guideline)
        Guideline guideline;
        @BindView(R.id.tvJumlahDibayar)
        TextView tvJumlahDibayar;
        @BindView(R.id.tvJumlahPinjaman)
        TextView tvJumlahPinjaman;


        @BindView(R.id.recyclerViewDetailRow)
        ConstraintLayout recyclerViewDetailRow;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AdapterRecyclerViewRiwayatPembayaranCallback {
        void onRowAdapterRecyclerViewRiwayatPembayaranClicked(int position);
    }
}