package waruagung.com.ksuapplicationPencairanPakisaji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.MessageFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelReportPencairanResponse;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterReportPencairan extends
        RecyclerView.Adapter<AdapterReportPencairan.ViewHolder> {

    private static final String TAG = AdapterReportPencairan.class.getSimpleName();


    private Context context;
    private List<ModelReportPencairanResponse> list;
    private AdapterReportPencairanCallback mAdapterCallback;
    private int result = -1;

    public AdapterReportPencairan(Context context, int result, List<ModelReportPencairanResponse> list, AdapterReportPencairanCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.result = result;
        this.mAdapterCallback = adapterCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_daftar_pencairan,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelReportPencairanResponse item = list.get(position);
        holder.namastaf.setText(item.getMembername());
        holder.total.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format(item.getTrnloanamt())));
        holder.idnasabah.setText(item.getTrnrequestno());
        holder.keterangan.setText(item.getApploancashstatus());
        holder.Rvdesaincari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onRowAdapterReportPencairanClicked(position);
            }
        });
    }

    public void addItems(List<ModelReportPencairanResponse> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.namastaf)
        TextView namastaf;
        @BindView(R.id.idnasabah)
        TextView idnasabah;
        @BindView(R.id.total)
        TextView total;
        @BindView(R.id.keterangan)
        TextView keterangan;
        @BindView(R.id.Rvdesaincari)
        LinearLayout Rvdesaincari;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AdapterReportPencairanCallback {
        void onRowAdapterReportPencairanClicked(int position);
    }
}