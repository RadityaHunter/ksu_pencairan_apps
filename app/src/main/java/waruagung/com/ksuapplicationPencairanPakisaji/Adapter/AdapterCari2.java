package waruagung.com.ksuapplicationPencairanPakisaji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelNamaAnggotaNasabah;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterCari2 extends
        RecyclerView.Adapter<AdapterCari2.ViewHolder> {

    private static final String TAG = AdapterCari2.class.getSimpleName();

    private Context context;
    private List<ModelNamaAnggotaNasabah> list;
    private AdapterCari2Callback mAdapterCallback;
    private int result = -1;

    public AdapterCari2(Context context, int result, List<ModelNamaAnggotaNasabah> list, AdapterCari2Callback adapterCallback) {
        this.context = context;
        this.list = list;
        this.result = result;
        this.mAdapterCallback = adapterCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_cari_anggota,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelNamaAnggotaNasabah item = list.get(position);
        holder.nama.setText(item.getMembername());
        holder.ID.setText(item.getMembermasterno());
        holder.Rvcari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onRowAdapterCari2Clicked(item);

            }
        });

    }

    public void addItems(List<ModelNamaAnggotaNasabah> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface AdapterCari2Callback {
        void onRowAdapterCari2Clicked(ModelNamaAnggotaNasabah modelNamaAnggotaNasabah);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.nama)
        TextView nama;
        @BindView(R.id.ID)
        TextView ID;
        @BindView(R.id.gambar1)
        ImageView gambar1;
        @BindView(R.id.view1)
        View view1;
        @BindView(R.id.Rvcari)
        LinearLayout Rvcari;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}