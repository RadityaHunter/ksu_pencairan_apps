package waruagung.com.ksuapplicationPencairanPakisaji.Adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelMenuHome;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterMenuHome extends
        RecyclerView.Adapter<AdapterMenuHome.ViewHolder> {

    private static final String TAG = AdapterMenuHome.class.getSimpleName();


    private Context context;
    private List<ModelMenuHome> list;
    private AdapterMenuHomeCallback mAdapterCallback;
    private int result = -1;

    public AdapterMenuHome(Context context, int result, List<ModelMenuHome> list, AdapterMenuHomeCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.result = result;
        this.mAdapterCallback = adapterCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_menu_home,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelMenuHome item = list.get(position);
        holder.imageBanner.setImageResource(item.getMenuImage());
        holder.tvMenuTItle.setText(Html.fromHtml(item.getMenuname()));
        holder.LaporanPencairan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onRowAdapterMenuHomeClicked(item.getMenuId());
            }
        });
    }

    public void addItems(List<ModelMenuHome> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface AdapterMenuHomeCallback {
        void onRowAdapterMenuHomeClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageBanner)
        ImageView imageBanner;
        @BindView(R.id.tvMenuTItle)
        TextView tvMenuTItle;
        @BindView(R.id.LaporanPencairan)
        LinearLayout LaporanPencairan;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}