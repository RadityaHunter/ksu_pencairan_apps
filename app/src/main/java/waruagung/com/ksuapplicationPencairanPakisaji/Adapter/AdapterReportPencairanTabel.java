package waruagung.com.ksuapplicationPencairanPakisaji.Adapter;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelReportPencairanResponse;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterReportPencairanTabel extends
        RecyclerView.Adapter<AdapterReportPencairanTabel.ViewHolder> {

    private static final String TAG = AdapterReportPencairanTabel.class.getSimpleName();


    private Context context;
    private List<ModelReportPencairanResponse> list;
    //    private AdapterReportPencairanCallback mAdapterCallback;
    private int result = -1;

    public AdapterReportPencairanTabel(Context context, int result, List<ModelReportPencairanResponse> list) {
        this.context = context;
        this.list = list;
        this.result = result;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_laporan_pencairan,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelReportPencairanResponse item = list.get(position);
        holder.tvNoAC.setText(item.getNoac());
        holder.tvNoSPK.setText(item.getNospk());
        holder.tvNama.setText(item.getMembername());
        double dt = item.getTrnloanamt();
        double dt2 = item.getTrnloanamtrev();
        holder.jmlahPinjaman.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format((int) dt)));
        holder.jumlahDiterima.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format((int) dt2)));
        holder.tvJenisPinjaman.setText(item.getLoandesc());
//        if (item.getApploancashstatus().equals("Rejected")) {
//            holder.title_keterangan.setText("Dibatalkan oleh");
//        }
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(item.getTglcair());
            String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
            holder.tglPencairan.setText(formattedDate);

        } catch (Exception e) {
            holder.tglPencairan.setVisibility(View.GONE);
        }
        holder.tvAgunan.setText(item.getAgunan());
        holder.dicairkanoleh.setText(item.getApploancashnote());

        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context, Locale.getDefault());
            addresses = geocoder.getFromLocation(Double.parseDouble(list.get(position).getGeolat()), Double.parseDouble(list.get(position).getGeolng()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            Log.e(TAG, "getAddresCheckIn: " + MessageFormat.format("{0}, {1}, {2}, {3}, {4}", address, city, state, country, postalCode));
            holder.alamat_check_in.setText(MessageFormat.format("{0}, {1}, {2}, {3}, {4}", address, city, state, country, postalCode));

        } catch (Exception e) {
            holder.alamat_check_in.setText("-");
            Log.e(TAG, "getAddresCheckIn: " + e.getMessage());
        }


//        holder.total.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format(item.getTrnloanamt())));
//        holder.idnasabah.setText(item.getTrnrequestno());
//        holder.keterangan.setText(item.getApploancashstatus());
//        holder.Rvdesaincari.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mAdapterCallback.onRowAdapterReportPencairanClicked(position);
//            }
//        });
    }

    public void addItems(List<ModelReportPencairanResponse> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface AdapterReportPencairanCallback {
        void onRowAdapterReportPencairanClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvNama)
        TextView tvNama;
        @BindView(R.id.tvNoAC)
        TextView tvNoAC;
        @BindView(R.id.tvNoSPK)
        TextView tvNoSPK;
        @BindView(R.id.jmlahPinjaman)
        TextView jmlahPinjaman;
        @BindView(R.id.jumlahDiterima)
        TextView jumlahDiterima;
        @BindView(R.id.tvJenisPinjaman)
        TextView tvJenisPinjaman;
        @BindView(R.id.tvjangkawaktu)
        TextView tvjangkawaktu;
        @BindView(R.id.tvAgunan)
        TextView tvAgunan;
        @BindView(R.id.tgl_pencairan)
        TextView tglPencairan;
        @BindView(R.id.dicairkanoleh)
        TextView dicairkanoleh;
        @BindView(R.id.alamat_check_in)
        TextView alamat_check_in;

//        @BindView(R.id.lnapproval_pegawas)
//        LinearLayout lnapproval_pegawas;
//        @BindView(R.id.lnapproval_kabagops)
//        LinearLayout lnapproval_kabagops;
//        @BindView(R.id.lnapproval_Cashier)
//        LinearLayout lnapproval_Cashier;
//        @BindView(R.id.lnapproval_Pencair)
//        LinearLayout lnapproval_Pencair;
//
//        @BindView(R.id.setuju_pengawas)
//        Button setuju_pengawas;
//        @BindView(R.id.tolak_pengawas)
//        Button tolak_pengawas;
//        @BindView(R.id.setuju_kabagops)
//        Button setuju_kabagops;
//        @BindView(R.id.tolak_kabagops)
//        Button tolak_kabagops;
//        @BindView(R.id.setuju_Cashier)
//        Button setuju_Cashier;
//        @BindView(R.id.tolak_Cashier)
//        Button tolak_Cashier;
//        @BindView(R.id.setuju_Pencair)
//        Button setuju_Pencair;
//        @BindView(R.id.tolak_Pencair)
//        Button tolak_Pencair;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}