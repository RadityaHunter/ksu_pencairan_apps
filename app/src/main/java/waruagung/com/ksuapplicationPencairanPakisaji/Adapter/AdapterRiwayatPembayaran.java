package waruagung.com.ksuapplicationPencairanPakisaji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelDaftarPinjamanList;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterRiwayatPembayaran extends
    RecyclerView.Adapter<AdapterRiwayatPembayaran.ViewHolder> {

  private static final String TAG = AdapterRiwayatPembayaran.class.getSimpleName();

  private Context context;
  private List<ModelDaftarPinjamanList> list;
  private AdapterRiwayatPembayaranCallback mAdapterCallback;
  private int result = -1;

  public AdapterRiwayatPembayaran(Context context, int result, List<ModelDaftarPinjamanList> list, AdapterRiwayatPembayaranCallback adapterCallback) {
    this.context = context;
    this.list = list;
    this.result = result;
    this.mAdapterCallback = adapterCallback;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.desain_adapter_riwayat_pembayaran,
        parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    ModelDaftarPinjamanList item = list.get(position);
    holder.nama1.setText(item.getMembername());
    holder.noid1.setText(String.valueOf(item.getMembermasterno()));
    holder.jumlahuang1.setText(MainActivity.nf.format(item.getNLoanNom()));
    holder.jumlahpinjaman.setText(item.getNLoan() + " Pinjaman");
    holder.RLitemsMenu.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mAdapterCallback.onRowAdapterRiwayatPembayaranClicked(position);

      }
    });
  }

  public void addItems(List<ModelDaftarPinjamanList> items) {
    this.list.addAll(this.list.size(), items);
    notifyDataSetChanged();
  }

  @Override
  public int getItemCount() {
    if (list.size() == 0) {
      return 0;
    } else {
      if (result >= 1) {
        return Math.min(list.size(), result);
      } else {
        return list.size();
      }
    }
  }

  public void clear() {
    int size = this.list.size();
    this.list.clear();
    notifyItemRangeRemoved(0, size);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.nama1)
    TextView nama1;
    @BindView(R.id.jumlahuang1)
    TextView jumlahuang1;
    @BindView(R.id.id1)
    TextView id1;
    @BindView(R.id.noid1)
    TextView noid1;
    @BindView(R.id.tvjumlahpinjaman)
    TextView jumlahpinjaman;
    @BindView(R.id.RLitemsMenu)
    LinearLayout RLitemsMenu;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }

  public interface AdapterRiwayatPembayaranCallback {
    void onRowAdapterRiwayatPembayaranClicked(int position);
  }
}