package waruagung.com.ksuapplicationPencairanPakisaji.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelFormPinjaman;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterNoPinjamanPembayaran extends
        RecyclerView.Adapter<AdapterNoPinjamanPembayaran.ViewHolder> {

    private static final String TAG = AdapterNoPinjamanPembayaran.class.getSimpleName();


    private Context context;
    private List<ModelFormPinjaman> list;
    private AdapterNoPinjamanPembayaranCallback mAdapterCallback;
    private int result = -1;

    public AdapterNoPinjamanPembayaran(Context context, int result, List<ModelFormPinjaman> list, AdapterNoPinjamanPembayaranCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.result = result;
        this.mAdapterCallback = adapterCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.desain_adapter_nomor_pinjaman,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelFormPinjaman item = list.get(position);
        holder.txtTitle.setText("Angsuran Ke - " + item.getTrnloandtlseq());
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(item.getTrnloandtldate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
        holder.tglJatuhtempo.setText(formattedDate);
        holder.jumlahpinjamanuang.setText("Rp " + MainActivity.nf.format(item.getTrnloandtlamt() + item.getTrnloandtlinterest()));
        holder.ListNomorPinjaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onRowAdapterNoPinjamanPembayaranClicked(position);

            }
        });
    }

    public void addItems(List<ModelFormPinjaman> items) {
        this.list.addAll(this.list.size(), items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface AdapterNoPinjamanPembayaranCallback {
        void onRowAdapterNoPinjamanPembayaranClicked(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgIcon)
        ImageView imgIcon;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.jumlahpinjamanuang)
        TextView jumlahpinjamanuang;
        @BindView(R.id.jth_tempo)
        TextView jthTempo;
        @BindView(R.id.tgl_jatuhtempo)
        TextView tglJatuhtempo;
        @BindView(R.id.ListNomorPinjaman)
        RelativeLayout ListNomorPinjaman;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}