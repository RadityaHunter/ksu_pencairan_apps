package waruagung.com.ksuapplicationPencairanPakisaji;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.API.model.ModelNamaAnggotaNasabah;

public class AdapterCari extends RecyclerView.Adapter<AdapterCari.myViewHolder> {
    static final String EXTRAS_DATA = "EXTRAS_data";
    private Context context;
//    private ArrayList<Modelcari> listMenu;
    private Intent intent;
    private List<ModelNamaAnggotaNasabah> listMenu;

    public AdapterCari(List<ModelNamaAnggotaNasabah> listMenu, Context context, int result) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @NonNull
    @Override
    public AdapterCari.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_cari_anggota, viewGroup, false);
        return new AdapterCari.myViewHolder(itemView);
    }
    private void MenuKategori(String names) {
        Intent intent = new Intent();
        intent.putExtra("_DATA", names);
        if (context instanceof Activity)
            ((Activity)context).setResult(Activity.RESULT_OK, intent);
        ((Activity)context).finish();

    }
    @Override
    public void onBindViewHolder(@NonNull AdapterCari.myViewHolder myViewHolder, final int i) {
        myViewHolder.MenuTitle.setText(listMenu.get(i).getMembername());
        myViewHolder.ListBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuKategori(listMenu.get(i).getMembername());
            }
        });

//        myViewHolder.harga.setText(listMenu.get(i).getHarga());

//        Glide.with(context)
//                .load(listMenu.get(i).getImageMenudeveloper()).into(myViewHolder.MenuImages);

//        myViewHolder.ListBarang.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int id = listMenu.get(i).getKodeMenu();
//                MenuKategori(id, v);
//            }
//        });
//
//    }
//
//    private void MenuKategori(int id, View v) {
//        switch (id) {
//            case 1:
////            Langsung Laku
//                intent = new Intent(context, FinisihingActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 2:
//                //Toko Member
//                intent = new Intent(context, KramikActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 3:
            //Pinjaman Online
//                intent = new Intent(context, ProfilMemberdddActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 4:
            //Tukar Tambah
//                intent = new Intent(context, ProfilMemberActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 5:
//                //Hotel
//                intent = new Intent(context, HotelActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 6:
            //Tiket Event
//                intent = new Intent(context, ProfilMemberActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 7:
////            Komisi
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 8:
////            TokoCabang
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 9:
////            Power Merchant
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 10:
////            Seller Center
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 11:
////            TopAds
//                intent = new Intent(context, Komisi_Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 12:
////            Pulsa
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 1);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 13:
////            Paket Data
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 2);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 14:
//                //Pascabayar
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 3);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 15:
//                //Roaming
//                intent = new Intent(context, BayarTagihanActivity.class);
//                intent.putExtra("page", 4);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 16:
//                //Air PDAM
//                intent = new Intent(context, AirPDAMActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 17:
//                //Angsuran Kredit
//                intent = new Intent(context, AngsuranKreditActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 18:
//                //Belajar
//                intent = new Intent(context, BelajarActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;

//            case 22:
//                //Topup OVO
//                intent = new Intent(context, OVOActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
//            case 23:
//                //Donasi
//                intent = new Intent(context, DonasiActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//            case 25:
//                //Penerimaan negara
//                intent = new Intent(context, PenerimaannegaraActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;



    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.ImageMenudeveloper)
//        ImageView MenuImages;
        @BindView(R.id.Rvcari)
        LinearLayout ListBarang;
        @BindView(R.id.nama)
        TextView MenuTitle;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
