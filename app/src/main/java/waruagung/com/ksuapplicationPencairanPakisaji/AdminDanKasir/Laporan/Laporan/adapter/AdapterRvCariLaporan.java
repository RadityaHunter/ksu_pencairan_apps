package waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.LaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.Model.ModelRvCariLaporan;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class AdapterRvCariLaporan extends RecyclerView.Adapter<AdapterRvCariLaporan.myViewHolder> {
    static final String EXTRAS_DATA = "EXTRAS_data";
    private Context context;
    private ArrayList<ModelRvCariLaporan> listMenu;
    private Intent intent;

    public AdapterRvCariLaporan(Context context, ArrayList<ModelRvCariLaporan> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @NonNull
    @Override
    public AdapterRvCariLaporan.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapte_desain_cari_laporan, viewGroup, false);
        return new AdapterRvCariLaporan.myViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRvCariLaporan.myViewHolder myViewHolder, final int i) {
        myViewHolder.namastaf.setText(listMenu.get(i).getNamanastaf());
        myViewHolder.nomorpinjaman.setText(listMenu.get(i).getIdnasabah());
        myViewHolder.total.setText(listMenu.get(i).getTotal());
        myViewHolder.tanggal.setText(listMenu.get(i).getTanggal());

//        Glide.with(context)
//                .load(listMenu.get(i).getImageMenudeveloper()).into(myViewHolder.MenuImages);

        myViewHolder.ListBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = listMenu.get(i).getKodeMenu();
                MenuKategori(id, v);
            }
        });

    }

    private void MenuKategori(int id, View v) {
        switch (id) {
            case 1:
//            Langsung Laku
                intent = new Intent(context, LaporanActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
            case 2:
//            Langsung Laku
                intent = new Intent(context, LaporanActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
            case 3:
//            Langsung Laku
                intent = new Intent(context, LaporanActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
            case 4:
//            Langsung Laku
                intent = new Intent(context, LaporanActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
            case 5:
//            Langsung Laku
                intent = new Intent(context, LaporanActivity.class);
                intent.putExtra("page", 0);
                v.getContext().startActivity(intent);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.Rvdesaincari)
        LinearLayout ListBarang;
        @BindView(R.id.namastaf)
        TextView namastaf;
        @BindView(R.id.nomorpinjaman)
        TextView nomorpinjaman;
        @BindView(R.id.total)
        TextView total;
        @BindView(R.id.tanggal)
        TextView tanggal;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
