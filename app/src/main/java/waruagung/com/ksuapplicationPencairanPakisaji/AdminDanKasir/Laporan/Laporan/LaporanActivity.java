package waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelReportPencairanResponse;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class LaporanActivity extends AppCompatActivity {

    public static final String TAG = LaporanActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.tvNama)
    TextView tvNama;
    @BindView(R.id.tvKodeLoan)
    TextView tvKodeLoan;
    @BindView(R.id.jmlahPinjaman)
    TextView jmlahPinjaman;
    @BindView(R.id.jumlahDiterima)
    TextView jumlahDiterima;
    @BindView(R.id.tvJenisPinjaman)
    TextView tvJenisPinjaman;
    @BindView(R.id.title_keterangan)
    TextView title_keterangan;
    @BindView(R.id.tvjangkawaktu)
    TextView tvjangkawaktu;
    @BindView(R.id.tvAgunan)
    TextView tvAgunan;
    @BindView(R.id.tgl_pencairan)
    TextView tglPencairan;
    @BindView(R.id.dicairkanoleh)
    TextView dicairkanoleh;
    private SessionManager sessionManager;
    private Boolean APP_PENCAIRAN_LAPANGAN = false;
    private ModelReportPencairanResponse modelDaftarPencairanList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        restoreActionBar();
        //navigationonclik
        try {
            Log.e(TAG, "onCreate: " + sessionManager.getRole());

            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    APP_PENCAIRAN_LAPANGAN = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (APP_PENCAIRAN_LAPANGAN) {
            bnMain.getMenu().clear();
            bnMain.inflateMenu(R.menu.menu_page_pencairan4);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setChecked(true);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setCheckable(true);
        } else {
            bnMain.getMenu().clear();
            bnMain.inflateMenu(R.menu.menu_page_pencairan);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setChecked(true);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setCheckable(true);
        }

        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;

            }
        });
        setupAwal();
    }

    private void setupAwal() {
        if (!getIntent().hasExtra("_DATA")) {
            finish();
        } else {
            modelDaftarPencairanList = getIntent().getParcelableExtra("_DATA");
            tvNama.setText(modelDaftarPencairanList.getMembername());
            tvKodeLoan.setText(modelDaftarPencairanList.getTrnrequestno());
            double dt = modelDaftarPencairanList.getTrnloanamt();
            double dt2 = modelDaftarPencairanList.getTrnloanamtrev();
            jmlahPinjaman.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format((int) dt)));
            jumlahDiterima.setText(MessageFormat.format("Rp. {0}", MainActivity.nf.format((int) dt2)));
            tvJenisPinjaman.setText(modelDaftarPencairanList.getLoandesc());
            if (modelDaftarPencairanList.getApploancashstatus().equals("Rejected")) {
                title_keterangan.setText("Dibatalkan oleh");
            }
            try {
                Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(modelDaftarPencairanList.getTglcair());
                String formattedDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
                tglPencairan.setText(formattedDate);

            } catch (Exception e) {
                        tglPencairan.setVisibility(View.GONE);
            }
            tvAgunan.setText(modelDaftarPencairanList.getAgunan());
            dicairkanoleh.setText(modelDaftarPencairanList.getPencair());
        }
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
