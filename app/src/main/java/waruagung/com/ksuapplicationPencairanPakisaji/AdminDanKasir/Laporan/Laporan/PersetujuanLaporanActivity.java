package waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.ModelDaftarPencairanListResponse2;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.VolleyErrorHelper;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;
import waruagung.com.ksuapplicationPencairanPakisaji.pembayaran.fragment.Pop_up_perjanjian_bayar_berhasilFragment;

public class PersetujuanLaporanActivity extends AppCompatActivity {

    public static final String TAG = PersetujuanLaporanActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tolak)
    Button tolak;
    @BindView(R.id.setuju)
    Button setuju;
    @BindView(R.id.nama_staf)
    TextView namaStaf;
    @BindView(R.id.nomor_pinjaman)
    TextView nomorPinjaman;
    @BindView(R.id.jumlah_pinjaman)
    TextView jumlahPinjaman;
    @BindView(R.id.jenis_pinjaman)
    TextView jenisPinjaman;
    @BindView(R.id.jumlahDiterima)
    TextView jumlahDiterima;
    @BindView(R.id.jangka_waktu)
    TextView jangkaWaktu;
    @BindView(R.id.agunan)
    TextView agunan;
    @BindView(R.id.Status_Pencairan)
    TextView StatusPencairan;
//    @BindView(R.id.nama_penyetuju)
//    TextView namaPenyetuju;
//    @BindView(R.id.admin_kredit)
//    TextView adminKredit;
    @BindView(R.id.lnAproval)
    LinearLayout lnAproval;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    private SessionManager sessionManager;
    private Boolean APP_PENCAIRAN_LAPANGAN = false;
    private ModelDaftarPencairanListResponse2 m;
    private String ACTIONAPPROVE = "";
    private String MASSAGE_FRAGMENT = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persetujuan_laporan);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        restoreActionBar();
        if (getIntent().hasExtra("_DATA")) {
            m = getIntent().getParcelableExtra("_DATA");
            namaStaf.setText(m.getMembername());
            nomorPinjaman.setText(m.getTrnrequestno());
//            jenisPinjaman.setText(m.getLoandesc());
            jumlahDiterima.setText(m.getLoandesc());
            jangkaWaktu.setText(MessageFormat.format("{0} Angsuran", m.getTrnloantime()));
//            String Coeg = "";
//            for (ModelDaftarPencairanAgunanList c : m.getAgunanList()) {
//                Coeg = Coeg + c.getName() + ", ";
//            }
            agunan.setText(m.getAgunan());
            Double dt = Double.valueOf(m.getTrnloanamt());
            Double dt2 = Double.valueOf(m.getTrnloanamtrev());
            jumlahPinjaman.setText("Rp. " + MainActivity.nf.format(dt.intValue()));
            jenisPinjaman.setText("Rp. " + MainActivity.nf.format(dt2.intValue()));
            StatusPencairan.setText(m.getApploancashnote());
        }
        lnAproval.setVisibility(View.GONE);

        //navigationonclik
        Log.e(TAG, "onCreate: " + sessionManager.getRole());
        try {
            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    APP_PENCAIRAN_LAPANGAN = true;
                    ACTIONAPPROVE = "APP_PENCAIRAN_LAPANGAN";
                }
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_PENGAWAS")) {
                    ACTIONAPPROVE = "APP_PENCAIRAN_PENGAWAS";
                }
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_KABAG")) {
                    ACTIONAPPROVE = "APP_PENCAIRAN_KABAG";
                }
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_CASHIER")) {
                    ACTIONAPPROVE = "APP_PENCAIRAN_CASHIER";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        bnMain.getMenu().clear();
        if (APP_PENCAIRAN_LAPANGAN) {
            bnMain.inflateMenu(R.menu.menu_page_pencairan4);
        } else {
            bnMain.inflateMenu(R.menu.menu_page_pencairan);
        }


        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(1).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;

            }
        });
        setupApproval();
        tolak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                sendApproved("Rejected");
            }
        });
        setuju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendApproved("Approved");


            }
        });
    }

    private void setupApproval() {

        Log.e(TAG, "onCreate: " + " getAppspvstatus:" + m.getAPPPENCAIRANPENGAWAS() +
                " getAppkabagstatus:" + m.getAPPPENCAIRANKABAG() +
                " getAppcashierstatus:" + m.getAPPPENCAIRANCASHIER() +
                " getApplapstatus:" + m.getAPPPENCAIRANLAPANGAN() +
                " getCheckinstatus:" + m.getCheckinstatus() +
                " getLoancashstatus:" + m.getApploancashstatus() +
                " getAppcashierstatus2:" + m.getAPPPENCAIRANCASHIER2() +
                " appkabagstatus2:" + m.getAPPPENCAIRANKABAG2()
        );
        if (m.getApploancashstatus().equals("In Approval")) {
            if (m.getAPPPENCAIRANPENGAWAS().equals("Dalam Proses") &&
                    ACTIONAPPROVE.equals("APP_PENCAIRAN_PENGAWAS")) {
                //menunggu persetujuan Pengawas
                lnAproval.setVisibility(View.VISIBLE);
                MASSAGE_FRAGMENT = "Pencairan Berhasil di ";
            } else if (m.getAPPPENCAIRANKABAG().equals("Dalam Proses") &&
                    m.getAPPPENCAIRANPENGAWAS().equals("Approved") &&
                    ACTIONAPPROVE.equals("APP_PENCAIRAN_KABAG")) {
                //menunggu persetujuan KABAG
                lnAproval.setVisibility(View.VISIBLE);
                MASSAGE_FRAGMENT = "Pencairan Berhasil di ";

            } else if (m.getAPPPENCAIRANPENGAWAS().equals("Approved") &&
                    m.getAPPPENCAIRANKABAG().equals("Approved") &&
                    m.getAPPPENCAIRANCASHIER().equals("Dalam Proses") &&
                    ACTIONAPPROVE.equals("APP_PENCAIRAN_CASHIER")) {
                //menunggu penyerahan uang
                MASSAGE_FRAGMENT = "Pencairan Berhasil Diserahkan";
                lnAproval.setVisibility(View.VISIBLE);
                setuju.setText("Serahkan");
                tolak.setText("Belum");
            } else if (m.getAPPPENCAIRANLAPANGAN().equals("Dalam Proses") &&
                    ACTIONAPPROVE.equals("APP_PENCAIRAN_LAPANGAN")) {
                //uang sudah di serahkan
                lnAproval.setVisibility(View.VISIBLE);
                MASSAGE_FRAGMENT = "Uang Telah Di terima.";
                setuju.setText("Terima");
                tolak.setText("Belum");
            } else if (m.getAPPPENCAIRANPENGAWAS().equals("Approved") &&
                    m.getAPPPENCAIRANKABAG().equals("Approved") &&
                    m.getAPPPENCAIRANCASHIER().equals("Approved") &&
                    m.getAPPPENCAIRANLAPANGAN().equals("Approved") &&
                    m.getCheckinstatus().equals("Tidak Ada") &&
                    m.getApploancashstatus().equals("") &&
                    m.getAPPPENCAIRANCASHIER2().equals("Dalam Proses") &&
                    ACTIONAPPROVE.equals("APP_PENCAIRAN_CASHIER")) {
                //menunggu pembatalan Pencairan Kasir
                lnAproval.setVisibility(View.VISIBLE);
                setuju.setText("Terima");
                tolak.setText("Belum");
            } else if (m.getAPPPENCAIRANPENGAWAS().equals("Approved") &&
                    m.getAPPPENCAIRANKABAG().equals("Approved") &&
                    m.getAPPPENCAIRANCASHIER().equals("Approved") &&
                    m.getAPPPENCAIRANLAPANGAN().equals("Approved") &&
                    m.getCheckinstatus().equals("Tidak Ada") &&
                    m.getApploancashstatus().equals("") &&
                    m.getAPPPENCAIRANCASHIER2().equals("Approved") &&
                    m.getAPPPENCAIRANKABAG2().equals("Dalam Proses") &&
                    ACTIONAPPROVE.equals("APP_PENCAIRAN_KABAG")) {
                //menunggu pembatalan Pencairan Kabag
                lnAproval.setVisibility(View.VISIBLE);
                setuju.setText("Terima");
                tolak.setText("Belum");
            } else if (m.getAPPPENCAIRANPENGAWAS().equals("Approved") &&
                    m.getAPPPENCAIRANKABAG().equals("Approved") &&
                    m.getAPPPENCAIRANCASHIER().equals("Approved") &&
                    m.getAPPPENCAIRANLAPANGAN().equals("Approved") &&
                    m.getCheckinstatus().equals("Ada") &&
                    m.getApploancashstatus().equals("Closed") &&
                    m.getAPPPENCAIRANCASHIER2().equals("") &&
                    m.getAPPPENCAIRANKABAG2().equals("Dalam Proses") &&
                    ACTIONAPPROVE.equals("APP_PENCAIRAN_KABAG")) {
                //menunggu penyerahan berkas
                lnAproval.setVisibility(View.VISIBLE);
                setuju.setText("Terima");
                tolak.setText("Belum");
            }
        }
        if (m.getApploancashstatus().equals("Rejected")) {

        }
    }

    private void sendApproved(String appactionss) {
        String url = Http.getUrl() + "InsertAprovalPencairan";
        Log.e(TAG, "sendCheckIn: " + url);
        final ProgressDialog dialog1 = new ProgressDialog(PersetujuanLaporanActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Sedang Menyimpan...");
        dialog1.show();
        RequestQueue mQueue = Volley.newRequestQueue(PersetujuanLaporanActivity.this);
        //        Log.e(TAG, "accessWebService: Katalog Start with Token " + sessionManager.getKeyToken());
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog1.dismiss();
                        try {

                            Log.d(TAG, "onResponse() called with: response = " + response);
                            if (response != null) {
                                //                                Gson gson = new Gson();
                                //                                ModelName modelName = gson.fromJson(String.valueOf(response), ModelName.class)
                                try {
                                    String[] respondde = response.split(";");
                                    if (respondde[0].equals("OK")) {
                                        Bundle bundle = new Bundle();
                                        if (appactionss.equals("Rejected")) {
                                            bundle.putString("DATA", MASSAGE_FRAGMENT);
                                        } else {
                                            bundle.putString("DATA", MASSAGE_FRAGMENT);
                                        }
                                        Pop_up_perjanjian_bayar_berhasilFragment pop_up_perjanjian_bayar_berhasilFragment = new Pop_up_perjanjian_bayar_berhasilFragment();
                                        FragmentManager mFragmentManager = getSupportFragmentManager();
                                        pop_up_perjanjian_bayar_berhasilFragment.setArguments(bundle);
                                        pop_up_perjanjian_bayar_berhasilFragment.setCancelable(false);
                                        pop_up_perjanjian_bayar_berhasilFragment.show(mFragmentManager, Pop_up_perjanjian_bayar_berhasilFragment.class.getSimpleName());

                                    } else {
                                        Toast.makeText(PersetujuanLaporanActivity.this, respondde[1], Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(PersetujuanLaporanActivity.this, "Gagal Menyimpan.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }

                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse() called with: response = [" + e.getMessage() + "]");
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                try {
                    String bodyError = VolleyErrorHelper.getMessage(error, PersetujuanLaporanActivity.this);
                    Toast.makeText(PersetujuanLaporanActivity.this, bodyError, Toast.LENGTH_SHORT).show();

                } catch (Exception e) {
                    Log.e(TAG, "onErrorResponse: " + e.getMessage());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                //                params.put("Authorization", "Bearer " + sessionManager.getKeyToken());
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("sCmp", Http.getsCmp());
                params.put("apploancashoid", String.valueOf(m.getApploancashoid()));
                params.put("appaction", appactionss);
                params.put("createuser", sessionManager.getPID());
                Log.e(TAG, "getParams: " + params.toString());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjectRequest);
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
