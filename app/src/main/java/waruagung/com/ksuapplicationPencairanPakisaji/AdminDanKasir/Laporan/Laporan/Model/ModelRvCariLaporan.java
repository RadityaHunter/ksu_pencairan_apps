package waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelRvCariLaporan implements Parcelable {
    public static final Parcelable.Creator<ModelRvCariLaporan> CREATOR = new Parcelable.Creator<ModelRvCariLaporan>() {
        @Override
        public ModelRvCariLaporan createFromParcel(Parcel source) {
            return new ModelRvCariLaporan(source);
        }

        @Override
        public ModelRvCariLaporan[] newArray(int size) {
            return new ModelRvCariLaporan[size];
        }
    };

    private String namanastaf, idnasabah,total, tanggal;
    private int kodeMenu;


    public ModelRvCariLaporan(String namanastaf, String idnasabah, String total, String tanggal, int kodeMenu) {
        this.namanastaf = namanastaf;
        this.idnasabah = idnasabah;
        this.total = total;
        this.tanggal = tanggal;
        this.kodeMenu = kodeMenu;
    }

    protected ModelRvCariLaporan(Parcel in) {
        this.namanastaf = in.readString();
        this.idnasabah = in.readString();
        this.total = in.readString();
        this.tanggal = in.readString();
        this.kodeMenu = in.readInt();
    }

    public String getNamanastaf() {
        return namanastaf;
    }

    public void setNamanastaf(String namanastaf) {
        this.namanastaf = namanastaf;
    }

    public String getIdnasabah() {
        return idnasabah;
    }

    public void setIdnasabah(String idnasabah) {
        this.idnasabah = idnasabah;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(int kodeMenu) {
        this.kodeMenu = kodeMenu;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.namanastaf);
        dest.writeString(this.idnasabah);
        dest.writeString(this.total);
        dest.writeString(this.tanggal);
        dest.writeInt(this.kodeMenu);

    }
}
