package waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class DetailLaporanPencairanActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nama_nasabah)
    TextView nama_nasabah;
    @BindView(R.id.nomor_pinjaman)
    TextView nomorPinjaman;
    @BindView(R.id.jumlah_pinjaman)
    TextView jumlahPinjaman;
    @BindView(R.id.jenis_pinjaman)
    TextView jenisPinjaman;
    @BindView(R.id.jangka_waktu)
    TextView jangkaWaktu;
    @BindView(R.id.agunan)
    TextView agunan;
    @BindView(R.id.tanggal_pencairan)
    TextView tanggalPencairan;
    @BindView(R.id.Status_Pencairan)
    TextView StatusPencairan;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    private SessionManager sessionManager;
    private Boolean APP_PENCAIRAN_LAPANGAN = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_laporan_pencairan);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        restoreActionBar();
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //navigationonclik
        try {
            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    APP_PENCAIRAN_LAPANGAN = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        bnMain.getMenu().clear();
        if (APP_PENCAIRAN_LAPANGAN) {
            bnMain.inflateMenu(R.menu.menu_page_pencairan4);
        } else {
            bnMain.inflateMenu(R.menu.menu_page_pencairan);
        }
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(3).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;

            }
        });
    }
}
