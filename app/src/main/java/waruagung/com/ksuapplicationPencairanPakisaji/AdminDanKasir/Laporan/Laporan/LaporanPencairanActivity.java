package waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIClient;
import waruagung.com.ksuapplicationPencairanPakisaji.API.APIInterface;
import waruagung.com.ksuapplicationPencairanPakisaji.Adapter.AdapterReportPencairanTabel;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanTabelActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelReportPencairanList;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelReportPencairanResponse;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class LaporanPencairanActivity extends AppCompatActivity {
    public static final String TAG = LaporanPencairanActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cari)
    EditText cari;
    @BindView(R.id.rvcari_laporan)
    RecyclerView rvcariLaporan;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    //    @BindView(R.id.title_keterangan)
//    TextView title_keterangan;
//    @BindView(R.id.tvunduh)
//    TextView tvunduh;
    @BindView(R.id.swap)
    SwipeRefreshLayout swap;
    @BindView(R.id.tanggalawal)
    EditText tanggalawal;
    @BindView(R.id.tanggal_awal)
    LinearLayout tanggalAwal;
    @BindView(R.id.tanggalakhir)
    EditText tanggalakhir;
    @BindView(R.id.tanggal_akhir)
    LinearLayout tanggalAkhir;
    @BindView(R.id.export_pdf)
    ImageView exportPdf;
    @BindView(R.id.imgcalstart)
    ImageView imgcalstart;
    @BindView(R.id.imgcalEnd)
    ImageView imgcalEnd;
    @BindView(R.id.TRow)
    TableRow TRow;
    @BindView(R.id.scrollView1)
    ScrollView scrollView1;
    private SessionManager sessionManager;
    private Boolean APP_PENCAIRAN_LAPANGAN = false;
    private List<ModelReportPencairanResponse> modelDaftarPencairanListResponses;
    private List<ModelReportPencairanList> modelDaftarPencairanLists = new ArrayList<>();
    private DatePickerDialog picker;
    private APIInterface apiInterface;
    private APIClient ApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_pencairan);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);

//        if (modelDaftarPencairanList.getApploancashstatus().equals("Rejected")) {
//            title_keterangan.setText("Dibatalkan oleh");
//        }


//        DaftarPencairan(cari.getText().toString().trim());
//navigationonclik
        try {
//            Log.e(TAG, "onCreate: " + sessionManager.getRole());

            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    APP_PENCAIRAN_LAPANGAN = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (APP_PENCAIRAN_LAPANGAN) {
            bnMain.getMenu().clear();
            bnMain.inflateMenu(R.menu.menu_page_pencairan4);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setChecked(true);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setCheckable(true);
        } else {
            bnMain.getMenu().clear();
            bnMain.inflateMenu(R.menu.menu_page_pencairan);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setChecked(true);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setCheckable(true);
        }

        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
//                    case R.id.DaftarPencairan:
//                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
//                        startActivity(intent2);
//                        break;

                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanTabelActivity.class);
                        startActivity(intent2);
                        break;


//                    case R.id.LaporanPencairan:
//                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
//                        startActivity(intent3);
//                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), LaporanPencairanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;

            }
        });

        //tanggal otomatis sekarang
        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.DATE, -90);  // number of days to add
        String currentDate = new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime());
        TextView textViewDate = findViewById(R.id.tanggalawal);
        textViewDate.setText(currentDate);

//        calendar.add(Calendar.DATE, -90);  // number of days to add
        String currentDate2 = new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime());
        TextView textViewDate2 = findViewById(R.id.tanggalakhir);
        textViewDate2.setText(currentDate2);

        settingDatePicker();
        restoreActionBar();
//        DaftarPencairan("");
        swap.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                DaftarPencairan(cari.getText().toString().trim());
                rvlaporanPencairan();
                swap.setRefreshing(false);
            }
        });
        rvlaporanPencairan();
        cari.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//                    DaftarPencairan(v.getText().toString().trim());
                    rvlaporanPencairan();
                    //keyboard otomatis hilang jika sudah cari nama/item selesai
                    InputMethodManager inputMethodManager1 = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager1.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    return true;
                }
                return false;
            }
        });

        //download pdf
        exportPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String urlParsing = Http.getUrl() + "Report_ExportPencairan?sCmp=KSP04&datestart=" + tanggalawal.getText().toString().trim() + "&dateend=" + tanggalakhir.getText().toString().trim() + "&membername=" + cari.getText().toString();
                String urlParsing = Http.getUrl() + "Report_ExportPencairan?sCmp=KSP27&membername=&datestart=" + tanggalawal.getText().toString().trim() + "&dateend=" + tanggalakhir.getText().toString().trim() + cari.getText().toString();

//                Intent intent = new Intent(LaporanPenagihanTabelActivity.this, WebViewReportPDFActivity.class);
//                intent.putExtra("url_pdf", urlParsing);
//                startActivity(intent);

                //Cara 1
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(urlParsing));
                startActivity(i);
                //Cara 2
//                try {
//                    new DownloadTaskHelper(LaporanPenagihanTabelActivity.this, urlParsing);
//                } catch (Exception e) {
//                    Log.i(TAG, "onClick: " + e.getMessage());
//                    Intent i = new Intent(Intent.ACTION_VIEW);
//                    i.setData(Uri.parse(urlParsing));
//                    startActivity(i);
//                }
            }
        });
    }

    private void settingDatePicker() {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");// HH:mm:ss");
        tanggalawal.setInputType(InputType.TYPE_NULL);
        tanggalawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                picker = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int dayOfMonth, int monthOfYear, int year) {
                                Calendar newDate = Calendar.getInstance();
//                                newDate.set(year, monthOfYear, dayOfMonth);
                                newDate.set(dayOfMonth, monthOfYear, year);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                                String formatted = dateFormat.format(newDate.getTime());
                                tanggalawal.setText(formatted);
                            }
                        }, year, month, day);
                picker.show();
            }
        });


        tanggalakhir.setInputType(InputType.TYPE_NULL);
        tanggalakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
//                cldr.add(Calendar.DATE, -90);  // number of days to add
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                picker = new DatePickerDialog(v.getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int dayOfMonth, int monthOfYear, int year) {
                                Calendar newDate = Calendar.getInstance();
//                                newDate.set(year, monthOfYear, dayOfMonth);
                                newDate.set(dayOfMonth, monthOfYear, year);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                                String formatted = dateFormat.format(newDate.getTime());
                                tanggalakhir.setText(formatted);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        tanggalawal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cariTanggal = tanggalawal.getText().toString();
                if (cariTanggal.length() > 3) {
                    if (isOnline()) {
                        String caritanggal = cari.getText().toString().toUpperCase();
                        rvlaporanPencairan();
                    } else {
                        Snack("Tidak ada koneksi internet");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        tanggalakhir.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cariTanggal = tanggalawal.getText().toString();
                if (cariTanggal.length() > 3) {
                    if (isOnline()) {
                        String caritanggal = cari.getText().toString().toUpperCase();
                        rvlaporanPencairan();
                    } else {
                        Snack("Tidak ada koneksi internet");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void rvlaporanPencairan() {
        String searchDate = tanggalawal.getText().toString();
        String endDate = tanggalakhir.getText().toString();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LaporanPencairanActivity.this);
        rvcariLaporan.setLayoutManager(linearLayoutManager);
        rvcariLaporan.setHasFixedSize(true);
        rvcariLaporan.setNestedScrollingEnabled(false);
        rvcariLaporan.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                swap.setEnabled(linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0);
            }
        });
        rvcariLaporan.setAdapter(null);

        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelReportPencairanResponse>> call = apiInterface.doGetPencairanReport(
                Http.getsCmp(),
                tanggalawal.getText().toString().trim(),
                tanggalakhir.getText().toString().trim(),
                cari.getText().toString()

        );
        call.enqueue(new Callback<List<ModelReportPencairanResponse>>() {
            @Override
            public void onResponse(Call<List<ModelReportPencairanResponse>> call, Response<List<ModelReportPencairanResponse>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.e(TAG, "onResponse: " + response.toString());
                    Log.e(TAG, "onResponseRaw: " + response.raw());
//                    Log.e(TAG, "onResponse:Body " + response.body());
                    AdapterReportPencairanTabel adapterReportPencairanTabel = new AdapterReportPencairanTabel(LaporanPencairanActivity.this, -1, response.body());
                    rvcariLaporan.setAdapter(adapterReportPencairanTabel);
                }
            }

            @Override
            public void onFailure(Call<List<ModelReportPencairanResponse>> call, Throwable t) {
                Toast.makeText(getBaseContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void Snack(String string) {
        Snackbar snackbar = Snackbar.make(cari, string, Snackbar.LENGTH_LONG)
                .setAction("Action", null);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

//    private void DaftarPencairan(String s) {
//        String url = Http.getUrl() + "Report_Pencairan_List?sCmp=" + Http.getsCmp() + "&membername=" + s;
//        Log.e(TAG, "rvlaporan: " + url);
//        final ProgressDialog dialog1 = new ProgressDialog(LaporanPencairanActivity.this);
//        dialog1.setCancelable(false);
//        dialog1.setCanceledOnTouchOutside(false);
//        dialog1.setMessage("Harap Menunggu...");
//        dialog1.show();
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LaporanPencairanActivity.this);
//        rvcariLaporan.setLayoutManager(linearLayoutManager);
//        rvcariLaporan.setHasFixedSize(true);
//        rvcariLaporan.setNestedScrollingEnabled(false);
//        rvcariLaporan.setOnScrollChangeListener(new View.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                swap.setEnabled(linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0);
//            }
//        });
//        rvcariLaporan.setAdapter(null);
//        RequestQueue mQueue = Volley.newRequestQueue(LaporanPencairanActivity.this);
//        //        Log.e(TAG, "accessWebService: Katalog Start with Token " + sessionManager.getKeyToken());
//        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url,
//                new Response.Listener<String>() {
//                    @SuppressLint("NewApi")
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//
//                            dialog1.dismiss();
//                            Log.d(TAG, "onResponse() called with: response = [" + response + "]");
//                            if (response != null | !response.equals("[]")) {
//                                Gson gson = new Gson();
//                                modelDaftarPencairanListResponses = gson.fromJson(response, new TypeToken<List<ModelReportPencairanResponse>>() {
//                                }.getType());
//
//
//                                if (!modelDaftarPencairanListResponses.isEmpty()) {
//                                    AdapterReportPencairanTabel adapterReportPencairanTabel = new AdapterReportPencairanTabel(LaporanPencairanActivity.this, -1, modelDaftarPencairanListResponses);
//                                    rvcariLaporan.setAdapter(adapterReportPencairanTabel);
//                                } else {
//                                    Toast.makeText(LaporanPencairanActivity.this, "Data Kosong!", Toast.LENGTH_SHORT).show();
//                                }
//                            } else {
//                                Toast.makeText(LaporanPencairanActivity.this, "Data Kosong!", Toast.LENGTH_SHORT).show();
//                            }
//                        } catch (Exception e) {
//                            Toast.makeText(LaporanPencairanActivity.this, "Data Kosong!", Toast.LENGTH_SHORT).show();
//                            Log.e(TAG, "onResponse() Error Exception" + e.getMessage());
//                        }
//
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                dialog1.dismiss();
//                String responseBodyError = VolleyErrorHelper.getMessage(error, LaporanPencairanActivity.this);
//                Toast.makeText(LaporanPencairanActivity.this, responseBodyError, Toast.LENGTH_SHORT).show();
//                error.printStackTrace();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
////                params.put("Content-Type", "application/json");
//                //                params.put("Authorization", "Bearer " + sessionManager.getKeyToken());
//                return params;
//            }
//
//            @Override
//            protected Response<String> parseNetworkResponse(NetworkResponse response) {
//                return super.parseNetworkResponse(response);
//            }
//        };
//
//        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        mQueue.add(jsonObjectRequest);
//    }
}
