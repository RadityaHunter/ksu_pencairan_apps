package waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.Adapter.AdapterReportPencairan;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.Http;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.VolleyErrorHelper;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelReportPencairanList;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelReportPencairanResponse;
import waruagung.com.ksuapplicationPencairanPakisaji.Model.ModelUserRole;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class CariLaporanActivity extends AppCompatActivity implements AdapterReportPencairan.AdapterReportPencairanCallback {

    public static final String TAG = CariLaporanActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cari)
    EditText cari;
    @BindView(R.id.rvcari_laporan)
    RecyclerView rvcariLaporan;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;
    @BindView(R.id.tvunduh)
    TextView tvunduh;
    @BindView(R.id.swap)
    SwipeRefreshLayout swap;
    private SessionManager sessionManager;
    private Boolean APP_PENCAIRAN_LAPANGAN = false;
    private List<ModelReportPencairanResponse> modelDaftarPencairanListResponses;
    private List<ModelReportPencairanList> modelDaftarPencairanLists = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_laporan);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
//navigationonclik
        try {
//            Log.e(TAG, "onCreate: " + sessionManager.getRole());

            List<ModelUserRole> modelRole = new Gson().fromJson(sessionManager.getRole(), new TypeToken<List<ModelUserRole>>() {
            }.getType());

            for (ModelUserRole m : modelRole) {
                if (m.getFORMADDRESS().equals("APP_PENCAIRAN_LAPANGAN")) {
                    APP_PENCAIRAN_LAPANGAN = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (APP_PENCAIRAN_LAPANGAN) {
            bnMain.getMenu().clear();
            bnMain.inflateMenu(R.menu.menu_page_pencairan4);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setChecked(true);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setCheckable(true);
        } else {
            bnMain.getMenu().clear();
            bnMain.inflateMenu(R.menu.menu_page_pencairan);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setChecked(true);
            bnMain.getMenu().getItem(0).setCheckable(false);
            bnMain.getMenu().getItem(2).setCheckable(true);
        }

        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;

            }
        });
        restoreActionBar();
        DaftarPencairan("");
        swap.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                DaftarPencairan(cari.getText().toString().trim());
                swap.setRefreshing(false);
            }
        });
        cari.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    DaftarPencairan(v.getText().toString().trim());
                    //keyboard otomatis hilang jika sudah cari nama/item selesai
                    InputMethodManager inputMethodManager1 = (InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager1.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    return true;
                }
                return false;
            }
        });
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void DaftarPencairan(String s) {
        String url = Http.getUrl() + "Report_Pencairan_List?sCmp=" + Http.getsCmp() + "&membername=" + s;
        Log.e(TAG, "rvlaporan: " + url);
        final ProgressDialog dialog1 = new ProgressDialog(CariLaporanActivity.this);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.setMessage("Harap Menunggu...");
        dialog1.show();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CariLaporanActivity.this);
        rvcariLaporan.setLayoutManager(linearLayoutManager);
        rvcariLaporan.setHasFixedSize(true);
        rvcariLaporan.setNestedScrollingEnabled(false);
        rvcariLaporan.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                swap.setEnabled(linearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0);
            }
        });
        rvcariLaporan.setAdapter(null);
        RequestQueue mQueue = Volley.newRequestQueue(CariLaporanActivity.this);
        //        Log.e(TAG, "accessWebService: Katalog Start with Token " + sessionManager.getKeyToken());
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onResponse(String response) {
                        try {

                            dialog1.dismiss();
                            Log.d(TAG, "onResponse() called with: response = [" + response + "]");
                            if (response != null | !response.equals("[]")) {
                                Gson gson = new Gson();
                                modelDaftarPencairanListResponses = gson.fromJson(response, new TypeToken<List<ModelReportPencairanResponse>>() {
                                }.getType());
                                if (!modelDaftarPencairanListResponses.isEmpty()) {
                                    AdapterReportPencairan adapterListLaporan = new AdapterReportPencairan(CariLaporanActivity.this, -1, modelDaftarPencairanListResponses, CariLaporanActivity.this);
                                    rvcariLaporan.setAdapter(adapterListLaporan);
                                } else {
                                    Toast.makeText(CariLaporanActivity.this, "Data Kosong!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(CariLaporanActivity.this, "Data Kosong!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(CariLaporanActivity.this, "Data Kosong!", Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "onResponse() Error Exception" + e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog1.dismiss();
                String responseBodyError = VolleyErrorHelper.getMessage(error, CariLaporanActivity.this);
                Toast.makeText(CariLaporanActivity.this, responseBodyError, Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/json");
                //                params.put("Authorization", "Bearer " + sessionManager.getKeyToken());
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                return super.parseNetworkResponse(response);
            }
        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(jsonObjectRequest);
    }


    @Override
    public void onRowAdapterReportPencairanClicked(int position) {
        Intent intent = new Intent(CariLaporanActivity.this, LaporanActivity.class);
        intent.putExtra("_DATA", modelDaftarPencairanListResponses.get(position));
        startActivity(intent);
    }
}
