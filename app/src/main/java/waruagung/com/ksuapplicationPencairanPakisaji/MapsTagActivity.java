package waruagung.com.ksuapplicationPencairanPakisaji;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.ModelDaftarPencairanListResponse2;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PertanyaanPencairanActivity;

public class MapsTagActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    public static final String TAG = MapsTagActivity.class.getSimpleName();
    public static final int RC_CAMERA_AND_LOCATION_AND_STORAGE = 10102;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 100;
    protected LocationManager locationManager;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.koordinatNow)
    TextView koordinatNow;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cariLocation)
    ImageView cariLocation;
    private GoogleMap mMap;
    private String Latitude, Longitude;
    private ProgressDialog progressDialog;
    private SupportMapFragment _SupportMapFragment;
    private LatLng latLng;
    private String LatLNG = "";
    private Location loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_tag);
        ButterKnife.bind(this);
        restoreActionBar();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
        progressDialog = new ProgressDialog(MapsTagActivity.this);
//        button.setEnabled(false);
        setupAwal();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(LatLNG) || TextUtils.isEmpty(koordinatNow.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Tunggu Sebentar. Butuh Waktu Untuk Mendapatkan GEO TAG Lokasi", Toast.LENGTH_SHORT).show();
                } else {
                    if (getIntent().hasExtra("TAG")) {
                        ModelDaftarPencairanListResponse2 m = getIntent().getParcelableExtra("DATA");
                        Intent intent = new Intent(getApplicationContext(), PertanyaanPencairanActivity.class);
                        intent.putExtra("MapsKoordinat", koordinatNow.getText().toString());
                        intent.putExtra("DATA", (Parcelable) m);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("MapsKoordinat", koordinatNow.getText().toString());
                        setResult(RESULT_OK, intent);
                        finish();
                    }

                }
            }
        });
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }

        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng( Double.parseDouble(Latitude),Double.parseDouble(Latitude));
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private void setupAwal() {
        try {
            Log.d(TAG, "getLocation: Started:");
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_DISTANCE_CHANGE_FOR_UPDATES, MIN_TIME_BW_UPDATES, this);
            if (locationManager != null) {
                loc = locationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (loc != null) {
                    Latitude = String.valueOf(loc.getLatitude());
                    Longitude = String.valueOf(loc.getLongitude());
                }
            }
        } catch (SecurityException e) {
            Log.d(TAG, "getLocation: Error Massage:" + e.getMessage());
            //Crashlytics.logException(e);
        }
        Log.d(TAG, "methodRequiresTwoPermission: TRUE");
        _SupportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (_SupportMapFragment != null) {
            _SupportMapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: " + location.getLatitude() + location.getLongitude());
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 10);
            Log.d(TAG, "onLocationChanged: Adress " + addresses.get(0).getAddressLine(0));
            latLng = new LatLng(location.getLatitude(), location.getLongitude());
            Latitude = Double.toString(location.getLatitude());
            Longitude = Double.toString(location.getLongitude());
            LatLNG = Latitude + "," + Longitude;
            koordinatNow.setText(Latitude + "," + Longitude);
            Log.d(TAG, "onMapReady: " + LatLNG);
//            _GoogleMap.clear();
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(latLng).title("Kamu Disini..."));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
            mMap.animateCamera(CameraUpdateFactory.zoomIn());
            mMap.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)      // Sets the center of the map to Mountain View
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(45)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            button.setEnabled(true);
        } catch (Exception e) {
            Log.d(TAG, "getLocation: Error Massage:" + e.getMessage());
            e.printStackTrace();
            //Crashlytics.logException(e);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
//        try {
//            Log.e(TAG, "onStatusChanged: " + status + provider);
//            progressDialog.setMessage("Peringatan"); // Setting Message
//            progressDialog.setTitle("Mohon Tetap Aktifkan GPS Anda. dan Data Aktif"); // Setting Title
//            progressDialog.setProgressStyle(ProgressDialog.BUTTON_NEGATIVE); // Progress Dialog Style Spinner
//            progressDialog.show(); // Display Progress Dialog
//            progressDialog.setCancelable(false);
//            new Thread(new Runnable() {
//                public void run() {
//                    try {
//                        Thread.sleep(10000);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    progressDialog.dismiss();
//                }
//            }).start();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        Log.e(TAG, "onStatusChanged: " + status);
    }

    @Override
    public void onProviderEnabled(String provider) {
        progressDialog.dismiss();
    }

    @Override
    public void onProviderDisabled(String provider) {
        try {
            progressDialog.setMessage("Peringatan"); // Setting Message
            progressDialog.setTitle("Mohon Aktifkan GPS Anda. dan Data Aktif"); // Setting Title
            progressDialog.setProgressStyle(ProgressDialog.BUTTON_NEGATIVE); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(10000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
