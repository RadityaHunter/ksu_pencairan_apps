package waruagung.com.ksuapplicationPencairanPakisaji.kunjungan;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class KunjunganActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.lat_now_checkin)
    TextView latNowCheckin;
    @BindView(R.id.lgt_now_checkin)
    TextView lgtNowCheckin;
    @BindView(R.id.label_check)
    TextView labelCheck;
    @BindView(R.id.label_check1)
    TextView labelCheck1;
    @BindView(R.id.name_mahasiswa_checkin)
    TextView nameMahasiswaCheckin;
    @BindView(R.id.tvLatitude)
    TextView tvLatitude;
    @BindView(R.id.notes)
    TextView notes;
    @BindView(R.id.tvLongitude)
    TextView tvLongitude;
    @BindView(R.id.distance_between)
    TextView distanceBetween;
    @BindView(R.id.button)
    TextView button;
    @BindView(R.id.btn_checkin_peserta)
    LinearLayout btnCheckinPeserta;
    @BindView(R.id.ET)
    EditText ET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kunjungan);
        ButterKnife.bind(this);
        restoreActionBar();
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
