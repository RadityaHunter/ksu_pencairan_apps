package waruagung.com.ksuapplicationPencairanPakisaji.LaporanPenagihan.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.LaporanPenagihan.Model.Model_Laporan_Penagihan;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class Adapter_Laporan_Penagihan extends RecyclerView.Adapter<Adapter_Laporan_Penagihan.myViewHolder> {

    static final String EXTRAS_DATA = "EXTRAS_data";
    private Context context;
    private ArrayList<Model_Laporan_Penagihan> listMenu;
    private Intent intent;


    public Adapter_Laporan_Penagihan(Context context, ArrayList<Model_Laporan_Penagihan> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_laporan_penagihan, viewGroup, false);
        return new myViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull myViewHolder myViewHolder, final int position) {
        Model_Laporan_Penagihan list = listMenu.get(position);
        myViewHolder.namastaf.setText(listMenu.get(position).getNamaMenu());
        myViewHolder.Tanggal.setText(listMenu.get(position).getTanggalMenu());
        myViewHolder.namanasabah.setText(listMenu.get(position).getNamaMenu2());
        myViewHolder.keterangan.setText(listMenu.get(position).getSubKeterangan());

//        myViewHolder.RLitemsMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int id = listMenu.get(position).getKodeMenu();
//                MenuNotifikasi(id, v);
//            }
//        });

    }

//    private void MenuNotifikasi(int id, View v) {
//        switch (id) {
//            case 1:
//                intent = new Intent(context, Pemberitahuan2Activity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
////            case 2:
////                intent = new Intent(context, Pemberitahuan3Activity.class);
////                intent.putExtra("page", 0);
////                v.getContext().startActivity(intent);
////                break;
//
//            case 3:
//                intent = new Intent(context, PemberitahuanActivity.class);
//                intent.putExtra("page", 0);
//                v.getContext().startActivity(intent);
//                break;
//
////            case 4:
////                intent = new Intent(context, Pemberitahuan4Activity.class);
////                intent.putExtra("page", 0);
////                v.getContext().startActivity(intent);
////                break;
//
//
//        }

//    }


    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.namastaf)
        TextView namastaf;
        @BindView(R.id.tanggal)
        TextView Tanggal;
        @BindView(R.id.namanasabah)
        TextView namanasabah;
        @BindView(R.id.keterangan)
        TextView keterangan;
        @BindView(R.id.RvLaporanPenagihan)
        LinearLayout RvLaporanPenagihan;


        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
