package waruagung.com.ksuapplicationPencairanPakisaji.LaporanPenagihan;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.AdminDanKasir.Laporan.Laporan.CariLaporanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.DaftarPencairan.DaftarPencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.LaporanPenagihan.Adapter.Adapter_Laporan_Penagihan;
import waruagung.com.ksuapplicationPencairanPakisaji.LaporanPenagihan.Model.Model_Laporan_Penagihan;
import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.PimpinandanKabagKop.Pencairan.PencairanActivity;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class LaporanPenagihanActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edtCariLaporanPenagihan)
    EditText edtCariLaporanPenagihan;
    @BindView(R.id.RvLaporanPenagihan)
    RecyclerView RvLaporanPenagihan;
    @BindView(R.id.unduh)
    TextView unduh;
    @BindView(R.id.btn_unduh)
    RelativeLayout btnUnduh;
    @BindView(R.id.bn_main)
    BottomNavigationView bnMain;

    private DatePickerDialog picker;
    RecyclerView.LayoutManager layoutManager;
    private Intent intent;
    private RecyclerView.Adapter adapter;
    private GridLayoutManager gridLayoutManager;
    private List<Icon> iconList;
    private int spanCount = 1;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_penagihan);
        ButterKnife.bind(this);
        restoreActionBar();
        sessionManager = new SessionManager(this);
        ListLaporanPenagihan();

        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setChecked(true);
        bnMain.getMenu().getItem(0).setCheckable(false);
        bnMain.getMenu().getItem(2).setCheckable(true);
        bnMain.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.pencairan:
                        Intent intent1 = new Intent(getApplicationContext(), PencairanActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.DaftarPencairan:
                        Intent intent2 = new Intent(getApplicationContext(), DaftarPencairanActivity.class);
                        startActivity(intent2);
                        break;
                    case R.id.LaporanPencairan:
                        Intent intent3 = new Intent(getApplicationContext(), CariLaporanActivity.class);
                        startActivity(intent3);
                        break;
                }
                return false;
            }
        });
    }

    private void ListLaporanPenagihan() {
        RvLaporanPenagihan.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, LinearLayoutManager.VERTICAL, false));
        RvLaporanPenagihan.setHasFixedSize(true);
        RvLaporanPenagihan.setNestedScrollingEnabled(false);
        ArrayList<Model_Laporan_Penagihan> items = new ArrayList<>();
        items.add(new Model_Laporan_Penagihan("APRILLIA", "23 Januari 2020", "BELLA", "Disetujui Pimpinan", 1));
        items.add(new Model_Laporan_Penagihan("APRILLIA", "23 Januari 2020", "LUSI APRILLIA", "Janji Bayar", 2));
        items.add(new Model_Laporan_Penagihan("APRILLIA", "23 Januari 2020", "OCTA", "Janji Bayar", 3));
        items.add(new Model_Laporan_Penagihan("OLIS", "23 Januari 2020", "ANDRY", "Disetujui Pimpinan", 4));
        items.add(new Model_Laporan_Penagihan("OLIS", "23 Januari 2020", "ULIL LATIFAH", "Belum Disetujui", 5));
        adapter = new Adapter_Laporan_Penagihan(this, items);
        RvLaporanPenagihan.setAdapter(adapter);
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
