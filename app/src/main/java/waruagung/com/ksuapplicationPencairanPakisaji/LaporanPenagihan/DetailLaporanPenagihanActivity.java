package waruagung.com.ksuapplicationPencairanPakisaji.LaporanPenagihan;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import waruagung.com.ksuapplicationPencairanPakisaji.Helper.SessionManager;
import waruagung.com.ksuapplicationPencairanPakisaji.R;

public class DetailLaporanPenagihanActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.textView5)
    TextView textView5;
    @BindView(R.id.nama_staff)
    TextView namaStaff;
    @BindView(R.id.nama_nasabah)
    TextView namaNasabah;
    @BindView(R.id.id_nasabah)
    TextView idNasabah;
    @BindView(R.id.total_pembayaran)
    TextView totalPembayaran;
    @BindView(R.id.alamat_nasabah)
    TextView alamatNasabah;
    @BindView(R.id.status_penagihan)
    TextView statusPenagihan;
    @BindView(R.id.RLitemsMenu)
    LinearLayout RLitemsMenu;
    @BindView(R.id.btn_tolak)
    Button btnTolak;
    @BindView(R.id.btn_setujui)
    Button btnSetujui;

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_laporan_penagihan);
        ButterKnife.bind(this);
        restoreActionBar();
        sessionManager = new SessionManager(this);
    }

    private void restoreActionBar() {
        setSupportActionBar(toolbar);
//        toolbarTitle.setText("Keranjang");
//        toolbarTitle.setTextColor(Color.BLACK);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_black_24dp);
//            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
