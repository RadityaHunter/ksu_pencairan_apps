package waruagung.com.ksuapplicationPencairanPakisaji.LaporanPenagihan.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Model_Laporan_Penagihan implements Parcelable {
    public static final Creator<Model_Laporan_Penagihan> CREATOR = new Creator<Model_Laporan_Penagihan>() {
        @Override
        public Model_Laporan_Penagihan createFromParcel(Parcel source) {
            return new Model_Laporan_Penagihan(source);
        }

        @Override
        public Model_Laporan_Penagihan[] newArray(int size) {
            return new Model_Laporan_Penagihan[size];
        }
    };

    private String namaMenu, TanggalMenu, namaMenu2, subKeterangan;
    private int kodeMenu;

    public Model_Laporan_Penagihan(String namaMenu, String tanggalMenu, String namaMenu2, String subKeterangan, int kodeMenu) {
        this.namaMenu = namaMenu;
        TanggalMenu = tanggalMenu;
        this.namaMenu2 = namaMenu2;
        this.subKeterangan = subKeterangan;
        this.kodeMenu = kodeMenu;
    }

    protected Model_Laporan_Penagihan(Parcel in) {
        this.namaMenu = in.readString();
        this.TanggalMenu = in.readString();
        this.namaMenu2 = in.readString();
        this.subKeterangan = in.readString();
        this.kodeMenu = in.readInt();
    }

    public String getNamaMenu() {
        return namaMenu;
    }

    public void setNamaMenu(String namaMenu) {
        this.namaMenu = namaMenu;
    }

    public String getTanggalMenu() {
        return TanggalMenu;
    }

    public void setTanggalMenu(String tanggalMenu) {
        TanggalMenu = tanggalMenu;
    }

    public String getNamaMenu2() {
        return namaMenu2;
    }

    public void setNamaMenu2(String namaMenu2) {
        this.namaMenu2 = namaMenu2;
    }

    public String getSubKeterangan() {
        return subKeterangan;
    }

    public void setSubKeterangan(String subKeterangan) {
        this.subKeterangan = subKeterangan;
    }

    public int getKodeMenu() {
        return kodeMenu;
    }

    public void setKodeMenu(int kodeMenu) {
        this.kodeMenu = kodeMenu;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.namaMenu);
        dest.writeString(this.TanggalMenu);
        dest.writeString(this.namaMenu2);
        dest.writeString(this.subKeterangan);
        dest.writeInt(this.kodeMenu);
    }
}
