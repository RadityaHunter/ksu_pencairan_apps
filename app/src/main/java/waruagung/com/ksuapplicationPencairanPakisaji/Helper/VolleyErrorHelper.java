package waruagung.com.ksuapplicationPencairanPakisaji.Helper;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.nio.charset.StandardCharsets;

import waruagung.com.ksuapplicationPencairanPakisaji.MainActivity;

public class VolleyErrorHelper {
    /**
     * Handles the server error, tries to determine whether to show a stock message or to
     * show a message retrieved from the server.
     *
     * @param err
     * @param context
     * @return
     */
    public static final String TAG = VolleyErrorHelper.class.getSimpleName();
    /**
     * Returns appropriate message which is to be displayed to the user
     * against the specified error object.
     *
     * @param error
     * @param context
     * @return
     */
    private SessionManager sessionManager;
    private Context context;

    public static String getMessage(Object error, Context context) {
        Log.e(TAG, "getMessage: " + error.toString());
        //get status code here

        if (error instanceof TimeoutError) {
            return "Server Time Out, Silakan kembali lagi nanti";
        } else if (isServerProblem(error)) {
            return handleServerError(error, context);
        } else if (isNetworkProblem(error)) {
            return "Webserver mungkin mati, Anda bisa coba lagi di lain waktu";
        } else if (error instanceof ParseError) {
            return "Error Parse Json";
        }
        return "Terjadi Kesalahan Tidak Diketahui";
    }

    /**
     * Determines whether the error is related to network
     *
     * @param error
     * @return
     */
    private static boolean isNetworkProblem(Object error) {

        return (error instanceof NetworkError) || (error instanceof NoConnectionError);
    }

    /**
     * Determines whether the error is related to server
     *
     * @param error
     * @return
     */
    private static boolean isServerProblem(Object error) {
        return (error instanceof ServerError) || (error instanceof AuthFailureError);
    }

    private static String handleServerError(Object err, Context context) {
        VolleyError error = (VolleyError) err;

        NetworkResponse response = error.networkResponse;
        Log.e(TAG, "handleServerError: " + response.statusCode);
        String body;

        String statusCode = String.valueOf(error.networkResponse.statusCode);
        //get response body and parse with appropriate encoding
        if (error.networkResponse.data != null) {
            body = new String(error.networkResponse.data, StandardCharsets.UTF_8);
            Log.e(TAG, "onErrorResponse: " + body);
        }
        if (response != null) {
            switch (response.statusCode) {
                case 404:
                    return "Halaman Tidak Ditemukan";
                case 500:
                    return "Server Error. Silakan Coba Lagi nanti.";
                case 422:
                    return "Error Code 422";
                case 401:
//                    try {
//                        // server might return error like this { "error": "Some error occured" }
//                        // Use "Gson" to parse the result
//                        HashMap<String, String> result = new Gson().fromJson(new String(response.data),
//                                new TypeToken<Map<String, String>>() {
//                                }.getType());
//
//                        if (result != null && result.containsKey("error")) {
//                            return result.get("error");
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    // invalid request
//                    return error.getMessage();
                    SessionManager sessionManager = new SessionManager(context);
                    sessionManager.destroySession();
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    return "";

                default:
                    return "Terjadi Kesalahan Silakan Coba Lagi nanti.";
            }
        }
        return "Terjadi Kesalahan Silakan Coba Lagi nanti.";
    }
}
